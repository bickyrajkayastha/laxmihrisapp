@extends('admin.layouts.master')
@push('css')
<link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
@endpush
@section('content')
<?php
    // if (session()->getOldInput()) {
    //     // dd(session()->getOldInput('options'));
    //     // dd($errors);
    // }
?>
<div class="row">
	<div class="co">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Add Poll
                    <button class="btn btn-sm btn-primary pull-right panel-btn" onclick="window.history.go(-1);">
                        <i class="fas fa-long-arrow-alt-left"></i> Back
                    </button>
                </h3>
            </div>

            <!--Horizontal Form-->
            <!--===================================================-->
            <form action="{{ route('admin.polls.store') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
                @csrf
                <div class="panel-body">
                    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                        <label class="col-sm-2 control-label" for="input-title">Title <span class="required_color">*</span></label>
                        <div class="col-sm-9">
                            <input type="text" placeholder="" value="{{ old('title') }}" name="title" id="input-title" class="form-control">
                            <small class="help-block text-danger">{{ $errors->first('title')}}</small>
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('closing_date') ? ' has-error' : '' }}">
                        <label class="col-sm-2 control-label" for="input-date">Closing Date</label>
                        <div class="col-sm-9">
                            <div class="input-group date">
                                <input type="text" class="form-control" autocomplete="off" name="closing_date" id="event-date" value="{{ old('closing_date') }}">
                                <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-th"></span>
                                </div>
                            </div>
                            @if($errors->has('closing_date'))
                            <small class="help-block text-danger">{{ $errors->first('closing_date')}}</small>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
	                    <label class="col-md-2 control-label">Is Published</label>
	                    <div class="col-md-9">
	                        <div class="radio">

	                            <!-- Inline radio buttons -->
	                            <input id="input-status" class="magic-radio" type="radio" name="status" value="1" <?= ((old('status') == 1)? 'checked':''); ?>>
	                            <label for="input-status">Yes</label>

	                            <input id="input-status-2" class="magic-radio" value="0" type="radio" name="status" <?= ((old('status') == 0)? 'checked':''); ?>>
	                            <label for="input-status-2">No</label>
	                        </div>
	                    </div>
	                </div>
                    <hr>
                    <div class="row">
                        <div class="col">
                            <h5 class="pull-left">Options
                            </h5>
                            <button id="add-option-btn" class="btn btn-sm btn-primary pull-right">
                                <i class="fas fa-plus"></i>
                            </button>
                        </div>
                    </div>
                    <div id="option-div">
                        @if(session()->getOldInput('options'))
                        <?php $n = 1; ?>
                        @foreach(session()->getOldInput('options') as $key => $option)
                        <div class="form-group{{ $errors->has('options.' . $key) ? ' has-error' : '' }}">
                            <label class="col-sm-2 control-label" for="input-title">Option <span class="option-number">{{ $n ++ }}</span> <span class="required_color">*</span></label>
                            <div class="col-sm-9">
                                <input type="text" placeholder="" value="{{ $option }}" name="options[{{ $key }}]" class="form-control">
                            </div>
                            <div><button class="btn btn-xs btn-danger option-remove-btn" title="Remove"><i class="fas fa-times"></i></button></div>
                        </div>
                        @endforeach
                        @else
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-title">Option 1 <span class="required_color">*</span></label>
                            <div class="col-sm-9">
                                <input type="text" placeholder="" value="" name="options[]" class="form-control">
                            </div>
                        </div>
                        @endif
                    </div>
                    <div class="col-sm-2"></div>
                    <div class="has-error">
                        @if($errors->first('options.*'))
                        <small class="help-block text-danger">{{ $errors->first('options.*')}}</small>
                        @else
                        <small class="help-block text-danger">{{ $errors->first('options')}}</small>
                        @endif
                    </div>
                </div>
                <div class="panel-footer text-right">
                	<div class="row">
		                <div class="col-lg-11">
		                    <button class="btn btn-sm btn-mint" type="submit">Create</button>
		                </div>
                	</div>
                </div>
            </form>
            <!--===================================================-->
            <!--End Horizontal Form-->

        </div>
    </div>
</div>
@endsection
@push('scripts')
<script src="{{ asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript">
    $(function() {
        $("#add-option-btn").on('click', function(event) {
            event.preventDefault();
            var n = $("#option-div>div").length + 1;
            var option_div = '<div class="form-group{{ $errors->has('options[]') ? ' has-error' : '' }}">\
                <label class="col-sm-2 control-label" for="input-title">Option <span class="option-number">'+n+'</span> <span class="required_color">*</span></label>\
                <div class="col-sm-9">\
                    <input type="text" placeholder="" value="{{ old('options[]') }}" name="options[]"" class="form-control">\
                    <small class="help-block text-danger">{{ $errors->first('options[]')}}</small>\
                </div>\
                <div><button class="btn btn-xs btn-danger option-remove-btn" title="Remove"><i class="fas fa-times"></i></button></div>\
            </div>';

            $("#option-div").append(option_div);
        });

        function rearrangeNumber() {
            var n = 1;
            $.each($("#option-div>div"), function(i, v) {
                $(v).find('.option-number').html(n++);
            });
        }

        $(document).on('click', '.option-remove-btn', function(event) {
            event.preventDefault();
            $(this).closest('.form-group').remove();
            rearrangeNumber();
        });

        $('.date').datepicker({
            format: 'yyyy-mm-dd',
        });
    });
</script>
@endpush
