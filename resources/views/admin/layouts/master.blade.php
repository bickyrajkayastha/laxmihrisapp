<!DOCTYPE html>
<html lang="en">
<head>
    <?php
        $auth_id = auth()->user()->id;
    ?>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- <title>Dashboard | Laxmi HRIS - Admin</title> -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Laxmi HRIS - {{ ((isset($pageTitle))?$pageTitle: "") }}</title>


    <link id="favicon" rel="icon" type="image/png" sizes="64x64" href="{{asset('img/laxmi-small.png')}}">

    <!--STYLESHEET-->
    <!--=================================================-->
    <!--Open Sans Font [ OPTIONAL ]-->
   <!-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous"> -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
    <!--Bootstrap Stylesheet [ REQUIRED ]-->
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">
    <!--Nifty Stylesheet [ REQUIRED ]-->
    <link href="{{ asset('assets/css/nifty.min.css') }}" rel="stylesheet">
    <!--Nifty Premium Icon [ DEMONSTRATION ]-->
    <link href="{{ asset('assets/css/demo/nifty-demo-icons.min.css') }}" rel="stylesheet">

        <!--Demo [ DEMONSTRATION ]-->
    <link href="{{ asset('assets/css/themes/type-b/theme-light.css') }}" rel="stylesheet">

    <!--Magic Checkbox [ OPTIONAL ]-->
    <link href="{{ asset('assets/plugins/magic-check/css/magic-check.min.css') }}" rel="stylesheet">

    <link href="{{ asset('assets/css/admin-style.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/plugins/animate-css/animate.min.css') }}" rel="stylesheet">
    @stack('css')

    <!--JAVASCRIPT-->
    <!--=================================================-->
    <!--Pace - Page Load Progress Par [OPTIONAL]-->
    <link href="{{ asset('assets/plugins/pace/pace.min.css') }}" rel="stylesheet">
    <script src="{{ asset('assets/plugins/pace/pace.min.js') }}"></script>
    <!--jQuery [ REQUIRED ]-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
    <!-- <script src="{{ asset('assets/js/jquery.min.js') }}"></script> -->
    <!--BootstrapJS [ RECOMMENDED ]-->
    <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
    <!--NiftyJS [ RECOMMENDED ]-->
    <script src="{{ asset('assets/js/nifty.min.js') }}"></script>
    <!--=================================================-->
    <!--Demo script [ DEMONSTRATION ]-->
    <script src="{{ asset('assets/js/demo/nifty-demo.min.js') }}"></script>

    <!--Sparkline [ OPTIONAL ]-->
    <script src="{{ asset('assets/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
    <script src="{{ asset('assets/js/socket.io.js') }}"></script>
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.3.0/socket.io.js"></script> --}}
    <!--=================================================

    REQUIRED
    You must include this in your project.


    RECOMMENDED
    This category must be included but you may modify which plugins or components which should be included in your project.


    OPTIONAL
    Optional plugins. You may choose whether to include it in your project or not.


    DEMONSTRATION
    This is to be removed, used for demonstration purposes only. This category must not be included in your project.


    SAMPLE
    Some script samples which explain how to initialize plugins or components. This category should not be included in your project.


    Detailed information and more samples can be found in the document.

    =================================================-->
</head>
<!--TIPS-->
<!--You may remove all ID or Class names which contain "demo-", they are only used for demonstration. -->

<body>
    <div id="container" class="effect aside-float aside-bright mainnav-lg">
        <!--NAVBAR-->
        <!--===================================================-->
        @include('admin.elements.header')
        <!--===================================================-->
        <!--END NAVBAR-->
        <div class="boxed">
            <!--CONTENT CONTAINER-->
            <!--===================================================-->
            <div id="content-container">
                <!--Page Title-->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <div id="page-head">
                    @include('admin.elements.breadcrumb')
                </div>
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <!--End page title-->
                <!--Page content-->
                <!--===================================================-->
                <div id="page-content">
                    <!-- <div class="row">
                        <div class="col-lg-12">
                            <form action="" id="message-form" class="form">
                                <div class="form-group">
                                    <label for="">Message</label>
                                    <textarea name="" id="messageArea" cols="30" rows="1" placeholder="message..." class="form-control"></textarea>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success btn-sm">Send</button>
                                </div>
                            </form>
                        </div>
                    </div> -->
                    @yield('content')
                </div>
                <!--===================================================-->
                <!--End page content-->
            </div>
            <!--===================================================-->
            <!--END CONTENT CONTAINER-->
            <!--ASIDE-->
            <!--===================================================-->
            <!--===================================================-->
            <!--END ASIDE-->
            <!--MAIN NAVIGATION-->
            <!--===================================================-->
            @include('admin.elements.side-bar')
            <!--===================================================-->
            <!--END MAIN NAVIGATION-->
        </div>
        <!-- FOOTER -->
        @include('admin.elements.footer')
        <!--===================================================-->
    </div>
    <!--===================================================-->
    <!-- END OF CONTAINER -->
    <script src="https://kit.fontawesome.com/5cbadad717.js" crossorigin="anonymous"></script>
    <script src="{{ asset('assets/plugins/ckeditor/ckeditor.js')}}"></script>
    <!-- <script type="text/javascript" src="{{ asset('js/message.js') }}" data-id="message" data-host="hey" data-auth-id="'{{ $auth_id }}'"></script> -->
    <script type="text/javascript">
        $(function() {
            // socket
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var auth_id = '{{ auth()->user()->id }}';
            let ip_address = '{!! config("constants.domain_address") !!}';
            let socket_port = '{!! config("constants.socket_port") !!}';
            let port_address = '{!! config("constants.port_address") !!}';
            let socket_option = {
                path: '/node/socket.io/',
                secure: true,
                // reconnectionDelay: 1000,
                // reconnection:true,
                // reconnectionAttempts: 10,
                // transports: ['websocket'],
                // agent: false, // [2] Please don't set this to true
                // upgrade: false,
                rejectUnauthorized: false
            };
            // var socket = io('/', socket_option);
            // var socket = io(ip_address + ':' + socket_port);

            $("#message-form").on('submit', function(event) {
                event.preventDefault()

                var message = $("#messageArea").val();
                $.ajax({
                    url: '{{ route("send-message") }}',
                    method: 'POST',
                    data: {"message": message},
                    dataType: "json",
                    async: "false",
                    success: function(res) {
                        alert("message sent");
                        $("messageArea").val("");
                    }
                });
                // socket.emit('userMessage', message);
                $(this)[0].reset();
            });

            $("#messageArea").keypress(function (e) {
                if(e.which == 13) {
                    $("#message-form").submit();
                }
            });

            // socket.on('connect', function() {
            //     socket.emit('user_connected', auth_id);
            // });

            // socket.on("private-channel:App\\Events\\PrivateMessageEvent", function(message) {
            //     $.niftyNoty({
            //         type: 'mint',
            //         container : 'floating',
            //         // title : 'Success',
            //         message : message.data.message,
            //         closeBtn : true,
            //         floating: {
            //             position: "top-right",
            //             animationIn: "fadeInDown",
            //             animationOut: "fadeOutUp"
            //         },
            //         timer: 3000,
            //         focus: true,
            //         onHide:function(){
            //             // alert("onHide Callback");
            //         }
            //     });
            //     // $('#addition').text(parseInt($('#addition').text()) + parseInt(message.data.addition));
            // });

            // socket.emit('joinGroup', {'user_id': auth_id, 'room': 'group1', 'group_id': 55});

            // socket.on('groupMessage', function(data) {
            //     console.log(data);
            // });

            // end of socket
            var nifty_msg = "";
            var nifty_type = "dark";
            if ('<?php echo Session::get('success_message'); ?>') {
                nifty_type = "success";
                nifty_msg = '<?php echo Session::get('success_message'); ?>'
            }

            if ('<?php echo Session::get('error_message'); ?>') {
                nifty_type = "danger";
                nifty_msg = '<?php echo Session::get('error_message'); ?>'
            }

            if ('<?php echo Session::get('update_success'); ?>') {
                nifty_type = "success";
                nifty_msg = '<?php echo Session::get('update_success'); ?>'
            }

            if (nifty_msg != "") {
                $.niftyNoty({
                    type: nifty_type,
                    container : 'page',
                    // title : 'Success',
                    message : nifty_msg,
                    closeBtn : true,
                    floating: {
                        position: "top-right",
                        animationIn: "slideInDown",
                        animationOut: "fadeOutUp"
                    },
                    timer: 3000,
                    focus: true,
                    onHide:function(){
                        // alert("onHide Callback");
                    }
                });
            }

        });
    </script>
    @stack('scripts')
</body>

</html>
