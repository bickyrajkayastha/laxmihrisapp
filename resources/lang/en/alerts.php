<?php

return [
    'create_success' => 'Success! Record created successfully.',
    'update_success' => 'Success! Record updated successfully.',
    'delete_success' => 'Success! Record deleted successfully.',
    'create_error' => 'Sorry! We have encountered an error while saving new record.',
    'update_error' => 'Sorry! We have encountered an error while updating the record.',
    'show_error' => 'Sorry! We are unable to locate the record you\'re trying to access.',
    'delete_error' => 'Sorry! We have encountered an error while deleting the record.',
    'delete_error_active' => 'Sorry! You can not delete active record.',
    'delete_error_default' => 'Sorry! You can not delete default record.',
    'no_permission' => 'Sorry! you do not have necessary permissions required to complete this action.'
];