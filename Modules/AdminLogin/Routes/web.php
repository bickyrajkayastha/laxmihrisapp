<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('admin', 'AdminLoginController@index');

Route::group(['middleware' => 'guest:admin', 'prefix' => 'admin', 'as' => 'admin.'], function() {

    Route::get('login','AdminLoginController@index')->name('login');
    Route::post('login','AdminLoginController@login');

});

Route::get('admin/logout', 'AdminLoginController@logOut')->name('admin.logout');