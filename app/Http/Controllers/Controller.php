<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
    * @SWG\Swagger(
    *     schemes={"http","https"},
    *     host="10.13.210.34:8005/api",
    *     basePath="",
    *     @SWG\Info(
    *         version="1.0.0",
    *         title="Laxmi HRIS",
    *         description="This document contains all the necessary apis for the android and ios application.",
    *         termsOfService="",
    *         @SWG\Contact(
    *             email="info@weplay.com"
    *         ),
    *         @SWG\License(
    *             name="Private License",
    *             url="URL to the license"
    *         )
    *     ),
    *     @SWG\ExternalDocumentation(
    *         description="Find out more the software.",
    *         url="/"
    *     ),
    *     @SWG\SecurityScheme(
    *         securityDefinition="bearerAuth",
    *         type="apiKey",
    *         name="Authorization",
    *         in="header"
    *     ),
    * )
    */
}
