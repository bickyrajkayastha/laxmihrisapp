<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class EmployeeMessageGroup extends Pivot
{
	protected $guarded = ['id'];
}
