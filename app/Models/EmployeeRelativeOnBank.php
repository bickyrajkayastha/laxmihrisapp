<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmployeeRelativeOnBank extends Model
{
	protected $guarded = ['id'];
}
