<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin/leaderboards', 'as' => 'admin.', 'middleware' => 'auth:admin'], function () {
    Route::get('/', 'AdminLeaderboardController@index')->name('leaderboards.index')->middleware('can:view-leaderboards');
    Route::get('/create', 'AdminLeaderboardController@create')->name('leaderboards.create')->middleware('can:create-leaderboards');
    Route::post('/create', 'AdminLeaderboardController@store')->name('leaderboards.store')->middleware('can:create-leaderboards');
    Route::post('/update-time', 'AdminLeaderboardController@updateTime')->name('leaderboards.updatetime')->middleware('can:create-leaderboards');
    Route::get('/{id}/edit', 'AdminLeaderboardController@edit')->name('leaderboards.edit')->middleware('can:edit-leaderboards');
    Route::post('/{id}/edit', 'AdminLeaderboardController@update')->name('leaderboards.update')->middleware('can:edit-leaderboards');
    Route::post('/{id}/delete', 'AdminLeaderboardController@destroy')->name('leaderboards.destroy')->middleware('can:delete-leaderboards');
    // Route::get('/{id}/candidates', 'AdminLeaderboardController@candidates')->name('leaderboards.candidates')->middleware('can:create-leaderboards');
    // Route::post('/candidates', 'AdminLeaderboardController@addCandidates')->name('leaderboards.candidates.add')->middleware('can:create-leaderboards');
    // Route::post('/candidate/{id}/remove', 'AdminLeaderboardController@removeCandidate')->name('leaderboards.candidate.remove')->middleware('can:create-leaderboards');
    // Route::get('/{id}/assessment', 'AdminLeaderboardController@assessment')->name('leaderboards.assessment')->middleware('can:create-leaderboards');
    // Route::get('/{id}/show', 'AdminLeaderboardController@show')->name('leaderboards.show')->middleware('can:create-leaderboards');
});
