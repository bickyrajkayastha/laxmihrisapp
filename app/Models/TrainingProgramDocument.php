<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TrainingProgramDocument extends Model
{
    protected $guarded = ['id'];

    public function getFullFilePathAttribute()
    {
        if ($this->file) {
            return url('uploads/training-documents/' . $this->training_id . '/' . $this->file);
        }
        return asset('') . config('constants.default_banner_image');
    }

    public function getFullFileUrlAttribute()
    {
        if ($this->file) {
            return url('uploads/training-documents/' . $this->training_id . '/' . $this->file);
        }
        return asset('') . config('constants.default_banner_image');
    }
}
