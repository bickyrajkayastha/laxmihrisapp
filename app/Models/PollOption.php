<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class PollOption extends Model
{
	protected $guarded = ['id'];

	protected $appends = ['countPercentage', 'count', 'myVote'];

	public function employees()
	{
	    return $this->belongsToMany(Employee::class, 'employee_poll_option', 'poll_option_id', 'employee_id')->withTimestamps();
	}

	public function getCountPercentageAttribute()
	{
		$total_employee_who_voted = \App\Models\Poll::where('id', '=', $this->poll_id)->first()->employees->count();
		if ($total_employee_who_voted > 0) {
			return round(($this->employees->count() * 100)/ $total_employee_who_voted, 2);
		}

		return 0;
	}

	public function getCountAttribute()
	{
		$total_employee = \App\Models\Employee::active()->count();
		return $this->employees->count();
	}

	public function getMyVoteAttribute()
	{
		$user = auth()->user();
		$option_id = $this->id;

		$has_voted = \App\Models\EmployeePollOption::where([
			['poll_option_id', '=', $option_id],
			['employee_id', '=', $user->id],
		])->first();

		if ($has_voted) {
			return true;
		}

		return false;
	}
}
