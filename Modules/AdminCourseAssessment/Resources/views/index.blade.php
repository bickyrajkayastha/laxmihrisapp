@extends('admin.layouts.master')

@section('content')
<div class="row">
    <div class="col">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Assessment Questions
                    <a href="{{ route('admin.courses.assessment.create', $course->id) }}" class="btn btn-primary btn-sm panel-btn pull-right"><i class="fa fa-plus"></i> Add Question</a>
                </h3>
            </div>

            <!-- Striped Table -->
            <!--===================================================-->
            <div class="panel-body">
                <div class="row" style="margin-bottom: 20px;">
                    <form class="form-horizontal" id="add-event" action="{{ route('admin.courses.updatetime') }}" method="POST"
                      enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="course_id" value="{{ $course->id }}">
                        <div class="col">
                            <div class="form-group">
                                <label class="col-sm-12 control-label" for=""><strong>Time Limit per question (in seconds)</strong></label>
                                <div class="col-lg-3 col-sm-3">
                                    <input type="text" class="form-control" name="time_limit" value="{{ $course->time_limit }}">
                                </div>
                                <div class="col-lg-2 col-sm-2 col-xs-12">
                                    <button class="btn btn-success" type="submit">Update</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
	                            <th>S.N</th>
	                            <th>Question</th>
	                            <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($questions as $question)
	                    	    <tr>
	                    	        <td>{{ $loop->iteration }}</td>
	                    	        <td>{{ $question->question }}</td>
	                    	        <td>
	                    	            <a href="{{ route('admin.courses.assessment.edit',$question->id) }}" class="btn btn-sm btn-primary" title="Edit">
	                    	                <i class="fas fa-edit"></i>
                                        </a>
	                    	            <a href="#" title="Remove" class="btn btn-sm btn-danger btn-delete"
	                    	               data-toggle="modal"
	                    	               data-id="{{ $question->id }}"
	                    	               data-target="#delete-modal">
	                    	                <i class="fas fa-trash"></i>
                                        </a>
	                    	        </td>
	                    	    </tr>
                            @empty
                    	    <tr>
                    	        <td colspan="5">
                    	            <em>No data available in table ...</em>
                    	        </td>
                    	    </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
            <!--===================================================-->
            <!-- End Striped Table -->

        </div>
    </div>
    @include('admin.elements.delete-modal')
</div>
@endsection
@push('scripts')
    <script type="text/javascript">
        $(document).on('click','.btn-delete', function () {
            var route = '{{ route("admin.courses.assessment.destroy", ":id") }}';

            route = route.replace(':id', $(this).attr('data-id'));

            $('#delete-modal').find('form').attr('action', route);
            $('#delete-modal').modal('show');

            $('#delete-final').click(function (e) {
                $(this).attr('disabled',true);
                $(this).html('<i class="fas fa-spinner fa-spin"></i> Please wait...');
                $(this).closest('form').submit();
            });
        });
    </script>
@endpush
