@extends('admin.layouts.master')

@section('content')
<div class="row">
    <div class="col">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Teams Management
                    <a href="{{ route('admin.teams.create') }}" class="btn btn-primary btn-sm panel-btn pull-right"><i class="fa fa-plus"></i> Add Team</a>
                </h3>
            </div>

            <!-- Striped Table -->
            <!--===================================================-->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
	                            <th>S.N</th>
	                            <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Position</th>
	                            <th>Status</th>
	                            <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        	@if(isset($teams) && !empty($teams))
	                        	@foreach($teams as $team)
	                    	    <tr>
	                    	        <td>{{ $loop->iteration }}</td>
	                    	        <td>{{ $team->name }}</td>
                                    <td>{{ $team->email }}</td>
                                    <td>{{ $team->phone }}</td>
	                    	        <td>{{ $team->position }}</td>
	                    	        <td>{!! $team->status ? "<span class='label label-success'>Active</span>" : "<span class='label label-danger'>Inactive</span>"!!}</td>
	                    	        <td>
	                    	            <a href="{{ route('admin.teams.edit',$team->id) }}" class="btn btn-sm btn-primary" title="Edit">
	                    	                <i class="fas fa-edit"></i>
	                    	            </a>
	                    	            <a href="#" title="Remove" class="btn btn-sm btn-danger btn-delete"
	                    	               data-toggle="modal"
	                    	               data-id="{{ $team->id }}"
	                    	               data-target="#delete-modal">
	                    	                <i class="fas fa-trash"></i>
	                    	            </a>
	                    	        </td>
	                    	    </tr>
	                        	@endforeach
	                        @else
                    	    <tr>
                    	        <td colspan="5">
                    	            <em>No data available in table ...</em>
                    	        </td>
                    	    </tr>
                        	@endif
                        </tbody>
                    </table>
                    {!! $teams->render() !!}
                </div>
            </div>
            <!--===================================================-->
            <!-- End Striped Table -->

        </div>
    </div>
    @include('admin.elements.delete-modal')
</div>
@endsection
@push('scripts')
    <script type="text/javascript">
        $(document).on('click','.btn-delete', function () {
            var route = '{{ route('admin.teams.destroy', ':id') }}';

            route = route.replace(':id', $(this).attr('data-id'));

            $('#delete-modal').find('form').attr('action', route);
            $('#delete-modal').modal('show');

            $('#delete-final').click(function (e) {
                $(this).attr('disabled',true);
                $(this).html('<i class="fas fa-spinner fa-spin"></i> Please wait...');
                $(this).closest('form').submit();
            });
        });
    </script>
@endpush
