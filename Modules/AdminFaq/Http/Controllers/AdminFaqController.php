<?php

namespace Modules\AdminFaq\Http\Controllers;

use App\Helpers\Permission;
use App\Models\Faq;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Log;

class AdminFaqController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $data['pageTitle'] = "FAQ's";
        $data['faqs'] = Faq::all();
        $data['breadcrumbs'] = '<li><a href="' . route('admin.faqs.index') . '">FAQ Management</a></li>';

        return view('adminfaq::index', $data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $data['breadcrumbs'] = '<li><a href="' . route('admin.faqs.index') . '">FAQ Management</a></li><li>Add</li>';

        return view('adminfaq::create', $data);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'status' => 'required',
            'description' => 'required'
        ]);

        try {

            $faq = new Faq;
            $faq->title = $request->title;
            $faq->status = $request->status;
            $faq->description = $request->description;

            $faq->save();

            session()->flash('success_message', __('alerts.create_success'));
            return redirect()->route('admin.faqs.index');

        } catch (\Exception $e) {
            // Log::info($e->getMessage());
            session()->flash('error_message', __('alerts.create_error'));
            return redirect()->route('admin.faqs.index');
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('adminfaq::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $data['pageTitle'] = "Edit FAQ";
        $faq = Faq::find($id);

        if (!$faq) {
            session()->flash('error_message', 'Faq not available for editing.');
            return redirect()->back();
        }

        $data['faq'] = $faq;
        $data['breadcrumbs'] = '<li><a href="' . route('admin.faqs.index') . '">FAQ Management</a></li><li>Edit</li>';

        return view('adminfaq::edit', $data);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $faq = Faq::find($id);
        $request->validate([
            'title' => 'required',
            'status' => 'required',
            'description' => 'required'
        ]);

        try {

            $faq->title = $request->title;
            $faq->status = $request->status;
            $faq->description = $request->description;

            $faq->save();

            session()->flash('success_message', __('alerts.update_success'));
            return redirect()->route('admin.faqs.index');

        } catch (\Exception $e) {
            session()->flash('error_message', __('alerts.update_error'));
            return redirect()->route('admin.faqs.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        if (!Permission::check('delete-faqs'))
            return redirect()->route('admin.401');


        try {

            $faq = Faq::find($id);

            if($faq->status == 1){
                session()->flash('error_message', __('alerts.delete_error_active'));
                return redirect()->route('admin.faqs.index');
            }
            $faq->delete();

            session()->flash('success_message', __('alerts.delete_success'));
            return redirect()->route('admin.faqs.index');

        } catch (\Exception $e) {

            session()->flash('error_message', __('alerts.delete_error'));
            return redirect()->route('admin.faqs.index');
        }
    }
}
