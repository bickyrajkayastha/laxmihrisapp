<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class EmployeeMessage extends Pivot
{
	protected $guarded = ['id'];

	public function message()
	{
		return $this->hasOne(Message::class, 'id', 'message_id');
	}

	public function sender()
	{
		return $this->hasOne(Employee::class, 'id', 'sender_id');
	}

	public function scopePersonal($query)
	{
	    return $query->where('type', '=', 1);
	}

	public function scopeGroup($query)
	{
	    return $query->where('type', '=', 2);
	}

	public function receivers()
	{
	    return $this->belongsToMany(Employee::class, 'employee_message_receiver', 'employee_message_id', 'receiver_id')->withTimestamps();
	}
}
