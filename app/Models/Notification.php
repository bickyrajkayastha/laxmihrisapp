<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    // notification_type [ 2= group notification]

	protected $guarded = ['id'];

	public function employees()
	{
	    return $this->belongsToMany(Employee::class)->withTimestamps();
	}
}
