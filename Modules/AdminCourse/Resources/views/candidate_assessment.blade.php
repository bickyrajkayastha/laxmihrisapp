@extends('admin.layouts.master')
@push('css')
<link href="{{ asset('assets/plugins/morris-js/morris.min.css') }}" rel="stylesheet">
@endpush
@section('content')
<!-- @if(Session::get('success_message'))
    <div class="alert alert-success containerAlert">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
        {{ Session::get('success_message') }}
    </div>
@endif
@if(Session::get('error_message'))
    <div class="alert alert-danger containerAlert">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
        {{ Session::get('error_message') }}
    </div>
@endif -->
<div class="row">
    <div class="col">
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">{{ $course->title }} Assessment
                    <small>{{ $employee->name }}</small>
                    <span class="badge badge-mint" style="float: right; margin-top: 12px;">Points - {{ $points }} </span>
                </h3>
            </div>

            <!-- Striped Table -->
            <!--===================================================-->
            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered">
                                        {{-- <thead>
                                            <tr>
                                                <th>S.N</th>
                                                <th>Question</th>
                                            </tr>
                                        </thead> --}}
                                        <tbody>
                                            @forelse($course_assessment as $assessment)
                                            <tr>
                                                <td width="10">#{{ $loop->iteration }}</td>
                                                <td>
                                                    {{ $assessment->question }}
                                                </td>
                                            </tr>
                                            @php
                                                $collect = collect($assessment->options);
                                                $right_ans = $collect->firstWhere('id', $assessment->answer());
                                                $color = 'red';
                                                if (isset($right_ans->employees) && !$right_ans->employees->isEmpty()) {
                                                    $color = '#68c100';
                                                }
                                            @endphp
                                            <tr style="border: 3px solid {{ $color }}; border-radius: 5px;">
                                                <td></td>
                                                <td colspan="10">
                                                    <ul class="poll-option-ul">
                                                        @foreach($assessment->options as $option)
                                                        <li><i class="fas {{ (!$option->employees->isEmpty())? 'fa-check-circle': 'fa-circle' }}"></i> {{ $option->option }}</li>
                                                        @endforeach
                                                    </ul>
                                                </td>
                                            </tr>
                                            @empty
                                            <tr>
                                                <td colspan="5">
                                                    <em>No data available in table ...</em>
                                                </td>
                                            </tr>
                                            @endforelse
                                        </tbody>
                                    </table>
                                </div>
            </div>
            <!--===================================================-->
            <!-- End Striped Table -->

        </div>
    </div>
    @include('admin.elements.delete-modal')
</div>
@endsection
