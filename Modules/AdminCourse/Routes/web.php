<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin/courses', 'as' => 'admin.', 'middleware' => 'auth:admin'], function () {
    Route::get('/', 'AdminCourseController@index')->name('courses.index')->middleware('can:view-courses');
    Route::get('/create', 'AdminCourseController@create')->name('courses.create')->middleware('can:create-courses');
    Route::post('/create', 'AdminCourseController@store')->name('courses.store')->middleware('can:create-courses');
    Route::post('/update-time', 'AdminCourseController@updateTime')->name('courses.updatetime')->middleware('can:create-courses');
    Route::get('/{id}/edit', 'AdminCourseController@edit')->name('courses.edit')->middleware('can:edit-courses');
    Route::post('/{id}/edit', 'AdminCourseController@update')->name('courses.update')->middleware('can:edit-courses');
    Route::post('/{id}/delete', 'AdminCourseController@destroy')->name('courses.destroy')->middleware('can:delete-courses');
    Route::get('/{id}/candidates', 'AdminCourseController@candidates')->name('courses.candidates')->middleware('can:create-courses');
    Route::post('/candidates', 'AdminCourseController@addCandidates')->name('courses.candidates.add')->middleware('can:create-courses');
    Route::post('/candidate/{id}/remove', 'AdminCourseController@removeCandidate')->name('courses.candidate.remove')->middleware('can:create-courses');
    Route::get('/{id}/assessment', 'AdminCourseController@assessment')->name('courses.assessment')->middleware('can:create-courses');
    Route::get('/{id}/show', 'AdminCourseController@show')->name('courses.show')->middleware('can:create-courses');
    Route::get('/candidates/{id}/{courseId}/course-assessment', 'AdminCourseController@employeeCourseAssessment')->name('courses.candidates.course-assessment')->middleware('can:view-courses');
});
