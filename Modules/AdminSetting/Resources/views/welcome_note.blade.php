@extends('admin.layouts.master')

@section('content')
<div class="row">
	<div class="col">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Welcome Note
                	<button class="btn btn-sm btn-primary pull-right panel-btn" onclick="window.history.go(-1);">
                	    <i class="fas fa-long-arrow-alt-left"></i> Back
                	</button>
                </h3>
            </div>

            <!--Horizontal Form-->
            <!--===================================================-->
            <form action="{{ route('admin.setting.welcome-note.store') }}" method="POST" class="panel-body form-horizontal form-padding" enctype="multipart/form-data">
                @csrf
                <div class="panel-body">
                	<div class="form-group">
                	    <label class="col-md-2 control-label" for="admin-first-name">Welcome Note <span class="required_color">*</span></label>
                	    <div class="col-md-10">
                            <textarea name="welcome_note" id="input-content" class="form-control ckeditor" cols="30" rows="10">{{ old('welcome_note') ? old('welcome_note') : Setting::get('welcome_note') }}</textarea>
                	        <small class="help-block text-danger">{{$errors->first('welcome_note')}}</small>
                	    </div>
                	</div>
                </div>
                <div class="panel-footer text-right">
                	<div class="row">
		                <div class="col">
		                    <button class="btn btn-sm btn-mint" type="submit">Update</button>
		                </div>
                	</div>
                </div>
            </form>
            <!--===================================================-->
            <!--End Horizontal Form-->

        </div>
    </div>
</div>
@endsection
