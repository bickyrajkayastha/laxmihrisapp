@extends('admin.layouts.master')

@section('content')
<div class="row">
	<div class="col">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">General Setting
                	<button class="btn btn-sm btn-primary pull-right panel-btn" onclick="window.history.go(-1);">
                	    <i class="fas fa-long-arrow-alt-left"></i> Back
                	</button>
                </h3>
            </div>

            <!--Horizontal Form-->
            <!--===================================================-->
            <form action="{{ route('admin.setting.general.store') }}" method="POST" class="panel-body form-horizontal form-padding" enctype="multipart/form-data">
                @csrf
                <div class="panel-body">
                	<div class="form-group">
                	    <label class="col-md-2 control-label" for="admin-first-name">System Name <span class="required_color">*</span></label>
                	    <div class="col-md-10">
                	        <input type="text" name="system_name" class="form-control" value="{{ old('system_name') ? old('system_name') : Setting::get('siteconfig')['system_name'] }}" placeholder="System Name" >
                	        <small class="help-block text-danger">{{$errors->first('system_name')}}</small>
                	    </div>{{--$_CONFIG['admin_name'];--}}
                	</div>

                	<div class="form-group">
                	    <label class="col-md-2 control-label" for="admin-first-name">Slogan <span class="required_color">*</span></label>
                	    <div class="col-md-10">
                	        <input type="text" name="system_slogan" class="form-control" value="{{ old('system_slogan') ? old('system_slogan') : Setting::get('siteconfig')['system_slogan'] }}" placeholder="System Slogan" >
                	        <small class="help-block text-danger">{{$errors->first('system_slogan')}}</small>
                	    </div>
                	</div>

                	<div class="form-group">
                	    <label class="col-md-2 control-label" for="admin-first-name">Admin Email <span class="required_color">*</span></label>
                	    <div class="col-md-10">
                	        <input type="text" name="system_email" class="form-control" value="{{ old('system_email') ? old('system_email') : Setting::get('siteconfig')['system_email'] }}" placeholder="System Email" >
                	        <small class="help-block text-danger">{{$errors->first('system_email')}}</small>
                	    </div>
                	</div>

                	<div class="form-group">
                	    <label class="col-md-2 control-label" for="admin-first-name">Feedback/Support Email <span class="required_color">*</span></label>
                	    <div class="col-md-10">
                	        <input type="text" name="system_feedback_email" class="form-control" value="{{ old('system_feedback_email') ? old('system_feedback_email') : Setting::get('system_feedback_email') }}" placeholder="Feedback Email" >
                	        <small class="help-block text-danger">{{$errors->first('system_feedback_email')}}</small>
                	    </div>
                	</div>

                	<div class="form-group">
                	    <label class="col-md-2 control-label" for="admin-first-name">Telephone No. <span class="required_color">*</span></label>
                	    <div class="col-md-10">
                	        <input type="text" name="system_telephone_no" class="form-control" value="{{ old('system_telephone_no') ? old('system_telephone_no') : Setting::get('system_telephone_no') }}" placeholder="Telephone Number" >
                	        <small class="help-block text-danger">{{$errors->first('system_telephone_no')}}</small>
                	    </div>
                	</div>

                	<div class="form-group">
                	    <label class="col-md-2 control-label" for="admin-first-name">Mobile No. <span class="required_color">*</span></label>
                	    <div class="col-md-10">
                	        <input type="text" name="system_mobile" class="form-control" value="{{ old('system_mobile') ? old('system_mobile') : Setting::get('system_mobile') }}" placeholder="Mobile Number" >
                	        <small class="help-block text-danger">{{$errors->first('system_mobile')}}</small>
                	    </div>
                	</div>
                </div>
                <div class="panel-footer text-right">
                	<div class="row">
		                <div class="col">
		                    <button class="btn btn-sm btn-mint" type="submit">Update</button>
		                </div>
                	</div>
                </div>
            </form>
            <!--===================================================-->
            <!--End Horizontal Form-->

        </div>
    </div>
</div>
@endsection
