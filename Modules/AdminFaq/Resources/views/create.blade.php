@extends('admin.layouts.master')
@section('content')
<div class="row">
	<div class="co">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Add Faq
                    <button class="btn btn-sm btn-primary pull-right panel-btn" onclick="window.history.go(-1);">
                        <i class="fas fa-long-arrow-alt-left"></i> Back
                    </button>
                </h3>
            </div>

            <!--Horizontal Form-->
            <!--===================================================-->
            <form action="{{ route('admin.faqs.store') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
                @csrf
                <div class="panel-body">
                    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                        <label class="col-sm-2 control-label" for="input-title">Title <span class="required_color">*</span></label>
                        <div class="col-sm-9">
                            <input type="text" placeholder="" value="{{ old('title') }}" name="title" id="input-title" class="form-control">
                            <small class="help-block text-danger">{{ $errors->first('title')}}</small>
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
	                    <label class="col-md-2 control-label">Is Published</label>
	                    <div class="col-md-9">
	                        <div class="radio">
	
	                            <!-- Inline radio buttons -->
	                            <input id="input-status" class="magic-radio" type="radio" name="status" value="1" <?= ((old('status') == 1)? 'checked':''); ?>>
	                            <label for="input-status">Yes</label>
	
	                            <input id="input-status-2" class="magic-radio" value="0" type="radio" name="status" <?= ((old('status') == 0)? 'checked':''); ?>>
	                            <label for="input-status-2">No</label>
	                        </div>
	                    </div>
	                </div>
                    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                        <label class="col-sm-2 control-label" for="description">Description <span class="required_color">*</span></label>
                        <div class="col-sm-9">
	                        <textarea name="description" id="input-description" class="form-control ckeditor">{!! old('description') !!}</textarea>
                            <small class="help-block text-danger">{{ $errors->first('description') }}</small>
                        </div>
                    </div>
                </div>
                <div class="panel-footer text-right">
                	<div class="row">
		                <div class="col-lg-11">
		                    <button class="btn btn-sm btn-mint" type="submit">Create</button>
		                </div>
                	</div>
                </div>
            </form>
            <!--===================================================-->
            <!--End Horizontal Form-->

        </div>
    </div>
</div>
@endsection