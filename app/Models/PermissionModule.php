<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PermissionModule extends Model
{
    protected $table = 'permission_modules';

    protected $guarded = [ 'id' ];

    public function permission_references()
    {
        return $this->hasMany(PermissionReference::class,'permission_modules_id');
    }
}
