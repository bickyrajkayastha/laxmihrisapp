<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Carbon;
use Illuminate\Support\Facades\Log;

class EmployeeMessageListCollection extends ResourceCollection
{
    public $success = true;
    public $message = "";

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        $data = "";

        if (iterator_count($this->resource)) {
            $this->message = "Employee list.";

            $data =  $this->map(function($employee) {
                $date = "";
                $collection = collect($employee->messages);
                $merged = $collection->merge($employee->received_messages);

                $latest_messages = $merged->sortByDesc('id')->values()->all();

                $created_at = "";
                if (count($latest_messages) > 0) {
                    $date = Carbon\Carbon::parse($latest_messages[0]->created_at)->diffForHumans();
                    $created_at = Carbon\Carbon::parse($latest_messages[0]->created_at);
                    $latest_messages = "<p>" . (($latest_messages[0]->pivot->sender_id == auth()->user()->id)? "<b>You: </b>": "") . $latest_messages[0]->message . "</p>";
                }

                return [
                    'id' => $employee->id,
                    'name' => $employee->name,
                    'thumbImgPath' => $employee->thumbImgPath,
                    'message' => $latest_messages,
                    'message_date' => $date,
                    'message_created' => $created_at
                ];
            });

            $data = $data->sortByDesc('message_created')->forget('message_created')->values()->all();

            

            return $data;

        } else {
            $this->message = "No conversation started yet.";
        }
    }

    public function with($request)
    {
        return [
            'success' => $this->success,
            'message' => $this->message
        ];
    }
}
