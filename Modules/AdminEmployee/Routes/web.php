<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin/employees', 'as' => 'admin.', 'middleware' => 'auth:admin'], function() {
	Route::get('/', 'AdminEmployeeController@index')->name('employees')->middleware('can:view-employees');

	Route::get('/create', 'AdminEmployeeController@create')
	->name('employees.create')
    ->middleware('can:create-employees');

    Route::get('/import', 'AdminEmployeeController@import')
    ->name('employees.import')
    ->middleware('can:create-employees');

    Route::post('/import', 'AdminEmployeeController@importExcel')
    ->name('employees.store-import')
    ->middleware('can:create-employees');

	Route::post('/create', 'AdminEmployeeController@store')
	->name('employees.store')
	->middleware('can:create-employees');

	Route::get('/{id}/edit', 'AdminEmployeeController@edit')
	->name('employees.edit')
	->middleware('can:edit-employees');

	Route::get('/{id}/detail', 'AdminEmployeeController@detail')
	->name('employees.detail')
	->middleware('can:detail-employees');

	Route::post('/{id}/edit', 'AdminEmployeeController@update')
	->name('employees.update')
	->middleware('can:edit-employees');

	Route::post('/{id}/delete', 'AdminEmployeeController@destroy')
	->name('employees.destroy')
	->middleware('can:delete-employees');

	Route::post('password-reset', 'AdminEmployeeController@passwordReset')
    ->name('employees.password-reset')
    ->middleware('can:edit-employees');

	Route::post('job-description', 'AdminEmployeeController@saveJobDescription')->name('employees.job-description');
});
