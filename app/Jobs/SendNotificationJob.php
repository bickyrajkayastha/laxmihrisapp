<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Notifications\ExpoNotification;
use ExponentPhpSDK\Exceptions\ExpoException;
use ExponentPhpSDK\Expo;
use App\Models\Employee;
use App\Models\EmployeeDevice;
use Illuminate\Support\Facades\Log;

class SendNotificationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $notification;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($notification)
    {
        $this->notification = $notification;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $notification_data = $this->notification;
        $recipients = EmployeeDevice::whereIn('employee_id', $notification_data['employees'])->get();

        $expo = Expo::normalSetup();
        foreach ($recipients as $recipient) {
            try {

                $channelName = 'laxmiAES52' . $recipient->id . "5842";
                $recipient_expo_token = $recipient->expo_token;

                // You can quickly bootup an expo instance

                // Subscribe the recipient to the server
                $expo->subscribe($channelName, $recipient_expo_token);

                // Build the notification data
                $notification = [
                    'title' => $notification_data['title'],
                    'body' => $notification_data['body'],
                    'data' => [
                        'type' => 1,
                        'messageData' => [
                            'title' => $notification_data['title'],
                            'body' => $notification_data['body']
                        ]
                    ]
                ];

                // Notify an interest with a notification
                $expo->notify($channelName, $notification);
            } catch (ExpoException $e) {
                Log::info($e->getMessage());
                $recipient->delete();
            }
        }
    }
}
