<?php

use Illuminate\Database\Seeder;

class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        \Illuminate\Support\Facades\DB::table('user_group')->truncate();
        \Illuminate\Support\Facades\DB::table('admins')->truncate();
        \Illuminate\Support\Facades\DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $superAdmin = \App\Models\Admin::create( [
            'first_name'=>'Admin',
            'last_name'=>'admin',
            'email'=>'admin@admin.com',
            'username'=>'admin',
            'password'=> bcrypt('123456'),
            'status'=> 1,
        ]);

        // super admin
        \App\Models\UserGroup::create([
            'user_id' => $superAdmin->id,
            'group_id' => 1
        ]);
    }
}
