<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserGroup extends Model
{
	protected $table = 'user_group';

	protected $guarded = ['id'];

	public function group_detail()
	{
	    return $this->belongsTo(Group::class,'group_id');
	}
}
