<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrainingProgramDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('training_program_documents', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('training_id');
            $table->foreign('training_id')->references('id')->on('training_programs')->onDelete('cascade');
            $table->string('file', 191)->nullable();
            $table->index('id', 'training_id');
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('training_program_documents');
    }
}
