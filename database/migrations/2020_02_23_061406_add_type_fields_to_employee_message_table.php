<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeFieldsToEmployeeMessageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employee_message', function (Blueprint $table) {
            $table->tinyInteger('type')->default(1)->comment("1 = personal message, 2 = group message");
            $table->unsignedInteger('message_group_id')->nullable();
            $table->foreign('message_group_id')->references('id')->on('message_groups')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employee_message', function (Blueprint $table) {
            //
        });
    }
}
