<?php

namespace Modules\AdminTrainingProgram\Http\Controllers;

use App\Helpers\Permission;
use App\Models\Course;
use App\Models\TrainingProgram;
use App\Models\TrainingProgramDocument;
use App\Services\ImageUpload\Strategy\UploadWithAspectRatio;
use App\Traits\Slugger;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Intervention\Image\Facades\Image;
use Modules\AdminTrainingProgram\Services\TrainingProgramImageUploader;
use Illuminate\Support\Facades\Log;

class AdminTrainingProgramController extends Controller
{
    use Slugger;

    private $indexRoute;

    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->indexRoute = route('admin.trainings.index');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $data['pageTitle'] = "Training Program Management";
        $data['trainings'] = TrainingProgram::orderBy('start_date', 'desc')->paginate(100);

        return view('admintrainingprogram::index', $data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create($courseId)
    {
        $data['course'] = Course::find($courseId);
        $data['pageTitle'] = "Add Training Program";
        $data['breadcrumbs'] = '<li><a href="' . route('admin.courses.index') . '">Courses</a></li><li><a href="' . route('admin.courses.show', $data['course']->id) . '">'.$data['course']->title.'</a></li><li><a href="' . route('admin.trainings.index') . '">Training Program Management</a></li><li>Add</li>';
        return view('admintrainingprogram::create', $data);
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        $success = false;
        $time = date('G:i:s', strtotime($this->request->start_time));
        $this->request->merge(['start_time' => $time]);

        $time = date('G:i:s', strtotime($this->request->end_time));
        $this->request->merge(['end_time' => $time]);

        $this->request->validate([
            'course_id' => 'required',
            'title' => 'required',
            'description' => 'nullable',
            'file' => 'nullable|file|mimes:png,jpg,jpeg',
            'start_date' => 'required|date',
            'end_date' => 'nullable|date',
            'start_time' => 'nullable|date_format:G:i:s',
            'end_time' => 'nullable|date_format:G:i:s|after:start_time'
        ]);

        try {
            $data = $this->request->only('title', 'course_id', 'description', 'status', 'start_time', 'end_time');
            $data['slug'] = $this->createSlug($this->request->title, TrainingProgram::class);

            if ($this->request->start_date != "") {
                $data['start_date'] = date('y-m-d', strtotime($this->request->start_date));
            }

            if ($this->request->end_date != "") {
                $data['end_date'] = date('y-m-d', strtotime($this->request->end_date));
            }

            if ($this->request->hasFile('file')) {
                $file = $this->request->file('file');

                $uploader = new TrainingProgramImageUploader(new UploadWithAspectRatio());

                $data['file'] = $uploader->saveOriginalImage($file);

                $this->cropAndSaveImage(
                    $uploader,
                    $data['file'],
                    $this->request->x1,
                    $this->request->y1,
                    $this->request->w,
                    $this->request->h
                );
            }

            $training = TrainingProgram::create($data);

            // save files.
            if ($this->request->hasFile('files')) {
                foreach ($this->request->file('files') as $file) {
                    $document = new \App\Models\TrainingProgramDocument();
                    // save mother signature file.
                    $fileOriginal = $file->getClientOriginalName();
                    $extension = $file->getClientOriginalExtension();
                    $filesize = $file->getClientSize();
                    $filename = pathinfo($fileOriginal, PATHINFO_FILENAME);
                    $filename = time() . '_' . str_random(5) . '_' . str_slug($filename) . '.' . $extension;
                    $document_file_path = '/uploads/training-documents/' . $training->id . '/';
                    $file->move(public_path($document_file_path), $filename);
                    $document->file = $filename;
                    $training->documents()->save($document);
                }
            }

            $success = true;
            $message = __('alerts.create_success');
            session()->flash('success_message', __('alerts.create_success'));
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            session()->flash('error_message', __('alerts.create_error'));
            $message = __('alerts.create_error');
        }

        return response()->json([
            'success' => $success,
            'message' => $message,
            'data' => [
                'redirect_url' => route('admin.courses.trainings.index', $this->request->course_id)
            ]
        ]);
        // return redirect()->route('admin.courses.trainings.index', $this->request->course_id);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('admintrainingprogram::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $data['pageTitle'] = "Edit TrainingProgram";
        $data['training'] = $this->getTrainingProgramById($id);
        $course = $data['training']->course;
        $data['breadcrumbs'] = '<li><a href="'.route('admin.courses.index').'">Course Management</a></li><li><a href="'.route('admin.courses.trainings.index', $course->id).'">'.$course->title.'</a></li><li>'.$data['training']->title.'</li><li>Edit</li>';

        return view('admintrainingprogram::edit', $data);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update($id)
    {
        $success = true;
        $message = "";
        $time = date('G:i:s', strtotime($this->request->start_time));
        $this->request->merge(['start_time' => $time]);
        $end_time = date('G:i:s', strtotime($this->request->end_time));
        $this->request->merge(['end_time' => $end_time]);

        $this->request->validate([
            'title' => 'required',
            'description' => 'nullable',
            'file' => 'nullable|file|mimes:png,jpg,jpeg',
            'start_date' => 'required|date',
            'end_date' => 'nullable|date',
            'start_time' => 'nullable|date_format:G:i:s',
            'end_time' => 'nullable|date_format:G:i:s|after:start_time'
        ], [
            'end_time.after' => 'The end time must be greater than start time.'
        ]);

        try {
            $data = $this->request->only('title', 'description', 'status', 'start_time', 'end_time');

            $data['slug'] = $this->createSlug($this->request->name, TrainingProgram::class);

            if ($this->request->start_date != "") {
                $data['start_date'] = date('y-m-d', strtotime($this->request->start_date));
            }

            if ($this->request->end_date != "") {
                $data['end_date'] = date('y-m-d', strtotime($this->request->end_date));
            }

            $training = TrainingProgram::find($id);

            if ($this->request->hasFile('file') || $this->request->croppreviousfile == 'true') {
                $uploader = new TrainingProgramImageUploader(new UploadWithAspectRatio());

                $uploader->deleteThumbImage($training->file);

                $uploader->deleteCroppedImage($training->file);

                if ($this->request->croppreviousfile == 'true') {
                    $data['file'] = $training->file;
                } else {
                    $uploader->deleteFullImage($training->file);

                    $data['file'] = $uploader->saveOriginalImage($this->request->file('file'));
                }

                $this->cropAndSaveImage(
                    $uploader,
                    $data['file'],
                    $this->request->x1,
                    $this->request->y1,
                    $this->request->w,
                    $this->request->h
                );
            }

            // save files.
            if ($this->request->hasFile('files')) {
                foreach ($this->request->file('files') as $file) {
                    $document = new \App\Models\TrainingProgramDocument();
                    // save mother signature file.
                    $fileOriginal = $file->getClientOriginalName();
                    $extension = $file->getClientOriginalExtension();
                    $filesize = $file->getClientSize();
                    $filename = pathinfo($fileOriginal, PATHINFO_FILENAME);
                    $filename = time() . '_' . str_random(5) . '_' . str_slug($filename) . '.' . $extension;
                    $document_file_path = '/uploads/training-documents/' . $training->id . '/';
                    $file->move(public_path($document_file_path), $filename);
                    $document->file = $filename;
                    $training->documents()->save($document);
                }
            }

            // remove files if any.
            if (!empty($this->request->remove_files_arr)) {
                $remove_files = explode(',', $this->request->remove_files_arr);

                foreach ($remove_files as $file) {
                    $doc = TrainingProgramDocument::find($file);
                    $fullImage = public_path('uploads/training-documents/' . $training->id . '/' . $doc->file);
                    if(file_exists($fullImage) && is_file($fullImage)){
                        unlink($fullImage);
                    }
                    $doc->delete();
                }
            }

            $training->update($data);
            $message = __('alerts.update_success');
            session()->flash('success_message', __('alerts.update_success'));
        } catch (\Exception $e) {
            session()->flash('error_message', __('alerts.update_error'));
            $message = __('alerts.update_error');
            $success = false;
        }

        return response()->json([
            'success' => $success,
            'message' => $message,
            'data' => [
                'redirect_url' => route('admin.courses.trainings.index', $training->course_id)
            ]
        ]);
        // return redirect()->to($this->indexRoute);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        try {

            $training = $this->getTrainingProgramById($id);

            if ($training->status == 1) {
                session()->flash('error_message', __('alerts.delete_error_active'));
                return redirect()->back();
            }

            $uploader = new TrainingProgramImageUploader();

            $uploader->deleteFullImage($training->file);

            $uploader->deleteThumbImage($training->file);

            $uploader->deleteCroppedImage($training->file);

            $training->delete();

            session()->flash('success_message', __('alerts.delete_success'));
        } catch (\Exception $e) {
            session()->flash('error_message', __('alerts.delete_error'));
        }

        return redirect()->back();
    }

    private function cropAndSaveImage($uploader, $filename, $posX1, $posY1, $width, $height)
    {
        $imgPath = $uploader->getFullImagePath($filename);

        $fullImage = Image::make($imgPath);

        $cropDestPath = $uploader->getCroppedImagePath($filename);

        $uploader->cropAndSaveImage($fullImage, $cropDestPath, $posX1, $posY1, $width, $height);

        $uploader->cropAndSaveImageThumb($fullImage, $filename);
    }

    private function getTrainingProgramById($id)
    {
        return TrainingProgram::find($id);
    }

    public function coursesTainings($id)
    {
        $data['course'] = Course::find($id);
        $data['trainings'] = $data['course']->trainings;
        $data['breadcrumbs'] = '<li><a href="' . route('admin.courses.index') . '">Courses Management</a></li><li><a href="' . route('admin.courses.show', $data['course']->id) . '">'.$data['course']->title.'</a></li><li>Trainings</li>';

        return view('admintrainingprogram::index', $data);
    }
}
