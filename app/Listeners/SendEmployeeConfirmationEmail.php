<?php

namespace App\Listeners;

use App\Helpers\Setting;
use App\Events\EmployeeAdded;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class SendEmployeeConfirmationEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  EmployeeAdded  $event
     * @return void
     */
    public function handle(EmployeeAdded $event)
    {
        try {
            $employee = $event->employee;
            $commonVars = [
                '[[SYSTEM_NAME]]' => Setting::get('siteconfig')['system_name'],
                '[[SYSTEM_ADDRESS]]' => "Kathmandu, Nepal",
                '[[SYSTEM_EMAIL]]' => Setting::get('siteconfig')['system_email'],
                '[[SYSTEM_CONTACT]]' => Setting::get('system_telephone_no'),
                '[[SYSTEM_LINK]]' => url('/'),
                '[[DATE]]' => date('Y'),
                // '[[SYSTEM_FACEBOOK]]' => Setting::get('facebook_url'),
                // '[[SYSTEM_INSTAGRAM]]' => Setting::get('instagram_url'),
                // '[[SYSTEM_TWITTER]]' => Setting::get('twitter_url'),
                // '[[SYSTEM_YOUTUBE]]' => Setting::get('youtube_url'),
                '[[PRIVACY_POLICY_LINK]]' => url('/'),
                '[[UNSUBSCRIBE_LINK]]' => url('/'),
                '[[SUPPORT_LINK]]' => url('/'),
            ];

            $mailData = \App\Models\EmailTemplate::find(1)->toArray();
            $mailData['to'] = $employee->email;
            $mailData['vars'] = [
                '[[FULL_NAME]]' => $employee->name,
                '[[EMPLOYEE_EMAIL]]' => $employee->email,
                '[[EMPLOYEE_PASSWORD]]' => $employee->employee_password,
                '[[VERIFY_LINK]]' => url('/employee-verification?_token=') . $employee->employee_verification_token
            ];

            // $mailData['vars'] = isset($mailData['vars']) ? $mailData['vars'] : [];

            $varsToReplace = array_merge($commonVars, $mailData['vars']);

            $mailData['description'] = strtr($mailData['description'], $varsToReplace);

            Mail::send('emails.common', ['body' => $mailData['description']], function ($message) use ($mailData) {
                // $message->to($mailData['to']);
                $message->to($mailData['to']);
                $message->from(Setting::get('siteconfig')['system_email']);
                $message->subject($mailData['subject']);

                if (isset($mailData['cc'])) {
                    $message->cc($mailData['cc']);
                }

                if (isset($mailData['attachment'])) {
                    $message->attach($mailData['attachment']);
                }
            });
        } catch (\Throwable $th) {
            \Log::info($th->getMessage());
        }

    }
}
