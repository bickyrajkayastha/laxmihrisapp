<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmployeeFamilyDetail extends Model
{
	protected $guarded = ['id'];
}
