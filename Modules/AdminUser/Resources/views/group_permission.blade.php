@extends('admin.layouts.master')
@section('content')
<div class="row">
    <div class="col">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Groups Permission | {{ $group_details->name }}
                </h3>
            </div>
            <div class="panel-body">
                <form method="POST" action="{{ route('admin.user.groups.permission-store') }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="_group_id" value="{{ $group_details->id }}">
                    <table class="table  table-bordered adminMgmtTable">
                        <thead>
                            <tr>
                                <th class="text-center">
                                    <div class="checkbox" style="margin-top: 0px;margin-bottom: 0px;">
                                        <input id="select-all" class="magic-checkbox" type="checkbox">
                                        <label for="select-all" style="color: #000023;"></label>
                                    </div>
                                </th>
                                <th>Permission Name</th>
                                <th>Permission Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if( count( $modules ) > 0 )
                            @foreach( $modules as $module )
                            <tr>
                                <td></td>
                                <td colspan="5">
                                    <strong>{{ strtoupper( $module->name ) }}</strong>
                                </td>
                            </tr>
                            @if ( isset( $module->permission_references ) && $module->permission_references->count() > 0 )
                            @foreach( $module->permission_references as $per_ref )
                            <tr>
                                <td class="text-center">
                                    <div class="checkbox" style="margin-top: 0px;margin-bottom: 0px;">
                                        <input id="permission-{{ $per_ref->code }}" class="magic-checkbox permission" type="checkbox" name="permission_reference_id[]" value="{{ $per_ref->id }}" {{ in_array($per_ref->id,$group_permissions) ? "checked" : ""}}>
                                        <label for="permission-{{ $per_ref->code }}" style="color: #000023;"></label>
                                    </div>
                                </td>
                                <td>{{ $per_ref->short_desc }}</td>
                                <td>{{ $per_ref->description }}</td>
                            </tr>
                            @endforeach
                            @endif
                            @endforeach
                            @else
                            <tr>
                                <td class="text-center" colspan="6">
                                    <strong>No records found!!!</strong>
                                </td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2">
                                <input type="submit" class="btn btn-sm btn-block btn-primary" name="submit" value="UPDATE PERMISSIONS">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script type="text/javascript">
$(function() {
    $('#select-all').click(function() {
        console.log($(this).is(':checked'));
        $(this).closest('form').find('input.permission').prop('checked', $(this).is(':checked'));
    });
})

</script>
@endpush
