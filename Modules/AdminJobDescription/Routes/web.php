<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin/job-descriptions', 'as' => 'admin.', 'middleware' => 'auth:admin'], function () {
    Route::get('/', 'AdminJobDescriptionController@index')->name('job-descriptions.index')->middleware('can:view-job-descriptions');
    Route::get('/create', 'AdminJobDescriptionController@create')->name('job-descriptions.create')->middleware('can:create-job-descriptions');
    Route::post('/create', 'AdminJobDescriptionController@store')->name('job-descriptions.store')->middleware('can:create-job-descriptions');
    Route::get('/{id}/edit', 'AdminJobDescriptionController@edit')->name('job-descriptions.edit')->middleware('can:edit-job-descriptions');
    Route::post('/{id}/edit', 'AdminJobDescriptionController@update')->name('job-descriptions.update')->middleware('can:edit-job-descriptions');
    Route::post('/{id}/delete', 'AdminJobDescriptionController@destroy')->name('job-descriptions.destroy')->middleware('can:delete-job-descriptions');
    Route::get('/{id}/show', 'AdminJobDescriptionController@show')->name('job-descriptions.show')->middleware('can:create-job-descriptions');
});
