<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class GroupMessageMemberCollection extends ResourceCollection
{
    public $success = true;
    public $message = "";

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);

        if (iterator_count($this->resource)) {
            $this->message = "Member list.";
            return $this->map(function($member) {
                return [
                    'id' => $member->id,
                    "email" => $member->email,
                    "name" => $member->name,
                    "phone" => $member->phone,
                    "thumbImgPath" => $member->thumbImgPath
                ];
            });
        } else {
            $this->message = "No members.";
        }
    }

    public function with($request)
    {
        return [
            'success' => $this->success,
            'message' => $this->message
        ];
    }
}
