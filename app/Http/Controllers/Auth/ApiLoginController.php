<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Resources\ApiResource;
use App\Jobs\ResetPasswordEmailJob;
use App\Models\Employee;
use App\Models\EmployeeDevice;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;
use Exception;
use Auth;

class ApiLoginController extends Controller
{
	public function test()
	{
		// return response()->json([
		// 	'data' => [1,2,3,4,5,6,6,7]
		// ]);
	}

	public function registerEmployee(Request $request)
	{
	    $request->validate([
	        'email' => 'required|unique:employees,email',
	        'password' => 'required',
	    ]);

	    $email = $request->email;

	    $token = '';
	    $success = false;
	    $status_code = 404;
	    $message = "";

	    // create employee.
	    $password = Hash::make($request->password);
	    $request->merge(['password' => $password]);
	    $employee = $this->createEmployee($request);
	    if ($employee) {
	        $token = JWTAuth::fromUser($employee);
	        $status_code = 201;
	        $success = true;
	        $message = "The employee been registered successfully.";
	    } else {
	        $message = __('alerts.create_error');
	        $status_code = 404;
	    }

	    return response()->json([
	        'success' => $success,
	        'message' => $message,
	        'token' => $token
	    ], $status_code);
	}

	public function createEmployee(Request $request)
	{
	    $employee = new Employee;
	    $employee->fill($request->all());
	    $employee->status = 0;

	    if ($employee->save()) {
	        return $employee;
	    }

	    return 0;
	}

	/**
	 * @SWG\Post(
	 *     path="/login",
	 *     description="authenticate customer",
	 *     tags={"Employee Apis"},
	 *     @SWG\Parameter(
	 *         in="query",
	 *         name="email",
	 *         type="string",
	 *         description="email of the customer.",
	 *         required=true,
	 *     ),
	 *     @SWG\Parameter(
	 *         in="query",
	 *         name="password",
	 *         type="string",
	 *         description="password",
	 *         required=true,
	 *     ),
	 *     @SWG\Parameter(
	 *         in="query",
	 *         name="expo_token",
	 *         type="string",
	 *         description="token of expo",
	 *         required=true,
	 *     ),
	 *     @SWG\Response(
	 *         response=200,
	 *         description="status, message",
	 *     ),
	 *     @SWG\Response(
	 *         response=422,
	 *         description="status, message"
	 *     )
	 * )
	 */
	public function getAccessToken(Request $request)
	{
		$request->validate([
			'email' => 'required|email',
			'password' => 'required',
			'expo_token' => 'required'
		]);

	    $token = '';
	    $message = "";
	    $success = false;
	    $already_logged_in = false;
	    $is_verified = false;
	    $user_id = "";

        $employee = Employee::where([
        	['email', '=', $request->email],
        	['status', '=', 1]
        ])->first();

        if ($employee) {

        	// check if the employee has been verified.
        	if ($employee->is_verified == 1) {
        		$is_verified = true;
        	}

            if (Hash::check($request->password, $employee->password) && $is_verified) {
            	$user_id = $employee->id;
            	if ($employee->employee_personal_detail) {
            		$already_logged_in = true;
            	}

                $message = "Valid.";
                $token = JWTAuth::fromUser($employee);
                $this->saveEmployeeDevice($employee->id, $request->only('expo_token'));
                $success = true;
            } else {
            	if (!$is_verified) {
	                $message = "Please check your email for verification before login. Thank you.";
            	} else {
	                $message = "Invalid username or password.";
            	}
            }
        } else {
            $message = "Invalid username or password.";
        }

	    return response()->json([
	        'data' => [
	        	'token' => $token,
	        	'already_logged_in' => $already_logged_in,
                'user_id' => $user_id,
                'employee_code' => $employee->employee_code ?? ''
	        ],
	        'message' => $message,
	        'success' => $success
	    ]);
	}

	public function saveEmployeeDevice($employee_id, $data)
	{
		$device = EmployeeDevice::firstOrNew(['expo_token' => $data['expo_token']]);
		$device->employee_id = $employee_id;
		$device->device_type = 'android';
		$device->save();
	}

	/**
	 * @SWG\Get(
	 *     path="/employee/refresh-token",
	 *     description="refresh the token",
	 *     tags={"Employee Apis"},
	 *     security={{"bearerAuth":{}}},
	 *     @SWG\Response(
	 *         response=200,
	 *         description="status, message",
	 *     ),
	 *     @SWG\Response(
	 *         response=422,
	 *         description="status, message"
	 *     )
	 * )
	 */
	public function refreshToken()
	{
	    try {
	        $newToken = Auth::refresh();
	    } catch (TokenInvalidException $e) {
	        return response()->json(['error' => $e->getMessage()], 401);
	    }

	    return response()->json(['token' => $newToken]);
	}

	/**
	 * @SWG\Get(
	 *     path="/employee/logout",
	 *     description="refresh the token",
	 *     tags={"Employee Apis"},
	 *     security={{"bearerAuth":{}}},
	 *     @SWG\Parameter(
	 *         name="expo-token",
	 *         in="header",
	 *         description="ID",
	 *         required=false,
	 *         type="string",
	 *     ),
	 *     @SWG\Response(
	 *         response=200,
	 *         description="success, message",
	 *     ),
	 *     @SWG\Response(
	 *         response=422,
	 *         description="success, message"
	 *     )
	 * )
	 */
	public function logout(Request $request)
	{
		$success = false;
		$message = "";
		$http_status_code = 200;

		if (!$request->headers->has('expo-token')) {

			$message = "Invalid expo token";
			$http_status_code = 401;
		} else {

			$message = "Something went wrong. Please try again.";
			$expo_token = $request->header('expo-token');

			if (auth()->invalidate(true)) {
				$logged_in_device = \App\Models\EmployeeDevice::where('expo_token', '=', $expo_token)->first();
				if ($logged_in_device->delete()) {
					$message = "You've successfully logged out.";
				}
			}
			$success = true;
		}


		return response()->json([
			'data' => [],
			'success' => $success,
			'message' => $message
		], $http_status_code);
    }

    /**
    * @SWG\Post(
    *     path="/reset-password",
    *     description="reset password.",
    *     tags={"Employee Apis"},
    *     @SWG\Parameter(
    *         in="body",
    *         name="reset password.",
    *         description="save reset password.",
    *         required=true,
    *             @SWG\Property(
    *                   property="reset password",
    *                   type="object",
    *                   required={"email"},
    *                   @SWG\Property(
    *		                 property="email",
    *		                 type="string",
    *		                 description="email of employee",
    *                   ),
    *              ),
    *     ),
    *     @SWG\Response(
    *         response=200,
    *         description="status, message",
    *     ),
    *     @SWG\Response(
    *         response=422,
    *         description="status, message"
    *     )
    * )
    */
    public function resetPassword(Request $request)
    {
        $data = (object)[];
        $success = true;
        $message = "";
        $request->validate([
            'email' => 'required|email|exists:employees,email'
        ]);

        $employee = Employee::where('email', $request->email)->first();

        if ($employee) {
            $new_password = str_random(8);
            $employee->password = Hash::make($new_password);
            $employee->save();

            // send email to employee.
            $maildata = [];
            $maildata = [
                'name' => $employee->name,
                'password' => $new_password,
                'email' => $employee->email
            ];

            dispatch(new ResetPasswordEmailJob($maildata));

            $message = "Please check your email for new password.";
        }

        return response()->json([
            'data' => $data,
            'success' => $success,
            'message' => $message,
        ]);
    }

    /**
     * @SWG\Post(
     *     path="/employee/change-password",
     *     description="change password.",
     *     tags={"Employee Apis"},
     *     @SWG\Parameter(
     *         in="body",
     *         name="change password.",
     *         description="change password.",
     *         required=true,
     *             @SWG\Property(
     *                   property="change password",
     *                   type="object",
     *                   required={"old_password"},
     *                   @SWG\Property(
     *		                 property="old_password",
     *		                 type="string",
     *		                 description="old_password of employee",
     *                   ),
     *                  @SWG\Property(
     *		                 property="new_password",
     *		                 type="string",
     *		                 description="new_password of employee",
     *                   ),
     *              ),
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="status, message",
     *     ),
     *     @SWG\Response(
     *         response=422,
     *         description="status, message"
     *     )
     * )
     */
    public function changePassword(Request $request)
    {
        $message = "";
        $success = true;
        $request->validate([
            'old_password' => 'required',
            'new_password' => 'required'
        ]);

        if (Hash::check($request->old_password, auth()->user()->password)) {
            // update new password
            Employee::find(auth()->user()->id)->update(['password' => Hash::make($request->new_password)]);
            $message = "Your password has been changed successfully.";
            auth()->user()->employee_devices()->delete();
            auth()->invalidate(true);
        } else {
            $message = "Your old password did not matched";
            $success = false;
        }

        return new ApiResource([], $success, $message);
    }
}
