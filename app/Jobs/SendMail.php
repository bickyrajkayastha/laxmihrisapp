<?php

namespace App\Jobs;

use App\Helpers\Setting;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendMail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $data;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        //
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $commonVars = [
            '[[SYSTEM_NAME]]' => Setting::get('system_name'),
            '[[SYSTEM_ADDRESS]]' => "Kathmandu, Nepal",
            '[[SYSTEM_EMAIL]]' => "admin@weplay.com",
            '[[SYSTEM_CONTACT]]' => "+977 9841045236",
            '[[SYSTEM_LINK]]' => url('/'),
            '[[DATE]]' => date('Y'),
            '[[SYSTEM_FACEBOOK]]' => Setting::get('facebook_url'),
            '[[SYSTEM_INSTAGRAM]]' => Setting::get('instagram_url'),
            '[[SYSTEM_TWITTER]]' => Setting::get('twitter_url'),
            '[[SYSTEM_YOUTUBE]]' => Setting::get('youtube_url'),
            '[[PRIVACY_POLICY_LINK]]' => url('/'),
            '[[UNSUBSCRIBE_LINK]]' => url('/'),
            '[[SUPPORT_LINK]]' => url('/'),
        ];

        $mailData = $this->data;

        $mailData['vars'] = isset($mailData['vars']) ? $mailData['vars'] : [];

        $varsToReplace = array_merge($commonVars, $mailData['vars']);

        $mailData['body'] = strtr($mailData['body'], $varsToReplace);

        Mail::send('common.email', $mailData, function ($message) use ($mailData) {
            $message->to($mailData['to']);
            $message->subject($mailData['subject']);

            if (isset($mailData['cc'])) {
                $message->cc($mailData['cc']);
            }

            if (isset($mailData['attachment'])) {
                $message->attach($mailData['attachment']);
            }
        });
    }
}
