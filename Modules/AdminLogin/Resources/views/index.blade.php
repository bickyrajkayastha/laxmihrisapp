<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ isset( $title ) ? $title : Setting::get('siteconfig')['system_name'] }}</title>


    <!--STYLESHEET-->
    <!--=================================================-->

    <!--Open Sans Font [ OPTIONAL ] -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&amp;subset=latin" rel="stylesheet">
    <link id="favicon" rel="icon" type="image/png" sizes="64x64" href="{{asset('backend/favicon-be.ico')}}">


    <!--Bootstrap Stylesheet [ REQUIRED ]-->
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">


    <!--Nifty Stylesheet [ assets ]-->
    <link href="{{ asset('assets/css/nifty.min.css') }}" rel="stylesheet">


    <!--Premium Icons [ OPTIONAL ]-->
    <link href="{{ asset('assets/premium/icon-sets/icons/line-icons/premium-line-icons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/premium/icon-sets/icons/solid-icons/premium-solid-icons.min.css') }}" rel="stylesheet">


    <!--Magic Checkbox [ OPTIONAL ]-->
    <link href="{{ asset('assets/plugins/magic-check/css/magic-check.min.css') }}" rel="stylesheet">


    <!--JAVASCRIPT-->
    <!--=================================================-->

    <!--Page Load Progress Bar [ OPTIONAL ]-->
    <link href="{{ asset('assets/css/pace.min.css') }}" rel="stylesheet">
    <script src="{{ asset('assets/js/pace.min.js') }}"></script>


    <!--jQuery [ REQUIRED ]-->
    <script src="{{ asset('assets/js/jquery.min.js') }}"></script>


    <!--BootstrapJS [ RECOMMENDED ]-->
    <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>


    <!--Nifty Admin [ RECOMMENDED ]-->
    <script src="{{ asset('assets/js/nifty.min.js') }}"></script>

</head>

<body>
<!-- BACKGROUND IMAGE -->
<div id="bg-overlay" class="bg-img"></div>

<!-- LOGIN FORM -->
<div class="cls-content">
    <div class="cls-content-sm panel">
        <div class="panel-body" style="padding-bottom: 0px;">
            <div class="mar-ver pad-btm text-center">
                <img src="{{ asset('img/laxmi-logo.png') }}" style="max-height: 95px;">
                <h3 class="h4 mar-no">{{ isset( $title ) ? "Laxmi HRIS" : Setting::get('siteconfig')['system_name'] }}</h3>
            </div>

            @if(Session::has('login_failed'))
                <div class="alert alert-danger">
                    <button class="close" data-dismiss="alert"><i class="pci-cross pci-circle"></i></button>
                    <strong>{{Session::get('login_failed')}}</strong>
                </div>
            @endif
            @if(Session::has('success_message'))
                <div class="alert alert-success">
                    <button class="close" data-dismiss="alert"><i class="pci-cross pci-circle"></i></button>
                    <strong>{{Session::get('success_message')}}</strong>
                </div>
            @endif

            <p class="text-center"><strong>Sign In to your account</strong></p>

            <form action="{{ route('admin.login') }}" method="post">
                {{ csrf_field() }}

                <div class="form-group {{{ $errors->first('username') ? 'has-error' : '' }}}">
                    @if($errors->first('username'))
                        <label class="control-label" style="float: left;">{{$errors->first('username')}}</label>
                    @endif

                    <input type="text" name="username" class="form-control" placeholder="Email / Username"
                           value="{{ isset($admin_rem_username) && $admin_rem_username !="" ? $admin_rem_username : old('username') }}" autocomplete="off">
                </div>

                <div class="form-group {{{ $errors->first('password') ? 'has-error' : '' }}}">
                    @if($errors->first('password'))
                        <label class="control-label" style="float: left;">{{$errors->first('password')}}</label>
                    @endif

                    <input type="password" name="password" class="form-control" placeholder="Password"
                           value="{{ isset($admin_rem_password) && $admin_rem_password !="" ? $admin_rem_password : old('password') }}" autocomplete="off">
                </div>

                <div class="checkbox pad-btm text-left">
                    <input id="demo-form-checkbox" class="magic-checkbox" type="checkbox" name="remember_me"
                           value="yes" {{ (isset($admin_rem_password) && $admin_rem_password !="") && (isset($admin_rem_username) && $admin_rem_username !="") ?'checked' :''}}>
                    <label for="demo-form-checkbox">Remember me</label>
                </div>

                <button class="btn btn-primary btn-block" type="submit" style="margin-bottom: 20px;">Login</button>
            </form>
        </div>
    </div>
</div>
</body>

</html>
