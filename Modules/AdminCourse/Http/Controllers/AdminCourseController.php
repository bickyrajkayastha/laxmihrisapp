<?php

namespace Modules\AdminCourse\Http\Controllers;

use App\Helpers\Permission;
use App\Models\Course;
use App\Models\CourseCategory;
use App\Models\Employee;
use App\Services\ImageUpload\Strategy\UploadWithAspectRatio;
use App\Traits\Slugger;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Intervention\Image\Facades\Image;
use Modules\AdminCourse\Services\CourseImageUploader;
use Illuminate\Support\Facades\Log;

class AdminCourseController extends Controller
{
    use Slugger;

    private $indexRoute;

    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->indexRoute = route('admin.courses.index');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $data['pageTitle'] = "Course Management";
        $data['courses'] = Course::orderBy('start_date', 'desc')->paginate(20);

        return view('admincourse::index', $data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $data['pageTitle'] = "Add Course";
        $data['categories'] = CourseCategory::where('status', 1)->get();
        $data['breadcrumbs'] = '<li><a href="' . route('admin.courses.index') . '">Course Management</a></li><li>Add</li>';
        return view('admincourse::create', $data);
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        $this->request->validate([
            'title' => 'required',
            'category_id' => 'nullable|exists:course_categories,id',
            'sub_title' => 'nullable',
            'description' => 'required',
            'image' => 'nullable|image|mimes:png,jpg,jpeg',
            'start_date' => 'required|date',
            'end_date' => 'nullable|date'
        ]);

        try {
            $data = $this->request->only('title', 'sub_title', 'category_id', 'description', 'status');
            $data['slug'] = $this->createSlug($this->request->title, Course::class);

            if ($this->request->start_date != "") {
                $data['start_date'] = date('y-m-d', strtotime($this->request->start_date));
            }

            if ($this->request->end_date != "") {
                $data['end_date'] = date('y-m-d', strtotime($this->request->end_date));
            }

            if ($this->request->hasFile('image')) {
                $image = $this->request->file('image');

                $uploader = new CourseImageUploader(new UploadWithAspectRatio());

                $data['image'] = $uploader->saveOriginalImage($image);

                $this->cropAndSaveImage(
                    $uploader,
                    $data['image'],
                    $this->request->x1,
                    $this->request->y1,
                    $this->request->w,
                    $this->request->h
                );
            }

            Course::create($data);

            session()->flash('success_message', __('alerts.create_success'));
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            session()->flash('error_message', __('alerts.create_error'));
        }

        return redirect()->to($this->indexRoute);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($course_id)
    {
        $data = [];
        $data['breadcrumbs'] = '<li><a href="' . route('admin.courses.index') . '">Test & Assessment Management</a></li><li>Detail</li>';
        $data['course'] = Course::find($course_id);
        return view('admincourse::show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $data['pageTitle'] = "Edit Course";
        $data['course'] = $this->getCourseById($id);
        $data['categories'] = CourseCategory::where('status', 1)->get();
        $data['breadcrumbs'] = '<li><a href="' . route('admin.courses.index') . '">Course Management</a></li><li>Edit</li>';

        return view('admincourse::edit', $data);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update($id)
    {
        $this->request->validate([
            'title' => 'required',
            'sub_title' => 'nullable',
            'category_id' => 'nullable|exists:course_categories,id',
            'description' => 'required',
            'image' => 'nullable|image|mimes:png,jpg,jpeg',
            'start_date' => 'required|date',
            'end_date' => 'nullable|date'
        ], [
            'end_time.after' => 'The end time must be greater than start time.'
        ]);

        try {
            $data = $this->request->only('title', 'description', 'category_id', 'status', 'location', 'sub_title');

            $data['slug'] = $this->createSlug($this->request->title, Course::class);

            if ($this->request->start_date != "") {
                $data['start_date'] = date('y-m-d', strtotime($this->request->start_date));
            }

            if ($this->request->end_date != "") {
                $data['end_date'] = date('y-m-d', strtotime($this->request->end_date));
            }

            $course = Course::find($id);

            if ($this->request->hasFile('image') || $this->request->croppreviousimage == 'true') {
                $uploader = new CourseImageUploader(new UploadWithAspectRatio());

                $uploader->deleteThumbImage($course->image);

                $uploader->deleteCroppedImage($course->image);

                if ($this->request->croppreviousimage == 'true') {
                    $data['image'] = $course->image;
                } else {
                    $uploader->deleteFullImage($course->image);

                    $data['image'] = $uploader->saveOriginalImage($this->request->file('image'));
                }

                $this->cropAndSaveImage(
                    $uploader,
                    $data['image'],
                    $this->request->x1,
                    $this->request->y1,
                    $this->request->w,
                    $this->request->h
                );
            }

            $course->update($data);

            session()->flash('success_message', __('alerts.update_success'));
        } catch (\Exception $e) {
            session()->flash('error_message', __('alerts.update_error'));
        }

        return redirect()->to($this->indexRoute);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        try {

            $course = $this->getCourseById($id);

            if ($course->status == 1) {
                session()->flash('error_message', __('alerts.delete_error_active'));
                return redirect()->to($this->indexRoute);
            }

            $uploader = new CourseImageUploader();

            $uploader->deleteFullImage($course->image);

            $uploader->deleteThumbImage($course->image);

            $uploader->deleteCroppedImage($course->image);

            $course->delete();

            session()->flash('success_message', __('alerts.delete_success'));
        } catch (\Exception $e) {
            session()->flash('error_message', __('alerts.delete_error'));
        }

        return redirect()->to($this->indexRoute);
    }

    private function cropAndSaveImage($uploader, $filename, $posX1, $posY1, $width, $height)
    {
        $imgPath = $uploader->getFullImagePath($filename);

        $fullImage = Image::make($imgPath);

        $cropDestPath = $uploader->getCroppedImagePath($filename);

        $uploader->cropAndSaveImage($fullImage, $cropDestPath, $posX1, $posY1, $width, $height);

        $uploader->cropAndSaveImageThumb($fullImage, $filename);
    }

    private function getCourseById($id)
    {
        return Course::find($id);
    }

    public function candidates($course_id)
    {
        $data = [];
        $data['course'] = Course::find($course_id);
        $data['candidates'] = $data['course']->employees->map(function($employee) use ($course_id) {
            $employee->points = $employee->courses()->wherePivot('course_id', $course_id)->first()->pivot->assessment_points;
            return $employee;
        });
        $candidate_ids = $data['candidates']->pluck('id')->toArray();
        $data['employees'] = Employee::whereNotIn('id', $candidate_ids)->get();
        $data['breadcrumbs'] = '<li><a href="' . route('admin.courses.index') . '">Course Management</a></li><li><a href="' . route('admin.courses.show', $data['course']->id) . '">'.$data['course']->title.'</a></li><li>Candidates</li>';

        return view('admincourse::candidates', $data);
    }

    public function addCandidates(Request $request)
    {
        $request->validate([
            'course_id' => 'required',
            'candidates' => 'required'
        ]);

        try {
            // save to course_employee table
            $course = Course::find($request->course_id);
            $course->employees()->attach($request->candidates);
            session()->flash('success_message', __('alerts.create_success'));
        } catch (\Throwable $th) {
            session()->flash('error_message', __('alerts.create_error'));
        }

        return redirect()->back();
    }

    public function removeCandidate(Request $request, $id)
    {
        $request->validate([
            'course_id' => 'required'
        ]);

        try {
            // remove from course_employee table
            $course = Course::find($request->course_id);
            $course->employees()->detach($id);
            session()->flash('success_message', __('alerts.delete_success'));
        } catch (\Throwable $th) {
            session()->flash('error_message', __('alerts.delete_error'));
        }

        return redirect()->back();
    }

    public function assessment($course_id)
    {
        $data = [];
        $data['breadcrumbs'] = '<li><a href="' . route('admin.courses.index') . '">Test & Assessment Management</a></li><li>Detail</li>';
        $data['course'] = Course::find($course_id);
        return view('admincourse::assessments', $data);
    }

    public function updateTime()
    {
        $this->request->validate([
            'course_id' => 'required',
            'time_limit' => 'required'
        ]);

        $course = Course::find($this->request->course_id);
        $course->time_limit = $this->request->time_limit;

        $course->save();
        session()->flash('success_message', __('alerts.update_success'));
        return redirect()->back();
    }

    public function employeeCourseAssessment($employee_id, $course_id)
    {
        $data['employee'] = Employee::find($employee_id);
        $data['course'] = Course::find($course_id);
        $data['points'] = $data['employee']->courses()->wherePivot('course_id', $course_id)->first()->pivot->assessment_points;
        $data['breadcrumbs'] = '<li><a href="' . route('admin.courses.index') . '">Course Management</a></li><li><a href="' . route('admin.courses.show', $data['course']->id) . '">' . $data['course']->title . '</a></li><li>Candidates</li>';
        $data['course_assessment'] = $data['course']->assessments()->with([
            'options' => function($q) use ($employee_id) {
                $q->with([
                    'employees' => function($q) use ($employee_id) {
                        $q->select('employees.id')->where('employee_id', $employee_id);
                    }
                ]);
            }
        ])->get();

        return view('admincourse::candidate_assessment', $data);
    }
}
