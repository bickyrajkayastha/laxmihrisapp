@extends('admin.layouts.master')

@push('css')
    <link rel="stylesheet" href="{{ asset('assets/plugins/jcrop/jquery.Jcrop.min.css') }}">
@endpush
@section('content')

<div class="row">
    <div class="col">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Add Team
                    <button class="btn btn-sm btn-primary pull-right panel-btn" onclick="window.history.go(-1);">
                        <i class="fas fa-long-arrow-alt-left"></i> Back
                    </button>
                </h3>
            </div>
            <!--Hover Rows-->
            <!--===================================================-->
            <form class="form-horizontal" action="{{ route('admin.teams.store') }}" method="POST"
                  enctype="multipart/form-data">
                @csrf
                <div class="panel-body">
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label class="col-sm-2 control-label" for="">Name</label>
                        <div class="col-sm-9">
                            <input type="text" placeholder="Name" name="name" value="{{ old('name') }}" id="input-name"
                                   class="form-control" >
                            @if($errors->has('name'))
                            <small class="help-block text-danger">{{ $errors->first('name')}}</small>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label class="col-sm-2 control-label" for="">Email</label>
                        <div class="col-sm-9">
                            <input type="email" placeholder="Email" name="email" value="{{ old('email') }}" id="input-name"
                                   class="form-control" >
                            @if($errors->has('email'))
                            <small class="help-block text-danger">{{ $errors->first('email')}}</small>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                        <label class="col-sm-2 control-label" for="">Phone no.</label>
                        <div class="col-sm-9">
                            <input type="text" placeholder="Phone" name="phone" value="{{ old('phone') }}" id="input-name"
                                   class="form-control" >
                            @if($errors->has('phone'))
                            <small class="help-block text-danger">{{ $errors->first('phone')}}</small>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('position') ? ' has-error' : '' }}">
                        <label class="col-sm-2 control-label" for="">Position</label>
                        <div class="col-sm-9">
                            <input type="text" placeholder="Position" name="position" value="{{ old('position') }}" id="input-position"
                                   class="form-control" >
                            @if($errors->has('position'))
                            <small class="help-block text-danger">{{ $errors->first('position')}}</small>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="demo-hor-inputemail">Type</label>
                        <div class="col-sm-9">
                            <select name="team_member_type" id="input-type" class="form-control" id="">
                                <option value="1">Ceo</option>
                                <option value="2">Manager</option>
                                <option value="3">Staff</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="demo-hor-inputemail">Image</label>
                        <div class="col-md-10">
                            <small class="help-block text-danger">{{ $errors->first('image')}}</small>
                            <img id="preview" class="mar-btm"><br>

                            <span class="btn btn-rose btn-primary btn-file">
                                <span>Browse</span>
                                <input type="file" name="image" id="input-image-file" onchange="fileSelectHandler()"
                                   />
                            </span>
                            <div class="image-error" style="color:red"></div>
                            <input type="hidden" id="x1" name="x1"/>
                            <input type="hidden" id="y1" name="y1"/>
                            <input type="hidden" id="x2" name="x2"/>
                            <input type="hidden" id="y2" name="y2"/>
                            <input type="hidden" id="w" name="w"/>
                            <input type="hidden" id="h" name="h"/>
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                        <label class="col-md-2 control-label">Status</label>
                        <div class="col-md-9">
                            <div class="radio">

                                <!-- Inline radio buttons -->
                                <input id="input-status" class="magic-radio" type="radio" name="status" value="1" <?= ((old('status') == 1)? 'checked':''); ?>>
                                <label for="input-status">Yes</label>

                                <input id="input-status-2" class="magic-radio" value="0" type="radio" name="status" <?= ((old('status') == 0)? 'checked':''); ?>>
                                <label for="input-status-2">No</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="demo-contact-input">Description</label>
                        <div class="col-md-10">
                            <textarea name="description" id="input-description" cols="30" rows="10" class="form-control ckeditor">{{ old('description') }}</textarea>
                        </div>
                    </div>
                    <!-- <div class="form-group">
                        <label class="col-md-3 control-label" for="demo-contact-input">Status</label>
                        <div class="col-md-10">
                            <div class="radio">
                                <input id="status" class="magic-radio" type="radio" name="status" value="1"
                                       checked>
                                <label for="status" style="color:#000000;">YES</label>
                                <input id="status-2" class="magic-radio" type="radio" name="status"
                                       value="0">
                                <label for="status-2" style="color:#000000;">NO</label>
                            </div>
                        </div>
                    </div> -->
                    <div class="form-group">
                        <div class="col-lg-2 col-sm-12 col-xs-12 pull-right">
                            <button class="btn btn-block btn-success" type="submit">Create</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
@push('scripts')
<script src="{{ asset('assets/plugins/jcrop/jquery.Jcrop.min.js') }}"></script>
<script>
    var aspRatio = 3/2;
    // convert bytes into friendly format
    function bytesToSize(bytes) {
        var sizes = ['Bytes', 'KB', 'MB'];
        if (bytes === 0) return 'n/a';
        var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
        return (bytes / Math.pow(1024, i)).toFixed(1) + ' ' + sizes[i];
    }

    // check for selected crop region
    function checkForm() {
        if (parseInt($('#w').val())) return true;
        $('.image-error').html('Please select a crop region and then press Upload').show();
        return false;
    }

    // update info by cropping (onChange and onSelect events handler)
    function updateInfo(e) {
        $('#x1').val(e.x);
        $('#y1').val(e.y);
        $('#x2').val(e.x2);
        $('#y2').val(e.y2);
        $('#w').val(e.w);
        $('#h').val(e.h);
    }

    // clear info by cropping (onRelease event handler)
    function clearInfo(e) {
        $('#x1').val(e.x);
        $('#y1').val(e.y);
        $('#x2').val(e.x2);
        $('#y2').val(e.y2);
        $('#w').val(300);
        $('#h').val(200);
    }
    // Create variables (in this scope) to hold the Jcrop API and image size
    var jCropApi, boundX, boundY;

    function fileSelectHandler() {
        // get selected file
        var oFile = $('#input-image-file')[0].files[0];

        if (!$('#input-image-file')[0].files[0]) {
            $('.jcrop-holder').remove();
            return;
        }
        // hide all errors
        $('.image-error').hide();
        // check for image type (jpg and png are allowed)
        var rFilter = /^(image\/jpeg|image\/png|image\/jpg|image\/gif|image\/xcf|image\/svg)$/i;
        if (!rFilter.test(oFile.type)) {
            $('#submit').prop("disabled", "disabled");
            $('.image-error').html('Please select a valid image file (jpg and png are allowed)').show();
            return;
        } else {
            $('#submit').prop("disabled", false);
        }
        // preview element
        var oImage = document.getElementById('preview');
        // prepare HTML5 FileReader
        var oReader = new FileReader();
        oReader.onload = function (e) {
            // e.target.result contains the DataURL which we can use as a source of the image
            oImage.src = e.target.result;
            oImage.onload = function () { // onload event handler
                var height = oImage.naturalHeight;

                var width = oImage.naturalWidth;

                // console.log(height);
                // console.log(width);
                window.URL.revokeObjectURL(oImage.src);

                if (height < 200 || width < 300) {

                    oImage.src = "";
                    $('#input-image-file').val('');
                    // $('#submit').prop("disabled","disabled");

                    $('.image-error').html('You have selected too small file, please select a one image with minimum size 300 X 200 px').show();

                } else {

                    $('#submit').prop("disabled", false);

                }
                var sResultFileSize = bytesToSize(oFile.size);

                // destroy Jcrop if it is existed
                if (typeof jCropApi !== 'undefined') {
                    jCropApi.destroy();
                    jCropApi = null;
                    $('#preview').width(oImage.naturalWidth);
                    $('#preview').height(oImage.naturalHeight);
                }
                setTimeout(function () {
                    // initialize Jcrop
                    $('#preview').Jcrop({
                        setSelect: [0, 0, 300, 200],
                        boxWidth: 300,
                        // boxHeight: 300,
                        minSize: [300, 200], // min crop size
                        aspectRatio: aspRatio,
                        bgFade: true, // use fade effect
                        bgOpacity: .3, // fade opacity
                        onChange: updateInfo,
                        onSelect: updateInfo,
                        onRelease: clearInfo,
                        trueSize: [oImage.naturalWidth, oImage.naturalHeight]
                    }, function () {
                        // use the Jcrop API to get the real image size
                        var bounds = this.getBounds();
                        boundx = bounds[0];
                        boundy = bounds[1];
                        // Store the Jcrop API in the jCropApi variable
                        jCropApi = this;
                    });
                }, 500);
            };
        };
        // read selected file as DataURL
        oReader.readAsDataURL(oFile);
    }

</script>
@endpush
