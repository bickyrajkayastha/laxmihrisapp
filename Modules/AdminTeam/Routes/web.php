<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin/teams', 'as' => 'admin.', 'middleware' => 'auth:admin'], function() {
    Route::get('/', 'AdminTeamController@index')->name('teams.index')->middleware('can:view-teams');
    Route::get('/create', 'AdminTeamController@create')->name('teams.create')->middleware('can:create-teams');
    Route::post('/create', 'AdminTeamController@store')->name('teams.store')->middleware('can:create-teams');
    Route::get('/{id}/edit', 'AdminTeamController@edit')->name('teams.edit')->middleware('can:edit-teams');
    Route::post('/{id}/edit', 'AdminTeamController@update')->name('teams.update')->middleware('can:edit-teams');
    Route::post('/{id}/delete', 'AdminTeamController@destroy')->name('teams.destroy')->middleware('can:delete-teams');
});