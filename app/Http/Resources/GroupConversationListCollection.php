<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Carbon;
use Auth;

class GroupConversationListCollection extends ResourceCollection
{
    public $success = true;
    public $message = "";

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);

        if (iterator_count($this->resource)) {
            $this->message = "Conversation list.";
            return $this->map(function($employee_message) {
                $message = $employee_message->message;
                $sender = $employee_message->sender;
                return [
                    'id' => $message->id,
                    'sender_id' => $sender->id,
                    'receiver_id' => $employee_message->receiver_id,
                    'sender_name' => $sender->name,
                    'sender_image' => $sender->thumbImgPath,
                    'in' => ($employee_message->sender_id == auth()->user()->id)? false: true,
                    'type' => $message->type,
                    'content' => $message->message,
                    'message_date' => Carbon\Carbon::parse($message->created_at)->diffForHumans(),
                    'file_size' => formatSizeUnits($message->file_size),
                    'file_name' => $message->file_original_name,
                    'file_url' => $message->filePath
                ];
            });
        } else {
            $this->message = "No conversation started yet.";
        }
    }

    public function with($request)
    {
        return [
            'success' => $this->success,
            'message' => $this->message,
            'item_type' => "1 = text, 2 = file, 3 = image"
        ];
    }
}
