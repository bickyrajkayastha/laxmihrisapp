<?php

namespace App\Exceptions;

use Exception;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\Log;

class Handler extends ExceptionHandler
{
    protected $validation_exception = false;

    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($request->wantsJson()) {   //add Accept: application/json in request
            return $this->handleApiException($request, $exception);
        } else {
            if($exception instanceof \Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException){
                return abort('404');
            }
            $retval = parent::render($request, $exception);
        }
        
        return $retval;
    }

    private function handleApiException($request, Exception $exception)
    {
        // jwt exception
        if ($exception instanceof UnauthorizedHttpException) {
            $preException = $exception->getPrevious();
            if ($preException instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException) {
                return response()->json(['error' => 'TOKEN_EXPIRED']);
            } else if ($preException instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException) {
                return response()->json(['error' => 'TOKEN_INVALID']);
            } else if ($preException instanceof \Tymon\JWTAuth\Exceptions\TokenBlacklistedException) {
                return response()->json(['error' => 'TOKEN_BLACKLISTED']);
            }
            if ($exception->getMessage() === 'Token not provided') {
               return response()->json(['error' => 'Token not provided']);
            }
        }

        // other exception
        $exception = $this->prepareException($exception);
        if ($exception instanceof \App\Exceptions\customApiException) {
            return response()->json([
                'data' => [],
                'message' => $exception->getMessage(),
                'success' => false
            ], $exception->getStatusCode());
        }
        if ($exception instanceof \Illuminate\Http\Exception\HttpResponseException) {
            $exception = $exception->getResponse();
        }
        if ($exception instanceof \Illuminate\Auth\AuthenticationException) {
            $exception = $this->unauthenticated($request, $exception);
        }
        if ($exception instanceof \Illuminate\Validation\ValidationException) {
            $this->validation_exception = true;
            $exception = $this->convertValidationExceptionToResponse($exception, $request);
        }
        if ($exception instanceof \Illuminate\Database\QueryException) {
            if (config('app.debug')) {
                $response['message'] = "Database error";
                $response['error'] = $exception->errorInfo;
                $response['data'] = [];
                $statusCode = 422;
                $response['success'] = false;
                return response()->json($response, $statusCode);
            }
        }
        return $this->customApiResponse($exception);
    }

    private function customApiResponse($exception)
    {
        if (method_exists($exception, 'getStatusCode')) {
            $statusCode = $exception->getStatusCode();
        } else {
            $statusCode = 500;
        }
        $response = [];
        switch ($statusCode) {
            case 401:
                $response['message'] = 'Unauthorized';
                break;
            case 403:
                $response['message'] = 'Forbidden';
                break;
            case 404:
                $response['message'] = 'Not Found';
                break;
            case 405:
                $response['message'] = 'Method Not Allowed';
                break;
            case 422:
                $response['message'] = $exception->original['message'];
                if ($this->validation_exception) {
                    $error_message = "";
                    foreach ($exception->original['errors'] as $key => $value) {
                        $error_message .= " " . implode(" ", $value);
                    }
                    $response['message'] = $error_message;
                }
                // $response['errors'] = $exception->original['errors'];
                break;
            default:
                $response['message'] = ($statusCode == 500) ? 'Whoops, looks like something went wrong' : $exception->getMessage();
                break;
        }
        if (config('app.debug')) {
            // $response['trace'] = $exception->getTrace();
            // $response['code'] = $exception->getCode();
        }
        $response['success'] = false;
        $response['data'] = [];
        return response()->json($response, $statusCode);
    }
}
