<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\ApiResource;
use App\Models\Employee;
use App\Models\EmployeePersonalDetail;
use App\Models\EmployeePermanentAddressDetail;
use Illuminate\Support\Facades\Validator;
use App\Models\EmployeeTemporaryAddressDetail;
use App\Models\EmployeeFamilyDetail;
use App\Models\EmployeeEducation;
use App\Models\EmployeeChildren;
use App\Models\EmployeeEmergencyContact;
use App\Models\EmployeeRelativeOnBank;
use Illuminate\Support\Facades\Log;
use App\Http\Resources\EmployeeResource;
use App\Http\Resources\EmployeeDirectoryResource;
use Exception;

class EmployeeController extends Controller
{
    /**
    * @SWG\Post(
    *     path="/employee/save",
    *     description="save employee detail.",
    *     tags={"Employee Apis"},
    *     security={{"bearerAuth":{}}},
    *     @SWG\Parameter(
    *         in="body",
    *         name="employee details",
    *         description="save employee details",
    *         required=true,
    *         @SWG\Schema(
    *             collectionFormat="multi",
    *             type="object",
     *             @SWG\Property(
     *                   property="personal_details",
     *                   type="object",
     *                   required={"full_name", "gender", "dob", "nationality_at_birth", "present_nationality", "citizenship_no", "citizenship_issuing_office"},
     *                   @SWG\Property(
     *		                 property="full_name",
     *		                 type="string",
     *                   ),
     *                   @SWG\Property(
     *		                 property="gender",
     *		                 type="number",
     *                   ),
     *                   @SWG\Property(
     *		                 property="dob",
     *		                 type="string",
     *		                 format="date",
     *                   ),
     *                   @SWG\Property(
     *		                 property="nationality_at_birth",
     *		                 type="string",
     *                   ),
     *                   @SWG\Property(
     *		                 property="present_nationality",
     *		                 type="string",
     *                   ),
     *                   @SWG\Property(
     *		                 property="citizenship_no",
     *		                 type="string",
     *                   ),
     *                   @SWG\Property(
     *		                 property="citizenship_issuing_office",
     *		                 type="string",
     *                   ),
     *                   @SWG\Property(
     *		                 property="driving_license_no",
     *		                 type="string",
     *                   ),
     *              ),
     *             @SWG\Property(
     *                   property="permanent_address_details",
     *                   type="object",
     *                   required={"street_name", "municipality", "district", "zone"},
     *                   @SWG\Property(
     *		                 property="house_no",
     *		                 type="string",
     *                   ),
     *                   @SWG\Property(
     *		                 property="street_name",
     *		                 type="string",
     *                   ),
     *                   @SWG\Property(
     *		                 property="municipality",
     *		                 type="string",
     *                   ),
     *                   @SWG\Property(
     *		                 property="ward_no",
     *		                 type="number",
     *                   ),
     *                   @SWG\Property(
     *		                 property="district",
     *		                 type="string",
     *                   ),
     *                   @SWG\Property(
     *		                 property="zone",
     *		                 type="string",
     *                   ),
     *                   @SWG\Property(
     *		                 property="same_as_permanent",
     *		                 type="boolean",
     *                   ),
     *              ),
     *             @SWG\Property(
     *                   property="temporary_address_details",
     *                   type="object",
     *                   @SWG\Property(
     *		                 property="house_no",
     *		                 type="string",
     *                   ),
     *                   @SWG\Property(
     *		                 property="street_name",
     *		                 type="string",
     *                   ),
     *                   @SWG\Property(
     *		                 property="municipality",
     *		                 type="string",
     *                   ),
     *                   @SWG\Property(
     *		                 property="ward_no",
     *		                 type="number",
     *                   ),
     *                   @SWG\Property(
     *		                 property="district",
     *		                 type="string",
     *                   ),
     *                   @SWG\Property(
     *		                 property="zone",
     *		                 type="string",
     *                   ),
     *              ),
     *             @SWG\Property(
     *                   property="family_details",
     *                   type="object",
     *                   required={"father_name", "mother_name", "grand_father_name", "grand_mother_name"},
     *                   @SWG\Property(
     *		                 property="father_name",
     *		                 type="string",
     *                   ),
     *                   @SWG\Property(
     *		                 property="father_occupation",
     *		                 type="string",
     *                   ),
     *                   @SWG\Property(
     *		                 property="mother_name",
     *		                 type="string",
     *                   ),
     *                   @SWG\Property(
     *		                 property="mother_occupation",
     *		                 type="string",
     *                   ),
     *                   @SWG\Property(
     *		                 property="grand_father_name",
     *		                 type="string",
     *                   ),
     *                   @SWG\Property(
     *		                 property="grand_mother_name",
     *		                 type="string",
     *                   ),
     *                   @SWG\Property(
     *		                 property="spouse_name",
     *		                 type="string",
     *                   ),
     *                   @SWG\Property(
     *		                 property="spouse_occupation",
     *		                 type="string",
     *                   ),
     *                   @SWG\Property(
     *		                 property="spouse_dob",
     *		                 type="string",
     *     	                 format="date",
     *                   ),
     *                   @SWG\Property(
     *		                 property="wedding_anniversary",
     *		                 type="string",
     *     	                 format="date",
     *                   ),
     *                   @SWG\Property(
     *		                 property="children",
     *		                 type="boolean",
     *                   ),
     *              ),
     *             @SWG\Property(
     *                   property="employee_children",
     *                   type="array",
     *                   @SWG\Items(
     *		                 type="object",
     *                       @SWG\Property(
     *     	                   property="full_name",
     *     	                   type="string",
     *                       ),
     *                       @SWG\Property(
     *     	                   property="dob",
     *     	                   type="string",
     *     	                   format="date",
     *                       ),
     *                       @SWG\Property(
     *     	                   property="relationship",
     *     	                   type="string",
     *                       ),
     *                   ),
     *              ),
     *             @SWG\Property(
     *                   property="employee_educations",
     *                   type="array",
     *                   @SWG\Items(
     *		                 type="object",
     *                       @SWG\Property(
     *     	                   property="academic_level",
     *     	                   type="string",
     *                       ),
     *                       @SWG\Property(
     *     	                   property="institute",
     *     	                   type="string",
     *                       ),
     *                       @SWG\Property(
     *     	                   property="address",
     *     	                   type="string",
     *                       ),
     *                       @SWG\Property(
     *     	                   property="graduating_year",
     *     	                   type="string",
     *     	                   format="date",
     *                       ),
     *                       @SWG\Property(
     *     	                   property="major_subject",
     *     	                   type="string",
     *                       ),
     *                   ),
     *              ),
     *             @SWG\Property(
     *                   property="employee_work_experiences",
     *                   type="array",
     *                   @SWG\Items(
     *		                 type="object",
     *                       @SWG\Property(
     *     	                   property="from",
     *     	                   type="string",
     *     	                   format="date",
     *                       ),
     *                       @SWG\Property(
     *     	                   property="to",
     *     	                   type="string",
     *     	                   format="date",
     *                       ),
     *                       @SWG\Property(
     *     	                   property="post_title",
     *     	                   type="string",
     *                       ),
     *                       @SWG\Property(
     *     	                   property="employer_name",
     *     	                   type="string",
     *                       ),
     *                       @SWG\Property(
     *     	                   property="supervisor_name",
     *     	                   type="string",
     *                       ),
     *                       @SWG\Property(
     *     	                   property="reason_of_leaving",
     *     	                   type="string",
     *                       ),
     *                   ),
     *              ),
     *             @SWG\Property(
     *                   property="employee_emergency_contact",
     *                   type="object",
     *                   required={"contact_name", "address", "phone", "relationship"},
     *                   @SWG\Property(
     *		                 property="contact_name",
     *		                 type="string",
     *                   ),
     *                   @SWG\Property(
     *		                 property="address",
     *		                 type="string",
     *                   ),
     *                   @SWG\Property(
     *		                 property="phone",
     *		                 type="string",
     *                   ),
     *                   @SWG\Property(
     *		                 property="relationship",
     *		                 type="string",
     *                   )
     *              ),
     *             @SWG\Property(
     *                   property="employee_other_information",
     *                   type="object",
     *                   @SWG\Property(
     *                          property="relative_on_bank",
     *                          type="boolean",
     *                   ),
     *                   @SWG\Property(
     *                          property="family_member_on_laxmi_bank",
     *                          type="boolean",
     *                   )
     *              ),
     *             @SWG\Property(
     *                   property="employee_relative_on_banks",
     *                   type="array",
     *                   @SWG\Items(
     *		                 type="object",
     *                       @SWG\Property(
     *     	                   property="full_name",
     *     	                   type="string",
     *                       ),
     *                       @SWG\Property(
     *     	                   property="relationship",
     *     	                   type="string",
     *                       ),
     *                       @SWG\Property(
     *     	                   property="organisation",
     *     	                   type="string",
     *                       ),
     *                   ),
     *              ),
     *             @SWG\Property(
     *                   property="employee_family_loans",
     *                   type="array",
     *                   @SWG\Items(
     *		                 type="object",
     *                       @SWG\Property(
     *     	                   property="full_name",
     *     	                   type="string",
     *                       ),
     *                       @SWG\Property(
     *     	                   property="relationship",
     *     	                   type="string",
     *                       ),
     *                       @SWG\Property(
     *     	                   property="loan_type",
     *     	                   type="string",
     *                       ),
     *                   ),
     *              ),
    *         )
    *     ),
    *     @SWG\Response(
    *         response=200,
    *         description="status, message",
    *     ),
    *     @SWG\Response(
    *         response=422,
    *         description="status, message"
    *     )
    * )
    */
    public function saveEmployeeDetail(Request $request)
    {
        $success = false;
        $message = "";
        $status_code = 404;

        // personal detail
        if (isset($request->personal_details) && !empty($request->personal_details)) {
            $request->validate([
                'personal_details.full_name' => 'required',
                'personal_details.gender' => 'required|numeric',
                'personal_details.dob' => 'required|date',
                'personal_details.nationality_at_birth' => 'required',
                'personal_details.present_nationality' => 'required',
                'personal_details.citizenship_no' => 'required',
                'personal_details.citizenship_issuing_office' => 'required',
                   'personal_details.driving_license_no' => 'nullable'
            ]);

            if ($this->savePersonalDetail($request->personal_details)) {
                $success = true;
            }
        }

        // permanent address
        if (isset($request->permanent_address_details) && !empty($request->permanent_address_details)) {
            $request->validate([
                'permanent_address_details.house_no' => 'nullable',
                'permanent_address_details.street_name' => 'required',
                'permanent_address_details.municipality' => 'required',
                'permanent_address_details.ward_no' => 'numeric|nullable',
                'permanent_address_details.district' => 'required',
                'permanent_address_details.zone' => 'required',
                'permanent_address_details.same_as_permanent' => 'boolean|nullable'
            ]);

            if ($this->saveEmployeePermanentAddressDetail($request->permanent_address_details)) {
                $success = true;
            }
        }

        // check if the same as permanent address is true.
        // if true fill the data to temporary address as well.
        if ($request->permanent_address_details['same_as_permanent']) {
            if ($this->saveEmployeeTemporaryAddressDetail($request->permanent_address_details)) {
                $success = true;
            }
        } else {

             // temporary address
            if (isset($request->temporary_address_details) && !empty($request->temporary_address_details)) {
                $request->validate([
                     'temporary_address_details.house_no' => 'nullable',
                     'temporary_address_details.street_name' => 'required',
                     'temporary_address_details.municipality' => 'nullable',
                     'temporary_address_details.ward_no' => 'numeric|nullable',
                     'temporary_address_details.district' => 'nullable',
                     'temporary_address_details.zone' => 'nullable',
                 ]);

                if ($this->saveEmployeeTemporaryAddressDetail($request->temporary_address_details)) {
                    $success = true;
                }
            }
        }

        // family details
        if (isset($request->family_details) && !empty($request->family_details)) {
            $request->validate([
                'family_details.father_name' => 'required',
                // 'family_details.father_occupation' => 'required',
                'family_details.mother_name' => 'required',
                // 'family_details.mother_occupation' => 'numeric',
                'family_details.grand_father_name' => 'required',
                'family_details.grand_mother_name' => 'nullable',
                // 'family_details.spouse_name' => 'numeric',
                // 'family_details.spouse_occupation' => 'numeric',
                'family_details.spouse_dob' => 'date|nullable',
                'family_details.wedding_anniversary' => 'date|nullable',
                'family_details.children' => 'boolean|nullable'
            ]);

            if ($this->saveEmployeeFamilyDetail($request->family_details)) {
                $success = true;
            }
        }

        // employee children
        if (isset($request->employee_children) && !empty($request->employee_children)) {
            $request->validate([
                'employee_children.*.full_name' => 'required',
                'employee_children.*.dob' => 'required|date',
                'employee_children.*.relationship' => 'required',
            ]);

            if ($this->saveEmployeeChildren($request->employee_children)) {
                $success = true;
            }
        }

        // employee educations
        $employee_educations = $request->employee_educations;
        if (isset($employee_educations) && !empty($employee_educations)) {
            $is_valid_exp = false;
            foreach ($employee_educations as $key => $exp) {
                foreach ($exp as $k => $value) {
                    if (!is_null($value)) {
                        $is_valid_exp = true;
                        break 1;
                    }
                }

                if (!$is_valid_exp) {
                    unset($employee_educations[$key]);
                }
            }

            $validator = Validator::make($employee_educations, [
                '*.academic_level' => 'nullable',
                '*.institute' => 'nullable',
                '*.address' => 'nullable',
                '*.graduating_year' => 'date|nullable',
                '*.major_subject' => 'nullable'
            ]);

            if ($validator->fails()) {
                return response()->json([
                         'data' => [],
                         'message' => $validator->messages(),
                         'success' => false,
                    ], 422);
            }

            if ($this->saveEmployeeEducations($employee_educations)) {
                $success = true;
            }
        }

        // employee work experiences
        $work_experiences = $request->employee_work_experiences;
        if (isset($work_experiences) && !empty($work_experiences)) {
            $is_valid_exp = false;
            foreach ($work_experiences as $key => $exp) {
                foreach ($exp as $k => $value) {
                    if (!is_null($value)) {
                        $is_valid_exp = true;
                        break 1;
                    }
                }

                if (!$is_valid_exp) {
                    unset($work_experiences[$key]);
                }
            }

            $validator = Validator::make($work_experiences, [
                '*.from' => 'nullable|date',
                '*.to' => 'date|nullable',
                '*.post_title' => 'nullable',
                '*.employer_name' => 'nullable',
                '*.supervisor_name' => 'nullable',
                '*.reason_of_leaving' => 'nullable'
            ]);

            if ($validator->fails()) {
                return response()->json([
                         'data' => [],
                         'message' => $validator->messages(),
                         'success' => false,
                    ], 422);
            }

            if (!$validator->fails() && (isset($work_experiences) && !empty($work_experiences))) {
                if ($this->saveEmployeeWorkExperiences($work_experiences)) {
                    $success = true;
                }
            }
        }

        // emergency contact
        if (isset($request->employee_emergency_contact) && !empty($request->employee_emergency_contact)) {
            $request->validate([
                'employee_emergency_contact.contact_name' => 'required',
                'employee_emergency_contact.address' => 'required',
                'employee_emergency_contact.phone' => 'required',
                'employee_emergency_contact.relationship' => 'required'
            ]);

            if ($this->saveEmployeeEmergencyContact($request->employee_emergency_contact)) {
                $success = true;
            }
        }

        // emergency contact
        if (isset($request->employee_other_information) && !empty($request->employee_other_information)) {
            $request->validate([
                   'employee_other_information.relative_on_bank' => 'boolean',
                   'employee_other_information.family_member_on_laxmi_bank' => 'boolean',
               ]);

            $employee_id = auth()->user()->id;
            $employee_other_information = \App\Models\EmployeeOtherInformation::where('employee_id', '=', $employee_id)->first();

            if (!$employee_other_information) {
                $employee_other_information = new \App\Models\EmployeeOtherInformation;
                $employee_other_information->employee_id = $employee_id;
            }

            $employee_other_information->same_as_permanent_address = $request->permanent_address_details['same_as_permanent'];
            $employee_other_information->have_children = $request->family_details['children'];
            $employee_other_information->relative_on_bank = $request->employee_other_information['relative_on_bank'];
            $employee_other_information->family_member_on_laxmi_bank = $request->employee_other_information['family_member_on_laxmi_bank'];

            if ($employee_other_information->save()) {
                $success = true;
            }
        }

        // emplyee relative on bank
        if (isset($request->employee_relative_on_banks) && !empty($request->employee_relative_on_banks)) {
            $request->validate([
                'employee_relative_on_banks.*.full_name' => 'required',
                'employee_relative_on_banks.*.relationship' => 'required',
                'employee_relative_on_banks.*.organisation' => 'required',
            ]);

            if ($this->saveEmployeeRelativeOnBanks($request->employee_relative_on_banks)) {
                $success = true;
            }
        }

        // emplyee family loans
        if (isset($request->employee_family_loans) && !empty($request->employee_family_loans)) {
            $request->validate([
                'employee_family_loans.*.full_name' => 'required',
                'employee_family_loans.*.relationship' => 'required',
                'employee_family_loans.*.loan_type' => 'required',
            ]);

            if ($this->saveEmployeeFamilyLaons($request->employee_family_loans)) {
                $success = true;
            }
        }

        if ($success) {
            $message = "Your detail has been saved.";
        }

        return response()->json([
            'data' => [],
            'success' => $success,
            'message' => $message
        ]);
    }

    public function savePersonalDetail($data)
    {
        $employee_id = auth()->user()->id;

        $employee_detail = EmployeePersonalDetail::where('employee_id', '=', $employee_id)->first();

        if (!$employee_detail) {
            $employee_detail = new EmployeePersonalDetail;
            $employee_detail->employee_id = $employee_id;
        }

        $employee_detail->fill($data);

        if ($employee_detail->save()) {
            return 1;
        }

        return 0;
    }

    public function saveEmployeePermanentAddressDetail($data)
    {
        $employee_id = auth()->user()->id;

        $employee_permanent_address_detail = EmployeePermanentAddressDetail::where('employee_id', '=', $employee_id)->first();

        if (!$employee_permanent_address_detail) {
            $employee_permanent_address_detail = new EmployeePermanentAddressDetail;
            $employee_permanent_address_detail->employee_id = $employee_id;
        }

        $employee_permanent_address_detail->fill($data);

        if ($employee_permanent_address_detail->save()) {
            return 1;
        }

        return 0;
    }

    public function saveEmployeeTemporaryAddressDetail($data)
    {
        $employee_id = auth()->user()->id;

        $employee_temporary_address_detail = EmployeeTemporaryAddressDetail::where('employee_id', '=', $employee_id)->first();

        if (!$employee_temporary_address_detail) {
            $employee_temporary_address_detail = new EmployeeTemporaryAddressDetail;
            $employee_temporary_address_detail->employee_id = $employee_id;
        }

        $employee_temporary_address_detail->fill($data);

        if ($employee_temporary_address_detail->save()) {
            return 1;
        }

        return 0;
    }

    public function saveEmployeeFamilyDetail($data)
    {
        $employee_id = auth()->user()->id;

        $employee_family_detail = EmployeeFamilyDetail::where('employee_id', '=', $employee_id)->first();

        if (!$employee_family_detail) {
            $employee_family_detail = new EmployeeFamilyDetail;
            $employee_family_detail->employee_id = $employee_id;
        }

        $employee_family_detail->fill($data);

        if ($employee_family_detail->save()) {
            return 1;
        }

        return 0;
    }

    public function saveEmployeeEducations($data)
    {
        $employee = auth()->user();

        $education = $employee->employee_educations()->delete();
        $education = $employee->employee_educations()->createMany($data);

        if ($education) {
            return 1;
        }

        return 0;
    }

    public function saveEmployeeChildren($data)
    {
        $employee = auth()->user();

        $children = $employee->employee_children()->delete();
        $children = $employee->employee_children()->createMany($data);

        if ($children) {
            return 1;
        }

        return 0;
    }

    public function saveEmployeeWorkExperiences($data)
    {
        $employee = auth()->user();

        $work = $employee->employee_work_experiences()->delete();
        $work = $employee->employee_work_experiences()->createMany($data);

        if ($work) {
            return 1;
        }

        return 0;
    }

    public function saveEmployeeEmergencyContact($data)
    {
        $employee_id = auth()->user()->id;

        $emergency_contact = EmployeeEmergencyContact::where('employee_id', '=', $employee_id)->first();

        if (!$emergency_contact) {
            $emergency_contact = new EmployeeEmergencyContact;
            $emergency_contact->employee_id = $employee_id;
        }

        $emergency_contact->fill($data);

        if ($emergency_contact->save()) {
            return 1;
        }

        return 0;
    }

    public function saveEmployeeRelativeOnBanks($data)
    {
        $employee = auth()->user();

        $relative_bank = $employee->employee_relative_on_banks()->delete();
        $relative_bank = $employee->employee_relative_on_banks()->createMany($data);

        if ($relative_bank) {
            return 1;
        }

        return 0;
    }

    public function saveEmployeeFamilyLaons($data)
    {
        $employee = auth()->user();

        $family_loans = $employee->employee_family_loans()->delete();
        $family_loans = $employee->employee_family_loans()->createMany($data);

        if ($family_loans) {
            return 1;
        }

        return 0;
    }

    /**
     * @SWG\Get(
     *     path="/employee/profile",
     *     description="get profile detail",
     *     tags={"Employee Apis"},
     *     security={{"bearerAuth":{}}},
     *     @SWG\Response(
     *         response=200,
     *         description="status, message",
     *     ),
     *     @SWG\Response(
     *         response=422,
     *         description="status, message"
     *     )
     * )
     */
    public function employeeProfile()
    {
        $success = false;
        $message = "";
        $employee_id = auth()->user()->id;
        $employee = Employee::with([
               'employee_personal_detail',
               'employee_permanent_address_detail',
               'employee_temporary_address_detail',
               'employee_family_detail',
               'employee_children',
               'employee_educations',
               'employee_work_experiences',
               'employee_relative_contact',
               'employee_other_information',
               'employee_relative_on_banks',
               'employee_family_loans',
          ])->find($employee_id);

        if ($employee) {
            $success = true;
            $message = "Employee detail.";
        } else {
            $message = "Employee not found.";
        }

        return new EmployeeResource($employee);
    }

    /**
     * @SWG\Get(
     *     path="/employee/directories",
     *     description="get employee directories",
     *     tags={"Employee Apis"},
     *     security={{"bearerAuth":{}}},
     *     @SWG\Response(
     *         response=200,
     *         description="success, message",
     *     ),
     *     @SWG\Response(
     *         response=422,
     *         description="success, message"
     *     )
     * )
     */
    public function getEmployeeDirectory()
    {
        $employee_id = auth()->user()->id;
        $data = Employee::with('employee_permanent_address_detail')->orderBy('name', 'ASC')->where([
               ['status', '=', 1],
               ['id', '!=', $employee_id]
          ])->get();
        $success = false;
        $message = "";

        return new EmployeeDirectoryResource($data);
    }

    /**
     * @SWG\Get(
     *     path="/employee/job-description",
     *     description="get employee directories",
     *     tags={"Employee Apis"},
     *     security={{"bearerAuth":{}}},
     *     @SWG\Response(
     *         response=200,
     *         description="success, message",
     *     ),
     *     @SWG\Response(
     *         response=422,
     *         description="success, message"
     *     )
     * )
     */
    public function jobDescription()
    {
        $success = false;
        $message = "";

        $user = auth()->user();
        $description = "";

        if ($user->job_description) {
            $description = $user->job_description->description;
            $success = true;
            $message = "Fetched successfully.";
        } else {
            $message = "Job description not defined.";
        }

        return response()->json([
               'data' => [
                    'description' => $description
               ],
               'success' => $success,
               'message' => $message
          ]);
    }

    /**
     * @SWG\Get(
     *     path="/employee/leave-details",
     *     description="get leave details.",
     *     tags={"Employee Apis"},
     *     security={{"bearerAuth":{}}},
     *     @SWG\Response(
     *         response=200,
     *         description="success, message",
     *     ),
     *     @SWG\Response(
     *         response=422,
     *         description="success, message"
     *     )
     * )
     */
    public function leaveDetails()
    {
        try {
            // check if the employee with the employee code exists.
            // $exists = Employee::where('employee_code', $employeeId)->exists();

            // TODO uncomment this code for real api.
            // if (!$exists)
            //     throw new Exception("The employee does not exist.");

            $data = [
                [
                    "leave_id" =>  "1",
                    "leave_name" =>  "A. L.",
                    "total_days" =>  "39",
                    "remaining_balance" =>  "18.5"
                ],
                [
                    "leave_id" =>  "2",
                    "leave_name" =>  "S. L.",
                    "total_days" =>  "84",
                    "remaining_balance" =>  "62"
                ],
                [
                    "leave_id" =>  "3",
                    "leave_name" =>  "C. L.",
                    "total_days" =>  "15",
                    "remaining_balance" =>  "2"
                ]
            ];
        } catch (\Throwable $th) {
            return new ApiResource([], false, $th->getMessage());
        }

        return new ApiResource($data);
    }

    /**
    * @SWG\Get(
    *     path="/employee/salary-details",
    *     description="get salary details.",
    *     tags={"Employee Apis"},
    *     security={{"bearerAuth":{}}},
    *     @SWG\Response(
    *         response=200,
    *         description="success, message",
    *     ),
    *     @SWG\Response(
    *         response=422,
    *         description="success, message"
    *     )
    * )
    */
    public function salaryDetails()
    {
        $message = "";
        try {
            // check if the employee with the employee code exists.
            // $exists = Employee::where('employee_code', $employeeId)->exists();

            // TODO uncomment this code for real api.
            // if (!$exists)
            //     throw new Exception("The employee does not exist.");

            $data = [
                [
                    "val" => "81491",
                    "pay_desc" => "BASIC"
                ],
                [
                    "val" => "35939",
                    "pay_desc" => "Cloth / Trans All."
                ],
                [
                    "val" => "8149",
                    "pay_desc" => "PF cont. 10%"
                ],
                [
                    "val" => "16298",
                    "pay_desc" => "PF Cont 20%"
                ],
                [
                    "val" => "15469.9",
                    "pay_desc" => "S/Charge"
                ],
                [
                    "val" => "700",
                    "pay_desc" => "Mobile Allowance"
                ],
                [
                    "val" => "90",
                    "pay_desc" => "Union Fee"
                ],
                [
                    "val" => "98.45",
                    "pay_desc" => "Staff Advance"
                ],
                [
                    "val" => "814.91",
                    "pay_desc" => "Welfare Sub"
                ],
                [
                    "val" => "12500",
                    "pay_desc" => "CI Fund"
                ],
                [
                    "val" => "450",
                    "pay_desc" => "Cafe Meal"
                ],
                [
                    "val" => "500",
                    "pay_desc" => "S S S Fund"
                ],
                [
                    "val" => "21419.92",
                    "pay_desc" => "Income Tax"
                ]
            ];
            $message = "Salary Details";
        } catch (\Throwable $th) {
            return new ApiResource([], false, $th->getMessage());
        }

        return new ApiResource($data, true, $message);
    }
}
