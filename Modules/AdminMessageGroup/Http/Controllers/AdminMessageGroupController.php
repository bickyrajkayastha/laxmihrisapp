<?php

namespace Modules\AdminMessageGroup\Http\Controllers;

use App\Helpers\Permission;
use App\Models\MessageGroup;
use App\Models\EmployeeMessageGroup;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;

class AdminMessageGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $data['pageTitle'] = "Message Group Management";
        $data['message_groups'] = MessageGroup::latest()->get();
        $data['breadcrumbs'] = '<li><a href="' . route('admin.message-groups') . '">Message Group Management</a></li>';

        return view('adminmessagegroup::index', $data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $data['breadcrumbs'] = '<li><a href="' . route('admin.message-groups') . '">Message Group Management</a></li><li>Add</li>';

        return view('adminmessagegroup::create', $data);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);

        try {

            $message_group = new MessageGroup;
            $message_group->name = $request->name;
            $message_group->status = $request->status;
            $message_group->admin_id = auth()->user()->id;

            $message_group->save();
            session()->flash('success_message', __('alerts.create_success'));
            return redirect()->route('admin.message-groups');

        } catch (\Exception $e) {
            Log::info($e->getMessage());
            session()->flash('error_message', __('alerts.create_error'));
            return redirect()->route('admin.message-groups');
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('adminmessagegroup::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $message_group = MessageGroup::find($id);

        if (!$message_group) {
            session()->flash('error_message', 'Message Group not available for editing.');
            return redirect()->back();
        }

        $data['message_group'] = $message_group;
        $data['breadcrumbs'] = '<li><a href="' . route('admin.message-groups') . '">Message Group Management</a></li><li>Edit</li>';

        return view('adminmessagegroup::edit', $data);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $message_group = MessageGroup::find($id);

        $request->validate([
            'name' => 'required',
        ]);

        try {

            $message_group->name = $request->name;
            $message_group->status = $request->status;
            $message_group->save();
            session()->flash('success_message', __('alerts.update_success'));
            return redirect()->route('admin.message-groups');

        } catch (\Exception $e) {
            session()->flash('error_message', __('alerts.update_error'));
            return redirect()->route('admin.message-groups');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        if (!Permission::check('delete-message-groups'))
            return redirect()->route('admin.401');


        try {

            $message_group = MessageGroup::find($id);
            if($message_group->status == 1){
                session()->flash('error_message', __('alerts.delete_error_active'));
                return redirect()->back();
            }
            $message_group->delete();
            session()->flash('success_message', __('alerts.delete_success'));
            return redirect()->route('admin.message-groups');

        } catch (\Exception $e) {

            session()->flash('error_message', __('alerts.delete_error'));
            return redirect()->route('admin.message-groups');
        }
    }

    public function employees($id)
    {
        if (!Permission::check('view-message-groups'))
            return redirect()->route('admin.401');

        $group = MessageGroup::find($id);
        $data['message_group'] = $group;
        $data['participants'] = $group->employees;
        $data['all_employees'] = \App\Models\Employee::select('id', 'name')->active()->get();
        $data['breadcrumbs'] = '<li><a href="' . route('admin.message-groups') . '">Message Group Management</a></li><li>Participants</li>';

        return view('adminmessagegroup::employees', $data);
    }

    public function addEmployee(Request $request)
    {
        $request->validate([
            'message_group_id' => 'required',
            'employee_id' => ['required', 'numeric', Rule::unique('employee_message_group')->where(function ($query) use ($request) {
                return $query->where('message_group_id', $request->message_group_id);
            })],
        ], [
            'employee_id.required' => "The employee is required.",
            'employee_id.unique' => "The employee is already in this group."
        ]);

        $request->merge(['status' => 1]);

        try {
            $group = MessageGroup::find($request->message_group_id);
            $group->employees()->attach($request->employee_id, ['status' => $request->status]);
            session()->flash('success_message', "Employee added successfully.");
            return redirect()->back();

        } catch (\Exception $e) {
            \Log::info($e->getMessage());
            session()->flash('error_message', __('alerts.update_error'));
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id employee_message_group_id
     * @return Response
     */
    public function deleteEmployee($id)
    {
        if (!Permission::check('delete-message-groups'))
            return redirect()->route('admin.401');

        try {
            $employee_message_group = EmployeeMessageGroup::find($id);
            $employee_message_group->delete();
            session()->flash('success_message', __('alerts.delete_success'));
            return redirect()->back();

        } catch (\Exception $e) {
            session()->flash('error_message', __('alerts.delete_error'));
            return redirect()->back();
        }
    }

    public function updateEmployeeStatus(Request $request, $id)
    {
        $employee_message_group = EmployeeMessageGroup::find($id);
        $employee_message_group->status = $request->status;

        try {
            $employee_message_group->save();

            session()->flash('success_message', __('alerts.update_success'));
            return redirect()->back();

        } catch (\Exception $e) {
            session()->flash('error_message', __('alerts.update_error'));
            return redirect()->back();
        }
    }
}
