<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class Leaderboard extends Pivot
{
    protected $guarded = ['id'];
    protected $table = "leaderboards";

    public function employees()
    {
        return $this->belongsTo(Employee::class);
    }

    public function courses()
    {
        return $this->belongsTo(Course::class);
    }
}
