<?php

namespace App\Jobs;

use App\Models\EmployeeDevice;
use ExponentPhpSDK\Exceptions\ExpoException;
use ExponentPhpSDK\Expo;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Throwable;

class BirthdayNotificationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $notification;
    protected $recipient;
    public $tries = 3;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($notification, $recipient)
    {
        $this->notification = $notification;
        $this->recipient = $recipient;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $notification_data = $this->notification;
        $recipient = EmployeeDevice::where('employee_id', $this->recipient->id)->first();

        $expo = Expo::normalSetup();
        if ($recipient) {
            try {

                $channelName = 'laxmiAES52' . $recipient->id . "5842";
                $recipient_expo_token = $recipient->expo_token;

                // You can quickly bootup an expo instance

                // Subscribe the recipient to the server
                $expo->subscribe($channelName, $recipient_expo_token);

                // Build the notification data
                $notification = [
                    'title' => $notification_data['title'],
                    'body' => $notification_data['body'],
                    'data' => [
                        'type' => 1,
                        'messageData' => [
                            'title' => $notification_data['title'],
                            'body' => $notification_data['body']
                        ]
                    ]
                ];

                // Notify an interest with a notification
                $expo->notify($channelName, $notification);
            } catch (ExpoException $e) {
                Log::info($e->getMessage());
                $recipient->delete();
            }
        }
    }

    public function failed(Throwable $exception)
    {
        \Log::info($exception->getMessage());
    }
}
