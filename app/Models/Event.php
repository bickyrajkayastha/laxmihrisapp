<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
	protected $guarded = ['id'];

    protected $appends = ['fullImgPath', 'thumbImgPath', 'croppedImgPath', 'dateRange', 'timeRange'];

    public function getFullImgPathAttribute()
    {
        if ($this->image) {
            return url('uploads/events/full/' . $this->image);
        }
        return asset('') . config('constants.default_banner_image');
    }

    public function getCroppedImgPathAttribute()
    {
        if ($this->image) {
            return url('uploads/events/crop/' . $this->image);
        }
        return asset('') . config('constants.default_banner_image');
    }

    public function getThumbImgPathAttribute()
    {
        if ($this->image) {
            return url('uploads/events/thumb/' . $this->image);
        }
        return asset('') . config('constants.default_banner_image');
    }

    public function getDateRangeAttribute()
    {
        return formatDate($this->start_date) . (($this->end_date)? " - " . formatDate($this->end_date):'');
    }

    public function getTimeRangeAttribute()
    {
        return formatTime($this->start_time) . (($this->end_time)? " - " . formatTime($this->end_time):'');
    }

    public function scopePublished($query)
    {
        return $query->where('status', 1);
    }
}
