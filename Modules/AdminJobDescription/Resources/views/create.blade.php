@extends('admin.layouts.master')
@section('content')
<div class="row">
    <div class="col">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Add Job Description
                    <button class="btn btn-sm btn-primary pull-right panel-btn" onclick="window.history.go(-1);">
                        <i class="fas fa-long-arrow-alt-left"></i> Back
                    </button>
                </h3>
            </div>
            <!--Hover Rows-->
            <!--===================================================-->
            <form class="form-horizontal" id="add-course" action="{{ route('admin.job-descriptions.store') }}" method="POST"
                  enctype="multipart/form-data">
                @csrf
                <div class="panel-body">
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label class="col-sm-2 control-label" for="">Name <span
                                    class="required_color">*</span></label>
                        <div class="col-sm-9">
                            <input type="text" placeholder="Name" name="name" value="{{ old('name') }}" id="input-name"
                                   class="form-control" >
                            @if($errors->has('name'))
                            <small class="help-block text-danger">{{ $errors->first('name')}}</small>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                        <label class="col-md-2 control-label">Publish</label>
                        <div class="col-md-9">
                            <div class="radio">

                                <!-- Inline radio buttons -->
                                <input id="input-status" class="magic-radio" type="radio" name="status" value="1" <?= ((old('status') == 1)? 'checked':''); ?>>
                                <label for="input-status">Yes</label>

                                <input id="input-status-2" class="magic-radio" value="0" type="radio" name="status" <?= ((old('status') == 0)? 'checked':''); ?>>
                                <label for="input-status-2">No</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="demo-contact-input">Description <span
                                    class="required_color">*</span></label>
                        <div class="col-md-10">
                            <textarea name="description" id="input-description" cols="30" rows="10" class="form-control ckeditor">{{ old('description') }}</textarea>
                        <small class="help-block text-danger">{{ $errors->first('description')}}</small>
                        </div>
                    </div>
                    <!-- <div class="form-group">
                        <label class="col-md-3 control-label" for="demo-contact-input">Status</label>
                        <div class="col-md-10">
                            <div class="radio">
                                <input id="status" class="magic-radio" type="radio" name="status" value="1"
                                       checked>
                                <label for="status" style="color:#000000;">YES</label>
                                <input id="status-2" class="magic-radio" type="radio" name="status"
                                       value="0">
                                <label for="status-2" style="color:#000000;">NO</label>
                            </div>
                        </div>
                    </div> -->
                    <div class="form-group">
                        <div class="col-lg-2 col-sm-12 col-xs-12 pull-right">
                            <button class="btn btn-block btn-success" type="submit">Create</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
    $(function() {
        $("#add-course").on('submit', function(course) {
            $(this).find("button[type='submit']").prop('disabled', true).html('<i class="fas fa-spinner fa-spin"></i> Creating...');
        });
    });
</script>
@endpush
