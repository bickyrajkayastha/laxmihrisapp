@extends('admin.layouts.master')
@section('content')
<div class="row">
	<div class="col">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Welcome Note
                	<button class="btn btn-sm btn-primary pull-right panel-btn" onclick="window.history.go(-1);">
                	    <i class="fas fa-long-arrow-alt-left"></i> Back
                	</button>
                </h3>
            </div>

            <!--Horizontal Form-->
            <!--===================================================-->
            <form action="{{ route('admin.contents.update-welcome') }}" enctype="multipart/form-data" method="POST" class="">
                @csrf
                <div class="panel-body">
                    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                        <label class="col-sm-2 control-label" for="input-title">Title <span class="required_color">*</span></label>
                        <div class="col-sm-10">
                            <input type="text" placeholder="" value="{{ old('title',$content->title) }}" name="title" id="input-title" class="form-control">
                            <small class="help-block text-danger">{{ $errors->first('title')}}</small>
                        </div>
                    </div>
                    <!-- <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                                            <label class="col-md-2 control-label">Is Published</label>
                                            <div class="col-md-9">
                                                <div class="radio">
                        
                                                    Inline radio buttons
                                                    <input id="input-status" class="magic-radio" type="radio" name="status" value="1" {{ $content->status ? 'checked' :'' }}>
                                                    <label for="input-status">Yes</label>
                        
                                                    <input id="input-status-2" class="magic-radio" value="0" type="radio" name="status" {{ $content->status == 0 ? 'checked' : '' }}>
                                                    <label for="input-status-2">No</label>
                                                </div>
                                            </div>
                                        </div> -->
                    <!-- <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-type">Type <span class="required_color">*</span></label>
                        <div class="col-sm-10">
                            <select id="input-type-select" name="type" class="form-control" id="">
                                <option value="1" {{ old('type',$content->type) == 1 ? 'selected' :'' }}>Text</option>
                                <option value="2" {{ old('type',$content->type) == 2 ? 'selected' :'' }}>Image</option>
                                <option value="3" {{ old('type',$content->type) == 3 ? 'selected' :'' }}>Video</option>
                                <option value="4" {{ old('type',$content->type) == 4 ? 'selected' :'' }}>Power Point</option>
                            </select>
                        </div>
                    </div> -->
                    <!-- <div class="form-group{{ $errors->has('type_file') ? ' has-error' : '' }}">
                        <label class="col-sm-2 control-label" for="input-type-file"></label>
                        <div class="col-sm-10">
                            <div id="type-file" style="margin-top: 5px;">
                                <p id="type-file-name">{{ $content->type_file_name }}</p>
                                <input id="input-type-file" type="file" name="type_file" class="form-control">
                            </div>
                            <small class="help-block text-danger">{{ $errors->first('type_file')}}</small>
                        </div>
                    </div> -->
                    <div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">
                        <label class="col-sm-2 control-label" for="input-body">Content Body <span class="required_color">*</span></label>
                        <div class="col-sm-10">
	                        <textarea name="body" id="input-body" class="form-control ckeditor">{!! old('body',$content->body) !!}</textarea>
                            <small class="help-block text-danger">{{ $errors->first('body') }}</small>
                        </div>
                    </div>
                </div>
                <div class="panel-footer text-right">
                    <button class="btn btn-mint" type="submit">Update</button>
                </div>
            </form>
            <!--===================================================-->
            <!--End Horizontal Form-->

        </div>
    </div>
</div>
@endsection