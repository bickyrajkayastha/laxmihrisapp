<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeePollOptionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_poll_option', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('poll_option_id')->index();
            $table->foreign('poll_option_id')->references('id')->on('poll_options')->onDelete('cascade');
            $table->unsignedInteger('employee_id')->index();
            $table->foreign('employee_id')->references('id')->on('employees')->onDelete('cascade');
            $table->unsignedInteger('poll_id')->index();
            $table->foreign('poll_id')->references('id')->on('polls')->onDelete('cascade');
            $table->tinyInteger('seen_status')->default(0);
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_poll_option');
    }
}
