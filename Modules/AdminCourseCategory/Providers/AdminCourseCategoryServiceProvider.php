<?php

namespace Modules\AdminCourseCategory\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class AdminCourseCategoryServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(module_path('AdminCourseCategory', 'Database/Migrations'));
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path('AdminCourseCategory', 'Config/config.php') => config_path('admincoursecategory.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path('AdminCourseCategory', 'Config/config.php'), 'admincoursecategory'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/admincoursecategory');

        $sourcePath = module_path('AdminCourseCategory', 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ],'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/admincoursecategory';
        }, \Config::get('view.paths')), [$sourcePath]), 'admincoursecategory');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/admincoursecategory');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'admincoursecategory');
        } else {
            $this->loadTranslationsFrom(module_path('AdminCourseCategory', 'Resources/lang'), 'admincoursecategory');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (! app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(module_path('AdminCourseCategory', 'Database/factories'));
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
