<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeePersonalDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_personal_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('full_name');
            $table->unsignedInteger('employee_id')->index();
            $table->foreign('employee_id')->references('id')->on('employees')->onDelete('cascade');
            $table->date('dob');
            $table->tinyInteger('gender')->comment('1= male, 2= female');
            $table->string('nationality_at_birth')->nullable();
            $table->string('present_nationality')->nullable();
            $table->string('citizenship_no');
            $table->string('citizenship_issuing_office')->nullable();
            $table->string('driving_license_no')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_personal_details');
    }
}
