<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmployeePersonalDetail extends Model
{
	protected $guarded = ['id'];

	protected $appends = ['genderType']; 

	public function employee()
	{
		return $this->belongsTo(Employee::class);
	}

	public function getGenderTypeAttribute()
	{
		switch ($this->gender) {
			case 1:
				return "Male";
				break;
			
			case 2:
				return "Female";
				break;				
			default:
				return "Other";
				break;
		}
	}
}
