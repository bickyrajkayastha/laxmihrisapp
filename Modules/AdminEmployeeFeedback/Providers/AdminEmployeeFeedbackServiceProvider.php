<?php

namespace Modules\AdminEmployeeFeedback\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class AdminEmployeeFeedbackServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(module_path('AdminEmployeeFeedback', 'Database/Migrations'));
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path('AdminEmployeeFeedback', 'Config/config.php') => config_path('adminemployeefeedback.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path('AdminEmployeeFeedback', 'Config/config.php'), 'adminemployeefeedback'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/adminemployeefeedback');

        $sourcePath = module_path('AdminEmployeeFeedback', 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ],'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/adminemployeefeedback';
        }, \Config::get('view.paths')), [$sourcePath]), 'adminemployeefeedback');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/adminemployeefeedback');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'adminemployeefeedback');
        } else {
            $this->loadTranslationsFrom(module_path('AdminEmployeeFeedback', 'Resources/lang'), 'adminemployeefeedback');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (! app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(module_path('AdminEmployeeFeedback', 'Database/factories'));
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
