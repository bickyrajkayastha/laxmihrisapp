<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin/events', 'as' => 'admin.', 'middleware' => 'auth:admin'], function() {
    Route::get('/', 'AdminEventController@index')->name('events.index')->middleware('can:view-events');
    Route::get('/create', 'AdminEventController@create')->name('events.create')->middleware('can:create-events');
    Route::post('/create', 'AdminEventController@store')->name('events.store')->middleware('can:create-events');
    Route::get('/{id}/edit', 'AdminEventController@edit')->name('events.edit')->middleware('can:edit-events');
    Route::post('/{id}/edit', 'AdminEventController@update')->name('events.update')->middleware('can:edit-events');
    Route::post('/{id}/delete', 'AdminEventController@destroy')->name('events.destroy')->middleware('can:delete-events');
});
