<?php

namespace App\Listeners;

use App\Events\CreateNotification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Notifications\ExpoNotification;
use ExponentPhpSDK\Exceptions\ExpoException;
use ExponentPhpSDK\Expo;
use App\Models\Employee;
use App\Models\EmployeeDevice;
use Illuminate\Support\Facades\Log;

class SendNotification
{
    public $expo;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Expo $expo)
    {
        $this->expo = $expo;
    }

    /**
     * Handle the event.
     *
     * @param  CreateNotification  $event
     * @return void
     */
    public function handle(CreateNotification $event)
    {
        $notification_data = $event->notification;
        $recipients = EmployeeDevice::all();

        $expo = Expo::normalSetup();
        foreach ($recipients as $recipient) {
            try {
                
                $channelName = 'laxmiAES52' . $recipient->id . "5842";
                $recipient_expo_token = $recipient->expo_token;
                
                // You can quickly bootup an expo instance
                
                // Subscribe the recipient to the server
                $expo->subscribe($channelName, $recipient_expo_token);
                
                // Build the notification data
                $notification = [
                    'title' => $notification_data['title'], 
                    'body' => $notification_data['body']
                ];
                
                // Notify an interest with a notification
                $expo->notify($channelName, $notification);
            } catch (ExpoException $e) {
                Log::info($e->getMessage());
                $recipient->delete();
            }
        }
    }
}
