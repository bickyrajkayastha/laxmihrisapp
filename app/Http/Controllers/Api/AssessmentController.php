<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\ApiResource;
use App\Models\Course;

class AssessmentController extends Controller
{
    /**
     * @SWG\Post(
     *     path="/employee/post-answers",
     *     description="save employee detail.",
     *     tags={"Employee Apis"},
     *     security={{"bearerAuth":{}}},
     *     @SWG\Parameter(
     *         in="body",
     *         name="employee details",
     *         description="save employee details",
     *         required=true,
     *         @SWG\Schema(
     *             collectionFormat="multi",
     *             type="object",
     *             @SWG\Property(
     *                   property="course_id",
     *                   type="number",
     *              ),
     *             @SWG\Property(
     *                   property="assessments",
     *                   type="array",
     *                   @SWG\Items(
     *		                 type="array",
     *                       @SWG\Items(
     *                       )
     *                   ),
     *              ),
     *         )
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="status, message",
     *     ),
     *     @SWG\Response(
     *         response=422,
     *         description="status, message"
     *     )
     * )
     */
    public function postAssessment(Request $request)
    {
        $points = 0;
        $employee = auth()->user();
        $course = Course::find($request->course_id);
        $assessments = $course->assessments;
        $answers = [];

        foreach ($assessments as $key => $assessment) {
            $answers[$key][] = $assessment->id;
            $answers[$key][] = $assessment->answer();
        }

        // delete prev answers if exists
        $employee->assessment_options()->wherePivot('course_id', $course->id)->detach();
        foreach ($request->assessments as $test) {
            if ($test[1] != 0) {
                $employee->assessment_options()->attach($test[1], [
                    'course_id' => $course->id
                ]);
            }
            foreach ($answers as $answer) {
                $result = array_diff($test, $answer);
                if (empty($result)) {
                    $points++;
                    break;
                }
            }
        }

        // save to course_employee table
        $employee->courses()->wherePivot('course_id', $course->id)->detach();
        $employee->courses()->attach($course->id, [
            'assessment_points' => $points
        ]);

        $data = [
            'points' => $points
        ];

        $message = 'You obtained ' . $points . ' out of ' . $assessments->count();
        return new ApiResource($data, true, $message);
    }
}
