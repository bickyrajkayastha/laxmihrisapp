<?php

namespace App\Jobs;

use App\Helpers\Setting;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class ResetPasswordEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $data;
    public $tries = 3;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $commonVars = [
            '[[SYSTEM_NAME]]' => Setting::get('system_name'),
            '[[SYSTEM_EMAIL]]' => Setting::get('system_email'),
            // '[[SYSTEM_LINK]]' => url('/'),
            '[[DATE]]' => date('Y'),
            // '[[SYSTEM_FACEBOOK]]' => Setting::get('facebook_url'),
            // '[[SYSTEM_INSTAGRAM]]' => Setting::get('instagram_url'),
            // '[[SYSTEM_TWITTER]]' => Setting::get('twitter_url'),
            // '[[SYSTEM_YOUTUBE]]' => Setting::get('youtube_url'),
            // '[[PRIVACY_POLICY_LINK]]' => url('/'),
            // '[[UNSUBSCRIBE_LINK]]' => url('/'),
            // '[[SUPPORT_LINK]]' => url('/'),
        ];

        $data = $this->data;
        $mailData = \App\Models\EmailTemplate::find(2)->toArray();
        $mailData['to'] = $data['email'];
        $mailData['vars'] = [
            '[[PASSWORD]]' => $data['password'],
            '[[FULL_NAME]]' => $data['name'],
        ];

        $varsToReplace = array_merge($commonVars, $mailData['vars']);

        $mailData['description'] = strtr($mailData['description'], $varsToReplace);

        Mail::send('emails.common', ['body' => $mailData['description']], function ($message) use ($mailData) {
            $message->to($mailData['to']);
            $message->subject($mailData['subject']);

            if (isset($mailData['cc'])) {
                $message->cc($mailData['cc']);
            }

            if (isset($mailData['attachment'])) {
                $message->attach($mailData['attachment']);
            }
        });
    }

    public function failed()
    {
        \Log::info('failed');
        // Called when the job is failing...
    }
}
