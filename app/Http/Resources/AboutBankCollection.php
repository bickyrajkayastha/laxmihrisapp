<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\Log;

class AboutBankCollection extends ResourceCollection
{
    public $success = false;
    public $message = "";
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);

        if (iterator_count($this->collection)) {
            # code...
            $this->success = true;
            $this->message = "List fetched successfully.";
        } else {
            $this->success = true;
            $this->message = "Empty list.";
        }

        return $this->map(function ($content) {

            $content_type = "";
            switch ($content->type) {
                case '1':
                    $content_type = "text";
                    break;
                case '2':
                    $content_type = "image";
                    break;
                case '3':
                    $content_type = "video";
                    break;
                case '4':
                    $content_type = "ppt";
                    break;

                default:
                    # code...
                    break;
            }

            return [
                'id' => $content->id,
                'title' => $content->title,
                'content_type' => $content_type,
                'link' => $content->linkUrl,
                'description' => $content->body
            ];
        });
    }

    public function with($request)
    {
        return [
            'success' => $this->success,
            'message' => $this->message
        ];
    }
}
