@extends('admin.layouts.master')
@section('content')

<div class="row">
    <div class="col">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Email Templates
                    <!-- <a href="{{ route('admin.employees.create') }}" class="btn btn-primary btn-sm panel-btn pull-right"><i class="fa fa-plus"></i> Add Employee</a> -->
                </h3>
            </div>

            <!-- Striped Table -->
            <!--===================================================-->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
	                            <th>S.N</th>
	                            <th>Template</th>
	                            <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        	@forelse($templates as $k => $template)
                        	    <tr>
                        	        <td>{{ ++$k }}</td>
                        	        <td>{{ $template->title }}</td>
                        	        <td>
                        	            <a class="btn btn-sm btn-info" href="{{ route('admin.email-template.edit',$template->id) }}">
                        	                <i class="fas fa-edit"></i>
                        	            </a>
                        	        </td>
                        	    </tr>
                        	@empty
                    	    <tr>
                    	        <td colspan="5">
                    	            <em>No data available in table ...</em>
                    	        </td>
                    	    </tr>
                        	@endforelse
                        </tbody>
                    </table>
                </div>
            </div>
            <!--===================================================-->
            <!-- End Striped Table -->

        </div>
    </div>
</div>
@endsection