@extends('admin.layouts.master')
@push('css')
<link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/jcrop/jquery.Jcrop.min.css') }}">
@endpush
@section('content')

<div class="row">
    <div class="col">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Edit Event
                    <a href="{{ route('admin.events.index') }}" class="btn panel-btn btn-sm btn-primary pull-right"style="margin-top: 15px; margin-right: 15px;"> <i class="fas fa-long-arrow-alt-left"></i> Back</a>
                </h3>
            </div>
            <form class="form-horizontal" action="{{ route('admin.events.update',$event->id) }}"
                  method="POST" enctype="multipart/form-data">
                @csrf
                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-name">Title <span
                                    class="required_color">*</span></label>
                        <div class="col-sm-10">
                            <input type="text" placeholder="Title" name="title" value="{{ old('title',$event->title) }}"
                                   id="input-title" class="form-control" required="required">
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('start_date') ? ' has-error' : '' }}">
                        <label class="col-sm-2 control-label" for="input-date">Start Date</label>
                        <div class="col-sm-9">
                            <div class="input-group date">
                                <input type="text" class="form-control" autocomplete="off" name="start_date" id="event-date" value="{{ old('start_date', $event->start_date) }}">
                                <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-th"></span>
                                </div>
                            </div>
                            @if($errors->has('start_date'))
                            <small class="help-block text-danger">{{ $errors->first('start_date')}}</small>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('end_date') ? ' has-error' : '' }}">
                        <label class="col-sm-2 control-label" for="input-end-date">End Date</label>
                        <div class="col-sm-9">
                            <div class="input-group date">
                                <input type="text" class="form-control" autocomplete="off" name="end_date" id="event-end-date" value="{{ old('end_date', $event->end_date) }}">
                                <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-th"></span>
                                </div>
                            </div>
                            @if($errors->has('end_date'))
                            <small class="help-block text-danger">{{ $errors->first('end_date')}}</small>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('start_time') ? ' has-error' : '' }}">
                        <label class="col-sm-2 control-label" for="input-time">Start Time</label>
                        <div class="col-sm-9">
                            <div class="input-group datetimepicker">
                                <input type="text" class="form-control" autocomplete="off" name="start_time" id="start-time" value="{{ formatTime(old('start_time', $event->start_time)) }}">
                                <div class="input-group-addon">
                                    <span class="far fa-clock"></span>
                                </div>
                            </div>
                            @if($errors->has('start_time'))
                            <small class="help-block text-danger">{{ $errors->first('start_time')}}</small>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('end_time') ? ' has-error' : '' }}">
                        <label class="col-sm-2 control-label" for="input-end-time">End Time</label>
                        <div class="col-sm-9">
                            <div class="input-group datetimepicker">
                                <input type="text" class="form-control" autocomplete="off" name="end_time" id="event-end-time" value="{{ formatTime(old('end_time', $event->end_time)) }}">
                                <div class="input-group-addon">
                                    <span class="far fa-clock"></span>
                                </div>
                            </div>
                            @if($errors->has('end_time'))
                            <small class="help-block text-danger">{{ $errors->first('end_time')}}</small>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-name">Location </label>
                        <div class="col-sm-10">
                            <input type="text" placeholder="Location" name="location" value="{{ old('location',$event->location) }}" id="input-location" class="form-control">
                        </div>
                    </div>
                    <div class="form-group" id="image-homepage">
                        <label class="col-md-2 control-label" for="input-image">Ft. Image
                            <br><span class="tag-title"><strong>(600px * 600px)</strong></span>
                        </label>
                        <div class="col-md-10">
                            @if($event->image && $event->croppedImgPath && $event->fullImgPath)
                                <div>
                                    <img src="{{ asset('uploads/events/full/'.$event->image) }}"
                                         style="width: 600px; max-width:100%;" alt="" id="original_image">
                                    <img id="preview"
                                         src="{{ asset('uploads/events/crop/'.$event->image) }}"
                                         style="display:none">
                                </div>
                            @else
                                <img id="preview" alt="">
                            @endif
                            <br>
                            <span class="btn btn-sm btn-rose btn-primary btn-file">
                            <span>Browse</span>
                            <input type="file" name="image" id="input-image-file"
                                   onchange="fileSelectHandler()" {{ ($event->image && file_exists($event->croppedImgPath) && file_exists($event->fullImgPath)) ? '' : ''}} />
                        </span>
                            @if($event->image && file_exists($event->croppedImgPath) && file_exists($event->fullImgPath))
                                <a href="javascript:void(0)" class="btn btn-danger imagecrop"
                                   title="crop image"
                                   onclick="CropPreviousImage()">Crop Image</a>
                                <a href="javascript:void(0)" class="btn btn-danger" id="cancelimage"
                                   title="cancel"
                                   onclick="CancelImage()" style="display:none;">Cancel</a>
                            @endif
                            <div class="image-error" style="color:red"></div>
                            <input type="hidden" value="" name="croppreviousimage" id="croppreviousimage">
                            <input type="hidden" id="x1" name="x1"/>
                            <input type="hidden" id="y1" name="y1"/>
                            <input type="hidden" id="x2" name="x2"/>
                            <input type="hidden" id="y2" name="y2"/>
                            <input type="hidden" id="w" name="w"/>
                            <input type="hidden" id="h" name="h"/>
                            <small class="help-block text-danger">{{$errors->first('image')}}</small>
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                        <label class="col-md-2 control-label">Publish</label>
                        <div class="col-md-9">
                            <div class="radio">

                                <!-- Inline radio buttons -->
                                <input id="input-status" class="magic-radio" type="radio" name="status" value="1" {{ $event->status ? 'checked' :'' }}>
                                <label for="input-status">Yes</label>

                                <input id="input-status-2" class="magic-radio" value="0" type="radio" name="status" {{ $event->status == 0 ? 'checked' : '' }}>
                                <label for="input-status-2">No</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
                        <label class="col-md-2 control-label" for="input-content">Content</label>
                        <div class="col-md-10">
                            <textarea name="content" id="input-content" cols="30" rows="10" class="form-control ckeditor">{{ old('content',$event->content) }}</textarea>
                            @if($errors->has('content'))
                            <small class="help-block text-danger">{{ $errors->first('content')}}</small>
                            @endif
                        </div>
                    </div>
                    <!-- <div class="form-group">
                        <label class="col-md-2 control-label" for="demo-contact-input">Status</label>
                        <div class="col-md-10">
                            <div class="radio">
                                <input id="status" class="magic-radio" type="radio" name="status" value="1"
                                       {{ $event->status ? 'checked' : '' }}>
                                <label for="status" style="color:#000000;">YES</label>
                                <input id="status-2" class="magic-radio" type="radio" name="status"
                                       value="0" {{ $event->status == 0 ? 'checked' : '' }}>
                                <label for="status-2" style="color:#000000;">NO</label>
                            </div>
                        </div>
                    </div> -->
                    <div class="form-group">
                        <div class="col-lg-2 col-xs-12 col-md-12 pull-right">
                            <button class="btn btn-block btn-primary" type="submit">Update</button>
                        </div>
                    </div>
                </div>
            </form>
            <!--===================================================-->
            <!--End Hover Rows-->
        </div>
    </div>
</div>

@endsection

@push('scripts')
    <script src="{{ asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/jcrop/jquery.Jcrop.min.js') }}"></script>

    <script>
        var aspectRatioHomepage = 3 / 2;
        $(document).ready(function () {
            $('.date').datepicker({
                format: 'yyyy-mm-dd',
            });

            $('#start-time').timepicker({
                format: 'hh:ii',
                defaultTime: false
            });

            $('#event-end-time').timepicker({
                format: 'hh:ii',
                defaultTime: false
            });

            $('#image-type-homepage').on('change', function () {
                if ($('#image-type-homepage').val() == 'square') {
                    aspectRatioHomepage = 1;
                    $('.jcrop-holder').remove();
                    $('#image-homepage').removeClass('hidden');
                } else {
                    aspectRatioHomepage = 3 / 2;
                    $('.jcrop-holder').remove();
                    $('#image-homepage').removeClass('hidden');
                }
            });
        });

        $(function () {
            $('.resetbutton').click(function () {
                CancelImage();
            });
        });

        function CancelImage() {
            $('#input-image-file').val('');
            $('.imagecrop').show();
            $('#original_image').show();
            $('#cancelimage').hide();
            $('.jcrop-holder').hide();
            $('#croppreviousimage').val('false');
            $('.image-error').hide();

        }

        function CropPreviousImage() {
            var oImage = document.getElementById('preview');
            // oImage.src = e.target.result;
            $('#croppreviousimage').val('true');
            $('#original_image').hide();
            $('.imagecrop').hide();
            $('#cancelimage').show();
            $('.jcrop-holder').show();
            var previoussource = "{{ asset('uploads/events/full/'.$event->image) }}";
            $('#preview').prop('src', previoussource);
            $('#preview').Jcrop({
                setSelect: [0, 0, 300, 200],
                boxWidth: 300,
                // boxHeight: 600,
                minSize: [300, 200], // min crop size
                aspectRatio: aspectRatioHomepage,
                bgFade: true, // use fade effect
                bgOpacity: .3, // fade opacity
                onChange: updateInfo,
                onSelect: updateInfo,
                onRelease: clearInfo,
                trueSize: [oImage.naturalWidth, oImage.naturalHeight],
            }, function () {
                // use the Jcrop API to get the real image size
                var bounds = this.getBounds();
                boundx = bounds[0];
                boundy = bounds[1];
                // Store the Jcrop API in the jcrop_api variable
                jcrop_api = this;
            });
        }

        // convert bytes into friendly format
        function bytesToSize(bytes) {
            var sizes = ['Bytes', 'KB', 'MB'];
            if (bytes == 0) return 'n/a';
            var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
            return (bytes / Math.pow(1024, i)).toFixed(1) + ' ' + sizes[i];
        };

        // check for selected crop region
        function checkForm() {
            if (parseInt($('#w').val())) return true;
            $('.image-error').html('Please select a crop region and then press Uploads').show();
            return false;
        };

        // update info by cropping (onChange and onSelect events handler)
        function updateInfo(e) {
            $('#x1').val(e.x);
            $('#y1').val(e.y);
            $('#x2').val(e.x2);
            $('#y2').val(e.y2);
            $('#w').val(e.w);
            $('#h').val(e.h);
        };

        // clear info by cropping (onRelease event handler)
        function clearInfo(e) {
            $('#x1').val(e.x);
            $('#y1').val(e.y);
            $('#x2').val(e.x2);
            $('#y2').val(e.y2);
            $('#w').val(300);
            $('#h').val(200);
        }

        // Create variables (in this scope) to hold the Jcrop API and image size
        var jcrop_api, boundx, boundy;

        function fileSelectHandler() {
            $('#original_image').hide();
            $('.imagecrop').hide();
            $('#cancelimage').show();
            // get selected file
            var oFile = $('#input-image-file')[0].files[0];

            if (!$('#input-image-file')[0].files[0]) {
                $('.jcrop-holder').remove();
                return;
            }
            // hide all errors
            $('.image-error').hide();
            // check for image type (jpg and png are allowed)
            var rFilter = /^(image\/jpeg|image\/png|image\/jpg|image\/gif|image\/xcf|image\/svg)$/i;
            if (!rFilter.test(oFile.type)) {
                $('#input-image-file').val('');
                $('#original_image').show();
                $('.image-error').html('Please select a valid image file (jpeg, png, jpg, gif, svg are allowed)').show();
                return;
            } else {
                $('#submit').prop("disabled", false);
            }

            if (oFile.size > 15728640) {
                $('#input-image-file').val('');
                $('#original_image').show();
                $('.image-error').html('You have selected too big file, please select one with maximum size less than 10MB, Please choose another one or cancel').show();
                return;
            }
            // preview element
            var oImage = document.getElementById('preview');
            // prepare HTML5 FileReader
            var oReader = new FileReader();
            oReader.onload = function (e) {
                // e.target.result contains the DataURL which we can use as a source of the image
                oImage.src = e.target.result;
                oImage.onload = function () { // onload event handler
                    var height = oImage.naturalHeight;

                    var width = oImage.naturalWidth;

                    // console.log(height);
                    // console.log(width);
                    window.URL.revokeObjectURL(oImage.src);

                    if (height < 200 || width < 300) {

                        oImage.src = "";
                        $('#input-image-file').val('');
                        $('#original_image').show();


                        // $('#submit').prop("disabled","disabled");

                        $('.image-error').html('You have selected too small file, please select a one image with minimum size 600 X 600 px. Cancel or Browse another Image').show();

                    }
                    // display step 2
                    $('.step2').fadeIn(500);
                    // display some basic image info
                    var sResultFileSize = bytesToSize(oFile.size);

                    // destroy Jcrop if it is existed
                    if (typeof jcrop_api != 'undefined') {
                        jcrop_api.destroy();
                        jcrop_api = null;
                        $('#preview').width(oImage.naturalWidth);
                        $('#preview').height(oImage.naturalHeight);
                    }
                    setTimeout(function () {
                        // initialize Jcrop
                        $('#preview').Jcrop({
                            setSelect: [0, 0, 600, 600],
                            boxWidth: 300,
                            // boxHeight: 600,
                            minSize: [300, 200], // min crop size
                            aspectRatio: aspectRatioHomepage,

                            bgFade: true, // use fade effect
                            bgOpacity: .3, // fade opacity
                            onChange: updateInfo,
                            onSelect: updateInfo,
                            onRelease: clearInfo,
                            trueSize: [oImage.naturalWidth, oImage.naturalHeight],
                        }, function () {
                            // use the Jcrop API to get the real image size
                            var bounds = this.getBounds();
                            boundx = bounds[0];
                            boundy = bounds[1];
                            // Store the Jcrop API in the jcrop_api variable
                            jcrop_api = this;
                        });
                    }, 500);
                };
            };
            // read selected file as DataURL
            oReader.readAsDataURL(oFile);
        }
    </script>
@endpush
