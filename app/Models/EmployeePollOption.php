<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class EmployeePollOption extends Pivot
{
	protected $guarded = ['id'];
}
