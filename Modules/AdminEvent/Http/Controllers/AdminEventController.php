<?php

namespace Modules\AdminEvent\Http\Controllers;

use App\Helpers\Permission;
use App\Models\Event;
use App\Services\ImageUpload\Strategy\UploadWithAspectRatio;
use App\Traits\Slugger;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Intervention\Image\Facades\Image;
use Modules\AdminEvent\Services\EventImageUploader;
use Illuminate\Support\Facades\Log;

class AdminEventController extends Controller
{
    use Slugger;

    private $indexRoute;

    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->indexRoute = route('admin.events.index');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $data['pageTitle'] = "Event Management";
        $data['events'] = Event::orderBy('start_date', 'desc')->paginate(20);

        return view('adminevent::index', $data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $data['pageTitle'] = "Add Event";
        $data['breadcrumbs'] = '<li><a href="' . route('admin.events.index') . '">Event Management</a></li><li>Add</li>';
        return view('adminevent::create', $data);
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        $time = date('G:i:s', strtotime($this->request->start_time));
        $this->request->merge(['start_time' => $time]);

        $time = date('G:i:s', strtotime($this->request->end_time));
        $this->request->merge(['end_time' => $time]);

        $this->request->validate([
            'title' => 'required',
            'content' => 'required',
            'image' => 'nullable|image|mimes:png,jpg,jpeg',
            'start_date' => 'required|date',
            'end_date' => 'nullable|date',
            'start_time' => 'nullable|date_format:G:i:s',
            'end_time' => 'nullable|date_format:G:i:s|after:start_time'
        ]);

        try {
            $data = $this->request->only('title', 'content', 'status', 'start_time', 'end_time', 'location');
            $data['slug'] = $this->createSlug($this->request->title, Event::class);

            if ($this->request->start_date != "") {
                $data['start_date'] = date('y-m-d', strtotime($this->request->start_date));
            }

            if ($this->request->end_date != "") {
                $data['end_date'] = date('y-m-d', strtotime($this->request->end_date));
            }

            if ($this->request->hasFile('image')) {
                $image = $this->request->file('image');

                $uploader = new EventImageUploader(new UploadWithAspectRatio());

                $data['image'] = $uploader->saveOriginalImage($image);

                $this->cropAndSaveImage(
                    $uploader,
                    $data['image'],
                    $this->request->x1,
                    $this->request->y1,
                    $this->request->w,
                    $this->request->h
                );

            }

            Event::create($data);

            session()->flash('success_message', __('alerts.create_success'));
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            session()->flash('error_message', __('alerts.create_error'));
        }

        return redirect()->to($this->indexRoute);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('adminevent::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $data['pageTitle'] = "Edit Event";
        $data['event'] = $this->getEventById($id);
        $data['breadcrumbs'] = '<li><a href="' . route('admin.events.index') . '">Event Management</a></li><li>Edit</li>';

        return view('adminevent::edit', $data);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update($id)
    {
        $time = date('G:i:s', strtotime($this->request->start_time));
        $this->request->merge(['start_time' => $time]);
        $end_time = date('G:i:s', strtotime($this->request->end_time));
        $this->request->merge(['end_time' => $end_time]);

        $this->request->validate([
            'title' => 'required',
            'content' => 'required',
            'image' => 'nullable|image|mimes:png,jpg,jpeg',
            'start_date' => 'required|date',
            'end_date' => 'nullable|date',
            'start_time' => 'nullable|date_format:G:i:s',
            'end_time' => 'nullable|date_format:G:i:s|after:start_time'
        ], [
            'end_time.after' => 'The end time must be greater than start time.'
        ]);

        try {
            $data = $this->request->only('title', 'content', 'status', 'start_time', 'end_time', 'location');

            $data['slug'] = $this->createSlug($this->request->name, Event::class);

            if ($this->request->start_date != "") {
                $data['start_date'] = date('y-m-d', strtotime($this->request->start_date));
            }

            if ($this->request->end_date != "") {
                $data['end_date'] = date('y-m-d', strtotime($this->request->end_date));
            }

            $event = Event::find($id);

            if ($this->request->hasFile('image') || $this->request->croppreviousimage == 'true') {
                $uploader = new EventImageUploader(new UploadWithAspectRatio());

                $uploader->deleteThumbImage($event->image);

                $uploader->deleteCroppedImage($event->image);

                if ($this->request->croppreviousimage == 'true') {
                    $data['image'] = $event->image;
                } else {
                    $uploader->deleteFullImage($event->image);

                    $data['image'] = $uploader->saveOriginalImage($this->request->file('image'));
                }

                $this->cropAndSaveImage(
                    $uploader,
                    $data['image'],
                    $this->request->x1,
                    $this->request->y1,
                    $this->request->w,
                    $this->request->h
                );
            }

            $event->update($data);

            session()->flash('success_message', __('alerts.update_success'));
        } catch (\Exception $e) {
            session()->flash('error_message', __('alerts.update_error'));
        }

        return redirect()->to($this->indexRoute);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        try {

            $event = $this->getEventById($id);

            if($event->status == 1){
                session()->flash('error_message', __('alerts.delete_error_active'));
                return redirect()->to($this->indexRoute);
            }

            $uploader = new EventImageUploader();

            $uploader->deleteFullImage($event->image);

            $uploader->deleteThumbImage($event->image);

            $uploader->deleteCroppedImage($event->image);

            $event->delete();

            session()->flash('success_message', __('alerts.delete_success'));

        } catch (\Exception $e) {
            session()->flash('error_message', __('alerts.delete_error'));
        }

        return redirect()->to($this->indexRoute);
    }

    private function cropAndSaveImage($uploader, $filename, $posX1, $posY1, $width, $height)
    {
        $imgPath = $uploader->getFullImagePath($filename);

        $fullImage = Image::make($imgPath);

        $cropDestPath = $uploader->getCroppedImagePath($filename);

        $uploader->cropAndSaveImage($fullImage, $cropDestPath, $posX1, $posY1, $width, $height);

        $uploader->cropAndSaveImageThumb($fullImage, $filename);
    }

    private function getEventById($id)
    {
        return Event::find($id);
    }
}
