<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Illuminate\Support\Facades\Log;

class EmployeeMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->status == 1) {
            return $next($request);
        } else {
            return response()->json([
                'data' => [],
                'success' => false,
                'message' => "unauthenticated"
            ], 401);
        }
    }
}
