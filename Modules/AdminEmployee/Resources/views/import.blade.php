@extends('admin.layouts.master')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="panel">
            <div class="panel-heading">
                <div>
                    <h3 class="panel-title pull-left">Import Employees</h3>
                    <a href="{{ route('admin.employees') }}" class="btn btn-primary pull-right"
                       style="margin-top: 15px; margin-right: 15px;"> <i
                                class="fas fa-long-arrow-alt-left"></i> Back</a>

                    <a href="{{ asset('pdf-formats/sample-employees-list-format.xlsx') }}" download target="_blank" class="btn btn-warning pull-right"
                    style="margin-top: 15px; margin-right: 15px;"> Download Sample Format</a>
                </div>
            </div>
            <hr>
            <!--Hover Rows-->
            <!--===================================================-->
            <form class="form-horizontal" action="{{ route('admin.employees.store-import') }}" method="POST"
                  enctype="multipart/form-data">
                @csrf
                <div class="panel-body">
                    <div class="form-group{{ $errors->has('excel_file') ? ' has-error' : '' }}">
                        <label class="col-sm-3 control-label" for="demo-hor-inputemail">Select Excel File</label>
                        <div class="col-sm-9">
                            <input type="file" name="excel_file" required class="form-control" >
                            @if($errors->has('excel_file'))
                            <small class="help-block text-danger">{{ $errors->first('excel_file')}}</small>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-2 col-sm-12 col-xs-12 pull-right">
                            <button class="btn btn-block btn-success" type="submit">Import</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
