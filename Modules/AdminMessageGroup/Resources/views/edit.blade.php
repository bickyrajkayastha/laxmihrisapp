@extends('admin.layouts.master')
@section('content')
<div class="row">
	<div class="co">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Add Group
                    <button class="btn btn-sm btn-primary pull-right panel-btn" onclick="window.history.go(-1);">
                        <i class="fas fa-long-arrow-alt-left"></i> Back
                    </button>
                </h3>
            </div>

            <!--Horizontal Form-->
            <!--===================================================-->
            <form action="{{ route('admin.message-groups.update', ['id' => $message_group->id]) }}" method="POST" class="form-horizontal">
                @csrf
                <div class="panel-body">
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label class="col-sm-2 control-label" for="input-name">Group Name <span class="required_color">*</span></label>
                        <div class="col-sm-9">
                            <input type="text" placeholder="" value="{{ old('name', $message_group->name) }}" name="name" id="input-name" class="form-control">
                            <small class="help-block text-danger">{{ $errors->first('name')}}</small>
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                        <label class="col-md-2 control-label">Status</label>
                        <div class="col-md-9">
                            <div class="radio">
                    
                                <!-- Inline radio buttons -->
                                <input id="input-status" class="magic-radio" type="radio" name="status" value="1" {{ $message_group->status ? 'checked' :'' }}>
                                <label for="input-status">Active</label>
                    
                                <input id="input-status-2" class="magic-radio" value="0" type="radio" name="status" {{ $message_group->status == 0 ? 'checked' : '' }}>
                                <label for="input-status-2">Inactive</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer text-right">
                	<div class="row">
		                <div class="col-lg-11">
		                    <button class="btn btn-sm btn-mint" type="submit">Update</button>
		                </div>
                	</div>
                </div>
            </form>
            <!--===================================================-->
            <!--End Horizontal Form-->

        </div>
    </div>
</div>
@endsection