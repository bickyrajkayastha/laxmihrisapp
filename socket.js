var app = require('express')();
var https = require('https');
var http = require('http').Server(app);
var io = require('socket.io')(https);
var Redis = require('ioredis');
var redis = new Redis();
var users = [];
var groups = [];
var crypto = require('crypto');
var fs = require('fs');
const { Expo } = require('expo-server-sdk');
let expo = new Expo();
let messages = [];
let port = 8002;

// app.get('/node/testserver', (req, res) => {
//     res.send('from node server');
// });

const server = https.createServer({
    cert: fs.readFileSync('/etc/nginx/conf.d/certificate.pem'),
    key: fs.readFileSync('/etc/nginx/conf.d/key.key'),
    // ca: fs.readFileSync(/*full path to your intermediate cert*/),
    requestCert: true,
    rejectUnauthorized: false
}, app);

var io = require('socket.io')(server, {
    path: '/node/socket.io',
    // serveClient: false,
    // // below are engine.IO options
    // pingInterval: 10000,
    // pingTimeout: 5000,
    // cookie: false
});

// var io = require('socket.io').listen(server);

// io.set('origins', /*your desired origins*/ );

// io.set('transports', ['websocket',
//     'flashsocket',
//     'htmlfile',
//     'xhr-polling',
//     'jsonp-polling',
//     'polling'
// ]);


function sendMessageAsExpoNotification(message_data, channel) {
    // if (!Expo.isExpoPushToken(pushToken)) {
    //   console.error(`Push token ${pushToken} is not a valid Expo push token`);
    //   continue;
    // }

    // Construct a message (see https://docs.expo.io/versions/latest/guides/push-notifications)
    var channelName = 'laxmiAES52' + 3 + "5842";
    // console.log(message_data.data.expo_token);
    messages.push({
        to: message_data.data.expo_token,
        sound: 'default',
        title: message_data.data.sender_name,
        body: message_data.data.content,
        android: {
            channelId: channelName,
        },
        data: {
            type: 2,
            messageData: message_data.data
        },
    })

    let chunks = expo.chunkPushNotifications(messages);
    let tickets = [];
    (async () => {
        // Send the chunks to the Expo push notification service. There are
        // different strategies you could use. A simple one is to send one chunk at a
        // time, which nicely spreads the load out over time:
        for (let chunk of chunks) {
            try {
                let ticketChunk = await expo.sendPushNotificationsAsync(chunk);
                console.log(ticketChunk);
                tickets.push(...ticketChunk);
                messages = [];
                // NOTE: If a ticket contains an error code in ticket.details.error, you
                // must handle it appropriately. The error codes are listed in the Expo
                // documentation:
                // https://docs.expo.io/versions/latest/guides/push-notifications#response-format
            } catch (error) {
                console.error(error);
            }
        }
    })();
}

function sendGroupMessageAsExpoNotification(expo_tokens, data, channel) {
    // if (!Expo.isExpoPushToken(pushToken)) {
    //   console.error(`Push token ${pushToken} is not a valid Expo push token`);
    //   continue;
    // }

    // Construct a message (see https://docs.expo.io/versions/latest/guides/push-notifications)
    var channelName = 'laxmiAES52' + 4 + "5842";
    // console.log(message_data.data.expo_token);
    // console.log(data.data);

    messages.push({
        // to: data.message_data.expo_token,
        // to: ['ExponentPushToken[TpDSdoIlJKRJHZn4AJ0DY5]', 'ExponentPushToken[ykol4DMQs0xOzAoOhN75Ng]'],
        to: expo_tokens,
        sound: 'default',
        title: data.data.group_name,
        body: data.data.sender_name + ": " + data.data.content,
        android: {
            channelId: channelName,
        },
        data: {
            type: 3,
            messageData: data.data
        },
    })

    let chunks = expo.chunkPushNotifications(messages);
    let tickets = [];
    (async () => {
        // Send the chunks to the Expo push notification service. There are
        // different strategies you could use. A simple one is to send one chunk at a
        // time, which nicely spreads the load out over time:
        for (let chunk of chunks) {
            try {
                let ticketChunk = await expo.sendPushNotificationsAsync(chunk);
                // console.log(ticketChunk);
                tickets.push(...ticketChunk);
                messages = [];
                // NOTE: If a ticket contains an error code in ticket.details.error, you
                // must handle it appropriately. The error codes are listed in the Expo
                // documentation:
                // https://docs.expo.io/versions/latest/guides/push-notifications#response-format
            } catch (error) {
                console.error(error);
            }
            // messages = [];
        }
    })();
}

redis.subscribe('private-channel', function (err, count) {
    // console.log('private-channel');
});

redis.subscribe('group-channel', function (err, count) {
    // console.log('private-channel');
});

redis.on('message', function (channel, message) {
    message = JSON.parse(message);
    if (channel == 'private-channel') {
        if (users[message.data.data.receiver_id] != 0) {
            if (message.data.data.type == 2) {
                io.to(`${users[message.data.data.receiver_id]}`).emit(channel + ':' + message.event, message.data.data);
            }
        } else {
            sendMessageAsExpoNotification(message.data, channel);
        }
    }

    if (channel == 'group-channel') {
        let expo_tokens = [];
        message.data.receivers.forEach(function (i, v) {
            if (!checkIfUserExistINGroup(i.receiver_id, message.data.data.group_id)) {
                expo_tokens = expo_tokens.concat(i.expo_token);
            }
        });

        if (expo_tokens.length > 0) {
            sendGroupMessageAsExpoNotification(expo_tokens, message.data, channel);
        }

        if (message.data.data.type == 2) {
            let socket_id = getSocketIdOfUserInGroup(message.data.data.sender_id, message.data.data.group_id);
            let socket = io.sockets.connected[socket_id];
            socket.broadcast.to('group' + message.data.data.group_id).emit('groupMessage', message.data.data);
            // io.to(`${users[message.data.data.receiver_id]}`).emit(channel + ':' + message.event, message.data.data);
        }

        // if (!checkIfUserExistINGroup(message.data.data.receiver_id, message.data.data.group_id)) {
        // console.log(message.data);
        // }
    }
});

io.on('connection', function (socket) {
    console.log('a user connected');
    socket.on("user_connected", function (user_id) {
        users[user_id] = socket.id;
        var i = users.indexOf(socket);
    });

    socket.on('userMessage', function (data) {

        // io.to(`${users[2]}`).emit('getMessage', data);
        io.emit('getMessage', data);
        socket.broadcast.emit('getMessage', data); //sending to all the client except the sender
        // socket.emit('getMessage', data);
    });

    // instant messaging
    socket.on('privateMessage', function (data) {
        // message = JSON.parse(message);
        if (users[data.receiver_id] != 0) {
            io.to(`${users[data.receiver_id]}`).emit("privateMessage", data);
        }
    });
    // end of instant messaging.

    socket.on('groupMessage', function (data) {
        socket.broadcast.to('group' + data.group_id).emit('groupMessage', data);
    });

    socket.on('joinGroup', function (data) {
        data['socket_id'] = socket.id;
        if (groups[data.group_id]) {
            var valid = checkIfUserExistINGroup(data.user_id, data.group_id);
            if (!valid) {
                groups[data.group_id].push(data);
                socket.join(data.room);
            }
        } else {
            groups[data.group_id] = [data];
            socket.join(data.room);
        }
    });

    socket.on('disconnect', function () {
        var i = users.indexOf(socket.id);
        users.splice(i, 1, 0);
        console.log('disconnected');
    });

    socket.on('leaveRoom', function (data) {
        socket.leave(data.room);
        checkoutFromGroup(data.user_id, data.group_id);
    });
});

function checkoutFromGroup(user_id, group_id) {
    var group = groups[group_id];
    for (var i = 0; i < group.length; i++) {
        if (group[i]['user_id'] == user_id) {
            group.splice(i, 1);
            break;
        }
    }
}

function checkIfUserExistINGroup(user_id, group_id) {
    var group = groups[group_id];
    var valid = false;
    if (group.length > 0) {
        for (var i = 0; i < group.length; i++) {
            if (group[i]['user_id'] == user_id) {
                valid = true;
                break;
            }
        }
    }

    return valid;
}

function getSocketIdOfUserInGroup(user_id, group_id) {
    var group = groups[group_id];
    var valid = false;
    if (group.length > 0) {
        for (var i = 0; i < group.length; i++) {
            if (group[i]['user_id'] == user_id) {
                return group[i]['socket_id'];
            }
        }
    }
}

server.listen(port, function () {
    console.log('Listening on Port 8002');
});
