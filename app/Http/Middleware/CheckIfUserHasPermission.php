<?php

namespace App\Http\Middleware;

use App\Helpers\Permission;
use Closure;

class CheckIfUserHasPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $module)
    {
        if(!Permission::check($module)) {
            
            session()->flash('error_message', __('alerts.no_permission'));
            return back();
        }

        return $next($request);
    }
}
