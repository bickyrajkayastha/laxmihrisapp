<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(EmailTableSeeder::class);
        $this->call(ModuleTableSeeder::class);
        $this->call(GroupSeeder::class);
        $this->call(AdminUserSeeder::class);
    }
}
