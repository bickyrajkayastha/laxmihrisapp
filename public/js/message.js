var auth_id = document
  .querySelector('script[data-id="message"]')
  .getAttribute('auth-id');

var socket = io('10.13.210.34:8000');

$("#message-form").on('submit', function(event) {
    event.preventDefault()

    var message = $("#messageArea").val();
    socket.emit('userMessage', message);
    $(this)[0].reset();
    $("messageArea").val("");
});

$("#messageArea").keypress(function (e) {
    if(e.which == 13) {
        $("#message-form").submit();
    }
});

socket.on('connect', function() { 
    // console.log(socket);
    // console.log('connected');
    socket.emit('user_connected', auth_id);
});

socket.on("getMessage", function(data) {
    $.niftyNoty({
        type: 'dark',
        container : 'floating',
        // title : 'Success',
        message : data,
        closeBtn : true,
        floating: {
            position: "top-right",
            animationIn: "fadeInDown",
            animationOut: "fadeOutUp"
        },
        timer: 3000,
        focus: true,
        onHide:function(){
            // alert("onHide Callback");
        }
    });
});

socket.on('onlineUsers', function(data) {
    console.log('online users: ' + data);
});
socket.on('your_use_id', function(user_id) {
    console.log('you are now connected and your user id is: ' + user_id);
});

socket.on("test-channel:App\\Events\\TestEvent", function(message) {
    $.niftyNoty({
        type: 'mint',
        container : 'floating',
        // title : 'Success',
        message : "Message from the server.",
        closeBtn : true,
        floating: {
            position: "top-right",
            animationIn: "fadeInDown",
            animationOut: "fadeOutUp"
        },
        timer: 3000,
        focus: true,
        onHide:function(){
            // alert("onHide Callback");
        }
    });
    // $('#addition').text(parseInt($('#addition').text()) + parseInt(message.data.addition));
});

socket.on('new',function(data) {
    console.log(data);
});