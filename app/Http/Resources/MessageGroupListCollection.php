<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Carbon;

class MessageGroupListCollection extends ResourceCollection
{
    public $success = true;
    public $message = "";

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        $data = "";

        if (iterator_count($this->resource)) {
            $this->message = "Group list.";

            $data =  $this->map(function($group) {
                $date = "";

                $participants = $group->employees->map(function($participant) {
                    return [
                        'id' => $participant->id,
                        'name' => $participant->name,
                        'email' => $participant->email,
                        'phone' => $participant->phone,
                        'thumbImgPath' => $participant->thumbImgPath
                    ];
                });

                $latestMessage = "";
                $date = "";
                $created_at = "";
                if ($group->latest_message) {
                    $sender_name = "You";
                    if ($group->latest_message->sender->id != auth()->user()->id) {
                        $sender_name = $group->latest_message->sender->name;
                    }
                    $date = Carbon\Carbon::parse($group->latest_message->created_at)->diffForHumans();
                    $created_at = Carbon\Carbon::parse($group->latest_message->created_at);
                    $latestMessage = "<p><b>" . $sender_name . ": </b>" . $group->latest_message->message . "</p>";
                }

                return [
                    'id' => $group->id,
                    'name' => $group->name,
                    'message' => $latestMessage,
                    'message_date' => $date,
                    'message_created' => $created_at,
                    'participants' => $participants

                ];
            });

            $data = $data->sortByDesc('message_created')->forget('message_created')->values()->all();

            

            return $data;

        } else {
            $this->message = "No conversation started yet.";
        }
    }

    public function with($request)
    {
        return [
            'success' => $this->success,
            'message' => $this->message
        ];
    }
}
