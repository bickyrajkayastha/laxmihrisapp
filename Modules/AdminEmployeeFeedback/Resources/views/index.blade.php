@extends('admin.layouts.master')

@section('content')
<!-- @if(Session::get('success_message'))
    <div class="alert alert-success containerAlert">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
        {{ Session::get('success_message') }}
    </div>
@endif
@if(Session::get('error_message'))
    <div class="alert alert-danger containerAlert">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
        {{ Session::get('error_message') }}
    </div>
@endif -->
<div class="row">
    <div class="col">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Feedback Management
                </h3>
            </div>

            <!-- Striped Table -->
            <!--===================================================-->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
	                            <th>S.N</th>
	                            <th>Employee</th>
                                <th>Date</th>
	                            <!-- <th>Status</th> -->
	                            <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        	@forelse($feedbacks as $feedback)
                    	    <tr data-title="Feedback" data-n-id="{{ $feedback->id }}" data-body="{{ $feedback->feedback }}">
                    	        <td>{{ $loop->iteration }}</td>
                    	        <td>{{ $feedback->employee->name }}</td>
                                <td>{{ formatDate($feedback->created_at) }}</td>
                    	        <!-- <td>{!! $feedback->status ? "<span class='label label-success'>Active</span>" : "<span class='label label-default'>Inactive</span>"!!}
                                </td> -->
                                <td>
                                    <button class="btn btn-sm btn-mint feedback-send-btn" data-target="#feedback-send-modal" data-toggle="modal">
                                         view
                                    </button>
                    	            <a href="#" title="Remove" class="btn btn-sm btn-danger btn-delete"
                    	               data-toggle="modal"
                    	               data-id="{{ $feedback->id }}"
                    	               data-target="#delete-modal">
                    	                <i class="fas fa-trash"></i>
                    	            </a>
                    	        </td>
                    	    </tr>
                        	@empty
                    	    <tr>
                    	        <td colspan="5">
                    	            <em>No data available in table ...</em>
                    	        </td>
                    	    </tr>
                        	@endforelse
                        </tbody>
                    </table>
                </div>
            </div>
            <!--===================================================-->
            <!-- End Striped Table -->

        </div>
    </div>
    <!-- send feedback modal -->
    <div id="feedback-send-modal" class="modal fade" tabindex="-1" data-n-id="">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><i class="pci-cross pci-circle"></i></button>
                        <h4 class="modal-title" id="mySmallModalLabel"><i class="fas fa-comment-alt"></i> <span id="n-modal-title">title</span></h4>
                    </div>
                    <div class="modal-body">
                        <div class="body-data" style="margin-bottom: 10px;">
                            
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button data-bb-handler="cancel" data-dismiss="modal" type="button" class="btn btn-sm btn-default">Ok</button>
                    </div>
                </div>
            </div>
    </div>
    <!-- end of send feedback modal -->
    @include('admin.elements.delete-modal')
</div>
@endsection
@push('scripts')
    <script type="text/javascript">
        $(document).on('click', '#send-feedback-btn', function(event) {
            var btn = $(this);
            btn.attr('disabled',true);
            btn.html('<i class="fas fa-spinner fa-spin"></i> sending...');
            $(this).closest('form').submit();
        });

        $(document).on('click', '.feedback-send-btn', function(e) {
            e.preventDefault();
            var tr = $(this).closest('tr');
            var title = tr.data('title');
            var body = tr.data('body');
            var feedback_id = tr.data('n-id');
            $("#feedback-send-form").find('input[name="n_id"]').val(feedback_id);

            $("#feedback-send-modal").find('#n-modal-title').html(title);
            $("#feedback-send-modal").find('.modal-body>.body-data').html(body);
        });

        $(document).on('click','.btn-delete', function () {
            var route = '{{ route('admin.feedbacks.destroy', ':id') }}';

            route = route.replace(':id', $(this).attr('data-id'));

            $('#delete-modal').find('form').attr('action', route);
            $('#delete-modal').modal('show');

            $('#delete-final').click(function (e) {
                $(this).attr('disabled',true);
                $(this).html('<i class="fas fa-spinner fa-spin"></i> Please wait...');
                $(this).closest('form').submit();
            });
        });
    </script>
@endpush
