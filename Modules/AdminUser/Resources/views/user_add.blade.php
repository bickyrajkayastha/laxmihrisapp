@extends('admin.layouts.master')
@section('content')
<div class="row">
    <div class="col">
        
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Create New User
                    <span class="required_fields pull-right"><strong class="required_color">*</strong> These fields are required.</span>
                </h3>
            </div>

            <form action="{{ route('admin.user.store') }}" method="POST" class="form-horizontal form-padding" enctype="multipart/form-data">
                <div class="panel-body">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label class="col-md-2 control-label" for="admin-first-name">Full Name <span class="required_color">*</span></label>
                        <div class="col-lg-10">
                            <div class="row">
                                <div class="col-md-4">
                                    <input type="text" id="admin-first-name" name="firstname" class="form-control" value="{{ old('firstname') }}" placeholder="First Name" >
                                    <small class="help-block text-danger">{{$errors->first('firstname')}}</small>
                                </div>
                                <div class="col-md-4">
                                    <input type="text" id="admin-first-name" name="middlename" class="form-control" value="{{ old('middlename') }}" placeholder="Middle Name" >
                                    <small class="help-block text-danger">{{$errors->first('middlename')}}</small>
                                </div>
                                <div class="col-md-4">
                                    <input type="text" id="admin-last-name" name="lastname" class="form-control" value="{{ old('lastname') }}" placeholder="Last Name" >
                                    <small class="help-block text-danger">{{$errors->first('lastname')}}</small>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label" for="demo-address-input">Address</label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="address" value="{{ old('address') }}">
                        </div>
                        <label class="col-md-1 control-label" for="demo-address-input">Contact</label>
                        <div class="col-md-5">
                            <input type="text" class="form-control" name="phone" value="{{ old('phone') }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label" for="demo-address-input">Username <span class="required_color">*</span></label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="username" value="{{ old('username') }}">
                            <small class="help-block text-danger">{{$errors->first('username')}}</small>
                        </div>
                        <label class="col-md-1 control-label" for="demo-address-input">Email<span class="required_color">*</span></label>
                        <div class="col-md-5">
                            <input type="text" class="form-control" name="email" value="{{ old('email') }}">
                            <small class="help-block text-danger">{{$errors->first('email')}}</small>
                        </div>
                    </div>

                    <div class="form-group">

                        <label class="col-md-2 control-label" for="demo-address-input">Password <span class="required_color">*</span></label>
                        <div class="col-md-3">
                            <input type="password" class="form-control" name="password" value="{{ old('password') }}">
                            <small class="help-block text-danger">{{$errors->first('password')}}</small>
                        </div>

                        <label class="col-md-2 control-label" for="demo-address-input">Confirm Password <span class="required_color">*</span></label>
                        <div class="col-md-5">
                            <input type="password" class="form-control" name="re_password" value="{{ old('re_password') }}">
                            <small class="help-block text-danger">{{$errors->first('re_password')}}</small>
                        </div>

                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Group <span class="required_color">*</span></label>
                        <div class="col-md-9">
                            <div class="checkbox">
                                @if( count($groups) >0 )
                                  @foreach($groups as $g)
                                    <input id="group-{{$g->id}}" class="magic-checkbox" type="checkbox" name="groups[]" value="{{ $g->id }}" {{ old("groups[]") == $g->id ? "checked" : '' }}>
                                    <label for="group-{{$g->id}}" style="color: #000023;">{{ $g->name }}</label>
                                  @endforeach
                                @endif
                            </div>
                            <small class="help-block text-danger">{{$errors->first('groups')}}</small>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label" for="demo-contact-input">Status</label>
                        <div class="col-md-9">
                            <div class="radio">
                                <input id="status" class="magic-radio" type="radio" name="status" value="1" checked>
                                <label for="status" style="color:#000000;">Active</label>
                                <input id="status-2" class="magic-radio" type="radio" name="status" value="0">
                                <label for="status-2" style="color:#000000;">Inactive</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label"></label>
                        <div class="col-md-3">
                            <input type="submit" class="btn btn-sm btn-block btn-primary" name="submit" value="CREATE">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@stop