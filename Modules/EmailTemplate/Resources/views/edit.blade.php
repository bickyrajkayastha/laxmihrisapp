@extends('admin.layouts.master')
@section('content')
<div class="row">
	<div class="col">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Edit Template
                	<button class="btn btn-sm btn-primary pull-right panel-btn" onclick="window.history.go(-1);">
                	    <i class="fas fa-long-arrow-alt-left"></i> Back
                	</button>
                </h3>
            </div>

            <!--Horizontal Form-->
            <!--===================================================-->
            <form action="{{ route('admin.email-template.update',$template->id ) }}" method="POST" class="panel-body form-horizontal form-padding">
                @csrf
                <div class="panel-body">
					 <div class="form-group">
					     <label class="col-md-2 control-label" for="admin-first-name">Title <span class="required_color">*</span></label>
					     <div class="col-md-4">
					         <input type="text" name="title" class="form-control" value="{{ old('title') ? old('title') : $template->title }}" placeholder="Title" >
					         <small class="help-block text-danger">{{$errors->first('title')}}</small>
					     </div>
					 </div>

					 <div class="form-group">
					     <label class="col-md-2 control-label" for="admin-first-name">Subject <span class="required_color">*</span></label>
					     <div class="col-md-4">
					         <input type="text" name="subject" class="form-control" value="{{ old('subject') ? old('subject') : $template->subject }}" placeholder="Title" >
					         <small class="help-block text-danger">{{$errors->first('subject')}}</small>
					     </div>
					 </div>

					 <div class="form-group">
					     <label class="col-md-2 control-label" for="admin-first-name">Preview <span class="required_color">*</span></label>
					     <div class="col-md-9" style="height: auto;">
					         <textarea name="description" class="form-control" id="ckeditor_fullpage">{!! $template->description !!}</textarea>
					         <small class="help-block text-danger">{{$errors->first('description')}}</small>
					         <small class="help-block text-danger" style="font-weight: bold;font-size: 13px;">
					             Note : Texts inside [[ ]] brackets are the placeholders and system reserved and you cannot change or edit them.
					         </small>
					     </div>
					 </div>
                </div>
                <div class="panel-footer text-right">
                	<div class="row">
		                <div class="col-lg-11">
		                    <button class="btn btn-sm btn-mint" type="submit">Update</button>
		                </div>
                	</div>
                </div>
            </form>
            <!--===================================================-->
            <!--End Horizontal Form-->

        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
    CKEDITOR.replace('ckeditor_fullpage', { // ckeditor with full page editing : eg Email Template
        fullPage: true,
        allowedContent: true,
        extraPlugins: 'docprops',
        contentsCss: 'html {overflow:scroll;}'
    });
</script>
@endpush