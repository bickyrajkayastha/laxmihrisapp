<?php

namespace Modules\AdminEmployeeFeedback\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Helpers\Permission;
use App\Models\EmployeeFeedback;

class AdminEmployeeFeedbackController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $feedback = 
        $data['pageTitle'] = "Employee Feedbacks";
        $data['feedbacks'] = EmployeeFeedback::orderBy('created_at', 'DESC')->with('employee')->get();
        $data['breadcrumbs'] = '<li><a href="' . route('admin.feedbacks.index') . '">Employee Feedbacks</a></li>';

        return view('adminemployeefeedback::index', $data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('adminemployeefeedback::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('adminemployeefeedback::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('adminemployeefeedback::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        if (!Permission::check('delete-feedbacks'))
            return redirect()->route('admin.401');
        try {
            EmployeeFeedback::find($id)->delete();
            session()->flash('success_message', __('alerts.delete_success'));
            return redirect()->route('admin.feedbacks.index');

        } catch (\Exception $e) {
            session()->flash('error_message', __('alerts.delete_error'));
            return redirect()->route('admin.feedbacks.index');
        }
    }
}
