<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
	protected $guarded = ['id'];

	public function groupUsers()
	{
	    return $this->hasMany('App\UserGroup','group_id');
	}
}
