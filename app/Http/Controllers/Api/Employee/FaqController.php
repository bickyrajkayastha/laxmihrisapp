<?php

namespace App\Http\Controllers\Api\Employee;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Faq;

class FaqController extends Controller
{
	
	public function faqs()
	{
		$success = false;
		$message = "";
		$data = Faq::select('id', 'title', 'description')->orderBy('created_at', 'asc')->where('status', '=', 1)->get();

		if (iterator_count($data)) {
			$success = true;
			$message = "Faq list";
		} else {
			$message = "Empty list.";
		}

		return response()->json([
			'data' => $data,
			'success' => $success,
			'message' => $message
		], 200);
	}
}
