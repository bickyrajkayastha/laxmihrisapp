<?php

namespace Modules\EmailTemplate\Http\Controllers;

use App\Helpers\Setting;
use App\Models\EmailTemplate;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class EmailTemplateController extends Controller
{
    public function emailTemplate()
    {
        $data['pageTitle'] = "Email Management";
        $data['breadcrumbs'] = '<li>Email Templates</li>';
        $data['templates'] = EmailTemplate::get();

        return view('emailtemplate::index', $data);
    }

    public function edit($id)
    {
        $data['pageTitle'] = "Edit Email Template";
        $data['breadcrumbs'] = '<li><a href="' . route('admin.email.template') . '">Email Templates</a></li><li>Edit</li>';
        $data['title'] = "General Configuration - " . Setting::get('siteconfig')['system_name'];
        $data['side_nav'] = 'master_config';
        $data['side_sub_nav'] = 'email_template';

        $template = $this->getTemplateById($id);

        if(!$template){
            session()->flash('success_message',__('alerts.show_error'));

            return redirect()->route('admin.email.template');
        }

        $data['template'] = $template;

        return view('emailtemplate::edit',$data);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
           'title' => 'required',
           'subject' => 'required',
           'description' => 'required'
        ]);

        try{

            $data = $request->only('title','subject','description');

            $template = $this->getTemplateById($id);

            $template->update($data);

            session()->flash('success_message',__('alerts.update_success'));
        }catch (\Exception $e){
            session()->flash('error_message',__('alerts.update_error'));
        }

        return redirect()->back();
    }

    private function getTemplateById($id){
        return EmailTemplate::find($id);
    }
}
