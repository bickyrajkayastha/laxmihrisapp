<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameDescriptionColumnToEmployeeFeedbacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employee_feedbacks', function (Blueprint $table) {
            $table->renameColumn('description', 'feedback');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employee_feedbacks', function (Blueprint $table) {
            $table->renameColumn('feedback', 'description');
        });
    }
}
