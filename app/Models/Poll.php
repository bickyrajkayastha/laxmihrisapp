<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Poll extends Model
{
	protected $guarded = ['id'];

	protected $appends = ['total_voters'];

	public function poll_options()
	{
		return $this->hasMany(PollOption::class);
	}

	public function employees()
	{
		return $this->hasMany(EmployeePollOption::class, 'poll_id');
	}

	public function getTotalVotersAttribute()
	{
		return $this->employees->count();
	}

	public function admin()
	{
		return $this->belongsTo(Admin::class);
	}
}
