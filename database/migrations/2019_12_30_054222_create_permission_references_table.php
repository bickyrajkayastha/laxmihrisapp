<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermissionReferencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permission_references', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('permission_modules_id')->unsigned()->nullable()->default(null);
            $table->string('code',191)->nullable()->default(null)->index();
            $table->string('short_desc',255)->nullable()->default(null);
            $table->text('description')->nullable()->default(null);
            $table->timestamps();

            $table->foreign('permission_modules_id')
                ->references('id')->on('permission_modules')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permission_references');
    }
}
