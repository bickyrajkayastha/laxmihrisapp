<?php

namespace Modules\AdminNotification\Http\Controllers;

use App\Helpers\Permission;
use App\Models\Notification;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use App\Jobs\SendNotificationJob;
use App\Models\Employee;
use Image;

class AdminNotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $data['pageTitle'] = "Notification Management";
        $data['notifications'] = Notification::paginate(20);
        $data['breadcrumbs'] = '<li><a href="' . route('admin.notifications') . '">Notification Management</a></li>';

        return view('adminnotification::index', $data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $data['breadcrumbs'] = '<li><a href="' . route('admin.notifications') . '">Notification Management</a></li><li>Add</li>';

        return view('adminnotification::create', $data);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'body' => 'required',
        ]);

        try {

            $notification = new Notification;
            $notification->title = $request->title;
            $notification->body = $request->body;
            $notification->notification_type = 1;

            $notification->save();
            session()->flash('success_message', __('alerts.create_success'));
            return redirect()->route('admin.notifications');

        } catch (\Exception $e) {
            session()->flash('error_message', __('alerts.create_error'));
            return redirect()->route('admin.notifications');
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('adminnotification::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $notification = Notification::find($id);

        if (!$notification) {
            session()->flash('error_message', 'Notification not available for editing.');
            return redirect()->back();
        }

        $data['notification'] = $notification;
        $data['breadcrumbs'] = '<li><a href="' . route('admin.notifications') . '">Notification Management</a></li><li>Edit</li>';

        return view('adminnotification::edit', $data);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $notification = Notification::find($id);
        $validation = 'nullable';

        $request->validate([
            'title' => 'required',
            'body' => 'required'
        ]);

        try {

            $notification->title = $request->title;
            $notification->body = $request->body;
            $notification->notification_type = 1;

            $notification->save();
            session()->flash('success_message', __('alerts.update_success'));
            return redirect()->route('admin.notifications');

        } catch (\Exception $e) {
            session()->flash('error_message', __('alerts.update_error'));
            return redirect()->route('admin.notifications');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        if (!Permission::check('delete-notifications'))
            return redirect()->route('admin.401');


        try {

            Notification::find($id)->delete();
            session()->flash('success_message', __('alerts.delete_success'));
            return redirect()->route('admin.notifications');

        } catch (\Exception $e) {

            session()->flash('error_message', __('alerts.delete_error'));
            return redirect()->route('admin.notifications');
        }
    }

    public function sendNotification(Request $request)
    {
        $route = 'admin.notifications';
        try {
            $notification = Notification::find($request->n_id);
            $data = [
                'title' => $notification->title,
                'body' => $notification->body
            ];

            // attach notification to all the employees.
            if (isset($request->employees)) {
                $employee_ids = $request->employees;
                $route = 'admin.notifications.group';
            } else {
                $employee_ids = \App\Models\Employee::get()->pluck('id');
            }

            $data['employees'] = $employee_ids;
            // if (iterator_count($notification->employees)) {
            //     $notification->employees()->detach();
            // }
            $notification->employees()->attach($employee_ids);
            // end of attachment
            SendNotificationJob::dispatch($data);

            session()->flash('success_message', 'Notification has been sent.');
            return redirect()->route($route);

        } catch (\Exception $e) {
            session()->flash('error_message', 'Could not send. Please try again.');
            return redirect()->route($route);
        }
    }

    public function groupNotification()
    {
        $data['pageTitle'] = "Notification Management";
        $data['employees'] = Employee::where('status', 1)->get();
        $data['notifications'] = Notification::where('notification_type', 2)->paginate(20);
        $data['breadcrumbs'] = '<li><a href="' . route('admin.notifications') . '">Group Notification Management</a></li>';

        return view('adminnotification::group_notification', $data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function createGroup()
    {
        $data['breadcrumbs'] = '<li><a href="' . route('admin.notifications') . '">Group Notification Management</a></li><li>Add</li>';
        return view('adminnotification::create_group', $data);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function storeGroup(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'body' => 'required',
        ]);

        try {

            $notification = new Notification;
            $notification->title = $request->title;
            $notification->body = $request->body;
            $notification->notification_type = 2;

            $notification->save();
            session()->flash('success_message', __('alerts.create_success'));

        } catch (\Exception $e) {
            session()->flash('error_message', __('alerts.create_error'));
        }
        return redirect()->route('admin.notifications.group');
    }
}
