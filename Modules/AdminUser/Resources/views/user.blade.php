@extends('admin.layouts.master')
@section('content')

<div class="row">
    <div class="col">
                        @include('adminuser::header_tab')
                    <div class="panel">


                        <div class="panel-body">
                            <div class="row textright searchWrapperPosition">
                                <div class="col">
                                    <span class="text-main text-semibold">
                                       Users Management
                                    </span>
                                    <a href="{{ route('admin.user.add') }}" style="float: right;" class="btn btn-sm btn-sm btn-info addBtnAdminPanel"><i
                                                class="fa fa-plus"></i> Add New</a>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col">
                                    
                                    <table class="table table-striped table-bordered adminMgmtTable">
                                        <thead>
                                        <tr>
                                            <th>S.N</th>
                                            <th>User Type</th>
                                            <th>Full Name</th>
                                            <th>Username</th>
                                            <th>Email</th>
                                            <th>Phone</th>
                                            <th class="text-center">Status</th>
                                            <th>Operation</th>
                                        </tr>
                                        </thead>

                                        <tbody>

                                        @if( count($users) > 0 )
                                            <?php $i = 1; ?>
                                            @foreach($users as $ur)
                                                <tr>
                                                    <td align="center">{{ $i++ }} </td>
                                                    <td>
                                                        <?php
                                                        $grps_str = "";
                                                        $counter = 1;
                                                        ?>
                                                        @if( isset($ur->user_group) && count($ur->user_group) >0 )
                                                            @foreach($ur->user_group as $ugrp)
                                                                <?php
                                                                if ($counter != 1)
                                                                    $grps_str .= " | ";

                                                                $grps_str .= isset($ugrp->group_detail->name) ? $ugrp->group_detail->name : "";

                                                                $counter++;
                                                                ?>
                                                            @endforeach
                                                        @endif
                                                        <strong>{{ $grps_str }}</strong>
                                                    </td>
                                                    <td>{{ $ur->first_name." ".$ur->middle_name." ".$ur->last_name }}</td>
                                                    <td>{{ $ur->username }}</td>
                                                    <td>{{ $ur->email }}</td>
                                                    <td>{{ $ur->phone }}</td>
                                                    <td align="center">
                                                        {!! $ur->status ? "<span class='label label-success'>Active</span>" : "<span class='label label-danger'>Inactive</span>"!!}
                                                    </td>
                                                    <td width="150">
                                                        <a href="{{ route('admin.user.edit',[$ur->id]) }}"
                                                           class="btn btn-sm btn-info adminMgmtTableBtn" title="Edit User Group">
                                                            <i class="fa fa-edit"></i>
                                                        </a>

                                                        <a href="#" class="btn btn-sm btn-primary adminMgmtTableBtn btnResetPass"
                                                           title="Reset Password"
                                                           data-user-id="{{ $ur->id }}"
                                                           data-user-name="{{ $ur->first_name." ".$ur->middle_name." ".$ur->last_name }}"
                                                           data-user-email="{{ $ur->email }}">
                                                            <i class="fas fa-key"></i>
                                                        </a>


                                                        @if ( isset( $ur->user_is_superadmin ) && $ur->user_is_superadmin->count() == 0 )
                                                            <a href="#" class="btn btn-sm btn-danger btn-delete"
                                                               data-id="{{ $ur->id }}"
                                                               data-toggle="modal"
                                                               data-target="#delete-modal">
                                                                <i class="fa fa-trash"></i>
                                                            </a>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td align="center" colspan="10">
                                                    <em>No data available in table...</em>
                                                </td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
    </div>
</div>

    <!-- Modal reset Password -->
    <div id="reset-password-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><i class="pci-cross pci-circle"></i>
                    </button>
                    <h4 class="modal-title" id="myLargeModalLabel">Reset Password</h4>
                </div>

                <form class="form-horizontal" action="{{ route('admin.user.password-reset-user') }}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="_user_id" id="_user_id" value="">
                    <div class="modal-body">
                        <div class="bootbox-body">
                            <div class="row">
                                <div class="col-md-12">


                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="name">Customer Name: </label>
                                        <div class="col-md-6" style="text-align: left;padding: 7px 0 0 0;">
                                            <label id="_user_name" style="font-weight: 600;"></label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="name">Email: </label>
                                        <div class="col-md-6" style="text-align: left;padding: 7px 0 0 0;">
                                            <label id="_user_email" style="font-weight: 600;"></label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="name">Reset Password</label>
                                        <div class="col-md-6">
                                            <input name="st_user_password" type="password" class="form-control input-md"
                                                   value="" required>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-sm btn-primary">Reset</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
    @include('admin.elements.delete-modal')
@stop

@push('scripts')
<script type="text/javascript">
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();

        $('.btnResetPass').click(function () {

            var user_id = $(this).attr('data-user-id');
            if (user_id == '' || user_id == null) {
                return false;
            }
            var user_name = $(this).attr('data-user-name');
            var user_email = $(this).attr('data-user-email');

            $('#_user_id').val(user_id);
            $('#_user_name').html(user_name);
            $('#_user_email').html(user_email);

            $('#reset-password-modal').modal({show: true});
        });
    });

    $(document).on('click','.btn-delete', function () {
        var route = '{{ route('admin.user.destroy', ':id') }}';

        route = route.replace(':id', $(this).attr('data-id'));

        $('#delete-modal').find('form').attr('action', route);
        $('#delete-modal').modal('show');

        $('#delete-final').click(function (e) {
            $(this).attr('disabled',true);
            $(this).html('<i class="fas fa-spinner fa-spin"></i> Please wait...');
            $(this).closest('form').submit();
        });
    });
</script>
@endpush