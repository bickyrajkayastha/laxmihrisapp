@extends('admin.layouts.master')
@push('css')
<link href="{{ asset('assets/plugins/switchery/switchery.min.css') }}" rel="stylesheet">
@endpush
@section('content')
<div class="row">
    <div class="col">
        <div class="panel">
            <div class="panel-body">
                <div class="row textright searchWrapperPosition">
                    <div class="col">
                        <div class="pageHeading">
                            <span class="pageTitle">
                               {{ $message_group->name }} | Participants
                            </span>
                        </div>
                    </div>
                    <hr>
                </div>
                <div class="row">
                    <div class="col">
                        <table class="table table-striped table-bordered adminMgmtTable">
                            <thead>
                            <tr>
                                <th>S.N</th>
                                <th>Participants</th>
                                <th>Operation</th>
                            </tr>
                            </thead>

                            <tbody>

                                <tr>
                                    <form action="{{ route('admin.message-groups.employees.store') }}"
                                          class="panel-body form-horizontal form-padding" method="post">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="message_group_id" value="{{ $message_group->id }}">
                                        <td align="center"></td>

                                        <td>
                                            <select name="employee_id" class="form-control" id="">
                                                <option value="">Select an employee</option>
                                                @foreach($all_employees as $employee)
                                                <option value="{{ $employee->id }}" {{ (old('employee_id') == $employee->id)?'selected' : '' }}>{{ $employee->name }}</option>
                                                @endforeach
                                            </select>
                                            <small class="help-block text-danger">{{$errors->first('employee_id')}}</small>
                                        </td>

                                        <!-- <td class="text-center">
                                            <div class="radio">
                                                <input id="status" class="magic-radio" type="radio" name="status" value="1"
                                                       checked>
                                                <label for="status" style="color:#000000;">Active</label>
                                                <input id="status-2" class="magic-radio" type="radio" name="status" value="0">
                                                <label for="status-2" style="color:#000000;">Inactive</label>
                                            </div>
                                        </td> -->

                                        <td>
                                            <input type="submit" class="btn btn-sm btn-block btn-primary" name="submit" value="CREATE">
                                        </td>

                                    </form>
                                </tr>
                                @forelse($participants as $employee)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>
                                        ​<picture>
                                          <img src="{{ $employee->thumbImgPath }}" class="img-fluid" width="40px" height="40px" alt="{{ $employee->name }}">
                                        </picture>
                                        {{ $employee->name }}
                                    </td>
                                    <!-- <td>
                                        !! $employee->status ? "<span class='label label-success'>Active</span>" : "<span class='label label-danger'>Inactive</span>"!!}
                                        <form action="{{ route('admin.message-groups.employees.update-status', ['id' => $employee->pivot->id]) }}" method="POST">
                                            @csrf
                                            <select name="status" class="form-control statusSelect" data-id="{{ $employee->id }}">
                                                <option value="0" {{ ($employee->pivot->status == 0)?'selected':'' }}>Inactive</option>
                                                <option value="1" {{ ($employee->pivot->status == 1)?'selected':'' }}>Active</option>
                                            </select>
                                        </form>
                                    </td> -->
                                    <td>
                                        <a href="#" title="Remove" class="btn btn-sm btn-danger btn-delete"
                                           data-toggle="modal"
                                           data-id="{{ $employee->pivot->id }}"
                                           data-target="#delete-modal">
                                            <i class="fas fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="5">
                                        <em>No data available in table ...</em>
                                    </td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
    @include('admin.elements.delete-modal')
@stop

@push('scripts')
    <script src="{{ asset('assets/plugins/switchery/switchery.min.js') }}"></script>
    <script>
        $(document).on('click', '.btn-delete', function () {
            var route = '{{ route("admin.message-groups.employees.destroy", ":id") }}';

            route = route.replace(':id', $(this).attr('data-id'));

            $('#delete-modal').find('form').attr('action', route);
            $('#delete-modal').modal('show');

            $('#delete-final').click(function (e) {
                $(this).attr('disabled', true);
                $(this).html('<i class="fas fa-spinner fa-spin"></i> Please wait...');
                $(this).closest('form').submit();
            });
        });

        $(".statusSelect").on('change', function(event) {
            var e = $(this);
            e.closest('form').submit();
        });
    </script>
@endpush