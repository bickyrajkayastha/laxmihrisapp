<?php

if ( !function_exists('formatDate') ) {
    function formatDate($date)
    {
    	if (isset($date) && !empty($date)) {
	    	return date('jS M, Y', strtotime($date));
    	}

    	return "--";
    }
}

if ( !function_exists('formatTime') ) {
    function formatTime($time)
    {
    	if (isset($time) && !empty($time)) {
	    	return date('g:i A', strtotime($time));
    	}

    	return "--";
    }
}

if (!function_exists('formatSizeUnits')) {
    function formatSizeUnits($bytes)
    {
        if ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        }
        elseif ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1)
        {
            $bytes = $bytes . ' byte';
        }
        else
        {
            $bytes = '0 bytes';
        }

        return $bytes;
    }
}

if (!function_exists('getFileType')) {
    function getFileType($ext)
    {

        switch ($ext) {
            case 'jpg':
            case 'jpeg':
            case 'png':
                return "image";
                break;
            case 'pdf':
                return "file";
                break;
            case 'mp4':
            case 'mpeg':
                return "video";
                break;
            case 'ppt':
            case 'pptx':
                return "file";
                break;
            case 'xlsx':
            case 'xls':
                return "file";
                break;

            default:
                return "file";
                break;
        }
    }
}
