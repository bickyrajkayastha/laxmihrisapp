<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
	// type = [1= text/page, 2= image, 3= video, 4= power point, 5= page]
	protected $guarded = ['id'];

    protected $appends = ['linkUrl', 'typeName'];

    public function getLinkUrlAttribute()
    {
    	$path = url('uploads/contents/');
    	if ($this->type != 1) {
	    	return $path . '/' . $this->id . '/' . $this->type_file_name;
    	} else {
    		return "";
    	}
    }

    public function getTypeNameAttribute()
    {
        switch ($this->type) {
            case '1':
                return 'Text';
                break;
            case '2':
                return 'Image';
                break;
            case '3':
                return 'Video';
                break;
            case '4':
                return 'Power point';
                break;
            case '5':
                return 'PDF';
                break;

            default:
                return '';
                break;
        }
    }

}
