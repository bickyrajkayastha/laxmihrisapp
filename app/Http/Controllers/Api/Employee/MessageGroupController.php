<?php

namespace App\Http\Controllers\Api\Employee;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\MessageGroup;
use App\Http\Resources\MessageGroupListCollection;
use App\Http\Resources\GroupMessageMemberCollection;

class MessageGroupController extends Controller
{
	/**
	 * @SWG\Get(
	 *     path="/employee/message-group-list",
	 *     description="get group list in message",
	 *     tags={"Employee Apis"},
	 *     security={{"bearerAuth":{}}},
	 *     @SWG\Response(
	 *         response=200,
	 *         description="status, message",
	 *     ),
	 *     @SWG\Response(
	 *         response=422,
	 *         description="status, message"
	 *     )
	 * )
	 */
	public function getMessageGroupList()
	{
		$user = auth()->user();
		$groups = $user->active_message_groups()->paginate(20);

		// return response()->json([
		// 	'data' => $groups
		// ]);
		// \Log::info($groups);
		return new MessageGroupListCollection($groups);
	}

	/**
	 * @SWG\Get(
	 *     path="/employee/message-group/{id}/members",
	 *     description="get conversations",
	 *     tags={"Employee Apis"},
	 *     security={{"bearerAuth":{}}},
	 *     @SWG\Parameter(
	 *         in="path",
	 *         name="id",
	 *         type="number",
	 *         description="id of the employee.",
	 *         required=true,
	 *     ),
	 *     @SWG\Response(
	 *         response=200,
	 *         description="status, message",
	 *     ),
	 *     @SWG\Response(
	 *         response=422,
	 *         description="status, message"
	 *     )
	 * )
	 */
	public function getMessageGroupMembers($id)
	{
		$group = MessageGroup::find($id);

		return new GroupMessageMemberCollection($group->employees);
	}
}
