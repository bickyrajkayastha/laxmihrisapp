<?php

use Illuminate\Database\Seeder;

class EmailTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$script = getcwd().'/database/seeds/db/email_template.sql';

    	\Illuminate\Support\Facades\DB::unprepared(file_get_contents($script));

    	$this->command->info('Email Template Seeded');
    }
}
