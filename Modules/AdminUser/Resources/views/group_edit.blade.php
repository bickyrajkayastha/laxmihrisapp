@extends('admin.layouts.master')
@section('content')
<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">
            Update User Group
            <span class="required_fields" style="float: right;"><strong class="required_color">*</strong> These fields are required.</span>
        </h3>
    </div>

    <form action="{{ route('admin.user.groups.update') }}" method="POST" class="form-horizontal form-padding" enctype="multipart/form-data">
        <div class="panel-body">
            {{ csrf_field() }}
            <input type="hidden" name="_id" value="{{ $group_details->id }}">


            <div class="form-group">
                <label class="col-md-2 control-label" for="demo-address-input">Group Name <span class="required_color">*</span></label>
                <div class="col-md-10">
                    <input type="text" class="form-control" name="name" value="{{ old('name') ? old('name') : $group_details->name }}">
                    <small class="help-block text-danger">{{$errors->first('name')}}</small>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label" for="demo-contact-input">Status</label>
                <div class="col-md-10">
                    <div class="radio">
                        <input id="status" class="magic-radio" type="radio" name="status" value="1" {{ $group_details->status == "1" ? "checked" : "" }}>
                        <label for="status" style="color:#000000;">Active</label>
                        <input id="status-2" class="magic-radio" type="radio" name="status" value="0" {{ $group_details->status == "0" ? "checked" : "" }}>
                        <label for="status-2" style="color:#000000;">Inactive</label>
                    </div>
                </div>
            </div>



            <div class="form-group">
                <label class="col-md-2 control-label"></label>
                <div class="col-md-2">
                    <input type="submit" class="btn btn-sm btn-block btn-primary" name="submit" value="UPDATE">
                </div>
            </div>
        </div>
    </form>
</div>
@stop