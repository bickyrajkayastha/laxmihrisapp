<?php

namespace Modules\AdminContent\Http\Controllers;

use App\Helpers\Permission;
use App\Models\Content;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Image;

class AdminContentController extends Controller
{
    private $indexRoute;

    public function __construct()
    {
        $this->indexRoute = route('admin.contents');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $data['pageTitle'] = "Page Management";
        $data['contents'] = Content::where('id', '!=', 1)->get();
        $data['breadcrumbs'] = '<li><a href="' . route('admin.contents') . '">Page Management</a></li>';

        return view('admincontent::index', $data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $data['pageTitle'] = "Add Page";
        $data['breadcrumbs'] = '<li><a href="' . route('admin.contents') . '">Page Management</a></li><li>Add</li>';

        return view('admincontent::create', $data);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $validation = 'nullable';
        if ($request->type != 1) {
            switch ($request->type) {
                case '2':
                    $validation = 'required|mimes:jpeg,jpg,png,gif';
                    break;
                case '3':
                    $validation = 'required|mimes:m4v,avi,flv,mp4,mov';
                    break;
                case '4':
                    $validation = 'required|mimes:ppt,pptx';
                    break;
                case '5':
                    $validation = 'required|mimes:pdf';
                    break;
                default:
                    break;
            }
        }

        $request->validate([
            'title' => 'required',
            'status' => 'required',
            'body' => 'required',
            'type_file' => $validation
        ]);

        try {

            $content = new Content;
            $content->title = $request->title;
            $content->status = $request->status;
            $content->type = $request->type;
            $content->body = $request->body;

            if ($request->hasFile('type_file')) {
                $imageName = $request->type_file->getClientOriginalName();
                $imageSize = $request->type_file->getClientSize();
                $imageType = $request->type_file->getClientOriginalExtension();
                $imageNameUniqid = md5($imageName . microtime()) . '.' . $imageType;
                $imageName = $imageNameUniqid;

                $content->type_file_name = $imageName;
            }


            if ($content->save()) {
                if ($request->hasFile('type_file')) {

                    $image_quality = 100;
                    $path = 'contents/';
                    Storage::disk('uploads')->putFileAs($path . $content->id, $request->type_file, $imageName);
                    // Storage::putFileAs($path . $content->id, $request->type_file, $imageName);
                    $file = $path . $content->id . '/' . $imageName;

                    if (!Storage::disk('uploads')->exists($file)) {
                        $content->type_file_name = "";
                        $content->save();
                    }
                }
            }

            session()->flash('success_message', __('alerts.create_success'));
            return redirect()->route('admin.contents');

        } catch (\Exception $e) {
            // Log::info($e->getMessage());
            session()->flash('error_message', __('alerts.create_error'));
            return redirect()->route('admin.contents');
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('admincontent::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $data['pageTitle'] = "Edit Page";
        $content = Content::find($id);

        if (!$content) {
            session()->flash('error_message', 'Content not available for editing.');
            return redirect()->back();
        }

        $data['content'] = $content;
        $data['breadcrumbs'] = '<li><a href="' . route('admin.contents') . '">Page Management</a></li><li>Edit</li>';

        return view('admincontent::edit', $data);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $content = Content::find($id);
        $validation = 'nullable';

        if ($request->type != 1) {
            if ($content->type_file_name == "") {
                switch ($request->type) {
                    case '2':
                        $validation = 'required|mimes:jpeg,jpg,png,gif';
                        break;
                    case '3':
                        $validation = 'required|mimes:m4v,avi,flv,mp4,mov';
                        break;
                    case '4':
                        $validation = 'required|mimes:ppt,pptx';
                        break;
                    default:
                        break;
                }
            }
        }

        $request->validate([
            'title' => 'required',
            'status' => 'required',
            'body' => 'required',
            'type_file' => $validation
        ]);

        try {

            $content->title = $request->title;
            $content->status = $request->status;
            $content->type = $request->type;
            $content->body = $request->body;

            if ($request->hasFile('type_file')) {
                $imageName = $request->type_file->getClientOriginalName();
                $imageSize = $request->type_file->getClientSize();
                $imageType = $request->type_file->getClientOriginalExtension();
                $imageNameUniqid = md5($imageName . microtime()) . '.' . $imageType;
                $imageName = $imageNameUniqid;

                $content->type_file_name = $imageName;
            }

            if ($content->save()) {
                if ($request->hasFile('type_file')) {

                    $image_quality = 100;
                    $path = 'contents/';
                    Storage::disk('uploads')->deleteDirectory($path . $id);
                    Storage::disk('uploads')->putFileAs($path . $content->id, $request->type_file, $imageName);
                    // Storage::putFileAs($path . $content->id, $request->type_file, $imageName);
                    $file = $path . $content->id . '/' . $imageName;

                    if (!Storage::disk('uploads')->exists($file)) {
                        $content->type_file_name = "";
                        $content->save();
                    }
                }
            }

            session()->flash('success_message', __('alerts.update_success'));
            return redirect()->route('admin.contents');

        } catch (\Exception $e) {
            session()->flash('error_message', __('alerts.update_error'));
            return redirect()->route('admin.contents');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        if (!Permission::check('delete-contents'))
            return redirect()->route('admin.401');


        try {

            $content = Content::find($id);
            if($content->status == 1){
                session()->flash('error_message', __('alerts.delete_error_active'));
                return redirect()->to($this->indexRoute);
            }

            $content->delete();
            $path = 'contents/';
            Storage::disk('uploads')->deleteDirectory($path . $id);
            session()->flash('success_message', __('alerts.delete_success'));
            return redirect()->route('admin.contents');

        } catch (\Exception $e) {

            session()->flash('error_message', __('alerts.delete_error'));
            return redirect()->route('admin.contents');
        }
    }

    public function welcomeNote()
    {
        $data['pageTitle'] = "Welcome Note";
        $content = Content::find(1);

        if (!$content) {
            session()->flash('error_message', 'Content not available for editing.');
            return redirect()->back();
        }

        $data['content'] = $content;
        $data['breadcrumbs'] = '<li>Welcome Note</li>';

        return view('admincontent::welcome-note', $data);

    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function updateWelcome(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'body' => 'required',
        ]);

        try {

            $content = Content::find(1);

            $content->title = $request->title;
            $content->body = $request->body;

            $content->save();

            session()->flash('success_message', __('alerts.update_success'));
            return redirect()->back();

        } catch (\Exception $e) {
            session()->flash('error_message', __('alerts.update_error'));
            return redirect()->back();
        }
    }
}
