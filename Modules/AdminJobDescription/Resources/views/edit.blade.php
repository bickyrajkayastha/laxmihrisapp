@extends('admin.layouts.master')
@section('content')

<div class="row">
    <div class="col">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Edit Job Description
                    <a href="{{ route('admin.job-descriptions.index') }}" class="btn panel-btn btn-sm btn-primary pull-right"style="margin-top: 15px; margin-right: 15px;"> <i class="fas fa-long-arrow-alt-left"></i> Back</a>
                </h3>
            </div>
            <form class="form-horizontal" action="{{ route('admin.job-descriptions.update',$job->id) }}"
                  method="POST" enctype="multipart/form-data">
                @csrf
                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-name">Name <span
                                    class="required_color">*</span></label>
                        <div class="col-sm-10">
                            <input type="text" placeholder="Name" name="name" value="{{ old('name',$job->name) }}"
                                   id="input-name" class="form-control" required="required">
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                        <label class="col-md-2 control-label">Publish</label>
                        <div class="col-md-9">
                            <div class="radio">

                                <!-- Inline radio buttons -->
                                <input id="input-status" class="magic-radio" type="radio" name="status" value="1" {{ $job->status ? 'checked' :'' }}>
                                <label for="input-status">Yes</label>

                                <input id="input-status-2" class="magic-radio" value="0" type="radio" name="status" {{ $job->status == 0 ? 'checked' : '' }}>
                                <label for="input-status-2">No</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                        <label class="col-md-2 control-label" for="input-description">Description <span
                                    class="required_color">*</span></label>
                        <div class="col-md-10">
                            <textarea name="description" id="input-description" cols="30" rows="10" class="form-control ckeditor">{{ old('description',$job->description) }}</textarea>
                            @if($errors->has('description'))
                            <small class="help-block text-danger">{{ $errors->first('description')}}</small>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-2 col-xs-12 col-md-12 pull-right">
                            <button class="btn btn-block btn-primary" type="submit">Update</button>
                        </div>
                    </div>
                </div>
            </form>
            <!--===================================================-->
            <!--End Hover Rows-->
        </div>
    </div>
</div>
@endsection
