<?php

use Illuminate\Database\Seeder;

class ModuleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        \Illuminate\Support\Facades\DB::table('groups')->truncate();
        \Illuminate\Support\Facades\DB::table('user_group')->truncate();
        \Illuminate\Support\Facades\DB::table('permission_modules')->truncate();
        \Illuminate\Support\Facades\DB::table('permission_references')->truncate();
        \Illuminate\Support\Facades\DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $settingModules = [
            'General Setting' , 'Social Media', 'Meta Setting', 'Image Setting'
        ];

        $generalPM = \App\Models\PermissionModule::create([
            'name' => 'Master Configuration',
            'status' => 1
        ]);

        foreach ($settingModules as $settingModule){
            \App\Models\PermissionReference::create([
                'permission_modules_id' => $generalPM->id,
                'code' => implode('-',explode(' ',strtolower($settingModule))),
                'short_desc' => $settingModule,
                'description' => $settingModule
            ]);
        }

        $modules = [
            'Users',
            'Groups',
            'Contents',
            'Employees',
            'Teams',
            'Faqs',
            'Notifications',
            'Events',
            'Polls',
            'Courses'
        ];

        $submodules = [
            'view',
            'create',
            'show',
            'edit',
            'delete',
        ];


        foreach ($modules as $module) {
            $permissionModule = \App\Models\PermissionModule::create([
                'name' => $module,
                'status' => 1
            ]);

            foreach ($submodules as $submodule){
                \App\Models\PermissionReference::create([
                    'permission_modules_id' => $permissionModule->id,
                    'code' => $submodule.'-'.strtolower($module),
                    'short_desc' => ucfirst($submodule).' '.$module,
                    'description' => ucfirst($submodule).' '.$module
                ]);
            }
        }
    }
}
