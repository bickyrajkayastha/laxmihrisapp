<?php

namespace App\Http\Controllers\Employee;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Message;
use Illuminate\Support\Facades\Log;
use App\Events\PrivateMessageEvent;
use Auth;

class MessageController extends Controller
{
	public function sendMessage(Request $request)
	{
		$sender_id = Auth::guard('admin')->user()->id;
		$receiver_id = 2;

		$message = new Message;
		$message->message = $request->message;

		if ($message->save()) {
			try {
				// $message->employees()->attach($sender_id, ['receiver_id' => $receiver_id]);
				$message->employees()->attach(3, ['receiver_id' => 9]);
				$data = [];
				$data['sender_id'] = 3;
				$data['receiver_id'] = 9;
				$data['message'] = $message->message;

				event(new PrivateMessageEvent($data));
			} catch (\Exception $e) {
				Log::info($e->getMessage());
				$message->delete();
			}
		}
	}
}
