@extends('admin.layouts.master')

@push('css')
<style>
	.employee-detail-block tr>td:first-child {
		width: 150px !important;
	}

	.employee-detail-block thead>tr>th {
		font-size: 13px !important;
	}

	.children-panel .panel-title {
		font-size: 13px !important;
	}

	/*tab scroll*/
	.tab-wrapper {
	    position:relative;
	    margin:0 auto;
	    overflow:hidden;
		padding:5px;
	  	height:50px;
	}

	.list {
	    position:absolute;
	    left:0px;
	    top:0px;
	  	min-width:3000px;
	  	margin-left:12px;
	    margin-top:0px;
	}

	.list li{
		display:table-cell;
	    position:relative;
	    text-align:center;
	    cursor:grab;
	    cursor:-webkit-grab;
	    color:#efefef;
	    vertical-align:middle;
	}

	.scroller {
	  text-align:center;
	  cursor:pointer;
	  display: block !important;
	  padding:7px;
	  padding-top:11px;
	  white-space:no-wrap;
	  vertical-align:middle;
	  background-color:#fff;
	}

	.scroller-right{
	  float:right;
	}

	.scroller-left {
	  float:left;
	}
	/*end of tab scroll*/
</style>
@endpush
@section('content')
<div class="row">
    <div class="col">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">{{ $employee['name'] }}
                	<small>Employee Detail</small>
                </h3>
            </div>

            <!-- Striped Table -->
            <!--===================================================-->
            <div class="panel-body">
            	<div class="tab-base">
		            <!--Nav Tabs-->
		            <div class="scroller scroller-left"><i class="glyphicon glyphicon-chevron-left"></i></div>
		            <div class="scroller scroller-right"><i class="glyphicon glyphicon-chevron-right"></i></div>
		            <div class="tab-wrapper">
			            <ul class="nav nav-tabs list">
			                <li class="active">
			                    <a data-toggle="tab" href="#demo-lft-tab-1" aria-expanded="true" style="color: #000000;font-weight: bold;">Personal Detail <!-- <span class="badge badge-purple">27</span> --></a>
			                </li>
			                <li class="">
			                    <a data-toggle="tab" href="#demo-lft-tab-2" aria-expanded="false" style="color: #000000;font-weight: bold;">Address</a>
			                </li>
			                <li class="">
			                    <a data-toggle="tab" href="#demo-lft-tab-3" aria-expanded="false" style="color: #000000;font-weight: bold;">Family Details</a>
			                </li>
			                <li class="">
			                    <a data-toggle="tab" href="#demo-lft-tab-4" aria-expanded="false" style="color: #000000;font-weight: bold;">Children</a>
			                </li>
			                <li class="">
			                    <a data-toggle="tab" href="#demo-lft-tab-5" aria-expanded="false" style="color: #000000;font-weight: bold;">Work Experiences</a>
			                </li>
			                <li class="">
			                    <a data-toggle="tab" href="#demo-lft-tab-6" aria-expanded="false" style="color: #000000;font-weight: bold;">Relatives</a>
			                </li>
			                <li class="">
			                    <a data-toggle="tab" href="#demo-lft-tab-7" aria-expanded="false" style="color: #000000;font-weight: bold;">Relatives On Bank</a>
			                </li>
			                <li class="">
			                    <a data-toggle="tab" href="#demo-lft-tab-8" aria-expanded="false" style="color: #000000;font-weight: bold;">Family Loans</a>
			                </li>
			                <li class="">
			                    <a data-toggle="tab" href="#demo-lft-tab-9" aria-expanded="false" style="color: #000000;font-weight: bold;">Job Description</a>
			                </li>
			            </ul>
		            </div>
		
		            <!--Tabs Content-->
		            <div class="tab-content">
		                <div id="demo-lft-tab-1" class="tab-pane fade active in">
		                    <p class="text-main text-semibold">Personal Information</p>
		                    <!-- <p class="text-main text-semibold">First Tab Content</p>
		                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p> -->
		                    <div class="table-responsive">
			                    <table class="table table-bordered table-striped employee-detail-block">
			                        <!-- <thead>
			                            <tr>
			                                <th>Invoice</th>
			                                <th>User</th>
			                                <th>Amount</th>
			                            </tr>
			                        </thead> -->
			                        <tbody>
			                            <tr>
			                                <td>Full Name</td>
			                                <td> {{ $employee['employee_personal_detail']['full_name'] ?? '--' }}</td>
			                            </tr>
			                            <tr>
			                                <td>Email</td>
			                                <td> {{ $employee['email'] ?? '--' }}</td>
			                            </tr>
			                            <tr>
			                                <td>Gender</td>
			                                <td> {{ $employee['employee_personal_detail']['genderType'] ?? '--' }}</td>
			                            </tr>
			                            <tr>
			                                <td>Date of Birth</td>
			                                <td> {{ formatDate($employee['employee_personal_detail']['dob']) }}</td>
			                            </tr>
			                            <tr>
			                                <td>Nationality at Birth</td>
			                                <td> {{ $employee['employee_personal_detail']['nationality_at_birth'] ?? '--' }}</td>
			                            </tr>
			                            <tr>
			                                <td>Present Nationality</td>
			                                <td> {{ $employee['employee_personal_detail']['present_nationality'] ?? '--' }}</td>
			                            </tr>
			                            <tr>
			                                <td>Citizenship no.</td>
			                                <td> {{ $employee['employee_personal_detail']['citizenship_no'] ?? '--' }}</td>
			                            </tr>
			                            <tr>
			                                <td>Citizenship issuing office</td>
			                                <td> {{ $employee['employee_personal_detail']['citizenship_issuing_office'] ?? '--' }}</td>
			                            </tr>
			                            <tr>
			                                <td>Driving License no.</td>
			                                <td> {{ $employee['employee_personal_detail']['driving_license_no'] ?? '--' }}</td>
			                            </tr>
			                            <tr>
			                                <td>Status</td>
			                    	        <td> {!! $employee['status'] ? "<span class='label label-success'>Active</span>" : "<span class='label label-default'>Inactive</span>"!!}</td>
			                            </tr>
			                        </tbody>
			                    </table>
			                </div>
		                </div>
		                <div id="demo-lft-tab-2" class="tab-pane fade">
		                    <p class="text-main text-semibold">Permanent Address</p>

    	                    <table class="table table-bordered table-striped employee-detail-block">
    	                        <!-- <thead>
    	                            <tr>
    	                                <th>Invoice</th>
    	                                <th>User</th>
    	                                <th>Amount</th>
    	                            </tr>
    	                        </thead> -->
    	                        <tbody>
		                            <tr>
		                                <td>House no.</td>
		                                <td> {{ $employee['employee_permanent_address_detail']['house_no'] ?? '--' }}</td>
		                            </tr>
		                            <tr>
		                                <td>Street Name</td>
		                                <td> {{ $employee['employee_permanent_address_detail']['street_name'] ?? '--' }}</td>
		                            </tr>
		                            <tr>
		                                <td>Municipality/VDC</td>
		                                <td> {{ $employee['employee_permanent_address_detail']['municipality'] ?? '--' }}</td>
		                            </tr>
		                            <tr>
		                                <td>Ward no.</td>
		                                <td> {{ $employee['employee_permanent_address_detail']['ward_no'] ?? '--' }}</td>
		                            </tr>
		                            <tr>
		                                <td>Disctrict</td>
		                                <td> {{ $employee['employee_permanent_address_detail']['district'] ?? '--' }}</td>
		                            </tr>
		                            <tr>
		                                <td>Zone</td>
		                                <td> {{ $employee['employee_permanent_address_detail']['zone'] ?? '--' }}</td>
		                            </tr>
    	                        </tbody>
    	                    </table>
    	                    <hr>

		                    <p class="text-main text-semibold">Temporary Address</p>

    	                    <table class="table table-bordered table-striped employee-detail-block">
    	                        <!-- <thead>
    	                            <tr>
    	                                <th>Invoice</th>
    	                                <th>User</th>
    	                                <th>Amount</th>
    	                            </tr>
    	                        </thead> -->
    	                        <tbody>
		                            <tr>
		                                <td>House no.</td>
		                                <td> {{ $employee['employee_temporary_address_detail']['house_no'] ?? '--' }}</td>
		                            </tr>
		                            <tr>
		                                <td>Street Name</td>
		                                <td> {{ $employee['employee_temporary_address_detail']['street_name'] ?? '--' }}</td>
		                            </tr>
		                            <tr>
		                                <td>Municipality/VDC</td>
		                                <td> {{ $employee['employee_temporary_address_detail']['municipality'] ?? '--' }}</td>
		                            </tr>
		                            <tr>
		                                <td>Ward no.</td>
		                                <td> {{ $employee['employee_temporary_address_detail']['ward_no'] ?? '--' }}</td>
		                            </tr>
		                            <tr>
		                                <td>Disctrict</td>
		                                <td> {{ $employee['employee_temporary_address_detail']['district'] ?? '--' }}</td>
		                            </tr>
		                            <tr>
		                                <td>Zone</td>
		                                <td> {{ $employee['employee_temporary_address_detail']['zone'] ?? '--' }}</td>
		                            </tr>
    	                        </tbody>
    	                    </table>
		                </div>
		                <div id="demo-lft-tab-3" class="tab-pane fade">
		                    <p class="text-main text-semibold">Family Details</p>

    	                    <table class="table table-bordered table-striped employee-detail-block">
    	                        <!-- <thead>
    	                            <tr>
    	                                <th>Invoice</th>
    	                                <th>User</th>
    	                                <th>Amount</th>
    	                            </tr>
    	                        </thead> -->
    	                        <tbody>
		                            <tr>
		                                <td>Father's Name</td>
		                                <td> {{ $employee['employee_family_detail']['father_name'] ?? '--' }}</td>
		                            </tr>
		                            <tr>
		                                <td>Father's Occupation</td>
		                                <td> {{ $employee['employee_family_detail']['father_occupation'] ?? '--' }}</td>
		                            </tr>
		                            <tr>
		                                <td>Mother's Name</td>
		                                <td> {{ $employee['employee_family_detail']['mother_name'] ?? '--' }}</td>
		                            </tr>
		                            <tr>
		                                <td>Mother's Occupation</td>
		                                <td> {{ $employee['employee_family_detail']['mother_occupation'] ?? '--' }}</td>
		                            </tr>
		                            <tr>
		                                <td>Grand Father's Name</td>
		                                <td> {{ $employee['employee_family_detail']['grand_father_name'] ?? '--' }}</td>
		                            </tr>
		                            <tr>
		                                <td>Grand Mother's Name</td>
		                                <td> {{ $employee['employee_family_detail']['grand_mother_name'] ?? '--' }}</td>
		                            </tr>
		                            <tr>
		                                <td>Spouse's Name</td>
		                                <td> {{ $employee['employee_family_detail']['spouse_name'] ?? '--' }}</td>
		                            </tr>
		                            <tr>
		                                <td>Spouse's Occupation</td>
		                                <td> {{ $employee['employee_family_detail']['spouse_occupation'] ?? '--' }}</td>
		                            </tr>
		                            <tr>
		                                <td>Spouse's Date of Birth</td>
		                                <td> {{ $employee['employee_family_detail']['spouse_dob'] ?? '--' }}</td>
		                            </tr>
		                            <tr>
		                                <td>Wedding Anniversary</td>
		                                <td> {{ $employee['employee_family_detail']['wedding_anniversary'] ?? '--' }}</td>
		                            </tr>
    	                        </tbody>
    	                    </table>
		                </div>
		                <div id="demo-lft-tab-4" class="tab-pane fade">
		                    <!-- <p class="text-main text-semibold">Children</p> -->

    	                    <table class="table table-bordered table-striped employee-detail-block">
    	                        <thead>
    	                            <tr>
    	                                <th>Sn</th>
    	                                <th>Full Name</th>
    	                                <th>Date of Birth</th>
    	                                <th>Relationship</th>
    	                            </tr>
    	                        </thead>
    	                        <tbody>
    	                        	@if($employee['employee_children'])
    	                        		@foreach($employee['employee_children'] as $key => $children)
    		                            <tr>
    		                                <td>{{ ++$key }}</td>
    		                                <td>{{ $children['full_name'] ?? '--' }}</td>
    		                                <td>{{ formatDate($children['dob']) }}</td>
    		                                <td>{{ $children['relationship'] ?? '--' }}</td>
    		                            </tr>
    	                        		@endforeach
                                    @else
                                        <tr><td colspan="4">No children.</td></tr>
    	                        	@endif
    	                        </tbody>
    	                    </table>
		                </div>
		                <div id="demo-lft-tab-5" class="tab-pane fade">
		                    <!-- <p class="text-main text-semibold">Children</p> -->

		                    <div class="row">
		                    	@if($employee['employee_work_experiences'])
		                    		@foreach($employee['employee_work_experiences'] as $key => $w_experience)
			                    	<div class="col-sm-4 eq-box-sm">
							            <div class="panel children-panel">
							                <div class="panel-heading">
							                    <h3 class="panel-title">{{ $w_experience['post_title'] }}</h3>
							                </div>
							                <div class="panel-body">
							                    <p>From - <span class="label label-default">{{  formatDate($w_experience['from'])  }}</span></p>
							                    <p>To - <span class="label label-default">{{  formatDate($w_experience['to'])  }}</span></p>
							                    <p>Supervisor Name - <span class="label label-default">{{  $w_experience['supervisor_name']  }}</span></p>
							                    <p>Reason of Leaving - <span class="label label-default">{{  $w_experience['reason_of_leaving']  }}</span></p>
							                </div>
							            </div>
							        </div>
		                    		@endforeach
                                @else
                                    <div>No work experiences.</div>
		                    	@endif
		                    </div>
		                </div>		
                        <div id="demo-lft-tab-6" class="tab-pane fade">

                        	@if($employee['employee_relative_contact'])
                            <p class="text-main text-semibold">Relative Detail</p>
                            <table class="table table-bordered table-striped employee-detail-block">
                                <!-- <thead>
                                    <tr>
                                        <th>Invoice</th>
                                        <th>User</th>
                                        <th>Amount</th>
                                    </tr>
                                </thead> -->
                                <tbody>
    	                            <tr>
    	                                <td>Contact Name</td>
    	                                <td> {{ $employee['employee_relative_contact']['contact_name'] ?? '--' }}</td>
    	                            </tr>
    	                            <tr>
    	                                <td>Address</td>
    	                                <td> {{ $employee['employee_relative_contact']['address'] ?? '--' }}</td>
    	                            </tr>
    	                            <tr>
    	                                <td>Phone</td>
    	                                <td> {{ $employee['employee_relative_contact']['phone'] ?? '--' }}</td>
    	                            </tr>
    	                            <tr>
    	                                <td>Relationship</td>
    	                                <td> {{ $employee['employee_relative_contact']['relationship'] ?? '--' }}</td>
    	                            </tr>
                                </tbody>
                            </table>
                            @else
                                <p>No relatives.</p>
                        	@endif
                        </div>	
                        <div id="demo-lft-tab-7" class="tab-pane fade">
                            <!-- <p class="text-main text-semibold">Children</p> -->

                        	@if($employee['employee_relative_on_banks'])
                            <table class="table table-bordered table-striped employee-detail-block">
                                <thead>
                                    <tr>
                                        <th>Sn</th>
                                        <th>Full Name</th>
                                        <th>Relationship</th>
                                        <th>Organisation</th>
                                    </tr>
                                </thead>
                                <tbody>
                                		@foreach($employee['employee_relative_on_banks'] as $key => $item)
        	                            <tr>
        	                                <td>{{ ++$key }}</td>
        	                                <td>{{ $item['full_name'] ?? '--' }}</td>
        	                                <td>{{ formatDate($item['relationship']) }}</td>
        	                                <td>{{ $item['organisation'] ?? '--' }}</td>
        	                            </tr>
        	                            @endforeach
                                </tbody>
                            </table>
                            @else
                                <p>No relatives on bank.</p>
                        	@endif
                        </div>	
                        <div id="demo-lft-tab-8" class="tab-pane fade">
                            <!-- <p class="text-main text-semibold">Children</p> -->

                        	@if($employee['employee_family_loans'])
                            <table class="table table-bordered table-striped employee-detail-block">
                                <thead>
                                    <tr>
                                        <th>Sn</th>
                                        <th>Full Name</th>
                                        <th>Relationship</th>
                                        <th>Loan Type</th>
                                    </tr>
                                </thead>
                                <tbody>
                                		@foreach($employee['employee_family_loans'] as $key => $item)
        	                            <tr>
        	                                <td>{{ ++$key }}</td>
        	                                <td>{{ $item['full_name'] ?? '--' }}</td>
        	                                <td>{{ formatDate($item['relationship']) }}</td>
        	                                <td>{{ $item['loan_type'] ?? '--' }}</td>
        	                            </tr>
        	                            @endforeach
                                </tbody>
                            </table>
                            @else
                                <p>No family loans.</p>
                        	@endif
                        </div>	
                        <div id="demo-lft-tab-9" class="tab-pane fade">
                        	<h4>Job Description</h4>
                        	<form class="form-horizontal" action="{{ route('admin.employees.job-description') }}" method="POST"
                        	      enctype="multipart/form-data">
                        	    @csrf
                        	    <input type="hidden" name="id" value="{{ $employee['id'] }}">
                        	    <div class="form-group">
                    	            <textarea name="description" id="input-description" cols="30" rows="10" class="form-control ckeditor"><?= $employee['job_description']['description']??'' ?></textarea>
                        	    </div>
                        	    <div class="form-group">
                        	    	<button class="btn btn-sm btn-success">Save</button>
                        	    </div>
                        	</form>
                        </div>               
		            </div>
		        </div>
            </div>
            <!--===================================================-->
            <!-- End Striped Table -->

        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
	var hidWidth;
	var scrollBarWidths = 40;

	$(".list>li").on('click', function(event) {
		var tab_count = $(".list>li").length;
		var tab = $(this);
		var wrapper = $(".tab-wrapper");
		var list = $(".list");
		var tab_left = tab.offset().left + tab.outerWidth();
		var wrapper_left = wrapper.offset().left + wrapper.outerWidth();

		if (tab_left > wrapper_left) {
			var offset_left = tab_left - wrapper_left;
			var fix_alignment = list.position().left - offset_left;
			if (tab.index() != (tab_count - 1)) {
				fix_alignment -= 30;
			}

		    $('.list').animate({left: fix_alignment + "px"},'fast');
		    return 1;
		}

		if (tab.offset().left < wrapper.offset().left) {
			var offset_left = wrapper.offset().left - tab.offset().left;
			var fix_alignment = list.position().left + offset_left;

			if (tab.index() > 0) {
				fix_alignment += 30;
			}

		    $('.list').animate({left: fix_alignment + "px"},'fast');
		    return 1;
		}
	});

	var widthOfList = function(){
	  var itemsWidth = 0;
	  $('.list li').each(function(){
	    var itemWidth = $(this).outerWidth();
	    itemsWidth+=itemWidth;
	  });
	  return itemsWidth;
	};

	var widthOfHidden = function(){
	  return (($('.tab-wrapper').outerWidth())-widthOfList()-getLeftPosi())-scrollBarWidths;
	};

	var getLeftPosi = function(){
	  return $('.list').position().left;
	};

	var reAdjust = function(){
	  if (($('.tab-wrapper').outerWidth()) < widthOfList()) {
	    // $('.scroller-right').show();
	  }
	  else {
	    // $('.scroller-right').hide();
	  }
	  
	  if (getLeftPosi()<0) {
	    // $('.scroller-left').show();
	  }
	  else {
	    $('.list').animate({left:"-="+getLeftPosi()+"px"},'fast');
	  	// $('.scroller-left').hide();
	  }
	}

	reAdjust();

	$(window).on('resize',function(e){  
	  	reAdjust();
	});

	$('.scroller-right').click(function() {
	  
	  $('.scroller-left').fadeIn('fast');
	  $('.scroller-right').fadeOut('fast');
	  
	  $('.list').animate({left:"+="+widthOfHidden()+"px"},'fast',function(){

	  });
	});

	$('.scroller-left').click(function() {
	  
		$('.scroller-right').fadeIn('fast');
		$('.scroller-left').fadeOut('fast');
	  
	  	$('.list').animate({left:"-="+getLeftPosi()+"px"},'fast',function(){
	  	
	  	});
	});    
</script>
@endpush
