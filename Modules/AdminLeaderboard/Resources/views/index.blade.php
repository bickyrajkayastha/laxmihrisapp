@extends('admin.layouts.master')

@section('content')
<div class="row">
    <div class="col">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Leaderbaord Management
                    <a href="{{ route('admin.leaderboards.create') }}" class="btn btn-primary btn-sm panel-btn pull-right"><i class="fa fa-plus"></i> Add New Leaderboard</a>
                </h3>
            </div>

            <!-- Striped Table -->
            <!--===================================================-->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
	                            <th>S.N</th>
	                            <th>Title</th>
	                            <th>Category</th>
	                            <th>Date</th>
                                <th>Published</th>
	                            <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($courses as $course)
	                    	    <tr>
	                    	        <td>{{ $loop->iteration }}</td>
	                    	        <td>{{ $course->title }}</td>
	                    	        <td>{{ $course->category->name??'' }}</td>
                                    <td>{{ $course->dateRange }}</td>
	                    	        <td>{!! $course->status ? "<span class='label label-success'>Active</span>" : "<span class='label label-danger'>Inactive</span>"!!}</td>
	                    	        <td>
	                    	            <a href="{{ route('admin.leaderboards.edit', $course->id) }}" class="btn btn-sm btn-primary" title="Edit">
	                    	                <i class="fas fa-edit"></i>
                                        </a>
                                        {{-- <a href="{{ route('admin.courses.trainings.index',$course->id) }}" class="btn btn-sm btn-primary" title="Trainings">
                                            <i class="fas fa-network-wired"></i> Trainings
                                        </a> --}}
	                    	            <a href="#" title="Remove" class="btn btn-sm btn-danger btn-delete"
	                    	               data-toggle="modal"
	                    	               data-id="{{ $course->id }}"
	                    	               data-target="#delete-modal">
	                    	                <i class="fas fa-trash"></i>
                                        </a>
	                    	        </td>
	                    	    </tr>
                            @empty
                    	    <tr>
                    	        <td colspan="10">
                    	            <em>No data available in table ...</em>
                    	        </td>
                    	    </tr>
                            @endforelse
                        </tbody>
                    </table>
                    {!! $courses->render() !!}
                </div>
            </div>
            <!--===================================================-->
            <!-- End Striped Table -->

        </div>
    </div>
    @include('admin.elements.delete-modal')
</div>
@endsection
@push('scripts')
    <script type="text/javascript">
        $(document).on('click','.btn-delete', function () {
            var route = '{{ route("admin.leaderboards.destroy", ":id") }}';

            route = route.replace(':id', $(this).attr('data-id'));

            $('#delete-modal').find('form').attr('action', route);
            $('#delete-modal').modal('show');

            $('#delete-final').click(function (e) {
                $(this).attr('disabled',true);
                $(this).html('<i class="fas fa-spinner fa-spin"></i> Please wait...');
                $(this).closest('form').submit();
            });
        });
    </script>
@endpush
