<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmployeeDevice extends Model
{
	protected $guarded = ['id'];
}
