<?php

namespace Modules\AdminJobDescription\Http\Controllers;

use App\Helpers\Permission;
use App\Models\JobDescription;
use App\Traits\Slugger;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Log;

class AdminJobDescriptionController extends Controller
{
    use Slugger;

    private $indexRoute;

    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->indexRoute = route('admin.job-descriptions.index');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $data['pageTitle'] = "Job Description Management";
        $data['jobs'] = JobDescription::paginate(20);

        return view('adminjobdescription::index', $data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $data['pageTitle'] = "Add Job Description";
        $data['breadcrumbs'] = '<li><a href="' . route('admin.job-descriptions.index') . '">Course Management</a></li><li>Add</li>';
        return view('adminjobdescription::create', $data);
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        $this->request->validate([
            'name' => 'required',
            'description' => 'required'
        ]);

        try {
            $data = $this->request->only('name', 'description', 'status');
            // $data['slug'] = $this->createSlug($this->request->name, JobDescription::class);

            JobDescription::create($data);

            session()->flash('success_message', __('alerts.create_success'));
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            session()->flash('error_message', __('alerts.create_error'));
        }

        return redirect()->to($this->indexRoute);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($course_id)
    {
        $data = [];
        $data['breadcrumbs'] = '<li><a href="' . route('admin.job-descriptions.index') . '">Job Desctiption</a></li><li>Detail</li>';
        $data['job'] = JobDescription::find($course_id);
        return view('adminjobdescription::show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $data['pageTitle'] = "Edit Course";
        $data['job'] = $this->getJobDescriptionById($id);
        $data['jobs'] = JobDescription::where('status', 1)->get();
        $data['breadcrumbs'] = '<li><a href="' . route('admin.job-descriptions.index') . '">Job Description Management</a></li><li>Edit</li>';

        return view('adminjobdescription::edit', $data);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update($id)
    {
        $this->request->validate([
            'name' => 'required',
            'description' => 'required'
        ]);

        try {
            $data = $this->request->only('name', 'description', 'status');

            // $data['slug'] = $this->createSlug($this->request->name, JobDescription::class);

            $job = JobDescription::find($id);

            $job->update($data);

            session()->flash('success_message', __('alerts.update_success'));
        } catch (\Exception $e) {
            session()->flash('error_message', __('alerts.update_error'));
        }

        return redirect()->to($this->indexRoute);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        try {

            $job = $this->getJobDescriptionById($id);

            if ($job->status == 1) {
                session()->flash('error_message', __('alerts.delete_error_active'));
                return redirect()->to($this->indexRoute);
            }

            $job->delete();
            session()->flash('success_message', __('alerts.delete_success'));
        } catch (\Exception $e) {
            session()->flash('error_message', __('alerts.delete_error'));
        }

        return redirect()->to($this->indexRoute);
    }

    private function getJobDescriptionById($id)
    {
        return JobDescription::find($id);
    }
}
