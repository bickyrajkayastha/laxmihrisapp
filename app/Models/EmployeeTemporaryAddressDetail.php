<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmployeeTemporaryAddressDetail extends Model
{
	protected $guarded = ['id'];

	public $hidden = [
	    'same_as_permanent'
	];
}
