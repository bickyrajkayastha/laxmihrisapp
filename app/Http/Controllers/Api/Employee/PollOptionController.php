<?php

namespace App\Http\Controllers\Api\Employee;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Models\PollOption;

class PollOptionController extends Controller
{
	/**
	 * @SWG\Get(
	 *     path="/employee/polls/vote/{id}/{comment}",
	 *     description="voting poll",
	 *     tags={"Employee Apis"},
	 *     security={{"bearerAuth":{}}},
	 *     @SWG\Parameter(
	 *         in="path",
	 *         name="id",
	 *         type="number",
	 *         description="id of the option_id.",
	 *         required=true,
	 *     ),
	 *     @SWG\Response(
	 *         response=200,
	 *         description="status, message",
	 *     ),
	 *     @SWG\Response(
	 *         response=422,
	 *         description="status, message"
	 *     )
	 * )
	 */
	public function vote(Request $request, $optionId, $comment = null)
	{
		$success = false;
		$message = "";
		$status_code = 404;
		$employee = auth()->user();
		$poll_option = PollOption::find($optionId);

		if ($poll_option) {
			$poll_id = $poll_option->poll_id;
			$request->request->add([
				'poll_option_id' => $optionId,
				'employee_id' => $employee->id,
				'poll_id' => $poll_id,
			]);

			$poll = \App\Models\Poll::find($poll_id);

			$this->validate($request, [
				'employee_id' => ['required', 'numeric', Rule::unique('employee_poll_option')->where(function ($query) use ($request) {
				    return $query->where('poll_id', $request->poll_id);
				})],
		    ], [
		    	'employee_id.unique' => "You've already voted for this poll."
		    ]);

			try {
				$employee->poll_options()->attach($optionId, [
                    'poll_id' => $poll_id,
                    'comment' => $comment
                ]);
				$success = true;
				$message = "Thanks.";
			} catch (\Exception $e) {
				$has_poll_options = $employee->poll_options()->where('poll_option_id', $optionId)->exists();
				if (!$has_poll_options) {
					$message = "The option does not exist.";
				}
			}
		} else {
			$message = "The option does not exist.";
		}

		return response()->json([
			'data' => [],
			'success' => $success,
			'message' => $message
		]);
	}
}
