<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeMessageReceiverTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_message_receiver', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('receiver_id');
            $table->foreign('receiver_id')->references('id')->on('employees')->onDelete('cascade');
            $table->unsignedInteger('employee_message_id');
            $table->foreign('employee_message_id')->references('id')->on('employee_message')->onDelete('cascade');
            $table->tinyInteger('deliver_status')->default(0);
            $table->tinyInteger('seen_status')->default(0);
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_message_receiver');
    }
}
