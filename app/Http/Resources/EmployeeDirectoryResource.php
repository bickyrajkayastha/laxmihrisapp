<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class EmployeeDirectoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);

        return $this->resource->map(function($item) {

            return [
                'id' => $item->id,
                'name' => $item->name,
                'email' => $item->email,
                'phone' => $item->phone,
                'address' => $item->employee_permanent_address_detail['street_name'],
                'designation' => $item->designation,
                'thumbImgPath' => $item->thumbImgPath,
                'image' => $item->croppedImgPath
            ];
        });
    }

    public function with($request)
    {
        if ($this->resource) {
            $this->message = "Directory detail.";
        } else {
            $this->message = "List empty.";
        }

        return [
            'success' => true,
            'message' => $this->message
        ];
    }

    /**
     * Customize the outgoing response for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Http\Response  $response
     * @return void
     */
    public function withResponse($request, $response)
    {
        $response->setStatusCode(200);
    }
}
