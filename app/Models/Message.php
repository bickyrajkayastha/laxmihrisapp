<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
	// item_type = [1 = text, 2 = file, 3 = image]
	protected $guarded = ['id'];

    protected $appends = ['filePath'];

	public function employees()
	{
	    return $this->belongsToMany(Employee::class, 'employee_message', 'message_id', 'sender_id')->withTimestamps();
	}

	public function sender()
	{
	    return $this->belongsToMany(Employee::class, 'employee_message', 'message_id', 'sender_id')->withTimestamps();
	}

	public function getSenderAttribute()
	{
		return $this->sender()->first();
	}

	public function getFilePathAttribute()
	{
	    if ($this->file) {
	        return url('uploads/messages/files/' . $this->file);
	    }
	    return "";
	}
}
