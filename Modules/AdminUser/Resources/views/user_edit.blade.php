@extends('admin.layouts.master')
@section('content')
<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">
            Create New User
            <span class="required_fields" style="float: right;"><strong class="required_color">*</strong> These fields are required.</span>
        </h3>
    </div>

    <form action="{{ route('admin.user.update') }}" method="POST" class="form-horizontal form-padding" enctype="multipart/form-data">
        <div class="panel-body">
            {{ csrf_field() }}
            <input type="hidden" name="_id" value="{{ $user_details->id }}">
            <input type="hidden" name="old_username" value="{{ $user_details->username }}">
            <input type="hidden" name="old_email" value="{{ $user_details->email }}">
            <input type="hidden" name="user_type" value="{{ $user_type }}">


            <div class="form-group">
                <label class="col-md-2 control-label" for="admin-first-name">Full Name <span class="required_color">*</span></label>
                <div class="col-md-10">
                    <div class="row">
                        <div class="col-md-4">
                            <input type="text" id="admin-first-name" name="firstname" class="form-control" value="{{ old('firstname') ? old('firstname') : $user_details->first_name }}" placeholder="First Name" >
                            <small class="help-block text-danger">{{$errors->first('first_name')}}</small>
                        </div>
                        <div class="col-md-4">
                            <input type="text" id="admin-first-name" name="middlename" class="form-control" value="{{ old('middlename') ? old('middlename') : $user_details->middle_name }}" placeholder="Middle Name" >
                            <small class="help-block text-danger">{{$errors->first('middlename')}}</small>
                        </div>
                        <div class="col-md-4">
                            <input type="text" id="admin-last-name" name="lastname" class="form-control" value="{{ old('lastname') ? old('lastname') : $user_details->last_name }}" placeholder="Last Name" >
                            <small class="help-block text-danger">{{$errors->first('lastname')}}</small>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label" for="demo-address-input">Username <span class="required_color">*</span></label>
                <div class="col-md-4">
                    <input type="text" class="form-control" name="username" value="{{ old('username') ? old('username') : $user_details->username }}">
                    <small class="help-block text-danger">{{$errors->first('username')}}</small>
                </div>
                <label class="col-md-1 control-label" for="demo-address-input">Email<span class="required_color">*</span></label>
                <div class="col-md-5">
                    <input type="text" class="form-control" name="email" value="{{ old('email') ? old('email') : $user_details->email }}">
                    <small class="help-block text-danger">{{$errors->first('email')}}</small>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label" for="demo-address-input">Phone </label>
                <div class="col-md-10">
                    <input type="text" class="form-control" name="phone" value="{{ old('phone') ? old('phone') : $user_details->phone }}">
                    <small class="help-block text-danger">{{$errors->first('phone')}}</small>
                </div>
            </div>

            @if ( $user_type != 'superadmin' )
            <div class="form-group">
                <label class="col-md-2 control-label">Group <span class="required_color">*</span></label>
                <div class="col-md-9">
                    <div class="checkbox">
                        @if( count($groups) >0 )
                            @foreach($groups as $g)
                                <input id="group-{{$g->id}}" class="magic-checkbox" type="checkbox" name="groups[]" value="{{ $g->id }}" {{ in_array($g->id,$current_user_grps) ? "checked" : '' }}>
                                <label for="group-{{$g->id}}" style="color: #000023;">{{ $g->name }}</label>
                            @endforeach
                        @endif
                    </div>
                    <small class="help-block text-danger">{{$errors->first('groups')}}</small>
                </div>
            </div>
            @endif

            <div class="form-group">
                <label class="col-md-2 control-label" for="demo-contact-input">Status</label>
                <div class="col-md-9">
                    <div class="radio">
                        <input id="status" class="magic-radio" type="radio" name="status" value="1" {{ $user_details->status == '1' ? "checked" : "" }}>
                        <label for="status" style="color:#000000;">Active</label>
                        <input id="status-2" class="magic-radio" type="radio" name="status" value="0" {{ $user_details->status == '0' ? "checked" : "" }}>
                        <label for="status-2" style="color:#000000;">Inactive</label>
                    </div>
                </div>
            </div>



            <div class="form-group">
                <label class="col-md-2 control-label"></label>
                <div class="col-md-2">
                    <input type="submit" class="btn btn-sm btn-block btn-primary" name="submit" value="UPDATE">
                </div>
            </div>
        </div>
    </form>
</div>
@stop