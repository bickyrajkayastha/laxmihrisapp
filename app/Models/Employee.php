<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Log;

class Employee extends Authenticatable implements JWTSubject
{
    use Notifiable;

	// gender [1 = male, 2= female, 3 = other]
	protected $guarded = ['id'];

	public $hidden = [
	    'password', 'remember_token', 'reset_token', 'employee_code'
	];

    protected $appends = ['fullImgPath', 'thumbImgPath', 'croppedImgPath'];

	public function getJWTIdentifier() {
	    return $this->getKey();
	}

	public function getJWTCustomClaims() {
	    return [];
	}

	public function employee_personal_detail()
	{
		return $this->hasOne(EmployeePersonalDetail::class);
	}

	public function employee_permanent_address_detail()
	{
		return $this->hasOne(EmployeePermanentAddressDetail::class);
	}

	public function employee_temporary_address_detail()
	{
		return $this->hasOne(EmployeeTemporaryAddressDetail::class);
	}

	public function employee_family_detail()
	{
		return $this->hasOne(EmployeeFamilyDetail::class);
	}

	public function employee_children()
	{
		return $this->hasMany(EmployeeChildren::class);
	}

	public function employee_educations()
	{
		return $this->hasMany(EmployeeEducation::class);
	}

	public function employee_work_experiences()
	{
		return $this->hasMany(EmployeeWorkExperience::class);
	}

	public function employee_relative_contact()
	{
		return $this->hasOne(EmployeeEmergencyContact::class);
	}

	public function employee_other_information()
	{
		return $this->hasOne(EmployeeOtherInformation::class);
	}

	public function employee_relative_on_banks()
	{
		return $this->hasMany(EmployeeRelativeOnBank::class);
	}

	public function employee_family_loans()
	{
		return $this->hasMany(EmployeeFamilyLoan::class);
	}

	public function employee_devices()
	{
		return $this->hasMany(EmployeeDevice::class);
	}

	public function getFullImgPathAttribute()
	{
	    if ($this->image) {
	        return url('uploads/employees/full/' . $this->image);
        }
	    return asset('') . config('constants.default_user_image_thumb');
	}

	public function getCroppedImgPathAttribute()
	{
	    if ($this->image) {
	        return url('uploads/employees/crop/' . $this->image);
        }
	    return asset('') . config('constants.default_user_image_thumb');
	}

	public function getThumbImgPathAttribute()
	{
	    if ($this->image) {
	        return url('uploads/employees/thumb/' . $this->image);
	    }
	    return asset('') . config('constants.default_user_image_thumb');
    }

	public function notifications()
	{
	    return $this->belongsToMany(Notification::class)->withTimestamps()->withPivot('id', 'seen_status');
	}

	public function messages()
	{
	    return $this->belongsToMany(Message::class, 'employee_message', 'sender_id', 'message_id')
	    ->withTimestamps()
        ->withPivot('id', 'seen_status', 'deliver_status', 'sender_id', 'receiver_id', 'message_group_id', 'message_id');
	}

	public function received_messages()
	{
	    return $this->belongsToMany(Message::class, 'employee_message', 'receiver_id', 'message_id')
	    ->withTimestamps()
        ->withPivot('id', 'seen_status', 'deliver_status', 'sender_id', 'receiver_id', 'message_group_id', 'message_id');
	}

	public function scopeLatestMessageWithEmployee($query, $value)
	{
	    return $query->with([
	    	'messages' => function($q) use ($value) {
	    		$q->latest()->limit(1)->where('receiver_id', '=', $value);
	    	}
	    ]);
	}

	public function feedbacks()
	{
		return $this->hasMany(EmployeeFeedback::class);
	}

	public function poll_options()
	{
	    return $this->belongsToMany(PollOption::class, 'employee_poll_option', 'employee_id', 'poll_option_id')->withTimestamps()->withPivot('id', 'poll_option_id', 'employee_id', 'seen_status');
	}

	public function scopeActive($query)
	{
		return $query->where('status', '=', 1);
	}

	public function scopePollOption($query, $value)
	{
	    return $query->whereHas('poll_options', function($q) use ($value) {
	    	$q->where('poll_options.poll_id', '=', $value);
	    });
	}

	public function job_description()
	{
		return $this->hasOne(EmployeeJobDescription::class, 'employee_id', 'id');
	}

	public function message_groups()
	{
		return $this->belongsToMany(MessageGroup::class, 'employee_message_group');
	}

	public function active_message_groups()
	{
		return $this->message_groups()->active();
	}

	public function isInGroup($group_id)
	{
		$exist = $this->message_groups()->where('message_group_id', '=', $group_id)->first();
		if ($exist) {
			return 1;
		}

		return 0;
    }

    public function assessment_options()
    {
        return $this->belongsToMany(AssessmentOption::class, 'employee_option', 'employee_id', 'option_id')->withPivot('course_id')->withTimestamps();
    }

    public function courses()
    {
        return $this->belongsToMany(Course::class, 'course_employee', 'employee_id', 'course_id')->withPivot('assessment_points')->withTimestamps();
    }

    public function leaderboards()
    {
        return $this->belongsToMany(Course::class, 'leaderboards', 'employee_id', 'course_id')->withPivot('points', 'position')->withTimestamps();
    }
}
