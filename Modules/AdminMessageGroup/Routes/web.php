<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin/message-groups', 'as' => 'admin.', 'middleware' => 'auth:admin'], function() {

    Route::get('/', 'AdminMessageGroupController@index')
        ->name('message-groups')
        ->middleware('can:view-message-groups');

    Route::get('/create', 'AdminMessageGroupController@create')
        ->name('message-groups.create')
        ->middleware('can:create-message-groups');

    Route::post('/create', 'AdminMessageGroupController@store')
        ->name('message-groups.store')
        ->middleware('can:create-message-groups');

    Route::get('/{id}/edit', 'AdminMessageGroupController@edit')
        ->name('message-groups.edit')
        ->middleware('can:edit-message-groups');

    Route::post('/{id}/edit', 'AdminMessageGroupController@update')
        ->name('message-groups.update')
        ->middleware('can:edit-message-groups');

    Route::post('/{id}/delete', 'AdminMessageGroupController@destroy')
        ->name('message-groups.destroy')
        ->middleware('can:delete-message-groups');

    Route::get('/{id}/employees', 'AdminMessageGroupController@employees')
        ->name('message-groups.employees')
        ->middleware('can:view-message-groups');

    Route::post('/employees', 'AdminMessageGroupController@addEmployee')
        ->name('message-groups.employees.store')
        ->middleware('can:create-message-groups');

    Route::post('/employees/{id}/delete', 'AdminMessageGroupController@deleteEmployee')
        ->name('message-groups.employees.destroy')
        ->middleware('can:delete-message-groups');

    Route::post('/employees/{id}/update-status', 'AdminMessageGroupController@updateEmployeeStatus')
        ->name('message-groups.employees.update-status')
        ->middleware('can:edit-message-groups');
});
