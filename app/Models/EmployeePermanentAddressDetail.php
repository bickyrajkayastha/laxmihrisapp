<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class EmployeePermanentAddressDetail extends Model
{
	protected $guarded = ['id'];

	public function employee()
	{
		return $this->belongsTo(Employee::class);
	}
}
