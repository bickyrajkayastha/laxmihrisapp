<?php

namespace App\Exceptions;

use Exception;

class CustomApiException extends Exception
{
	protected $status = 500;
	protected $message = "";

	public function __construct($data)
	{
		$this->status = $data['status_code'];
		$this->message = $data['message'];
	}

	public function report()
	{
		// 
	}

	/**
	 * Set the HTTP status code to be used for the response.
	 *
	 * @param integer $status
	 *
	 * @return $this
	 */
	public function getStatusCode()
	{
		return $this->status;
	}
}
