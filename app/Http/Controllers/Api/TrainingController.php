<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\ApiResource;
use App\Models\TrainingProgram;

class TrainingController extends Controller
{
    public function show($id)
    {
        $training = TrainingProgram::where('id', $id)->with('documents')->first();
        if ($training) {
            $training = [
                "id" => $training->id,
                "title" => $training->title,
                "description" => $training->description,
                // "start_date" => $training->start_date,
                // "end_date" => $training->end_date,
                // "start_time" => $training->start_time,
                // "end_time" => $training->end_time,
                "file" => $training->fullFileUrl,
                "dateRange" => $training->dateRange,
                "timeRange" => $training->timeRange,
                "documents" => $training->documents->map(function($document) {
                    return [
                        "id" => $document->id,
                        "file" => $document->full_file_url,
                        "type" => getFileType(pathinfo($document->file, PATHINFO_EXTENSION))
                    ];
                })
            ];
        }

        return new ApiResource($training, true, 'Training Details.');
    }
}
