<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestController extends Controller
{
	/**
	 * @SWG\Get(
	 *     path="/employee/faqs",
	 *     description="get employee directories",
	 *     tags={"Employee Apis"},
	 *     security={{"bearerAuth":{}}},
	 *     @SWG\Response(
	 *         response=200,
	 *         description="success, message",
	 *     ),
	 *     @SWG\Response(
	 *         response=422,
	 *         description="success, message"
	 *     )
	 * )
	 */
    public function faqs()
    {

    	$data = [
    		[
    			'id' => 1,
    			'title' => 'For what purposes does the Bank provide Home Loan?',
    			'description' => 'Land and/or Building purchase - Construction, extension or renovation of house - Ready-built House/Apartment purchase - For the purpose of furnishing, purchase of home appliances etc.'
    		],
    		[
    			'id' => 2,
    			'title' => ' How shall I apply for the loan?',
    			'description' => 'All you need to do is approach the Bank and submit the application form along with the necessary documents or you can simply download the application form from the Bank\'s website, fill it in and submit to the Bank. Loan can be applied in single or joint names.'
    		],
    		    		[
    			'id' => 3,
    			'title' => 'How is my Home Loan eligibility determined?',
    			'description' => 'Your Home Loan eligibility is basically determined considering the following factors: - Age of the borrower - Purpose of the loan facility - Collateral - Repayment capability'
    		],
    		    		[
    			'id' => 4,
    			'title' => ' How long does it take to get my application processed and the loan sanctioned?',
    			'description' => 'We are committed for the prompt processing of your application if all the essential documents are in place. Loans are disbursed upon completion/execution of all security documentation\'s website, fill it in and submit to the Bank. Loan can be applied in single or joint names.'
    		]
    	];

    	return response()->json([
    		'data' => $data,
    		'success' => true,
    		'message' => 'list'
    	], 200);
    }
}
