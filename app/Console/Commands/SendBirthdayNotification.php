<?php

namespace App\Console\Commands;

use App\Jobs\BirthdayNotificationJob;
use App\Jobs\SendNotificationJob;
use App\Models\Employee;
use Illuminate\Console\Command;

class SendBirthdayNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'birthday-notification:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send birthday notification to all the staff who has birthday.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $message = "";

        $current_month = \Carbon\Carbon::now()->format('m');
        try {
            $employees = Employee::whereHas('employee_personal_detail', function($q) use ($current_month) {
                $q->whereMonth('dob', $current_month);
            })->get();

            if (!$employees->isEmpty()) {
                foreach ($employees as $employee) {
                    $notification = [];
                    $notification['title'] = "HAPPY BIRTHDAY " . $employee->name;
                    $notification['body'] = "Many many happy returns of the day.";
                    BirthdayNotificationJob::dispatch($notification, $employee);
                }
                $message = "Birthday notification send.";
            } else {
                $message = "No employees has birthday today.";
            }

        } catch (\Throwable $th) {
            \Log::info($th->getMessage());
        }
        $this->info($message);
    }
}
