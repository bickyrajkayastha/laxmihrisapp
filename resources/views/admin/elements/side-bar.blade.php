<nav id="mainnav-container">
    <div id="mainnav">
        <!--Menu-->
        <!--================================-->
        <div id="mainnav-menu-wrap">
            <div class="nano">
                <div class="nano-content">
                    <ul id="mainnav-menu" class="list-group">
                        <!--Category name-->
                        <!-- <li class="list-header">Navigation</li> -->
                        <!--Menu list item-->
                        <li class="{{ (request()->routeIs('admin.dashboard*'))? 'active-link':'' }}">
                            <a href="{{ route('admin.dashboard') }}">
                                <i class="demo-psi-home"></i>
                                <span class="menu-title">
                                    <strong>Dashboard</strong>
                                </span>
                            </a>
                        </li>

                        <!--Menu list item-->
                        @if (auth()->user()->check_permission('settings'))
                        <li class="{{ (in_array(request()->segment(2), ['setting', 'email-templates'])) ? 'active-link' : '' }}">
                            <a href="#">
                                <i class="fa fa-cogs"></i>
                                <span class="menu-title">
                                    <strong>Master Config</strong>
                                </span>
                                <i class="arrow"></i>
                            </a>
                            <ul class="collapse {{ (in_array(request()->segment(2), ['setting', 'email-templates', 'users'])) ? 'in' : '' }}">
                                <li class="{{ (request()->routeIs('admin.setting*'))? 'active-link':'' }}"><a href="{{ route('admin.setting.general') }}">General</a></li>
                                <li class="{{ (request()->routeIs('admin.email*'))? 'active-link':'' }}"><a href="{{ route('admin.email.template') }}">Email Template</a></li>
                                <li class="{{ (request()->routeIs('admin.user*'))? 'active-link':'' }}"><a href="{{ route('admin.user.list') }}">Users</a></li>
                            </ul>
                        </li>
                        @endif

                        <li class="{{ (request()->routeIs('admin.setting.ceo'))? 'active-link':'' }}">
                            <a href="{{ route('admin.setting.ceo') }}">
                                <i class="fa fa-user"></i>
                                <span class="menu-title">
                                    <strong>CEO</strong>
                                </span>
                            </a>
                        </li>

                        <li class="{{ (request()->routeIs('admin.setting.welcome-note'))? 'active-link':'' }}">
                            <a href="{{ route('admin.setting.welcome-note') }}">
                                <i class="fa fa-sticky-note"></i>
                                <span class="menu-title">
                                    <strong>Welcome Note</strong>
                                </span>
                            </a>
                        </li>
                        @if (auth()->user()->check_permission('view-contents'))
                        <li class="{{ (request()->routeIs('admin.contents*'))? 'active-link':'' }}">
                            <a href="{{ route('admin.contents') }}">
                                <i class="fa fa-file"></i>
                                <span class="menu-title">
                                    <strong>Pages</strong>
                                </span>
                            </a>
                        </li>
                        @endif

                        @if (auth()->user()->check_permission('view-employees'))
                        <li class="{{ (request()->routeIs('admin.employees*'))? 'active-link':'' }}">
                            <a href="{{ route('admin.employees') }}">
                                <i class="fas fa-user-tie"></i>
                                <span class="menu-title">
                                    <strong>Employees</strong>
                                </span>
                            </a>
                        </li>
                        @endif
                        {{-- <li class="{{ (request()->routeIs('admin.teams*'))? 'active-link':'' }}">
                            <a href="{{ route('admin.teams.index') }}">
                                <i class="fas fa-users"></i>
                                <span class="menu-title">
                                    <strong>Teams</strong>
                                </span>
                            </a>
                        </li> --}}

                        @if (auth()->user()->check_permission('view-faqs'))
                        <li class="{{ (request()->routeIs('admin.faqs*'))? 'active-link':'' }}">
                            <a href="{{ route('admin.faqs.index') }}">
                                <i class="fas fa-question-circle"></i>
                                <span class="menu-title">
                                    <strong>FAQ's</strong>
                                </span>
                            </a>
                        </li>
                        @endif

                        @if (auth()->user()->check_permission('view-notifications'))
                        <li class="{{ (request()->routeIs('admin.notifications*'))? 'active-link':'' }}">
                            <a href="{{ route('admin.notifications') }}">
                                <i class="fas fa-bell"></i>
                                <span class="menu-title">
                                    <strong>Notifications</strong>
                                </span>
                            </a>
                        </li>
                        @endif

                        @if (auth()->user()->check_permission('view-notifications'))
                        <li class="{{ (request()->routeIs('admin.notifications*'))? 'active-link':'' }}">
                            <a href="{{ route('admin.notifications.group') }}">
                                <i class="fas fa-bell"></i>
                                <span class="menu-title">
                                    <strong>Group Notifications</strong>
                                </span>
                            </a>
                        </li>
                        @endif

                        @if (auth()->user()->check_permission('view-feedbacks'))
                        <li class="{{ (request()->routeIs('admin.feedbacks*'))? 'active-link':'' }}">
                            <a href="{{ route('admin.feedbacks.index') }}">
                                <i class="fas fa-comment-alt"></i>
                                <span class="menu-title">
                                    <strong>Feedbacks</strong>
                                </span>
                            </a>
                        </li>
                        @endif

                        @if (auth()->user()->check_permission('view-events'))
                        <li class="{{ (request()->routeIs('admin.events*'))? 'active-link':'' }}">
                            <a href="{{ route('admin.events.index') }}">
                                <i class="fas fa-store-alt"></i>
                                <span class="menu-title">
                                    <strong>Events</strong>
                                </span>
                            </a>
                        </li>
                        @endif

                        {{-- Course Management --}}
                        @if (auth()->user()->check_permission('view-courses'))
                        <li class="{{ (in_array(request()->segment(2), ['courses', 'course-categories'])) ? 'active-link' : '' }}">
                            <a href="#">
                                <i class="fa fa-file"></i>
                                <span class="menu-title">
                                    <strong>Course Management</strong>
                                </span>
                                <i class="arrow"></i>
                            </a>
                            <ul class="collapse {{ (in_array(request()->segment(2), ['courses', 'course-categories'])) ? 'in' : '' }}">
                                @if (auth()->user()->check_permission('view-categories'))
                                <li class="{{ (request()->routeIs('admin.coursecategories*'))? 'active-link':'' }}"><a href="{{ route('admin.coursecategories.index') }}">Category</a></li>
                                @endif
                                @if (auth()->user()->check_permission('view-courses'))
                                <li class="{{ (request()->routeIs('admin.courses*'))? 'active-link':'' }}"><a href="{{ route('admin.courses.index') }}">Courses</a></li>
                                @endif
                            </ul>
                        </li>
                        @endif

                        @if (auth()->user()->check_permission('view-polls'))
                        <li class="{{ (request()->routeIs('admin.polls*'))? 'active-link':'' }}">
                            <a href="{{ route('admin.polls.index') }}">
                                <i class="fas fa-poll-h"></i>
                                <span class="menu-title">
                                    <strong>Polls</strong>
                                </span>
                            </a>
                        </li>
                        @endif

                        @if (auth()->user()->check_permission('view-leaderboards'))
                        <li class="{{ (request()->routeIs('admin.leaderboards*'))? 'active-link':'' }}">
                            <a href="{{ route('admin.leaderboards.index') }}">
                                <i class="fas fa-poll-h"></i>
                                <span class="menu-title">
                                    <strong>Leaderboards</strong>
                                </span>
                            </a>
                        </li>
                        @endif

                        @if (auth()->user()->check_permission('veiw-messages'))
                        <li class="{{ (request()->routeIs('admin.message-groups*'))? 'active-link':'' }}">
                            <a href="{{ route('admin.message-groups') }}">
                                <i class="fas fa-users"></i>
                                <span class="menu-title">
                                    <strong>Messag Group</strong>
                                </span>
                            </a>
                        </li>
                        @endif

                        @if (auth()->user()->check_permission('veiw-job-descriptions'))
                        <li class="{{ (request()->routeIs('admin.job-descriptions*'))? 'active-link':'' }}">
                            <a href="{{ route('admin.job-descriptions.index') }}">
                                <i class="fas fa-list"></i>
                                <span class="menu-title">
                                    <strong>Job Description</strong>
                                </span>
                            </a>
                        </li>
                        @endif
                        <!-- <li class="">
                            <a href="{{ route('admin.email.template') }}">
                                <i class="fas fa-envelope"></i>
                                <span class="menu-title">
                                    <strong>Email Template</strong>
                                </span>
                            </a>
                        </li> -->
                    </ul>
                </div>
            </div>
        </div>
        <!--================================-->
        <!--End menu-->
    </div>
</nav>
