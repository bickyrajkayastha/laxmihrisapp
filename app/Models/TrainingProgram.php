<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TrainingProgram extends Model
{
    protected $guarded = ['id'];

    protected $appends = ['fullFilePath', 'thumbFilePath', 'croppedFilePath', 'dateRange', 'timeRange', 'fullFileUrl'];

    public function getFullFilePathAttribute()
    {
        if ($this->file) {
            return public_path('uploads/trainings/full/' . $this->file);
        }
        return asset('') . config('constants.default_banner_image');
    }

    public function getFullFileUrlAttribute()
    {
        if ($this->file) {
            return url('uploads/trainings/full/' . $this->file);
        }
        return asset('') . config('constants.default_banner_image');
    }

    public function getCroppedFilePathAttribute()
    {
        if ($this->file) {
            return public_path('uploads/trainings/crop/' . $this->file);
        }
        return asset('') . config('constants.default_banner_image');
    }

    public function getThumbFilePathAttribute()
    {
        if ($this->file) {
            return public_path('uploads/trainings/thumb/' . $this->file);
        }
        return asset('') . config('constants.default_banner_image');
    }

    public function getDateRangeAttribute()
    {
        return formatDate($this->start_date) . (($this->end_date)? " - " . formatDate($this->end_date):'');
    }

    public function getTimeRangeAttribute()
    {
        return formatTime($this->start_time) . (($this->end_time)? " - " . formatTime($this->end_time):'');
    }

    public function scopePublished($query)
    {
        return $query->where('status', 1);
    }

    public function documents()
    {
        return $this->hasMany(TrainingProgramDocument::class, 'training_id', 'id');
    }

    public function course()
    {
        return $this->belongsTo(Course::class, 'course_id', 'id');
    }
}
