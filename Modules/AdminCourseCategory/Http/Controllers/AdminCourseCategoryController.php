<?php

namespace Modules\AdminCourseCategory\Http\Controllers;

use App\Helpers\Permission;
use App\Models\Course;
use App\Models\CourseCategory;
use App\Traits\Slugger;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Log;

class AdminCourseCategoryController extends Controller
{
    use Slugger;

    private $indexRoute;

    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->indexRoute = route('admin.coursecategories.index');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $data['pageTitle'] = "Course Category Management";
        $data['categories'] = CourseCategory::orderBy('created_at', 'desc')->paginate(20);

        return view('admincoursecategory::index', $data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $data['pageTitle'] = "Add Course Category";
        $data['breadcrumbs'] = '<li><a href="' . route('admin.coursecategories.index') . '">Course Category Management</a></li><li>Add</li>';
        return view('admincoursecategory::create', $data);
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        $this->request->validate([
            'name' => 'required'
        ]);

        try {
            $data = $this->request->only('name', 'status');
            $data['slug'] = $this->createSlug($this->request->name, CourseCategory::class);

            CourseCategory::create($data);

            session()->flash('success_message', __('alerts.create_success'));
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            session()->flash('error_message', __('alerts.create_error'));
        }

        return redirect()->to($this->indexRoute);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('admincoursecategory::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $data['pageTitle'] = "Edit Course Category";
        $data['course'] = $this->getCourseCategoryById($id);
        $data['breadcrumbs'] = '<li><a href="' . route('admin.coursecategories.index') . '">Course Category Management</a></li><li>Edit</li>';

        return view('admincoursecategory::edit', $data);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update($id)
    {
        $this->request->validate([
            'name' => 'required'
        ]);

        try {
            $data = $this->request->only('name', 'status');

            $data['slug'] = $this->createSlug($this->request->name, CourseCategory::class);
            $category = CourseCategory::find($id);
            $category->update($data);

            session()->flash('success_message', __('alerts.update_success'));
        } catch (\Exception $e) {
            session()->flash('error_message', __('alerts.update_error'));
        }

        return redirect()->to($this->indexRoute);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        try {

            $category = $this->getCourseCategoryById($id);

            if ($category->status == 1) {
                session()->flash('error_message', __('alerts.delete_error_active'));
                return redirect()->to($this->indexRoute);
            }

            $category->delete();

            session()->flash('success_message', __('alerts.delete_success'));
        } catch (\Exception $e) {
            session()->flash('error_message', __('alerts.delete_error'));
        }

        return redirect()->to($this->indexRoute);
    }

    private function getCourseCategoryById($id)
    {
        return CourseCategory::find($id);
    }
}
