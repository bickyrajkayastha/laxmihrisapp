@extends('admin.layouts.master')
@push('css')
<link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/jcrop/jquery.Jcrop.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/select2/css/select2.min.css') }}">
@endpush
@section('content')
<div class="row">
	<div class="col">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Add Employee
                    <button class="btn btn-sm btn-primary pull-right panel-btn" onclick="window.history.go(-1);">
                        <i class="fas fa-long-arrow-alt-left"></i> Back
                    </button>
                </h3>
            </div>

            <!--Horizontal Form-->
            <!--===================================================-->
            <form id="employee-add" action="{{ route('admin.employees.store') }}" enctype="multipart/form-data" method="POST" class="form-horizontal">
                @csrf
                <div class="panel-body">
                    <div class="form-group{{ $errors->has('employee_code') ? ' has-error' : '' }}">
                        <label class="col-sm-2 control-label" for="input-full-employee_code">Employee Code <span class="required_color">*</span></label>
                        <div class="col-sm-9">
                            <input type="text" placeholder="" value="{{ old('employee_code') }}" name="employee_code" id="input-full-employee-code" class="form-control">
                            <small class="help-block text-danger">{{ $errors->first('employee_code')}}</small>
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label class="col-sm-2 control-label" for="input-full-name">Full Name <span class="required_color">*</span></label>
                        <div class="col-sm-9">
                            <input type="text" placeholder="" value="{{ old('name') }}" name="name" id="input-full-name" class="form-control">
                            <small class="help-block text-danger">{{ $errors->first('name')}}</small>
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label class="col-sm-2 control-label" for="input-email">Email <span class="required_color">*</span></label>
                        <div class="col-sm-9">
                            <input type="email" placeholder="" value="{{ old('email') }}" name="email" id="input-email" class="form-control">
                            <small class="help-block text-danger">{{ $errors->first('email')}}</small>
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
                        <label class="col-sm-2 control-label" for="input-gender">gender</label>
                        <div class="col-sm-9">
                            <select name="gender" id="" class="form-control">
                                <option value="" selected disabled>--Select--</option>
                                <option value="male" {{ (old('gender') == 'male')?'selected':'' }}>Male</option>
                                <option value="female" {{ (old('gender') == 'female')?'selected':'' }}>Female</option>
                            </select>
                            <small class="help-block text-danger">{{ $errors->first('gender')}}</small>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-position">Position</label>
                        <div class="col-sm-9">
                            <input type="text" placeholder="" value="{{ old('position') }}" name="position" id="input-position" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-designation">Designation</label>
                        <div class="col-sm-9">
                            <input type="text" placeholder="" value="{{ old('designation') }}" name="designation" id="input-designation" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-title">Job Description</label>
                        <div class="col-lg-9">
                            <select name="job_description_id" id="select-tags" class="form-control">
                                <option value="">--Select--</option>
                                @foreach($job_descriptions as $job)
                                    <option value="{{ $job->id }}">{{ $job->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-designation">Joined Date</label>
                        <div class="col-sm-9">
                            <div class="input-group date">
                                <input type="text" class="form-control" autocomplete="off" name="joined_date" id="joined-date" value="{{ old('joined_date') }}">
                                <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-th"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
                        <label class="col-sm-2 control-label" for="input-mobile">Mobile No.<span class="required_color">*</span></label>
                        <div class="col-sm-9">
                            <input type="text" placeholder="" value="{{ old('mobile') }}" name="mobile" id="input-mobile" class="form-control">
                            <small class="help-block text-danger">{{ $errors->first('mobile')}}</small>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="demo-hor-inputemail">Profile Picture</label>
                        <div class="col-md-10">
                            <small class="help-block text-danger">{{ $errors->first('image')}}</small>
                            <img id="preview" class="mar-btm"><br>

                            <span class="btn btn-sm btn-rose btn-primary btn-file">
                                <span>Browse</span>
                                <input type="file" name="image" id="input-image-file" onchange="fileSelectHandler()"
                                   />
                            </span>
                            <div class="image-error" style="color:red"></div>
                            <input type="hidden" id="x1" name="x1"/>
                            <input type="hidden" id="y1" name="y1"/>
                            <input type="hidden" id="x2" name="x2"/>
                            <input type="hidden" id="y2" name="y2"/>
                            <input type="hidden" id="w" name="w"/>
                            <input type="hidden" id="h" name="h"/>
                        </div>
                    </div>
                    <!-- <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                                            <label class="col-md-2 control-label">Status</label>
                                            <div class="col-md-9">
                                                <div class="radio">

                                                    Inline radio buttons
                                                    <input id="input-status" class="magic-radio" type="radio" name="status" value="1" checked="">
                                                    <label for="input-status">Active</label>

                                                    <input id="input-status-2" class="magic-radio" value="0" type="radio" name="status">
                                                    <label for="input-status-2">Deactive</label>
                                                </div>
                                            </div>
                                        </div> -->
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label class="col-sm-2 control-label" for="input-password">Password <span class="required_color">*</span></label>
                        <div class="col-sm-9">
                            <input type="password" placeholder="" value="{{ old('password') }}" name="password" id="input-password" class="form-control">
                            <small class="help-block text-danger">{{ $errors->first('password')}}</small>
                        </div>
                    </div>
                </div>
                <div class="panel-footer text-right">
                	<div class="row">
		                <div class="col-lg-11">
		                    <button class="btn btn-mint" type="submit">Create</button>
		                </div>
                	</div>
                </div>
            </form>
            <!--===================================================-->
            <!--End Horizontal Form-->

        </div>
    </div>
</div>
@endsection
@push('scripts')
<script src="{{ asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('assets/plugins/jcrop/jquery.Jcrop.min.js') }}"></script>
<script src="{{ asset('assets/plugins/select2/js/select2.min.js') }}"></script>
<script type="text/javascript">
    $( "#select-tags" ).select2({
        // theme: "bootstrap"
    });
    var aspRatio = 1/1;
    // convert bytes into friendly format
    function bytesToSize(bytes) {
        var sizes = ['Bytes', 'KB', 'MB'];
        if (bytes === 0) return 'n/a';
        var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
        return (bytes / Math.pow(1024, i)).toFixed(1) + ' ' + sizes[i];
    }

    // check for selected crop region
    function checkForm() {
        if (parseInt($('#w').val())) return true;
        $('.image-error').html('Please select a crop region and then press Upload').show();
        return false;
    }

    // update info by cropping (onChange and onSelect events handler)
    function updateInfo(e) {
        $('#x1').val(e.x);
        $('#y1').val(e.y);
        $('#x2').val(e.x2);
        $('#y2').val(e.y2);
        $('#w').val(e.w);
        $('#h').val(e.h);
    }

    // clear info by cropping (onRelease event handler)
    function clearInfo(e) {
        $('#x1').val(e.x);
        $('#y1').val(e.y);
        $('#x2').val(e.x2);
        $('#y2').val(e.y2);
        $('#w').val(300);
        $('#h').val(300);
    }
    // Create variables (in this scope) to hold the Jcrop API and image size
    var jCropApi, boundX, boundY;

    function fileSelectHandler() {
        // get selected file
        var oFile = $('#input-image-file')[0].files[0];

        if (!$('#input-image-file')[0].files[0]) {
            $('.jcrop-holder').remove();
            return;
        }
        // hide all errors
        $('.image-error').hide();
        // check for image type (jpg and png are allowed)
        var rFilter = /^(image\/jpeg|image\/png|image\/jpg|image\/gif|image\/xcf|image\/svg)$/i;
        if (!rFilter.test(oFile.type)) {
            $('#submit').prop("disabled", "disabled");
            $('.image-error').html('Please select a valid image file (jpg and png are allowed)').show();
            return;
        } else {
            $('#submit').prop("disabled", false);
        }
        // preview element
        var oImage = document.getElementById('preview');
        // prepare HTML5 FileReader
        var oReader = new FileReader();
        oReader.onload = function (e) {
            // e.target.result contains the DataURL which we can use as a source of the image
            oImage.src = e.target.result;
            oImage.onload = function () { // onload event handler
                var height = oImage.naturalHeight;

                var width = oImage.naturalWidth;

                // console.log(height);
                // console.log(width);
                window.URL.revokeObjectURL(oImage.src);

                if (height < 300 || width < 300) {

                    oImage.src = "";
                    $('#input-image-file').val('');
                    // $('#submit').prop("disabled","disabled");

                    $('.image-error').html('You have selected too small file, please select a one image with minimum size 300 X 300 px').show();

                } else {

                    $('#submit').prop("disabled", false);

                }
                var sResultFileSize = bytesToSize(oFile.size);

                // destroy Jcrop if it is existed
                if (typeof jCropApi !== 'undefined') {
                    jCropApi.destroy();
                    jCropApi = null;
                    $('#preview').width(oImage.naturalWidth);
                    $('#preview').height(oImage.naturalHeight);
                }
                setTimeout(function () {
                    // initialize Jcrop
                    $('#preview').Jcrop({
                        setSelect: [0, 0, 300, 300],
                        boxWidth: 300,
                        // boxHeight: 300,
                        minSize: [300, 300], // min crop size
                        aspectRatio: aspRatio,
                        bgFade: true, // use fade effect
                        bgOpacity: .3, // fade opacity
                        onChange: updateInfo,
                        onSelect: updateInfo,
                        onRelease: clearInfo,
                        trueSize: [oImage.naturalWidth, oImage.naturalHeight]
                    }, function () {
                        // use the Jcrop API to get the real image size
                        var bounds = this.getBounds();
                        boundx = bounds[0];
                        boundy = bounds[1];
                        // Store the Jcrop API in the jCropApi variable
                        jCropApi = this;
                    });
                }, 500);
            };
        };
        // read selected file as DataURL
        oReader.readAsDataURL(oFile);
    }
    $(function() {
        $('#joined-date').datepicker({
            format: 'yyyy-mm-dd',
        });
        $("#employee-add").on('submit', function(event) {
            $(this).find("button[type='submit']").prop('disabled', true).html('<i class="fas fa-spinner fa-spin"></i> Creating...');
        });
    });
</script>
@endpush
