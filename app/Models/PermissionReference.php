<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PermissionReference extends Model
{
	protected $table = 'permission_references';

	protected $guarded = [ 'id' ];

	public function permission_groups()
	{
	    return $this->hasMany(PermissionGroup::class,'permission_reference_id');
	}
}
