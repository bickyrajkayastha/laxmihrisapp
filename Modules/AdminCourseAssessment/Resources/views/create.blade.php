@extends('admin.layouts.master')
@push('css')
<link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/jcrop/jquery.Jcrop.min.css') }}">
@endpush
@section('content')

<div class="row">
    <div class="col">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Add Question
                    <button class="btn btn-sm btn-primary pull-right panel-btn" onclick="window.history.go(-1);">
                        <i class="fas fa-long-arrow-alt-left"></i> Back
                    </button>
                </h3>
            </div>
            <!--Hover Rows-->
            <!--===================================================-->
            <form class="form-horizontal" id="add-course" action="{{ route('admin.courses.assessment.store') }}" method="POST"
                  enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="course_id" value="{{ $course->id }}">
                <div class="panel-body">
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label class="col-sm-2 control-label" for="">Question <span
                                    class="required_color">*</span></label>
                        <div class="col-sm-9">
                            <input type="text" placeholder="" name="question" value="{{ old('question') }}" id="input-question"
                                   class="form-control" >
                            @if($errors->has('question'))
                            <small class="help-block text-danger">{{ $errors->first('question')}}</small>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                        <label class="col-md-2 control-label">Publish</label>
                        <div class="col-md-9">
                            <div class="radio">

                                <!-- Inline radio buttons -->
                                <input id="input-status" class="magic-radio" type="radio" name="status" value="1" <?= ((old('status') == 1)? 'checked':''); ?>>
                                <label for="input-status">Yes</label>

                                <input id="input-status-2" class="magic-radio" value="0" type="radio" name="status" <?= ((old('status') == 0)? 'checked':''); ?>>
                                <label for="input-status-2">No</label>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col">
                            <h5 class="pull-left">Options
                            </h5>
                            <button id="add-option-btn" class="btn btn-sm btn-primary pull-right">
                                <i class="fas fa-plus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <small><i>Tick for correct answer.</i></small>
                        </div>
                    </div>
                    <div id="option-div">
                        @if(session()->getOldInput('options'))
                        <?php $n = 1; ?>
                        @foreach(session()->getOldInput('options') as $key => $option)
                        <div class="form-group{{ $errors->has('options.' . $key) ? ' has-error' : '' }}">
                            <label class="col-sm-2 control-label" for="input-title">Option <span class="option-number">{{ $n ++ }}</span> <span class="required_color">*</span></label>
                            <div class="col-sm-9">
                                <input type="text" placeholder="" value="{{ $option }}" name="options[{{ $key }}]" class="form-control">
                            </div>
                            <div><button class="btn btn-xs btn-danger option-remove-btn" title="Remove"><i class="fas fa-times"></i></button></div>
                        </div>
                        @endforeach
                        @else
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-title">Option 1 <span class="required_color">*</span></label>
                            <div class="col-sm-9">
                                <div class="radio">
                                    <!-- Inline radio buttons -->
                                    <input id="input-correct" class="magic-radio-option" type="radio" name="options[0][correct]" value="1">
                                    <label for="input-correct" style="width: 100%;">
                                        <input type="text" placeholder="" value="" name="options[0][option]" class="form-control">
                                    </label>
                                </div>

                            </div>
                        </div>
                        @endif
                    </div>
                    <div class="col-sm-2"></div>
                    <div class="has-error">
                        @if($errors->first('options.*'))
                        <small class="help-block text-danger">{{ $errors->first('options.*')}}</small>
                        @else
                        <small class="help-block text-danger">{{ $errors->first('options')}}</small>
                        @endif
                    </div>
                    <div class="form-group">
                        <div class="col-lg-2 col-sm-12 col-xs-12 pull-right">
                            <button class="btn btn-block btn-success" type="submit">Create</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@push('scripts')
<script src="{{ asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.js') }}"></script>
<script src="{{ asset('assets/plugins/jcrop/jquery.Jcrop.min.js') }}"></script>
<script>
    $(function() {
        $("#add-course").on('submit', function(course) {
            $(this).find("button[type='submit']").prop('disabled', true).html('<i class="fas fa-spinner fa-spin"></i> Creating...');
        });

        $(document).on('click', ".magic-radio-option", function() {
            $(".magic-radio-option").each(function(i, v) {
                $(v).prop('checked', false);
            });
            $(this).prop('checked', true);
        });

        $("#add-option-btn").on('click', function(event) {
            event.preventDefault();
            var n = $("#option-div>div").length + 1;
            var option_div = '<div class="form-group{{ $errors->has('options[]') ? ' has-error' : '' }}">\
                <label class="col-sm-2 control-label" for="input-title">Option <span class="option-number">'+n+'</span> <span class="required_color">*</span></label>\
                <div class="col-sm-9">\
                    <div class="radio">\
                        <input id="input-correct-'+n+'" class="magic-radio-option" type="radio" name="options['+n+'][correct]" value="1">\
                        <label for="input-correct-'+n+'" style="width: 100%;">\
                            <input type="text" placeholder="" value="{{ old('options[]') }}" name="options['+n+'][option]"" class="form-control">\
                            <small class="help-block text-danger">{{ $errors->first('options[]')}}</small>\
                        </lable>\
                    </div>\
                </div>\
                <div><button class="btn btn-xs btn-danger option-remove-btn" title="Remove"><i class="fas fa-times"></i></button></div>\
            </div>';

            $("#option-div").append(option_div);
        });

        function rearrangeNumber() {
            var n = 1;
            $.each($("#option-div>div"), function(i, v) {
                $(v).find('.option-number').html(n++);
            });
        }

        $(document).on('click', '.option-remove-btn', function(event) {
            event.preventDefault();
            $(this).closest('.form-group').remove();
            rearrangeNumber();
        });
    });
</script>
@endpush
