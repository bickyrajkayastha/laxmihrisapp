<?php

namespace Modules\AdminLogin\Http\Controllers;

use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AdminLoginController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('adminlogin::index');
    }

    public function login(Request $request)
    {
        $rules = [
            'username' => 'required',
            'password' => 'required'
        ];

        $messages = [
            'username.required' => 'Email/ Username is required',
            'password.required' => 'Password is required'
        ];

        $request->validate($rules, $messages);

        $username = $request->username;

        $password = $request->password;

        if (filter_var($username, FILTER_VALIDATE_EMAIL)) {
            $loginStatus = Auth::guard('admin')->attempt(['email' => $username, 'password' => $password],
                $request->has('remember_me'));
        } else {
            $loginStatus = Auth::guard('admin')->attempt(['username' => $username, 'password' => $password],
                $request->has('remember_me'));
        }

        if ($loginStatus) {
            $user = Admin::where('email', $username)
                ->orWhere('username', $username)
                ->first();

            Auth::guard('admin')->login($user);

            return redirect()->intended(route('admin.dashboard'));
        }

        session()->flash('login_failed', 'Credentials do not match our records.');

        return redirect()->route('admin.login');
    }

    public function logOut()
    {
        Auth::guard('admin')->logOut();

        return redirect()->route('admin.login');
    }
}
