<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmployeeCodeFieldsToEmployeesTeable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employees', function (Blueprint $table) {
            $table->string('employee_code', 191)->nullable();
            $table->string('position', 191)->nullable();
            $table->string('gender', 191)->nullable();
            $table->string('mobile', 191)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employees', function (Blueprint $table) {
            $table->dropColumn('employee_code');
            $table->dropColumn('position');
            $table->dropColumn('gender');
            $table->dropColumn('mobile');
        });
    }
}
