<?php

namespace Modules\AdminJobDescription\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class AdminJobDescriptionServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(module_path('AdminJobDescription', 'Database/Migrations'));
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path('AdminJobDescription', 'Config/config.php') => config_path('adminjobdescription.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path('AdminJobDescription', 'Config/config.php'), 'adminjobdescription'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/adminjobdescription');

        $sourcePath = module_path('AdminJobDescription', 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ],'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/adminjobdescription';
        }, \Config::get('view.paths')), [$sourcePath]), 'adminjobdescription');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/adminjobdescription');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'adminjobdescription');
        } else {
            $this->loadTranslationsFrom(module_path('AdminJobDescription', 'Resources/lang'), 'adminjobdescription');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (! app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(module_path('AdminJobDescription', 'Database/factories'));
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
