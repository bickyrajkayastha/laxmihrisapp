<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contents', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable()->default(null);
            $table->integer('parent_id')->nullable()->default(null);
            $table->string('title')->nullable()->default(null);
            $table->longText('body')->nullable()->default(null);
            $table->tinyInteger('type')->default(0);
            $table->string('slug','191')->nullable()->default(null)->index();
            $table->integer('order_by')->nullable()->default(0);
            $table->boolean('status')->nullable()->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contents');
    }
}
