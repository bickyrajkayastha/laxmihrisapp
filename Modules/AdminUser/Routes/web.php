<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin/users', 'as' => 'admin.', 'middleware' => 'auth:admin'], function() {

    // Users
    Route::get('/', 'AdminUserController@userLists')
        ->name('user.list')
        ->middleware('can:view-users');

    Route::get('add', 'AdminUserController@userAdd')
        ->name('user.add')
        ->middleware('can:create-users');

    Route::post('store','AdminUserController@userStore')
        ->name('user.store')
        ->middleware('can:create-users');

    Route::get('edit/{id}', 'AdminUserController@userEdit')
        ->name('user.edit')
        ->middleware('can:edit-users');

    Route::post('update', 'AdminUserController@userUpdate')
        ->name('user.update')
        ->middleware('can:edit-users');

    Route::post('{id}/destroy', 'AdminUserController@userDestroy')
        ->name('user.destroy')
        ->middleware('can:delete-users');

    Route::post('password-reset-user', 'AdminUserController@passwordResetUser')
        ->name('user.password-reset-user')
        ->middleware('can:edit-users');

    // Route : Profile Update
    Route::get('profile', 'AdminUserController@profile')
        ->name('user.profile');

    Route::post('profile/store', 'AdminUserController@profileStore')
        ->name('user.profile.store');

    // Route : Change Password
    Route::get('password-reset','AdminUserController@passwordReset')
        ->name('user.password-reset');

    Route::post('password-reset/store','AdminUserController@passwordResetStore')
        ->name('user.password-reset.store');

    // User Groups
    Route::get('groups', 'AdminUserController@groups')
        ->name('user.groups')
        ->middleware('can:view-groups');

    Route::post('groups/store', 'AdminUserController@groupStore')
        ->name('user.groups.store')
        ->middleware('can:create-groups');

    Route::get('groups/edit/{id}', 'AdminUserController@groupsEdit')
        ->name('user.groups.edit')
        ->middleware('can:edit-groups');

    Route::post('groups/update', 'AdminUserController@groupsUpdate')
        ->name('user.groups.update')
        ->middleware('can:edit-groups');

    Route::post('groups/destroy/{id}','AdminUserController@groupDestroy')
        ->name('user.groups.destroy')
        ->middleware('can:delete-groups');

    Route::get('groups/permission/{id}', 'AdminUserController@groupPermission')
        ->name('user.groups.permission')
        ->middleware('can:edit-groups');

    Route::post('groups/permission-store', 'AdminUserController@groupPermissionStore')
        ->name('user.groups.permission-store')
        ->middleware('can:edit-groups');

});
