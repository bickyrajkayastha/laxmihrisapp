<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_message', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('message_id')->index();
            $table->foreign('message_id')->references('id')->on('messages')->onDelete('cascade');
            $table->unsignedInteger('sender_id')->comment('employee id')->index();
            $table->foreign('sender_id')->references('id')->on('employees')->onDelete('cascade');
            $table->unsignedInteger('receiver_id')->comment('employee id')->nullable()->index();
            $table->foreign('receiver_id')->references('id')->on('employees')->onDelete('cascade');
            $table->tinyInteger('seen_status')->default(0);
            $table->tinyInteger('deliver_status')->default(0);
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_messages');
    }
}
