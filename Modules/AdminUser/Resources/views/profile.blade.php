@extends('admin.layouts.master')
@section('content')
<div class="row">
    <div class="col">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">
                    My Profile
                    <span class="required_fields" style="float: right;"><strong class="required_color">*</strong> These fields are required.</span>
                </h3>
            </div>

            <form action="{{ route('admin.user.profile.store') }}" method="POST" class="form-horizontal form-padding" enctype="multipart/form-data">
                <div class="panel-body">
                    @csrf
                    <input type="hidden" name="old_username" value="{{ $user->username }}">
                    <input type="hidden" name="old_email" value="{{ $user->email }}">
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="admin-first-name">Full Name <span class="required_color">*</span></label>
                        <div class="col-md-10">
                            <div class="row">
                                <div class="col-md-4">
                                    <input type="text" id="admin-first-name" name="firstname" class="form-control" value="{{ $user->first_name }}" placeholder="First Name" >
                                    <small class="help-block text-danger">{{$errors->first('firstname')}}</small>
                                </div>
                                <div class="col-md-4">
                                    <input type="text" id="admin-first-name" name="middlename" class="form-control" value="{{ $user->middle_name }}" placeholder="Middle Name" >
                                    <small class="help-block text-danger">{{$errors->first('middlename')}}</small>
                                </div>
                                <div class="col-md-4">
                                    <input type="text" id="admin-last-name" name="lastname" class="form-control" value="{{ $user->last_name }}" placeholder="Last Name" >
                                    <small class="help-block text-danger">{{$errors->first('lastname')}}</small>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label" for="demo-address-input">Username</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" name="username" value="{{ old('username') ? old('username') : $user->username }}">
                            <small class="help-block text-danger">{{$errors->first('username')}}</small>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label" for="demo-address-input">Email</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" name="email" value="{{ old('email') ? old('email') : $user->email }}">
                            <small class="help-block text-danger">{{$errors->first('email')}}</small>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label"></label>
                        <div class="col-md-2 pull-right">
                            <input type="submit" class="btn btn-sm btn-block btn-primary" value="Update Profile">
                        </div>
                    </div>
                </div>


            </form>
        </div>    
    </div>
</div>
@stop