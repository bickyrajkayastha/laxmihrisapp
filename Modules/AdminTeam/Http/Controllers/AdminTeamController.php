<?php

namespace Modules\AdminTeam\Http\Controllers;

use App\Helpers\Permission;
use App\Models\Team;
use App\Services\ImageUpload\Strategy\UploadWithAspectRatio;
use App\Traits\Slugger;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Intervention\Image\Facades\Image;
use Modules\AdminTeam\Services\TeamImageUploader;
use Illuminate\Support\Facades\Log;

class AdminTeamController extends Controller
{
    use Slugger;

    private $indexRoute;

    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->indexRoute = route('admin.teams.index');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $data['pageTitle'] = "Team Management";
        $data['teams'] = Team::paginate(20);

        return view('adminteam::index', $data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $data['pageTitle'] = "Add Team";
        $data['breadcrumbs'] = '<li><a href="' . route('admin.teams.index') . '">Team Management</a></li><li>Add</li>';
        return view('adminteam::create', $data);
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        $this->request->validate([
            'name' => 'required',
            'phone' => 'nullable|min:10|max:15',
            'image' => 'nullable|image|mimes:png,jpg,jpeg',
            'email' => 'required|unique:teams,email'
        ]);

        try {
            $data = $this->request->only('name', 'description', 'position', 'team_member_type', 'status', 'email', 'phone');

            $data['slug'] = $this->createSlug($this->request->name, Team::class);

            if ($this->request->hasFile('image')) {
                $image = $this->request->file('image');

                $uploader = new TeamImageUploader(new UploadWithAspectRatio());

                $data['image'] = $uploader->saveOriginalImage($image);

                $this->cropAndSaveImage(
                    $uploader,
                    $data['image'],
                    $this->request->x1,
                    $this->request->y1,
                    $this->request->w,
                    $this->request->h
                );

            }

            Team::create($data);

            session()->flash('success_message', __('alerts.create_success'));
        } catch (\Exception $e) {

            // Log::info($e->getMessage());
            session()->flash('error_message', __('alerts.create_error'));
        }

        return redirect()->to($this->indexRoute);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('adminteam::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $data['pageTitle'] = "Edit Team";
        $data['team'] = $this->getTeamById($id);
        $data['breadcrumbs'] = '<li><a href="' . route('admin.teams.index') . '">Team Management</a></li><li>Edit</li>';

        return view('adminteam::edit', $data);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update($id)
    {
        \Log::info($this->request->all());
        $this->request->validate([
            'name' => 'required',
            'phone' => 'nullable|min:10|max:15',
            'image' => 'nullable|image|mimes:png,jpg,jpeg',
            'email' => 'required|unique:teams,email,' . $id
        ]);

        try {
            $data = $this->request->only('name', 'description', 'position', 'team_member_type', 'status', 'email', 'phone');

            $data['slug'] = $this->createSlug($this->request->name, Team::class);

            $team = Team::find($id);

            if ($this->request->hasFile('image') || $this->request->croppreviousimage == 'true') {
                $uploader = new TeamImageUploader(new UploadWithAspectRatio());

                $uploader->deleteThumbImage($team->image);

                $uploader->deleteCroppedImage($team->image);

                if ($this->request->croppreviousimage == 'true') {
                    $data['image'] = $team->image;
                } else {
                    $uploader->deleteFullImage($team->image);

                    $data['image'] = $uploader->saveOriginalImage($this->request->file('image'));
                }

                $this->cropAndSaveImage(
                    $uploader,
                    $data['image'],
                    $this->request->x1,
                    $this->request->y1,
                    $this->request->w,
                    $this->request->h
                );
            }

            $team->update($data);

            session()->flash('success_message', __('alerts.update_success'));
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            session()->flash('error_message', __('alerts.update_error'));
        }

        return redirect()->to($this->indexRoute);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        try {

            $team = $this->getTeamById($id);

            if($team->status == 1){
                session()->flash('error_message', __('alerts.delete_error_active'));
                return redirect()->to($this->indexRoute);
            }

            $uploader = new TeamImageUploader();

            $uploader->deleteFullImage($team->image);

            $uploader->deleteThumbImage($team->image);

            $uploader->deleteCroppedImage($team->image);

            $team->delete();

            session()->flash('success_message', __('alerts.delete_success'));

        } catch (\Exception $e) {
            session()->flash('error_message', __('alerts.delete_error'));
        }

        return redirect()->to($this->indexRoute);
    }

    private function cropAndSaveImage($uploader, $filename, $posX1, $posY1, $width, $height)
    {
        $imgPath = $uploader->getFullImagePath($filename);

        $fullImage = Image::make($imgPath);

        $cropDestPath = $uploader->getCroppedImagePath($filename);

        $uploader->cropAndSaveImage($fullImage, $cropDestPath, $posX1, $posY1, $width, $height);

        $uploader->cropAndSaveImageThumb($fullImage, $filename);
    }

    private function getTeamById($id)
    {
        return Team::find($id);
    }
}
