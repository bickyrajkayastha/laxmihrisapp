<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin/polls', 'as' => 'admin.', 'middleware' => 'auth:admin'], function() {
    Route::get('/', 'AdminPollController@index')->name('polls.index')->middleware('can:view-polls');
    Route::get('/create', 'AdminPollController@create')->name('polls.create')->middleware('can:create-polls');
    Route::post('/create', 'AdminPollController@store')->name('polls.store')->middleware('can:create-polls');
    Route::get('/{id}/edit', 'AdminPollController@edit')->name('polls.edit')->middleware('can:edit-polls');
    Route::get('/{id}/detail', 'AdminPollController@show')->name('polls.view')->middleware('can:view-polls');
    // Route::post('/{id}/edit', 'AdminPollController@update')->name('polls.update')->middleware('can:edit-polls');
    Route::post('/{id}/delete', 'AdminPollController@destroy')->name('polls.destroy')->middleware('can:delete-polls');
});

