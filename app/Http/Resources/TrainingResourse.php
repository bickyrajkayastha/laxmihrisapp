<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TrainingResourse extends JsonResource
{
    protected $success = true;
    protected $message = "";

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        $chapters = collect($this['chapters'])->map(function($training) {
            return [
                'id' => $training->id,
                'title' => $training->title,
                'description' => $training->description,
                'dateRange' => $training->dateRange,
                'timeRange' => $training->timeRange,
            ];
        });

        return [
            'assessment' => $this['assessment'],
            'chapters' => $chapters
        ];
    }

    public function with($request)
    {
        if ($this->resource) {
            $this->message = "List fetched.";
        } else {
            $this->message = "Empty list.";
        }

        return [
            'success' => $this->success,
            'message' => $this->message
        ];
    }
}
