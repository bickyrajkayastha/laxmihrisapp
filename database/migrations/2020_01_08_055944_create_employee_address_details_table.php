<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeAddressDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_permanent_address_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('house_no')->nullable();
            $table->string('street_name')->nullable();
            $table->string('municipality')->nullable();
            $table->string('ward_no')->nullable();
            $table->string('district')->nullable();
            $table->string('zone')->nullable();
            $table->tinyInteger('same_as_permanent')->default(0);
            $table->unsignedInteger('employee_id')->index();
            $table->foreign('employee_id')->references('id')->on('employees')->onDelete('cascade');
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_address_details');
    }
}
