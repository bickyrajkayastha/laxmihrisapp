<?php

namespace Modules\AdminPoll\Http\Controllers;

use App\Helpers\Permission;
use App\Models\Poll;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Log;

class AdminPollController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $data['pageTitle'] = "Poll's";
        $data['polls'] = Poll::latest()->get();
        $data['breadcrumbs'] = '<li><a href="' . route('admin.polls.index') . '">Poll Management</a></li>';

        return view('adminpoll::index', $data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $data['breadcrumbs'] = '<li><a href="' . route('admin.polls.index') . '">Poll Management</a></li><li>Add</li>';

        return view('adminpoll::create', $data);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'options' => 'required|array|min:2',
            'closing_date' => 'required|date',
            'options.*' => 'required'
        ], [
            'options.*.required' => 'Every option is required.',
            'options.min' => 'At least 2 options is required.',
        ]);

        try {

            $poll = new Poll;
            $poll->title = $request->title;
            $poll->status = $request->status;
            $poll->description = $request->description;
            $poll->closing_date = $request->closing_date;
            $poll->admin_id = auth()->user()->id;

            if ($poll->save()) {
                foreach ($request->options as $option) {
                    $option_data[]['option'] = $option;
                }
                $poll->poll_options()->createMany($option_data);
            }

            session()->flash('success_message', __('alerts.create_success'));
            return redirect()->route('admin.polls.index');

        } catch (\Exception $e) {
            // Log::info($e->getMessage());
            session()->flash('error_message', __('alerts.create_error'));
            return redirect()->route('admin.polls.index');
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $data['poll'] = Poll::with('poll_options')->find($id);
        $data['employees'] = \App\Models\Employee::select('id', 'name', 'image')->with([
            'poll_options' => function($q) use ($id) {
                $q->select('poll_options.id', 'poll_options.option')->where('poll_options.poll_id', '=', $id);
            }
        ])->pollOption($id)->paginate(10);

        $data['breadcrumbs'] = '<li><a href="' . route('admin.polls.index') . '">Poll Management</a></li><li>Detail</li>';
        // $employees = \App\Model\EmployeePollOption::where('poll_id', '=', $id)->with('')->paginate(1);
        return view('adminpoll::show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $data['pageTitle'] = "Edit Poll";
        $poll = Poll::with('poll_options')->find($id);

        if (!$poll) {
            session()->flash('error_message', 'Poll not available for editing.');
            return redirect()->back();
        }

        $data['poll'] = $poll;
        $data['breadcrumbs'] = '<li><a href="' . route('admin.polls.index') . '">Poll Management</a></li><li>Edit</li>';

        return view('adminpoll::edit', $data);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $poll = Poll::find($id);
        $request->validate([
            'title' => 'required',
            'options' => 'required|array|min:2',
            'closing_date' => 'required|date',
            'options.*' => 'required'
        ], [
            'options.*.required' => 'Every option is required.',
            'options.min' => 'At least 2 options is required.',
        ]);

        try {

            $poll->title = $request->title;
            $poll->status = $request->status;
            $poll->description = $request->description;
            $poll->closing_date = $request->closing_date;

            if ($poll->save()) {
                if ($poll->poll_options()->delete()) {
                    foreach ($request->options as $option) {
                        $option_data[]['option'] = $option;
                    }
                    $poll->poll_options()->createMany($option_data);
                }
            }

            session()->flash('success_message', __('alerts.update_success'));
            return redirect()->route('admin.polls.index');

        } catch (\Exception $e) {
            session()->flash('error_message', __('alerts.update_error'));
            return redirect()->route('admin.polls.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        if (!Permission::check('delete-polls'))
            return redirect()->route('admin.401');
        try {

            $poll = Poll::find($id);

            if($poll->status == 1){
                session()->flash('error_message', __('alerts.delete_error_active'));
                return redirect()->route('admin.polls.index');
            }
            $poll->delete();

            session()->flash('success_message', __('alerts.delete_success'));
            return redirect()->route('admin.polls.index');

        } catch (\Exception $e) {

            session()->flash('error_message', __('alerts.delete_error'));
            return redirect()->route('admin.polls.index');
        }
    }
}
