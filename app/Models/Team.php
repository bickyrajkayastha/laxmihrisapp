<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
	protected $guarded = ['id'];

    protected $appends = ['fullImgPath', 'thumbImgPath'];

    public function getFullImgPathAttribute()
    {
        if ($this->image) {
            return url('uploads/teams/full/' . $this->image);
        }
        return "";
    }

    public function getCroppedImgPathAttribute()
    {
        if ($this->image) {
            return url('uploads/teams/crop/' . $this->image);
        }
        return "";
    }

    public function getThumbImgPathAttribute()
    {
        if ($this->image) {
            return url('uploads/teams/thumb/' . $this->image);
        }
        return "";
    }
}
