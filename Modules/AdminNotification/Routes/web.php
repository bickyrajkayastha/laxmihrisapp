<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin/notifications', 'as' => 'admin.', 'middleware' => 'auth:admin'], function() {

    Route::get('/', 'AdminNotificationController@index')
        ->name('notifications')
        ->middleware('can:view-notifications');

    Route::get('/create', 'AdminNotificationController@create')
        ->name('notifications.create')
        ->middleware('can:create-notifications');

    Route::post('/create', 'AdminNotificationController@store')
        ->name('notifications.store')
        ->middleware('can:create-notifications');

    Route::get('/{id}/edit', 'AdminNotificationController@edit')
        ->name('notifications.edit')
        ->middleware('can:edit-notifications');

    Route::post('/{id}/edit', 'AdminNotificationController@update')
        ->name('notifications.update')
        ->middleware('can:edit-notifications');

    Route::post('/{id}/delete', 'AdminNotificationController@destroy')
        ->name('notifications.destroy')
        ->middleware('can:delete-notifications');

    Route::post('/send-notification', 'AdminNotificationController@sendNotification')
        ->name('notifications.send-notification')
        ->middleware('can:send-notifications');

    Route::get('/send-group-notification', 'AdminNotificationController@groupNotification')
    ->name('notifications.group')
    ->middleware('can:view-notifications');

    Route::get('/create-group', 'AdminNotificationController@createGroup')
        ->name('notifications.group.create')
        ->middleware('can:create-notifications');

    Route::post('/create-group', 'AdminNotificationController@storeGroup')
        ->name('notifications.group.store')
        ->middleware('can:create-notifications');
});
