<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Log;

class EmployeeResource extends JsonResource
{
    public $message = "";

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        $personal_detail = [];
        $per_add_detail = [];
        $fam_detail = [];
        $temp_add_detail = [];
        $children = [];
        $education = [];
        $work = [];
        $emergency_contact = [];
        $relative_bank = [];
        $fam_loans = [];
        $ifnos = [];
        $other_info = [];

        if ($this->employee_personal_detail) {
            $personal_detail = [
                'id' => $this->employee_personal_detail->id,
                'full_name' => $this->employee_personal_detail->full_name,
                'employee_id' => $this->employee_personal_detail->employee_id,
                'dob' => $this->employee_personal_detail->dob,
                'gender' => $this->employee_personal_detail->gender,
                'nationality_at_birth' => $this->employee_personal_detail->nationality_at_birth,
                'present_nationality' => $this->employee_personal_detail->present_nationality,
                'citizenship_no' => $this->employee_personal_detail->citizenship_no,
                'citizenship_issuing_office' => $this->employee_personal_detail->citizenship_issuing_office,
                'driving_license_no' => $this->employee_personal_detail->driving_license_no,
                'pan_number' => $this->employee_personal_detail->pan_number
            ];
        }

        if ($this->employee_permanent_address_detail) {
            $per_add_detail = [
                'id' => $this->employee_permanent_address_detail->id,
                'house_no' => $this->employee_permanent_address_detail->house_no,
                'street_name' => $this->employee_permanent_address_detail->street_name,
                'municipality' => $this->employee_permanent_address_detail->municipality,
                'ward_no' => $this->employee_permanent_address_detail->ward_no,
                'district' => $this->employee_permanent_address_detail->district,
                'zone' => $this->employee_permanent_address_detail->zone,
                'same_as_permanent' => ($this->employee_permanent_address_detail->same_as_permanent)?true: false
            ];
        }

        if ($this->employee_temporary_address_detail) {
            $temp_add_detail = [
                'id' => $this->employee_temporary_address_detail->id,
                'house_no' => $this->employee_temporary_address_detail->house_no,
                'street_name' => $this->employee_temporary_address_detail->street_name,
                'municipality' => $this->employee_temporary_address_detail->municipality,
                'ward_no' => $this->employee_temporary_address_detail->ward_no,
                'district' => $this->employee_temporary_address_detail->district,
                'zone' => $this->employee_temporary_address_detail->zone
            ];
        }

        if ($this->employee_personal_detail) {
            $fam_detail = [
                'id' => $this->employee_personal_detail->id,
                'father_name' => $this->employee_personal_detail->father_name,
                'father_occupation' => $this->employee_personal_detail->father_occupation,
                'mother_name' => $this->employee_personal_detail->mother_name,
                'mother_occupation' => $this->employee_personal_detail->mother_occupation,
                'grand_father_name' => $this->employee_personal_detail->grand_father_name,
                'grand_mother_name' => $this->employee_personal_detail->grand_mother_name,
                'spouse_name' => $this->employee_personal_detail->spouse_name,
                'spouse_occupation' => $this->employee_personal_detail->spouse_occupation,
                'spouse_dob' => $this->employee_personal_detail->spouse_dob,
                'wedding_anniversary' => $this->employee_personal_detail->wedding_anniversary,
                'children' => ($this->employee_personal_detail->children)?true: false
            ];
        }

        if ($this->employee_children) {
            $children = collect($this->employee_children)->map(function($item) {
                return [
                    'id' => $item->id,
                    'full_name' => $item->full_name,
                    'dob' => $item->dob,
                    'relationship' => $item->relationship
                ];
            });
        }

        if ($this->employee_educations) {
            $education = collect($this->employee_educations)->map(function($item) {
                return [
                    'id' => $item->id,
                    'academic_level' => $item->academic_level,
                    'institute' => $item->institute,
                    'address' => $item->address,
                    'graduating_year' => $item->graduating_year,
                    'major_subject' => $item->major_subject
                ];
            });
        }

        if ($this->employee_work_experiences) {
            $work = collect($this->employee_work_experiences)->map(function($item) {
                return [
                    'id' => $item->id,
                    'from' => $item->from,
                    'to' => $item->to,
                    'post_title' => $item->post_title,
                    'employer_name' => $item->employer_name,
                    'supervisor_name' => $item->supervisor_name,
                    'reason_of_leaving' => $item->reason_of_leaving
                ];
            });
        }

        if ($this->employee_relative_contact) {
            $emergency_contact = [
                'id' => $this->employee_relative_contact->id,
                'contact_name' => $this->employee_relative_contact->contact_name,
                'address' => $this->employee_relative_contact->address,
                'phone' => $this->employee_relative_contact->phone,
                'relationship' => $this->employee_relative_contact->relationship
            ];
        }

        if ($this->employee_relative_on_banks) {
            $relative_bank = collect($this->employee_relative_on_banks)->map(function($item) {
                return [
                    'id' => $item->id,
                    'full_name' => $item->full_name,
                    'relationship' => $item->relationship,
                    'organisation' => $item->organisation
                ];
            });
        }

        if ($this->employee_family_loans) {
            $fam_loans = collect($this->employee_family_loans)->map(function($item) {
                return [
                    'id' => $item->id,
                    'full_name' => $item->full_name,
                    'relationship' => $item->relationship,
                    'loan_type' => $item->loan_type
                ];
            });
        }

        $ifnos = ($this->employee_other_information)?$this->employee_other_information: [];
        if ($this->relative_on_bank) {
            $other_info = [
                'id' => $ifnos->id,
                'relative_on_bank' => ($ifnos->relative_on_bank)?true: false,
                'family_member_on_laxmi_bank' => ($ifnos->family_member_on_laxmi_bank)?true: false,
            ];
        }

        return [
            'id' => $this->id,
            'employee_code' => $this->employee_code,
            'phone' => $this->phone,
            'email' => $this->email,
            'name' => ($this->employee_personal_detail)?$this->employee_personal_detail->full_name: $this->name,
            'designation' => $this->designation,
            'image' => $this->croppedImgPath,
            'employee_personal_detail' => $personal_detail,
            'employee_permanent_address_detail' => $per_add_detail,
            'employee_temporary_address_detail' => $temp_add_detail,
            'employee_family_detail' =>  $fam_detail,
            'employee_children' => $children,
            'employee_educations' => $education,
            'employee_work_experiences' => $work,
            'employee_emergency_contact' => $emergency_contact,
            'employee_other_information' => $other_info,
            'employee_relative_on_banks' => $relative_bank,
            'employee_family_loans' => $fam_loans
        ];

    }

    public function with($request)
    {
        if ($this->resource) {
            $this->message = "Employee detail.";
        } else {
            $this->message = "Employee not found.";
        }

        return [
            'success' => true,
            'message' => $this->message
        ];
    }

    /**
     * Customize the outgoing response for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Http\Response  $response
     * @return void
     */
    public function withResponse($request, $response)
    {
        $response->setStatusCode(200);
    }
}
