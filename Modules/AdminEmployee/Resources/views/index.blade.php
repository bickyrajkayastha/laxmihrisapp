@extends('admin.layouts.master')

@section('content')
<div class="row">
    <div class="col">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Employee Management
                    <a href="{{ route('admin.employees.import') }}" class="btn btn-sm panel-btn btn-warning pull-right"
                       style="margin-left: 3px;"> Import Employees
                    </a>
                    <a href="{{ route('admin.employees.create') }}" title="Import Employees List from Excel File" class="btn btn-primary btn-sm panel-btn pull-right"><i class="fa fa-plus"></i> Add Employee</a>
                </h3>
            </div>

            <!-- Striped Table -->
            <!--===================================================-->
            <div class="panel-body">
                <div class="pad-btm form-inline">
                    <div class="row">
                        <div class="col-sm-12">
                            <form action="{{ route('admin.employees') }}">
                                <div class="form-group">
                                    <input id="input-search-code" value="{{ request('employee_code') }}" name="employee_code" type="text" placeholder="Employee Code" class="form-control" autocomplete="off">
                                    <input id="input-search-name" value="{{ request('name') }}" name="name" type="text" placeholder="Name" class="form-control" autocomplete="off">
                                    <input id="input-search-email" value="{{ request('email') }}" name="email" type="text" placeholder="Email" class="form-control" autocomplete="off">
                                    <input id="input-search-mobile" value="{{ request('mobile') }}" name="mobile" type="text" placeholder="Mobile" class="form-control" autocomplete="off">
                                    <select class="form-control" name="status">
                                        <option value="all" {{ request('status') == 'all' ? 'selected="selected"' : "" }}>All Employees</option>
                                        <option value="1" {{ request('status') == '1' ? 'selected="selected"' : "" }}>Active</option>
                                        <option value="0" {{ request('status') == '0' ? 'selected="selected"' : "" }}>Inactive</option>
                                    </select>
                                </div>
                                <div class="btn-group">
                                    <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                                    <a href="{{ route('admin.employees') }}"  class="btn btn-default">Reset</a>
                                    <!-- <div class="btn-group dropdown">
                                        <button data-toggle="dropdown" class="btn btn-default dropdown-toggle">
                                            <i class="demo-pli-gear"></i>
                                            <span class="caret"></span>
                                        </button>
                                        <ul role="menu" class="dropdown-menu dropdown-menu-right">
                                            <li><a href="#">Action</a></li>
                                            <li><a href="#">Another action</a></li>
                                            <li><a href="#">Something else here</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#">Separated link</a></li>
                                        </ul>
                                    </div> -->
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
	                            <th>S.N</th>
	                            <th>Employee Code</th>
	                            <th>Full Name</th>
                                <th>Email</th>
                                <th>Designation</th>
                                <th>Joined Date</th>
                                <th>Mobile</th>
	                            <th>Status</th>
	                            <th style="min-width: 180px;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $i = $employees->perPage() * ($employees->currentPage() - 1);
                            @endphp
                        	@forelse($employees as $employee)
                    	    <tr>
                                <td> {{ ++$i }} </td>
                                <td>{{ $employee->employee_code ?? '--' }}</td>
                    	        <td>
                                    ​<picture>
                                      <img src="{{ $employee->thumbImgPath }}" class="img-fluid" width="40px" height="40px" alt="{{ $employee->name }}">
                                    </picture>
                                    {{ $employee->name }}
                                </td>
                                <td>{{ $employee->email ?? '--' }}</td>
                                <td>{{ $employee->designation ?? '--' }}</td>
                                <td>{{ formatDate($employee->joined_date) }}</td>
                                <td>{{ $employee->mobile?? '--' }}</td>
                    	        <td>{!! $employee->status ? "<span class='label label-success'>Active</span>" : "<span class='label label-danger'>Inactive</span>"!!}</td>
                    	        <td>
                    	            <a href="{{ route('admin.employees.edit',$employee->id) }}" class="btn btn-sm btn-primary" title="Edit">
                    	                <i class="fas fa-edit"></i>
                    	            </a>
                                    <a href="{{ route('admin.employees.detail',$employee->id) }}" class="btn btn-sm btn-mint" title="View Profile">
                                        <i class="fas fa-id-card"></i>
                                    </a>
                    	            <a href="#" title="Remove" class="btn btn-sm btn-danger btn-delete"
                    	               data-toggle="modal"
                    	               data-id="{{ $employee->id }}"
                    	               data-target="#delete-modal">
                    	                <i class="fas fa-trash"></i>
                    	            </a>
                                    <a href="#" class="btn btn-sm btn-primary btnResetPass"
                                       title="Reset Password"
                                       data-user-id="{{ $employee->id }}"
                                       data-user-name="{{ $employee->name}}"
                                       data-user-email="{{ $employee->email }}">
                                        <i class="fas fa-key"></i>
                                    </a>
                    	        </td>
                    	    </tr>
                        	@empty
                    	    <tr>
                    	        <td colspan="5">
                    	            <em>No data available in table ...</em>
                    	        </td>
                    	    </tr>
                        	@endforelse
                        </tbody>
                    </table>
                    {!! $employees->render() !!}
                </div>
            </div>
            <!--===================================================-->
            <!-- End Striped Table -->

        </div>
    </div>
    <!-- Modal reset Password -->
    <div id="reset-password-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><i class="pci-cross pci-circle"></i>
                    </button>
                    <h4 class="modal-title" id="myLargeModalLabel">Reset Password</h4>
                </div>

                <form class="form-horizontal" action="{{ route('admin.employees.password-reset') }}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="_user_id" id="_user_id" value="">
                    <div class="modal-body">
                        <div class="bootbox-body">
                            <div class="row">
                                <div class="col-md-12">


                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="name">Customer Name: </label>
                                        <div class="col-md-6" style="text-align: left;padding: 7px 0 0 0;">
                                            <label id="_user_name" style="font-weight: 600;"></label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="name">Email: </label>
                                        <div class="col-md-6" style="text-align: left;padding: 7px 0 0 0;">
                                            <label id="_user_email" style="font-weight: 600;"></label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="name">Reset Password</label>
                                        <div class="col-md-6">
                                            <input name="st_user_password" type="password" class="form-control input-md"
                                                   value="" required>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-sm btn-primary">Reset</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
    @include('admin.elements.delete-modal')
</div>
@endsection
@push('scripts')
    <script type="text/javascript">
        $(document).on('click','.btn-delete', function () {
            var route = '{{ route('admin.employees.destroy', ':id') }}';

            route = route.replace(':id', $(this).attr('data-id'));

            $('#delete-modal').find('form').attr('action', route);
            $('#delete-modal').modal('show');

            $('#delete-final').click(function (e) {
                $(this).attr('disabled',true);
                $(this).html('<i class="fas fa-spinner fa-spin"></i> Please wait...');
                $(this).closest('form').submit();
            });
        });

        $('.btnResetPass').click(function () {

            var user_id = $(this).attr('data-user-id');
            if (user_id == '' || user_id == null) {
                return false;
            }
            var user_name = $(this).attr('data-user-name');
            var user_email = $(this).attr('data-user-email');

            $('#_user_id').val(user_id);
            $('#_user_name').html(user_name);
            $('#_user_email').html(user_email);

            $('#reset-password-modal').modal({show: true});
        });
    </script>
@endpush
