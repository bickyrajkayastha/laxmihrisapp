<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CourseAssessment extends Model
{
    protected $guarded = ['id'];

    public function options()
    {
        return $this->hasMany(AssessmentOption::class, 'assessment_id', 'id');
    }

    public function answer()
    {
        return $this->options()->where('correct', 1)->first()->id ?? '';
    }
}
