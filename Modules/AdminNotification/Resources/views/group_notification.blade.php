@extends('admin.layouts.master')
@push('css')
<link rel="stylesheet" href="{{ asset('assets/plugins/select2/css/select2.min.css') }}">
<style>
    .select2 {
        width: 100% !important;
    }
</style>
@endpush
@section('content')
<!-- @if(Session::get('success_message'))
    <div class="alert alert-success containerAlert">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
        {{ Session::get('success_message') }}
    </div>
@endif
@if(Session::get('error_message'))
    <div class="alert alert-danger containerAlert">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
        {{ Session::get('error_message') }}
    </div>
@endif -->
<div class="row">
    <div class="col">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Group Notification Management
                    <a href="{{ route('admin.notifications.group.create') }}" class="btn btn-primary btn-sm panel-btn pull-right"><i class="fa fa-plus"></i> Add Group Notification</a>
                </h3>
            </div>

            <!-- Striped Table -->
            <!--===================================================-->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
	                            <th>S.N</th>
	                            <th>Title</th>
                                <!-- <th>Type</th> -->
	                            <!-- <th>Status</th> -->
                                <th>Send Notification</th>
	                            <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        	@forelse($notifications as $notification)
                    	    <tr data-title="{{ $notification->title }}" data-n-id="{{ $notification->id }}" data-body="{{ $notification->body }}">
                    	        <td>{{ $loop->iteration }}</td>
                    	        <td>{{ $notification->title }}</td>
                    	        <!-- <td>{!! $notification->status ? "<span class='label label-success'>Active</span>" : "<span class='label label-default'>Inactive</span>"!!}
                                </td> -->
                                <td>
                                    <button class="btn btn-sm btn-mint notification-send-btn" data-target="#notification-send-modal" data-toggle="modal">
                                        <i class="fas fa-paper-plane"></i> view and send
                                    </button>
                                </td>
                    	        <td>
                    	            <!-- <a href="{{ route('admin.notifications.edit',$notification->id) }}" class="btn btn-sm btn-primary" title="Edit">
                                        <i class="fas fa-edit"></i>
                                    </a> -->
                    	            <a href="#" title="Remove" class="btn btn-sm btn-danger btn-delete"
                    	               data-toggle="modal"
                    	               data-id="{{ $notification->id }}"
                    	               data-target="#delete-modal">
                    	                <i class="fas fa-trash"></i>
                    	            </a>
                    	        </td>
                    	    </tr>
                        	@empty
                    	    <tr>
                    	        <td colspan="5">
                    	            <em>No data available in table ...</em>
                    	        </td>
                    	    </tr>
                        	@endforelse
                        </tbody>
                    </table>
                    {!! $notifications->render() !!}
                </div>
            </div>
            <!--===================================================-->
            <!-- End Striped Table -->

        </div>
    </div>
    <!-- send notification modal -->
    <div id="notification-send-modal" class="modal fade" tabindex="-1" data-n-id="">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <form id="notification-send-form" class="form" method="POST" action="{{ route('admin.notifications.send-notification') }}">
                        @csrf
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><i class="pci-cross pci-circle"></i></button>
                            <h4 class="modal-title" id="mySmallModalLabel"><i class="fas fa-bell"></i> <span id="n-modal-title">title</span></h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="input-title">Employees <span class="required_color">*</span></label>
                                    <div class="col-lg-9">
                                        <select name="employees[]" id="select-tags" multiple class="form-control">
                                            @foreach($employees as $employee)
                                                <option value="{{ $employee->id }}">{{ $employee->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="body-data" style="margin-bottom: 10px;">

                            </div>
                            <small><i>By clicking send now. This notification will be sent to all the staffs of Laxmi Bank.</i></small>
                        </div>
                        <div class="modal-footer">
                            <input type="hidden" name="n_id">
                            <button type="submit" class="btn btn-sm btn-mint" id="send-notification-btn"><i class="fas fa-paper-plane"></i> Send Now</button>
                            <button data-bb-handler="cancel" data-dismiss="modal" type="button" class="btn btn-sm btn-default">Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
    </div>
    <!-- end of send notification modal -->
    @include('admin.elements.delete-modal')
</div>
@endsection
@push('scripts')
    <script src="{{ asset('assets/plugins/select2/js/select2.min.js') }}"></script>
    <script type="text/javascript">
        $( "#select-tags" ).select2({
            // theme: "bootstrap"
        });

        $(document).on('click', '#send-notification-btn', function(event) {
            var btn = $(this);
            btn.attr('disabled',true);
            btn.html('<i class="fas fa-spinner fa-spin"></i> sending...');
            $(this).closest('form').submit();
        });

        $(document).on('click', '.notification-send-btn', function(e) {
            e.preventDefault();
            var tr = $(this).closest('tr');
            var title = tr.data('title');
            var body = tr.data('body');
            var notification_id = tr.data('n-id');
            $("#notification-send-form").find('input[name="n_id"]').val(notification_id);

            $("#notification-send-modal").find('#n-modal-title').html(title);
            $("#notification-send-modal").find('.modal-body>.body-data').html(body);
        });

        $(document).on('click','.btn-delete', function () {
            var route = '{{ route('admin.notifications.destroy', ':id') }}';

            route = route.replace(':id', $(this).attr('data-id'));

            $('#delete-modal').find('form').attr('action', route);
            $('#delete-modal').modal('show');

            $('#delete-final').click(function (e) {
                $(this).attr('disabled',true);
                $(this).html('<i class="fas fa-spinner fa-spin"></i> Please wait...');
                $(this).closest('form').submit();
            });
        });
    </script>
@endpush
