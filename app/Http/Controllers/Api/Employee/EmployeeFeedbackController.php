<?php

namespace App\Http\Controllers\Api\Employee;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\EmployeeFeedback;
use Illuminate\Support\Facades\Log;

class EmployeeFeedbackController extends Controller
{
	/**
	* @SWG\Post(
	*     path="/employee/feedbacks",
	*     description="save employee detail.",
	*     tags={"Employee Apis"},
	*     security={{"bearerAuth":{}}},
	*     @SWG\Parameter(
	*         in="body",
	*         name="employee details",
	*         description="save employee details",
	*         required=true,
	*             @SWG\Property(
	*                   property="personal_details",
	*                   type="object",
	*                   required={"feedback"},
	*                   @SWG\Property(
	*		                 property="feedback",
	*		                 type="string",
	*		                 description="long text",
	*                   ),
	*              ),
	*     ),
	*     @SWG\Response(
	*         response=200,
	*         description="status, message",
	*     ),
	*     @SWG\Response(
	*         response=422,
	*         description="status, message"
	*     )
	* )
	*/
	public function store(Request $request)
	{
		$request->validate([
			'feedback' => 'required'
		]);

		$success = false;
		$message = "";
		$status_code = 200;
		$data = [];


		$feedback = new EmployeeFeedback;
		$feedback->employee_id = auth()->user()->id;
		$feedback->feedback = $request->feedback;

		if ($feedback->save()) {
			$success = true;
			$message = "Thank you for your feedback.";
		} else {
			$message = __('alerts.create_error');
		}

		return response()->json([
			'data' => $data,
			'success' => $success,
			'message' => $message
		], $status_code);
	}
}
