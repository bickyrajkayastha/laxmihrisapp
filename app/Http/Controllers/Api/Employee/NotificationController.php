<?php

namespace App\Http\Controllers\Api\Employee;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Employee;
use App\Models\EmployeeDevice;
use App\Models\Notification;
use Illuminate\Support\Facades\Log;
use App\Http\Resources\NotificationCollection;

class NotificationController extends Controller
{

	/**
	 * @SWG\Get(
	 *     path="/employee/notifications",
	 *     description="get employee directories",
	 *     tags={"Employee Apis"},
	 *     security={{"bearerAuth":{}}},
	 *     @SWG\Response(
	 *         response=200,
	 *         description="success, message",
	 *     ),
	 *     @SWG\Response(
	 *         response=422,
	 *         description="success, message"
	 *     )
	 * )
	 */
	public function getAllNotificationList()
	{
		$employee = auth()->user();
		$notifications = $employee->notifications()->orderBy('employee_notification.id', 'DESC')->paginate(100);
		// after notification fetch update new notification
		$employee->notifications()->newPivotStatement()->where([
			['seen_status', '=', 0],
			['employee_id', '=', $employee->id]
		])->update(['seen_status' => 1]);

		return new NotificationCollection($notifications);
	}
}
