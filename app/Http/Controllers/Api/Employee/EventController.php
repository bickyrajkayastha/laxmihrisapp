<?php

namespace App\Http\Controllers\Api\Employee;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\EventApiCollection;
use App\Models\Event;

class EventController extends Controller
{
	public $success = false;
	public $message = "";
	/**
	 * @SWG\Get(
	 *     path="/employee/events",
	 *     description="get event list in message",
	 *     tags={"Employee Apis"},
	 *     security={{"bearerAuth":{}}},
	 *     @SWG\Response(
	 *         response=200,
	 *         description="status, message",
	 *     ),
	 *     @SWG\Response(
	 *         response=422,
	 *         description="status, message"
	 *     )
	 * )
	 */
	public function index()
	{
		$data = Event::orderBy('start_date', 'desc')->published()->paginate(20);
		return new EventApiCollection($data);
	}
}
