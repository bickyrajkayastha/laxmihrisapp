<?php

use Illuminate\Database\Seeder;

class GroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	\App\Models\Group::create([
    	   'name' => 'Super Admin',
    	   'status' => 1
    	]);

    	\App\Models\Group::create([
    	    'name' => 'Admin',
    	    'status' => 1
    	]);
    }
}
