<?php

namespace App\Http\Controllers\Api\Employee;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Poll;
use App\Http\Resources\PollCollection;

class PollController extends Controller
{
	/**
	 * @SWG\Get(
	 *     path="/employee/polls",
	 *     description="all poll list",
	 *     tags={"Employee Apis"},
	 *     security={{"bearerAuth":{}}},
	 *     @SWG\Response(
	 *         response=200,
	 *         description="status, message",
	 *     ),
	 *     @SWG\Response(
	 *         response=422,
	 *         description="status, message"
	 *     )
	 * )
	 */
	public function pollList()
	{
		$current_date = date('y-m-d');
		$data = Poll::with('poll_options', 'admin')
		->where('closing_date', '>=', $current_date)
		->get();

		return new PollCollection($data);
	}
}
