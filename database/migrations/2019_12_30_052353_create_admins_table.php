<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name',255)->nullable()->default(null);
            $table->string('middle_name',255)->nullable()->default(null);
            $table->string('last_name',255)->nullable()->default(null);
            $table->string('email',191)->nullable()->default(null)->unique();
            $table->string('phone',191)->nullable()->default(null)->unique();
            $table->string('username',255)->nullable()->default(null);
            $table->string('password',255)->nullable()->default(null);
            $table->string('remember_token',100)->nullable()->default(null);
            $table->boolean('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
    }
}
