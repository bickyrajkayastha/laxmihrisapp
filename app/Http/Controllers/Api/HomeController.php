<?php

namespace App\Http\Controllers\Api;

use App\Helpers\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\AboutBankCollection;

class HomeController extends Controller
{
	/**
	 * @SWG\Get(
	 *     path="/employee/welcome-note",
	 *     description="refresh the token",
	 *     tags={"Employee Apis"},
	 *     security={{"bearerAuth":{}}},
	 *     @SWG\Response(
	 *         response=200,
	 *         description="status, message",
	 *     ),
	 *     @SWG\Response(
	 *         response=422,
	 *         description="status, message"
	 *     )
	 * )
	 */
	public function welcomeNote()
	{
		$data = [];
		$data['person_name'] = "Ceo";
		$data['person_image'] = "";
        $success = true;
        $message = "";

		$data['id'] = 1;
        $data['person_image'] = Setting::get('ceo_image') ? asset('uploads/config/'.Setting::get('ceo_image')) : '';
        $data['person_name'] = Setting::get('ceo_name') ?? '';
		$data['title'] = "Welcome Note";
		$data['body'] = Setting::get('ceo_message') ?? '';

		if (isset($welcome) && !empty($welcome)) {
			$data['id'] = $welcome->id;
			$data['title'] = $welcome->title;
			$data['body'] = $welcome->body;
		}

		$data['user_name'] = auth()->user()->name;
		$data['person_position'] = 'CEO/HR';

		return response()->json([
			'data' => $data,
			'success' => $success,
			'message' => $message
		]);
	}

	/**
	 * @SWG\Get(
	 *     path="/employee/about-bank",
	 *     description="refresh the token",
	 *     tags={"Employee Apis"},
	 *     security={{"bearerAuth":{}}},
	 *     @SWG\Response(
	 *         response=200,
	 *         description="success, message",
	 *     ),
	 *     @SWG\Response(
	 *         response=422,
	 *         description="success, message"
	 *     )
	 * )
	 */
	public function aboutBank()
	{
		$data = \App\Models\Content::where([
			['status', '=', 1]
		])->get();

		return new AboutBankCollection($data);
	}
}
