<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin/faqs', 'as' => 'admin.', 'middleware' => 'auth:admin'], function() {

    Route::get('/', 'AdminFaqController@index')
        ->name('faqs.index')
        ->middleware('can:view-faqs');

    Route::get('/create', 'AdminFaqController@create')
        ->name('faqs.create')
        ->middleware('can:create-faqs');

    Route::post('/create', 'AdminFaqController@store')
        ->name('faqs.store')
        ->middleware('can:create-faqs');

    Route::get('/{id}/edit', 'AdminFaqController@edit')
        ->name('faqs.edit')
        ->middleware('can:edit-faqs');

    Route::post('/{id}/edit', 'AdminFaqController@update')
        ->name('faqs.update')
        ->middleware('can:edit-faqs');

    Route::post('/{id}/delete', 'AdminFaqController@destroy')
        ->name('faqs.destroy')
        ->middleware('can:delete-faqs');
});
