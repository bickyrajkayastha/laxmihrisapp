<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AssessmentOption extends Model
{
    protected $guarded = ['id'];

    public function assessment()
    {
        return $this->belongsTo(CourseAssessment::class, 'assessment_id', 'id');
    }

    public function employees()
    {
        return $this->belongsToMany(Employee::class, 'employee_option', 'option_id', 'employee_id');
    }
}
