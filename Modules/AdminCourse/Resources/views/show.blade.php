@extends('admin.layouts.master')

@push('css')
<style>
    .card-icon {
        text-align: center;
        width: 100px;
        margin-bottom: 12px;
        height: 100px;
        background-color: #bdbdbd;
        padding: 27px 0px;
        font-size: 40px;
        border-radius: 100%;
        border: 2px solid gainsboro;
        color: #404040;
    }
</style>
@endpush
@section('content')
<div class="row">
    <div class="col-lg-12 col-sm-12 col-md-12">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title"> {{ $course->title }}
                    {{-- <a href="{{ route('admin.courses.create') }}" class="btn btn-primary btn-sm panel-btn pull-right"><i class="fa fa-plus"></i> Add Course</a> --}}
                </h3>
            </div>
        </div>
    </div>
</div>
<div class="row">
    {{-- Trainings --}}
    <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
        <div class="panel">
            <div class="panel-body text-center">
                <i class="fas fa-network-wired card-icon"></i>
                {{-- <img alt="Profile Picture" class="img-md img-circle mar-btm" src="img/profile-photos/1.png"> --}}
                <p class="text-lg text-semibold mar-no text-main">Trainings</p>
                <p class="text-muted">{{ $course->title }}</p>
                {{-- <p class="text-sm">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. </p> --}}
                <a href="{{ route('admin.courses.trainings.index',$course->id) }}" class="btn btn-primary mar-ver"><i class="fas fa-list"></i> Set Trainings</a>
                <ul class="list-unstyled text-center bord-top pad-top mar-no row">
                    <li class="col-xs-4">
                    </li>
                    <li class="col-xs-4">
                        <span class="text-lg text-semibold text-main">{{ $course->trainings->count() }}</span>
                        <p class="text-muted mar-no">Training Programs</p>
                    </li>
                    {{-- <li class="col-xs-4">
                        <span class="text-lg text-semibold text-main">23K</span>
                        <p class="text-muted mar-no">Followers</p>
                    </li>
                    <li class="col-xs-4">
                        <span class="text-lg text-semibold text-main">278</span>
                        <p class="text-muted mar-no">Post</p>
                    </li> --}}
                </ul>
            </div>
        </div>
    </div>

    {{-- candidates --}}
    <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
        <div class="panel">
            <div class="panel-body text-center">
                <i class="fas fa-users card-icon"></i>
                {{-- <img alt="Profile Picture" class="img-md img-circle mar-btm" src="img/profile-photos/1.png"> --}}
                <p class="text-lg text-semibold mar-no text-main">Candidates</p>
                <p class="text-muted">{{ $course->title }}</p>
                {{-- <p class="text-sm">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. </p> --}}
                <a href="{{ route('admin.courses.candidates',$course->id) }}" class="btn btn-primary mar-ver"><i class="fas fa-list"></i> View Candidates</a>
                <ul class="list-unstyled text-center bord-top pad-top mar-no row">
                    <li class="col-xs-4">
                    </li>
                    <li class="col-xs-4">
                        <span class="text-lg text-semibold text-main">{{ $course->employees->count() }}</span>
                        <p class="text-muted mar-no">Candidates</p>
                    </li>
                    {{-- <li class="col-xs-4">
                        <span class="text-lg text-semibold text-main">23K</span>
                        <p class="text-muted mar-no">Followers</p>
                    </li>
                    <li class="col-xs-4">
                        <span class="text-lg text-semibold text-main">278</span>
                        <p class="text-muted mar-no">Post</p>
                    </li> --}}
                </ul>
            </div>
        </div>
    </div>

    {{-- Assessment --}}
    <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
        <div class="panel">
            <div class="panel-body text-center">
                <i class="fas fa-file-alt card-icon"></i>
                {{-- <img alt="Profile Picture" class="img-md img-circle mar-btm" src="img/profile-photos/1.png"> --}}
                <p class="text-lg text-semibold mar-no text-main">Set Questions</p>
                <p class="text-muted">{{ $course->title }}</p>
                {{-- <p class="text-sm">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. </p> --}}
                <a href="{{ route('admin.courses.assessment.questions', $course->id) }}" class="btn btn-primary mar-ver"><i class="fas fa-list"></i> View & Set Question</a>
                <ul class="list-unstyled text-center bord-top pad-top mar-no row">
                    <li class="col-xs-4">
                    </li>
                    <li class="col-xs-4">
                        <span class="text-lg text-semibold text-main">{{ $course->assessments->count() }}</span>
                        <p class="text-muted mar-no">Questions</p>
                    </li>
                    {{-- <li class="col-xs-4">
                        <span class="text-lg text-semibold text-main">23K</span>
                        <p class="text-muted mar-no">Followers</p>
                    </li>
                    <li class="col-xs-4">
                        <span class="text-lg text-semibold text-main">278</span>
                        <p class="text-muted mar-no">Post</p>
                    </li> --}}
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection
