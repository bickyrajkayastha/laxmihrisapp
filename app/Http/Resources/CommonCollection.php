<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CommonCollection extends ResourceCollection
{
    protected $success = true;
    protected $message = "";
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);

        return $this->map(function($course) {
            return [
                'id' => $course->id,
                'title' => $course->title,
                'sub_title' => $course->sub_title,
                'date' => $course->date_range,
                // 'time' => $course->time_range,
                'image' => $course->full_img_url,
                'description' => $course->description
            ];
        });
    }

    public function with($request)
    {
        if ($this->resource) {
            $this->message = "List fetched.";
        } else {
            $this->message = "Empty list.";
        }

        return [
            'success' => $this->success,
            'message' => $this->message,
        ];
    }
}
