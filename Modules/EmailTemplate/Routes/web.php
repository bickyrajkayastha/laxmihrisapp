<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'auth:admin'], function () {

    // EMAIL TEMPLATES
    Route::get('email-templates', 'EmailTemplateController@emailTemplate')
        ->name('email.template')
        ->middleware('can:email-template');


    Route::get('email-templates/{id}/edit', 'EmailTemplateController@edit')
        ->name('email-template.edit');

    Route::post('email-templates/{id}/edit', 'EmailTemplateController@update')
        ->name('email-template.update')
        ->middleware('can:email-template');
});
