@extends('admin.layouts.master')

@push('css')
<link href="{{ asset('assets/plugins/morris-js/morris.min.css') }}" rel="stylesheet">
@endpush
@section('content')
<!-- @if(Session::get('success_message'))
    <div class="alert alert-success containerAlert">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
        {{ Session::get('success_message') }}
    </div>
@endif
@if(Session::get('error_message'))
    <div class="alert alert-danger containerAlert">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
        {{ Session::get('error_message') }}
    </div>
@endif -->
<div class="row">
    <div class="col">

        <!--Network Line Chart-->
        <!--===================================================-->
        <div id="demo-panel-network" class="panel">
            <div class="panel-heading">
                <div class="panel-control">
                    <span class="badge badge-mint">closing date - {{ formatDate($poll->closing_date) }} </span>
                    <!-- <div class="btn-group">
                        <button class="dropdown-toggle btn" data-toggle="dropdown" aria-expanded="false"><i class="demo-pli-gear icon-lg"></i></button>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Separated link</a></li>
                        </ul>
                    </div> -->
                </div>
                <h3 class="panel-title">Polls</h3>
            </div>

            <!--Chart information-->
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-12">
                        <p class="text-semibold text-main">{{ $poll->title }}</p>
                        <ul class="list-unstyled">
                            @foreach($poll->poll_options as $option)
                            <li style="margin-bottom: 10px;">
                                <div class="clearfix">
                                    <p class="pull-left mar-no">{{ $option->option }}</p>
                                    <p class="pull-right mar-no">Per: {{ $option->countPercentage }}%</p>
                                    <p class="pull-right mar-no">Total: {{ $option->count }} | </p>
                                </div>
                                <div class="progress progress-sm">
                                    <div class="progress-bar progress-bar-info" style="width: {{ $option->countPercentage }}%;">
                                        <span class="sr-only">{{ $option->countPercentage }}%</span>
                                    </div>
                                </div>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>


        </div>
        <!--===================================================-->
        <!--End network line chart-->

    </div>
</div>
<div class="row">
    <div class="col">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Voters
                </h3>
            </div>

            <!-- Striped Table -->
            <!--===================================================-->
            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>S.N</th>
                                                <th>Name</th>
                                                <th>{{ $poll->title }}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @forelse($employees as $employee)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>
                                                    ​<picture>
                                                      <img src="{{ $employee->thumbImgPath }}" class="img-fluid" width="40px" height="40px" alt="{{ $employee->name }}">
                                                    </picture>
                                                    {{ $employee->name }}
                                                </td>
                                                <td>
                                                    <ul class="poll-option-ul">
                                                        @foreach($poll->poll_options as $option)
                                                        <li><i class="fas {{ ($option->id == $employee->poll_options[0]['id'])? 'fa-check-circle': 'fa-circle' }}"></i> {{ $option->option }}</li>
                                                        @endforeach
                                                    </ul>
                                                </td>
                                            </tr>
                                            @empty
                                            <tr>
                                                <td colspan="5">
                                                    <em>No data available in table ...</em>
                                                </td>
                                            </tr>
                                            @endforelse
                                        </tbody>
                                    </table>
                                    {!! $employees->render() !!}
                                </div>
            </div>
            <!--===================================================-->
            <!-- End Striped Table -->

        </div>
    </div>
    @include('admin.elements.delete-modal')
</div>
@endsection
@push('scripts')
    <!--Morris.js [ OPTIONAL ]-->
    <script src="{{ asset('assets/plugins/morris-js/morris.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/morris-js/raphael-js/raphael.min.js') }}"></script>

    <script src="{{ asset('assets/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
    <script src="{{ asset('assets/js/demo/dashboard.js') }}"></script>

    <script type="text/javascript">
        $(document).on('click','.btn-delete', function () {
            var route = '{{ route('admin.faqs.destroy', ':id') }}';

            route = route.replace(':id', $(this).attr('data-id'));

            $('#delete-modal').find('form').attr('action', route);
            $('#delete-modal').modal('show');

            $('#delete-final').click(function (e) {
                $(this).attr('disabled',true);
                $(this).html('<i class="fas fa-spinner fa-spin"></i> Please wait...');
                $(this).closest('form').submit();
            });
        });
    </script>
@endpush
