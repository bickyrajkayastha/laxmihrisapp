<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PollCollection extends ResourceCollection
{
    public $success = true;
    public $message = "";

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);

        if (iterator_count($this->resource)) {
            $this->message = "Poll List.";
            return $this->map(function($poll) {
                return [
                    'id' => $poll->id,
                    'poll_title' => $poll->title,
                    'total_voters' => $poll->total_voters,
                    'posted_by' => "by " . strtolower($poll->admin->first_name),
                    'closing_date' => "closing at " . formatDate($poll->closing_date),
                    'options' => $poll->poll_options->map(function($option) {
                        return [
                            'id' => $option->id,
                            'option_title' => $option->option,
                            'total_voted' => $option->count,
                            'my_vote' => $option->my_vote
                        ];
                    })
                    // 'created_at' => Carbon\Carbon::parse($poll->pivot->created_at)->diffForHumans()
                ];
            });
        } else {
            $this->message = "No poll has been created.";
        }
    }

    public function with($request)
    {
        return [
            'success' => $this->success,
            'message' => $this->message
        ];
    }
}
