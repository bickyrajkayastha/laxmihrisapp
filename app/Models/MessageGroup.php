<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MessageGroup extends Model
{
	protected $guarded = ['id'];

	public function employees()
	{
		return $this->belongsToMany(Employee::class, 'employee_message_group', 'message_group_id', 'employee_id')->withPivot('id', 'status')->withTimestamps();
	}

	public function messages()
	{
		return $this->belongsToMany(Message::class, 'employee_message');
	}

	public function latest_message()
	{
		return $this->belongsToMany(Message::class, 'employee_message');
	}

	public function scopeActive($query)
	{
		return $query->where('message_groups.status', 1);
	}

	public function getLatestMessageAttribute()
	{
		return $this->latest_message()->latest()->first();
	}
}
