<?php

namespace Modules\AdminLeaderboard\Http\Controllers;

use App\Models\Course;
use App\Models\Employee;
use App\Models\Leaderboard;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class AdminLeaderboardController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $data['pageTitle'] = "Leaderboard Management";
        $data['courses'] = Course::orderBy('start_date', 'desc')->has('leaderboards')->paginate(50);

        return view('adminleaderboard::index', $data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $data['pageTitle'] = "Add Leaderboard";
        $data['courses'] = Course::where('status', 1)->get();
        $data['employees'] = Employee::where('status', 1)->get();
        return view('adminleaderboard::create', $data);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'course_id' => 'required',
            'employees.0.id' => 'required',
            'employees.0.points' => 'required',
            'employees.1.id' => 'required',
            'employees.1.points' => 'required',
            'employees.2.id' => 'required',
            'employees.2.points' => 'required',
        ]);

        try {
            $course = Course::find($request->course_id);
            $course->leaderboards()->where('course_id', $request->id)->detach();
            foreach ($request->employees as $key => $employee) {
                if ($employee['id'] != null) {
                    $emp = Employee::find($employee['id']);
                    $emp->leaderboards()->attach($request->course_id, [
                        'position' => $key + 1,
                        'points' => $employee['points']
                    ]);
                }
            }
            session()->flash('success_message', __('alerts.create_success'));
        } catch (\Throwable $th) {
            \Log::info($th->getMessage());
            session()->flash('error_message', __('alerts.create_error'));
        }

        return redirect()->route('admin.leaderboards.index');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('adminleaderboard::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $data['courses'] = Course::where('status', 1)->get();
        $data['course'] = Course::where([
            ['status', 1],
            ['id', $id]
        ])->first();
        $data['leaderboards'] = $data['course']->leaderboards()->withPivot('points', 'position')->orderBy('position', 'asc')->get();
        $data['pageTitle'] = "Edit Leaderboard for " . $data['course']->title;
        $data['employees'] = Employee::where('status', 1)->get();
        return view('adminleaderboard::edit', $data);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        try {
            $course = $this->getCourseById($id);
            $course->leaderboards()->where('course_id', $id)->detach();
            session()->flash('success_message', __('alerts.delete_success'));
        } catch (\Exception $e) {
            session()->flash('error_message', __('alerts.delete_error'));
        }

        return redirect()->route('admin.leaderboards.index');
    }

    private function getCourseById($id)
    {
        return Course::find($id);
    }
}
