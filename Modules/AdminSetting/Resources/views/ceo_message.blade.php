@extends('admin.layouts.master')

@section('content')
<div class="row">
	<div class="col">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">CEO
                	<button class="btn btn-sm btn-primary pull-right panel-btn" onclick="window.history.go(-1);">
                	    <i class="fas fa-long-arrow-alt-left"></i> Back
                	</button>
                </h3>
            </div>

            <!--Horizontal Form-->
            <!--===================================================-->
            <form action="{{ route('admin.setting.ceo.store') }}" method="POST" class="panel-body form-horizontal form-padding" enctype="multipart/form-data">
                @csrf
                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="admin-first-name">CEO Name <span class="required_color">*</span></label>
                        <div class="col-md-10">
                            <input type="text" name="ceo_name" class="form-control" required value="{{ old('ceo_name') ? old('ceo_name') : Setting::get('ceo_name') }}" placeholder="Name" >
                            <small class="help-block text-danger">{{$errors->first('ceo_name')}}</small>
                        </div>{{--$_CONFIG['admin_name'];--}}
                    </div>

                    @if( Setting::get('ceo_image') && Setting::get('ceo_image') != "" )
                        <div class="form-group">
                            <label class="col-md-2 control-label"></label>
                            <div class="col-md-10">
                                <img width="200" src="{{ asset('uploads/config/'.Setting::get('ceo_image')) }}">
                            </div>
                        </div>
                    @endif

                    <div class="form-group">
                        <label class="col-md-2 control-label">Image <span class="required_color">*</span></label>
                        <div class="col-md-10">
                            <input type="file" name="ceo_image" class="form-control">
                            <small class="help-block text-danger">{{$errors->first('ceo_image')}}</small>
                        </div>
                    </div>

                	<div class="form-group">
                	    <label class="col-md-2 control-label" for="admin-first-name">Welcome Note <span class="required_color">*</span></label>
                	    <div class="col-md-10">
                            <textarea name="ceo_message" id="input-content" required class="form-control ckeditor" cols="30" rows="10">{{ old('ceo_message') ? old('ceo_message') : Setting::get('ceo_message') }}</textarea>
                	        <small class="help-block text-danger">{{$errors->first('ceo_message')}}</small>
                	    </div>
                	</div>
                </div>
                <div class="panel-footer text-right">
                	<div class="row">
		                <div class="col">
		                    <button class="btn btn-sm btn-mint" type="submit">Update</button>
		                </div>
                	</div>
                </div>
            </form>
            <!--===================================================-->
            <!--End Horizontal Form-->

        </div>
    </div>
</div>
@endsection
