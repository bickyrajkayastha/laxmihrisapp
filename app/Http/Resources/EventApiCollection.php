<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\Log;

class EventApiCollection extends ResourceCollection
{
    public $success = false;
    public $message = "";

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        if (iterator_count($this->resource)) {
            $this->message = "Events list.";
            $this->success = true;
            return $this->map(function($event) {
                return [
                    'id' => $event->id,
                    'title' => $event->title,
                    'location' => $event->location??'',
                    'content' => $event->content??'',
                    'period_date' => $this->formatDate($event->start_date) . (($event->end_date)?' - ' . $this->formatDate($event->end_date):''),
                    'period_time' => $event->timeRange,
                    'image' => $event->croppedImgPath,
                    'start_date' => date('j', strtotime($event->start_date)),
                    'start_month' => date('M', strtotime($event->start_date)),
                    'start_time' => date('g:i A', strtotime($event->start_time))
                    // 'created_at' => Carbon\Carbon::parse($event->pivot->created_at)->diffForHumans()
                ];
            });
        } else {
            $this->message = "No events to show.";
        }
    }

    public function formatDate($date)
    {
        return date('D, jS M', strtotime($date));
    }

    public function with($request)
    {
        return [
            'success' => $this->success,
            'message' => $this->message
        ];
    }
}
