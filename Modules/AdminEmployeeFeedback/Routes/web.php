<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin/feedbacks', 'as' => 'admin.', 'middleware' => 'auth:admin'], function() {
	Route::get('/', 'AdminEmployeeFeedbackController@index')->name('feedbacks.index')->middleware('can:view-feedbacks');
	
	Route::get('/{id}/detail', 'AdminEmployeeFeedbackController@detail')
	->name('feedbacks.detail')
	->middleware('can:detail-feedbacks');

	Route::post('/{id}/delete', 'AdminEmployeeFeedbackController@destroy')
	->name('feedbacks.destroy')
	->middleware('can:delete-feedbacks');
});
