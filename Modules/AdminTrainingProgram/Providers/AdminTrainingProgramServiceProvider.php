<?php

namespace Modules\AdminTrainingProgram\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class AdminTrainingProgramServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(module_path('AdminTrainingProgram', 'Database/Migrations'));
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path('AdminTrainingProgram', 'Config/config.php') => config_path('admintrainingprogram.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path('AdminTrainingProgram', 'Config/config.php'), 'admintrainingprogram'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/admintrainingprogram');

        $sourcePath = module_path('AdminTrainingProgram', 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ],'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/admintrainingprogram';
        }, \Config::get('view.paths')), [$sourcePath]), 'admintrainingprogram');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/admintrainingprogram');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'admintrainingprogram');
        } else {
            $this->loadTranslationsFrom(module_path('AdminTrainingProgram', 'Resources/lang'), 'admintrainingprogram');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (! app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(module_path('AdminTrainingProgram', 'Database/factories'));
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
