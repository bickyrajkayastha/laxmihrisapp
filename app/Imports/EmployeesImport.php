<?php

namespace App\Imports;

use App\Models\Employee;
use App\Traits\Slugger;
use Carbon\Carbon;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Events\BeforeImport;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\RemembersRowNumber;
use Illuminate\Contracts\Queue\ShouldQueue;
use DB;
use Illuminate\Support\Facades\Hash;

class EmployeesImport implements ToModel, WithStartRow, WithEvents, WithChunkReading, ShouldQueue
{
    use Slugger, RegistersEventListeners, RemembersRowNumber;

    protected $current_row_number = 2;

    public function registerEvents(): array
    {
        return [
            // Handle by a closure.
            BeforeImport::class => function (BeforeImport $event) {
                // DB::statement('SET FOREIGN_KEY_CHECKS=0;');
                // Employee::truncate();
                // DB::statement('SET FOREIGN_KEY_CHECKS=1;');
            },
        ];
    }

    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        $this->current_row_number = $this->getRowNumber();
        if (strtotime($row[6])) {
            $from_date = Carbon::createFromFormat("d-M-Y", $row[6]);
        } else {
            $from_date = Carbon::now()->format('d-M-Y');
        }
        $exists = Employee::where('email', strtolower($row[8]))->first();
        if (!$exists) {
            return new Employee([
                'employee_code'   => $row[1],
                'name'            => $row[2],
                'gender'          => $row[3],
                'designation'     => $row[4],
                'position'        => $row[5],
                'joined_date'     => \Carbon\Carbon::parse($from_date)->format('Y-m-d'),
                'mobile'          => $row[7],
                'email'           => $row[8],
                'status'          => 1,
                'password'        => Hash::make(123456)
            ]);
        }
    }

    public function chunkSize(): int
    {
        return 500;
    }

    public function startRow(): int
    {
        return $this->current_row_number;
    }
}
