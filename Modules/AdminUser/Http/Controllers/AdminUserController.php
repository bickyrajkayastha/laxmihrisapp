<?php

namespace Modules\AdminUser\Http\Controllers;

use App\Helpers\Setting;
use App\Models\Admin;
use App\Models\Group;
use App\Models\PermissionGroup;
use App\Models\PermissionModule;
use App\Models\UserDetail;
use App\Models\UserGroup;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;

class AdminUserController extends Controller
{
    // Profile Update
    public function profile()
    {
        $data['pageTitle'] = "Profile";
        $data['breadcrumbs'] = '<li>Profile</li>';
        $data['title'] = "Profile - " . Setting::get('siteconfig')['system_name'];
        $data['user'] = Auth::guard('admin')->user();

        return view('adminuser::profile', $data);
    }

    public function profileStore(Request $request)
    {
        $rules = array(
            'firstname' => 'required',
            'username' => 'required',
            'email' => 'required',
        );

        $validator = Validator::make($request->all(), $rules);
        //extending the validation to further validation
        $validator->after(function ($validator) use ($request) {

            //checking username
            if ($request->get('old_username') != $request->get('username')) {
                if ($this->check_username()) {
                    $validator->errors()->add('username', 'Username already taken.');
                }
            }


            //checking email
            if ($request->get('old_email') != $request->get('email')) {
                if ($this->check_email()) {
                    $validator->errors()->add('email', 'Email already taken.');
                }
            }

        });

        if ($validator->fails()) {
            return redirect()->route('admin.user.profile')->withErrors($validator)->withInput();
        }

        // 1 : Updating in users table

        $userData = [
            'username' => $request->get('username'),
            'email' => $request->get('email'),
            'first_name' => $request->get('firstname'),
            'middle_name' => $request->get('middlename'),
            'last_name' => $request->get('lastname'),
        ];

        Admin::where('id', Auth::guard('admin')->user()->id)
            ->update($userData);

        session()->flash('success_message', 'Profile updated successfully.');
        return redirect()->route('admin.user.profile');
    }

    // Password Reset
    public function passwordReset()
    {
        $data['breadcrumbs'] = '<li>Password Reset</li>';
        $data['title'] = "Password Reset - " . Setting::get('siteconfig')['system_name'];
        return view('adminuser::password_reset', $data);
    }

    public function passwordResetStore(Request $request)
    {
        $request->validate([
            'current_password' => 'required',
            'new_password' => 'required',
            'confirm_password' => 'required|same:new_password',
        ]);

        if (Hash::check($request->get('current_password'), Auth::guard('admin')->user()->password) == false) {
            session()->flash('error_message', 'Invalid current password.');
            return redirect()->route('admin.user.password-reset');
        }

        // Resetting the password
        Admin::find(Auth::guard('admin')->user()->id)->update([
            'password' => Hash::make($request->get('current_password')),
        ]);

        Auth::guard('admin')->logOut();

        session()->flash('success_message', 'Password changed, please login again.');

        return redirect()->route('admin.login');
        /*return redirect()->route('admin.user.password-reset');*/
    }

    // User Groups
    public function groups()
    {
        $data['pageTitle'] = "Group Management";
        $data['breadcrumbs'] = '<li>Groups Management</li>';
        $data['title'] = "User Groups - " . Setting::get('siteconfig')['system_name'];
        $data['header_nav'] = 'groups';
        $data['groups'] = Group::get();
        return view('adminuser::group', $data);
    }

    public function groupStore(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'status' => 'required',
        ]);

        $data = [
            'name' => $request->get('name'),
            'status' => $request->get('status'),
        ];

        Group::create($data);

        session()->flash('success_message', 'Group created successfully.');

        return redirect()->route('admin.user.groups');

    }

    public function groupsEdit($id)
    {
        $data['breadcrumbs'] = '<li><a href="' . route('admin.user.groups') . '">Groups</a></li><li>Update User Groups</li>';
        $data['title'] = "User Groups Update - " . Setting::get('siteconfig')['system_name'];
        $data['group_details'] = Group::where('id', $id)->first();
        return view('adminuser::group_edit', $data);
    }

    public function groupsUpdate(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'status' => 'required',
        ]);

        $data = [
            'name' => $request->get('name'),
            'status' => $request->get('status'),
        ];

        Group::where('id', $request->get('_id'))->update($data);

        session()->flash('success_message', 'Group updated successfully.');

        return redirect()->route('admin.user.groups');

    }

    public function groupDestroy($id)
    {
        if (in_array($id, [config('constants.role_super_admin'), config('constants.role_admin')])) {
            session()->flash('error_message', 'Sorry ! You cannot delete this user group.');
            return redirect()->route('admin.user.groups');
        }

        $group_details = Group::where('id', $id)->first();

        if (!$group_details) {
            session()->flash('error_message', 'Something went wrong. Please try again.');
            return redirect()->route('admin.user.groups');
        }

        if ($group_details->status == 1) {
            session()->flash('error_message', 'Sorry! You cannot delete active group.');
            return redirect()->route('admin.user.groups');
        }

        $group_users = $group_details->group_users ?? null;
        if ($group_users != null && $group_users->count() > 0) {
            foreach ($group_users as $gu) {

                UserGroup::where('group_id', $id)->where('user_id', $gu->user_id)->delete();

                $user_has_default_role = UserGroup::where('group_id', config('constants.role_admin'))->where('user_id', $gu->user_id)->count();

                if ($user_has_default_role == 0) {
                    UserGroup::insert([
                        'user_id' => $gu->user_id,
                        'group_id' => config('constants.role_admin')
                    ]);
                }

            }
        }

        Group::where('id', $id)->delete();
        PermissionGroup::where('group_id', $id)->delete();
        session()->flash('success_message', 'User Group deleted successfully.');
        return redirect()->route('admin.user.groups');
    }

    public function groupPermission($id)
    {
        $data['breadcrumbs'] = '<li><a href="' . route('admin.user.groups') . '">Groups</a></li><li>Update Groups Permissions</li>';
        $data['title'] = "User Groups Permissions - " . Setting::get('siteconfig')['system_name'];

        $data['group_details'] = Group::where('id', $id)
            ->where('id', '!=', config('constants.role_super_admin'))
            ->where('status', 1)
            ->first();

        if (!$data['group_details']) {
            session()->flash('error_message', 'Group does not exists / Groups Permission cannot be modified.');
            return redirect()->route('admin.user.groups');
        }

        $data['modules'] = PermissionModule::where('status', '1')->orderBy('order_by', 'asc')->get();
        $data['group_permissions'] = PermissionGroup::where('group_id', $id)->pluck('permission_reference_id')->toArray();

        return view('adminuser::group_permission', $data);
    }

    public function groupPermissionStore(Request $request)
    {
        $rules = array(
            "_group_id" => "required",
        );

        $request->validate($rules);

        PermissionGroup::where('group_id', $request->get('_group_id'))->delete();
        if ($request->has('permission_reference_id')) {

            $permission_ids = $request->get('permission_reference_id');
            if (count($permission_ids) > 0) {
                $per_records = [];
                foreach ($permission_ids as $per_id) {
                    $per_records[] = [
                        'group_id' => $request->get('_group_id'),
                        'permission_reference_id' => $per_id,
                    ];
                }
                if (count($per_records) > 0) {
                    PermissionGroup::insert($per_records);
                }
            }

        }

        session()->flash('success_message', 'Groups Permissions updated successfully.');
        return redirect()->route('admin.user.groups.permission', [$request->get('_group_id')]);

    }


    // Users
    public function userLists()
    {
        $data['pageTitle'] = "User Management";
        $data['breadcrumbs'] = '<li>Users Management</li>';
        $data['title'] = "User Management - " . Setting::get('siteconfig')['system_name'];
        $data['header_nav'] = 'users';
        $data['users'] = Admin::with('user_group','user_is_superadmin')->get();
        $data['side_nav'] = 'users';
        
        return view('adminuser::user', $data);
    }

    public function userAdd()
    {
        $data['pageTitle'] = "Add User";
        $data['breadcrumbs'] = '<li><a href="' . route('admin.user.list') . '">Users</a></li><li>Create New User</li>';
        $data['title'] = "Create User - " . Setting::get('siteconfig')['system_name'];
        $data['side_nav'] = 'users';
        $data['side_sub_nav'] = 'users';
        $data['groups'] = Group::where('id', '!=', config('constants.role_super_admin'))
            ->where('id', '!=', config('constants.role_admin'))
            ->where('status', 1)
            ->get();

        return view('adminuser::user_add', $data);
    }

    public function userStore(Request $request)
    {
        $rules = array(
            'firstname' => 'required',
            'username' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            're_password' => 'required|same:password',
            'groups' => 'required',
            'status' => 'required',
        );

        $validator = Validator::make($request->all(), $rules);

        //extending the validation to further validation
        $validator->after(function ($validator) {

            //checking username
            if ($this->check_username()) {
                $validator->errors()->add('username', 'Username already taken.');
            }

            //checking email
            if ($this->check_email()) {
                $validator->errors()->add('email', 'Email already taken.');
            }

        });

        if ($validator->fails()) {
            return redirect()->route('admin.user.add')->withErrors($validator)->withInput();
        }

        // 1 : Inserting in users table
        $userData = [
            'first_name' => $request->get('firstname'),
            'middle_name' => $request->get('middlename'),
            'last_name' => $request->get('lastname'),
            'username' => $request->get('username'),
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password')),
            'status' => $request->get('status'),
        ];

        $user = Admin::create($userData);

        if ($user) {

            // 2 : Users Group Table

            $groups = $request->get('groups');

            if (count($groups) > 0) {

                $finalGroup = [];

                foreach ($groups as $grps) {
                    $temp['user_id'] = $user->id;
                    $temp['group_id'] = $grps;
                    $finalGroup[] = $temp;
                }

                if (count($finalGroup) > 0) {
                    UserGroup::insert($finalGroup);
                }

            }

            $success_mesg = "User created successfully. Please save the user credentials" . "<br>";
            $success_mesg .= "<strong>Username : " . $request->get('username') . "</strong><br>";
            $success_mesg .= "<strong>Password : " . $request->get('password') . "</strong>";

            session()->flash('success_message_special', $success_mesg);

            return redirect()->route('admin.user.list');

        } else {

            session()->flash('error_message', 'Something went wrong. Please try again.');
            return redirect()->route('admin.user.add');

        }
    }

    public function passwordResetUser(Request $request)
    {
        $rules = array(
            '_user_id' => 'required',
            'st_user_password' => 'required',
        );

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            session()->flash('error_message', 'Invalid Customer Details. Please try again.');
            return redirect()->route('admin.user.list');
        }

        $customer_data = [
            'password' => Hash::make($request->get('st_user_password')),
            'updated_at' => config('constants.current_date_time')
        ];

        Admin::where('id', $request->get('_user_id'))->update($customer_data);

        session()->flash('success_message', 'User password updated successfully.');
        return redirect()->route('admin.user.list');
    }

    private function check_username()
    {
        //$count = User::where('status','!=','deleted')->where('username',Input::get('username'))->count();
        $count = Admin::where('username', Input::get('username'))->count();
        return $count > 0 ? true : false;
    }

    private function check_email()
    {
        //$count = User::where('status','!=','deleted')->where('email',Input::get('email'))->count();
        $count = Admin::where('email', Input::get('email'))->count();
        return $count > 0 ? true : false;
    }

    public function userEdit($id)
    {
        $data['pageTitle'] = "Edit User";
        $data['user_details'] = Admin::where('id', $id)->first();

        if (!$data['user_details']) {
            session()->flash('error_message', 'Something went wrong. User records not found.');
            return redirect()->route('admin.user.list');
        }

        $data['user_type'] = 'others';
        // checking if this user is super admin
        $group_count = UserGroup::where('user_id', $id)->where('group_id', config('constants.role_super_admin'))->count();
        if ($group_count > 0) {
            $data['user_type'] = 'superadmin';
        }

        $data['current_user_grps'] = [];

        if (isset($data['user_details']->user_group) && count($data['user_details']->user_group) > 0) {
            foreach ($data['user_details']->user_group as $ugrp)
                $data['current_user_grps'][] = $ugrp->group_id;
        }

        $data['breadcrumbs'] = '<li><a href="' . route('admin.user.list') . '">Users</a></li><li>Update User</li>';
        $data['title'] = "Create User - " . Setting::get('siteconfig')['system_name'];
        $data['side_nav'] = 'users';
        $data['side_sub_nav'] = 'users';

        $data['groups'] = Group::where('id', '!=', config('constants.role_super_admin'))
            ->where('id', '!=', config('constants.role_admin'))
            ->where('status', 1)
            ->get();

        return view('adminuser::user_edit', $data);
    }

    public function userUpdate(Request $request)
    {
        $group_required = "";
        if ($request->get('user_type') != 'superadmin') {
            $group_required = "required";
        }
        $rules = array(
            'firstname' => 'required',
            'groups' => $group_required,
            'status' => 'required',
            'username' => 'required',
            'email' => 'required',
        );

        $validator = Validator::make($request->all(), $rules);


        //extending the validation to further validation
        $validator->after(function ($validator) use ($request) {

            //checking username
            if ($request->get('old_username') != $request->get('username')) {
                if ($this->check_username()) {
                    $validator->errors()->add('username', 'Username already taken.');
                }
            }

            //checking email
            if ($request->get('old_email') != $request->get('email')) {
                if ($this->check_email()) {
                    $validator->errors()->add('email', 'Email already taken.');
                }
            }

        });

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        // 1 : Updating users table
        $user_data = [
            'username' => $request->get('username'),
            'email' => $request->get('email'),
            'first_name' => $request->get('firstname'),
            'middle_name' => $request->get('middlename'),
            'last_name' => $request->get('lastname'),
            'phone' => $request->get('phone'),
            'status' => $request->get('status'),
        ];


         Admin::where('id', $request->get('_id'))->update($user_data);

        // 2 : Updating User Group Table
        if ($request->get('user_type') != 'superadmin') {
            $groups = $request->get('groups');
            if (count($groups) > 0) {
                $final_grps = [];


                foreach ($groups as $grps) {
                    $temp['user_id'] = $request->get('_id');
                    $temp['group_id'] = $grps;
                    $final_grps[] = $temp;
                }

                if (count($final_grps) > 0) {
                    //dropping previous data
                    UserGroup::where('user_id', $request->get('_id'))->delete();

                    UserGroup::insert($final_grps);
                }
            }
        }

        session()->flash('success_message', "User record updated successfully.");
        return redirect()->route('admin.user.list');

    }

    public function userDestroy($id)
    {
        try {
            // checking if this user is super admin
            $groupCount = UserGroup::where('user_id', $id)
                ->where('group_id', config('constants.role_super_admin'))
                ->where('group_id', config('constants.role_super_admin'))
                ->count();

            if ($groupCount > 0) {
                session()->flash('error_message', 'Sorry ! You cannot delete this user.');
                return redirect()->route('admin.user.list');
            }

            $user = Admin::where('id', $id)->first();

            if ($user->status = 1) {
                session()->flash('error_message', 'Sorry ! You cannot delete active user.');
                return redirect()->route('admin.user.list');
            }

            session()->flash('success_message', 'User deleted successfully.');
        } catch (\Exception $e) {
            session()->flash('error_message', 'User could not be deleted.');
        }

        return redirect()->route('admin.user.list');
    }
}
