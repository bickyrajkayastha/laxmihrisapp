<?php

namespace Modules\AdminSetting\Http\Controllers;

use App\Helpers\Setting;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class AdminSettingController extends Controller
{
    public function general()
    {
        $data['pageTitle'] = "General Settings";
        $data['breadcrumbs'] = '<li>General Configurations</li>';
        return view('adminsetting::index', $data);
    }

    public function welcomeNote()
    {
        $data['pageTitle'] = "Welcome Note";
        $data['breadcrumbs'] = '<li>Welcome Note</li>';
        return view('adminsetting::welcome_note', $data);
    }

    public function generalStore(Request $request)
    {
        $request->validate([
            'system_name' => 'required',
            'system_slogan' => 'required',
            'system_email' => 'required',
            'system_feedback_email' => 'required',
            'system_telephone_no' => 'required',
            'system_mobile' => 'required'
        ]);

        // site config data
        $siteConfig = [
            'system_name' => $request->get('system_name'),
            'system_email' => $request->get('system_email'),
            'system_slogan' => $request->get('system_slogan'),

        ];

        Setting::update('siteconfig', $siteConfig);
        Setting::update('system_feedback_email', $request->get('system_feedback_email'));
        Setting::update('system_telephone_no', $request->get('system_telephone_no'));
        Setting::update('system_mobile', $request->get('system_mobile'));

        Setting::update('outlets_verification_email1', $request->get('outlets_verification_email1'));
        Setting::update('outlets_verification_email2', $request->get('outlets_verification_email2'));

        if ($request->hasFile('logo')) {
            $image = $request->file('logo');
            $brand_image = time() . '-' . rand(111111, 999999) . '.' . $image->getClientOriginalExtension();

            $path = public_path() . "/uploads/config/";

            $image->move($path, $brand_image);
            Setting::update('brand_image', $brand_image);
        }

        if ($request->hasFile('brand_image_footer')) {
            $image = $request->file('brand_image_footer');
            $brand_image_footer = time() . '-' . rand(111111, 999999) . '.' . $image->getClientOriginalExtension();

            $path = public_path() . "/uploads/config/";

            $image->move($path, $brand_image_footer);
            Setting::update('brand_image_footer', $brand_image_footer);
        }

        session()->flash('success_message', __('alerts.update_success'));

        return redirect()->route('admin.setting.general');
    }

    public function storeWelcomeNote(Request $request)
    {
        $request->validate([
            'welcome_note' => 'required',
        ]);

        Setting::update('welcome_note', $request->get('welcome_note'));
        session()->flash('success_message', __('alerts.update_success'));
        return redirect()->route('admin.setting.welcome-note');
    }

    public function ceoMessage()
    {
        $data['pageTitle'] = "Ceo";
        $data['breadcrumbs'] = '<li>Ceo</li>';
        return view('adminsetting::ceo_message', $data);
    }

    public function storeCeoMessage(Request $request)
    {
        $request->validate([
            'ceo_name' => 'required',
            'ceo_message' => 'required',
            'ceo_image' => 'nullable'
        ]);

        if ($request->hasFile('ceo_image')) {
            $image = $request->file('ceo_image');
            $ceo_image = time() . '-' . rand(111111, 999999) . '.' . $image->getClientOriginalExtension();

            $path = public_path() . "/uploads/config/";

            $image->move($path, $ceo_image);
            Setting::update('ceo_image', $ceo_image);
        }

        Setting::update('ceo_name', $request->get('ceo_name'));
        Setting::update('ceo_message', $request->get('ceo_message'));
        session()->flash('success_message', __('alerts.update_success'));
        return redirect()->route('admin.setting.ceo');
    }

    public function imgDimension()
    {
        $data['breadcrumbs'] = '<li><a href="' . route('admin.dashboard') . '">Home</a></li><li>Meta Settings</li>';
        $data['title'] = "Meta Settings - " . Setting::get('siteconfig')['system_name'];
        $data['side_nav'] = 'master_config';
        $data['side_sub_nav'] = 'img-dimension';

        return view('adminsetting::image', $data);
    }

    public function imgDimensionStore(Request $request)
    {
        $request->get('mobile_ad_width') ? Setting::update('mobile_ad_width', $request->get('mobile_ad_width')) :'';
        $request->get('mobile_ad_height') ? Setting::update('mobile_ad_height', $request->get('mobile_ad_height')) :'';

        session()->flash('success_message', __('alerts.update_success'));

        return  redirect()->route('admin.setting.img-dimension');
    }
}
