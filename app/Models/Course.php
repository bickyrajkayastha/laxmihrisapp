<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $guarded = ['id'];

    protected $appends = ['fullImgPath', 'thumbImgPath', 'croppedImgPath', 'dateRange', 'timeRange', 'fullImgUrl'];

    public function getFullImgPathAttribute()
    {
        if ($this->image) {
            return public_path('uploads/courses/full/' . $this->image);
        }
        return asset('') . config('constants.default_banner_image');
    }

    public function getFullImgUrlAttribute()
    {
        if ($this->image) {
            return url('uploads/courses/full/' . $this->image);
        }
        return asset('') . config('constants.default_banner_image');
    }

    public function getCroppedImgPathAttribute()
    {
        if ($this->image) {
            return public_path('uploads/courses/crop/' . $this->image);
        }
        return asset('') . config('constants.default_banner_image');
    }

    public function getThumbImgPathAttribute()
    {
        if ($this->image) {
            return public_path('uploads/courses/thumb/' . $this->image);
        }
        return asset('') . config('constants.default_banner_image');
    }

    public function getDateRangeAttribute()
    {
        return formatDate($this->start_date) . (($this->end_date)? " - " . formatDate($this->end_date):'');
    }

    public function getTimeRangeAttribute()
    {
        return formatTime($this->start_time) . (($this->end_time)? " - " . formatTime($this->end_time):'');
    }

    public function scopePublished($query)
    {
        return $query->where('status', 1);
    }

    public function trainings()
    {
        return $this->hasMany(TrainingProgram::class, 'course_id', 'id');
    }

    public function category()
    {
        return $this->belongsTo(CourseCategory::class, 'category_id', 'id');
    }

    public function employees()
    {
        return $this->belongsToMany(Employee::class, 'course_employee', 'course_id', 'employee_id');
    }

    public function assessments()
    {
        return $this->hasMany(CourseAssessment::class);
    }

    public function leaderboards()
    {
        return $this->belongsToMany(Employee::class, 'leaderboards', 'course_id', 'employee_id');
    }
}
