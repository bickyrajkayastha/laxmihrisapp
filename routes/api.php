<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// authentication routes
Route::post('login', 'Auth\ApiLoginController@getAccessToken');
Route::post('reset-password', 'Auth\ApiLoginController@resetPassword');

Route::group(['prefix' => 'employee', 'as' => 'employee.', 'middleware' => ['auth:api', 'employee']], function () {

    //  apis-employee
	// Route::get('/test', 'Auth\ApiLoginController@test');
	Route::get('/refresh-token', 'Auth\ApiLoginController@refreshToken');
	Route::get('welcome-note', 'Api\HomeController@welcomeNote');
	Route::get('about-bank', 'Api\HomeController@aboutBank');
	Route::post('save', 'Api\EmployeeController@saveEmployeeDetail');
	Route::get('profile', 'Api\EmployeeController@employeeProfile');
	Route::get('directories', 'Api\EmployeeController@getEmployeeDirectory');
	Route::get('logout', 'Auth\ApiLoginController@logout');
	Route::get('notifications', 'Api\Employee\NotificationController@getAllNotificationList');
	Route::get('faqs', 'Api\Employee\FaqController@faqs');
    Route::post('feedbacks', 'Api\Employee\EmployeeFeedbackController@store');

    //  apis-message
	Route::post('send-message', 'Api\Employee\MessageController@sendMessage');
	Route::post('group/send-message', 'Api\Employee\MessageController@sendGroupMessage');
	Route::get('message-employee-list', 'Api\Employee\MessageController@getMessageEmployeeList');
	Route::get('message-group-list', 'Api\Employee\MessageGroupController@getMessageGroupList');
	Route::get('message-group/{id}/members', 'Api\Employee\MessageGroupController@getMessageGroupMembers');
	Route::get('conversations/{employeeId}', 'Api\Employee\MessageController@conversationList');
	Route::get('group/{groupId}/conversations', 'Api\Employee\MessageController@groupConversationList');
	Route::get('polls/vote/{optionId}/{comment?}', 'Api\Employee\PollOptionController@vote');
	Route::get('polls', 'Api\Employee\PollController@pollList');
	Route::get('events', 'Api\Employee\EventController@index');
	Route::get('job-description', 'Api\EmployeeController@jobDescription');
	Route::post('messages/file', 'Api\Employee\MessageController@sendFile');
    Route::post('message-group/file', 'Api\Employee\MessageController@sendFileToGroup');

    //  apis-courses
    Route::get('course-categories', 'Api\CourseController@allCourseCategories');
    Route::get('trainings/{id}', 'Api\TrainingController@show');
    Route::get('/{id}/get-all-courses', 'Api\CourseController@allCourses');
    Route::get('course/{id}/trainings', 'Api\CourseController@coursePrograms');
    Route::get('course/{id}/assessment', 'Api\CourseController@assessment');
    Route::post('course/{id}/feedback', 'Api\CourseController@giveFeedback');
    Route::post('post-answers', 'Api\AssessmentController@postAssessment');

    // apis-salary-and-leave
    Route::get('/salary-details', 'Api\EmployeeController@salaryDetails');
    Route::get('/leave-details', 'Api\EmployeeController@leaveDetails');

    // apis-change-password
    Route::post('change-password', 'Auth\ApiLoginController@changePassword');

    //  apis-leaderboard
    Route::get('/leaderboards', 'Api\CourseController@leaderboards');
});

