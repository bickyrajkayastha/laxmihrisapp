@extends('admin.layouts.master')
@push('css')
<link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/jcrop/jquery.Jcrop.min.css') }}">
@endpush
@section('content')

<div class="row">
    <div class="col">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Edit Course Category
                    <a href="{{ route('admin.coursecategories.index') }}" class="btn panel-btn btn-sm btn-primary pull-right"style="margin-top: 15px; margin-right: 15px;"> <i class="fas fa-long-arrow-alt-left"></i> Back</a>
                </h3>
            </div>
            <form class="form-horizontal" action="{{ route('admin.coursecategories.update',$course->id) }}"
                  method="POST" enctype="multipart/form-data">
                @csrf
                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-name">Name <span
                                    class="required_color">*</span></label>
                        <div class="col-sm-10">
                            <input type="text" placeholder="Name" name="name" value="{{ old('name',$course->name) }}"
                                   id="input-name" class="form-control" required="required">
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                        <label class="col-md-2 control-label">Publish</label>
                        <div class="col-md-9">
                            <div class="radio">

                                <!-- Inline radio buttons -->
                                <input id="input-status" class="magic-radio" type="radio" name="status" value="1" {{ $course->status ? 'checked' :'' }}>
                                <label for="input-status">Yes</label>

                                <input id="input-status-2" class="magic-radio" value="0" type="radio" name="status" {{ $course->status == 0 ? 'checked' : '' }}>
                                <label for="input-status-2">No</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-2 col-xs-12 col-md-12 pull-right">
                            <button class="btn btn-block btn-primary" type="submit">Update</button>
                        </div>
                    </div>
                </div>
            </form>
            <!--===================================================-->
            <!--End Hover Rows-->
        </div>
    </div>
</div>

@endsection

@push('scripts')
    <script src="{{ asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/jcrop/jquery.Jcrop.min.js') }}"></script>
@endpush
