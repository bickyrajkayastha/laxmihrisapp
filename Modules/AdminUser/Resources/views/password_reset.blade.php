@extends('admin.layouts.master')
@section('content')
<div class="row">
    <div class="col">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Change Password
                    <span class="required_fields" style="float: right;"><strong class="required_color">*</strong> These fields are required.</span>
                </h3>
            </div>

            <form action="{{ route('admin.user.password-reset.store') }}" method="POST" class="form-horizontal form-padding">
                <div class="panel-body">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label class="col-md-2 control-label" for="admin-first-name">Current Password <span class="required_color">*</span></label>
                        <div class="col-md-10">
                            <input type="password" name="current_password" class="form-control" value="{{ old('current_password') }}" placeholder="Current Password" >
                            <small class="help-block text-danger">{{$errors->first('current_password')}}</small>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label" for="admin-first-name">New Password <span class="required_color">*</span></label>
                        <div class="col-md-10">
                            <input type="password" name="new_password" class="form-control" value="{{ old('new_password') }}" placeholder="New Password" >
                            <small class="help-block text-danger">{{$errors->first('new_password')}}</small>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label" for="admin-first-name">Confirm Password <span class="required_color">*</span></label>
                        <div class="col-md-10">
                            <input type="password" name="confirm_password" class="form-control" value="{{ old('confirm_password') }}" placeholder="Confirm Password" >
                            <small class="help-block text-danger">{{$errors->first('confirm_password')}}</small>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label"></label>
                        <div class="col-md-2">
                            <input type="submit" class="btn btn-sm btn-block btn-primary" name="submit" value="Reset Password">
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
@stop