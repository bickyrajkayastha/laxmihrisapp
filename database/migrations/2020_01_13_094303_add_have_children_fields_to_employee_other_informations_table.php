<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHaveChildrenFieldsToEmployeeOtherInformationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employee_other_informations', function (Blueprint $table) {
            $table->boolean('have_children')->default(false);
            $table->boolean('same_as_permanent_address')->default(false);
            $table->boolean('family_member_on_laxmi_bank')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employee_other_informations', function (Blueprint $table) {
            //
        });
    }
}
