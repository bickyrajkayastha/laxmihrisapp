@extends('admin.layouts.master')
@push('css')
<link rel="stylesheet" href="{{ asset('assets/plugins/select2/css/select2.min.css') }}">
@endpush
@section('content')
<div class="row">
    <div class="col">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Candidates
                    {{-- <a href="{{ route('admin.courses.create') }}" class="btn btn-primary btn-sm panel-btn pull-right"><i class="fa fa-plus"></i> Add Course</a> --}}
                </h3>
            </div>

            <!-- Striped Table -->
            <!--===================================================-->
            <div class="panel-body">
                <div class="row" style="margin-bottom: 20px;">
                    <form class="form-horizontal" id="add-event" action="{{ route('admin.courses.candidates.add') }}" method="POST"
                      enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="course_id" value="{{ $course->id }}">
                        <div class="col">
                            <div class="form-group">
                                <label class="col-sm-12 control-label" for=""><strong>Add Employees</strong></label>
                                <div class="col-sm-12">
                                    {{-- <input type="text" data-role="tagsinput" value="jQuery,Script,Net"> --}}
                                    <select name="candidates[]" id="select-tags" multiple class="form-control">
                                        @foreach($employees as $employee)
                                            <option value="{{ $employee->id }}">{{ $employee->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-2 col-sm-12 col-xs-12" style="margin-top: 10px;">
                                    <button class="btn btn-success" type="submit">Add</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
	                            <th>S.N</th>
	                            <th>Employee</th>
	                            <th>Points</th>
	                            <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($candidates as $candidate)
	                    	    <tr>
	                    	        <td>{{ $loop->iteration }}</td>
                                    <td>{{ $candidate->name }}</td>
                                    <td>{{ $candidate->points }}</td>
	                    	        <td>
                                        <a href="{{ route('admin.courses.candidates.course-assessment', ['id' => $candidate->id, 'courseId' => $course->id]) }}" class="btn btn-sm btn-primary" title="Trainings">
                                            <i class="fas fa-network-wired"></i> Assessment
                                        </a>
	                    	            <a href="#" title="Remove" class="btn btn-sm btn-danger btn-delete"
	                    	               data-toggle="modal"
	                    	               data-id="{{ $candidate->id }}"
	                    	               data-target="#delete-modal">
	                    	                <i class="fas fa-trash"></i>
                                        </a>
	                    	        </td>
	                    	    </tr>
                            @empty
                    	    <tr>
                    	        <td colspan="5">
                    	            <em>No data available in table ...</em>
                    	        </td>
                    	    </tr>
                            @endforelse
                        </tbody>
                    </table>
                    {{-- {!! $candidates->render() !!} --}}
                </div>
            </div>
            <!--===================================================-->
            <!-- End Striped Table -->

        </div>
    </div>
    @include('admin.elements.delete-modal')
</div>
@endsection
@push('scripts')
<script src="{{ asset('assets/plugins/select2/js/select2.min.js') }}"></script>
<script type="text/javascript">
    $( "#select-tags" ).select2({
        // theme: "bootstrap"
    });
    $(document).on('click','.btn-delete', function () {
        var route = '{{ route("admin.courses.candidate.remove", ":id") }}';

        route = route.replace(':id', $(this).attr('data-id'));

        $('#delete-modal').find('form').attr('action', route);
        let course_input = document.createElement('INPUT');
        course_input.setAttribute("type", "hidden");
        course_input.setAttribute("name", "course_id");
        course_input.setAttribute("value", '{!! $course->id !!}');
        $('#delete-modal').find('form').prepend(course_input);
        $('#delete-modal').modal('show');

        $('#delete-final').click(function (e) {
            $(this).attr('disabled',true);
            $(this).html('<i class="fas fa-spinner fa-spin"></i> Please wait...');
            $(this).closest('form').submit();
        });
    });
</script>
@endpush
