@extends('admin.layouts.master')
@section('content')
<div class="row">
	<div class="co">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Add Notification
                    <button class="btn btn-sm btn-primary pull-right panel-btn" onclick="window.history.go(-1);">
                        <i class="fas fa-long-arrow-alt-left"></i> Back
                    </button>
                </h3>
            </div>

            <!--Horizontal Form-->
            <!--===================================================-->
            <form action="{{ route('admin.notifications.store') }}" method="POST" class="form-horizontal">
                @csrf
                <div class="panel-body">
                    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                        <label class="col-sm-2 control-label" for="input-title">Title <span class="required_color">*</span></label>
                        <div class="col-sm-9">
                            <input type="text" placeholder="" value="{{ old('title') }}" name="title" id="input-title" class="form-control">
                            <small class="help-block text-danger">{{ $errors->first('title')}}</small>
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">
                        <label class="col-sm-2 control-label" for="demo-hor-inputpass">Body <span class="required_color">*</span></label>
                        <div class="col-sm-9">
	                        <textarea name="body" id="input-body" rows="5" class="form-control">{!! old('body') !!}</textarea>
                            <small class="help-block text-danger">{{ $errors->first('body') }}</small>
                        </div>
                    </div>
                </div>
                <div class="panel-footer text-right">
                	<div class="row">
		                <div class="col-lg-11">
		                    <button class="btn btn-sm btn-mint" type="submit">Create</button>
		                </div>
                	</div>
                </div>
            </form>
            <!--===================================================-->
            <!--End Horizontal Form-->

        </div>
    </div>
</div>
@endsection