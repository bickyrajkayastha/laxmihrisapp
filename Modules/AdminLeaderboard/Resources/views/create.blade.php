@extends('admin.layouts.master')
@push('css')
<link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/jcrop/jquery.Jcrop.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/select2/css/select2.min.css') }}">
<style>
    .select2 {
        width: 100% !important;
    }
</style>
@endpush
@section('content')

<div class="row">
    <div class="col">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Add Leaderboard
                    <button class="btn btn-sm btn-primary pull-right panel-btn" onclick="window.history.go(-1);">
                        <i class="fas fa-long-arrow-alt-left"></i> Back
                    </button>
                </h3>
            </div>
            <!--Hover Rows-->
            <!--===================================================-->
            <form class="form-horizontal" id="add-course" action="{{ route('admin.leaderboards.store') }}" method="POST"
                  enctype="multipart/form-data">
                @csrf
                <div class="panel-body">
                    <div class="form-group{{ $errors->has('course_id') ? ' has-error' : '' }}">
                        <label class="col-sm-2 control-label" for="">Course <span class="required_color">*</span></label>
                        <div class="col-sm-9">
                            <select name="course_id" id="" class="form-control">
                                <option value="" selected>--Select--</option>
                                @foreach ($courses as $course)
                                    <option value="{{ $course->id }}">{{ $course->title }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('course_id'))
                            <small class="help-block text-danger">{{ $errors->first('course_id')}}</small>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                        <label class="col-md-2 control-label">Publish</label>
                        <div class="col-md-9">
                            <div class="radio">

                                <!-- Inline radio buttons -->
                                <input id="input-status" class="magic-radio" type="radio" name="status" value="1" <?= ((old('status') == 1)? 'checked':''); ?>>
                                <label for="input-status">Yes</label>

                                <input id="input-status-2" class="magic-radio" value="0" type="radio" name="status" <?= ((old('status') == 0)? 'checked':''); ?>>
                                <label for="input-status-2">No</label>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <h4>Positions</h4>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-title">1st <span class="required_color">*</span></label>
                        <div class="col-lg-6">
                            <select name="employees[0][id]" class="select-tags form-control">
                                <option value="">--Select--</option>
                                @foreach($employees as $employee)
                                    <option value="{{ $employee->id }}">{{ $employee->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-4">
                            <input type="number" placeholder="Points" class="form-control" name="employees[0][points]">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-title">2nd <span class="required_color">*</span></label>
                        <div class="col-lg-6">
                            <select name="employees[1][id]" class="select-tags form-control">
                                <option value="">--Select--</option>
                                @foreach($employees as $employee)
                                    <option value="{{ $employee->id }}">{{ $employee->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-4">
                            <input type="number" placeholder="Points" class="form-control" name="employees[1][points]">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-title">3rd <span class="required_color">*</span></label>
                        <div class="col-lg-6">
                            <select name="employees[2][id]" class="select-tags form-control">
                                <option value="">--Select--</option>
                                @foreach($employees as $employee)
                                    <option value="{{ $employee->id }}">{{ $employee->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-4">
                            <input type="number" placeholder="Points" class="form-control" name="employees[2][points]">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-title">4th</label>
                        <div class="col-lg-6">
                            <select name="employees[3][id]" class="select-tags form-control">
                                <option value="">--Select--</option>
                                @foreach($employees as $employee)
                                    <option value="{{ $employee->id }}">{{ $employee->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-4">
                            <input type="number" placeholder="Points" class="form-control" name="employees[3][points]">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-title">5th</label>
                        <div class="col-lg-6">
                            <select name="employees[4][id]" class="select-tags form-control">
                                <option value="">--Select--</option>
                                @foreach($employees as $employee)
                                    <option value="{{ $employee->id }}">{{ $employee->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-4">
                            <input type="number" placeholder="Points" class="form-control" name="employees[4][points]">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-title">6th</label>
                        <div class="col-lg-6">
                            <select name="employees[5][id]" class="select-tags form-control">
                                <option value="">--Select--</option>
                                @foreach($employees as $employee)
                                    <option value="{{ $employee->id }}">{{ $employee->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-4">
                            <input type="number" placeholder="Points" class="form-control" name="employees[5][points]">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-title">7th</label>
                        <div class="col-lg-6">
                            <select name="employees[6][id]" class="select-tags form-control">
                                <option value="">--Select--</option>
                                @foreach($employees as $employee)
                                    <option value="{{ $employee->id }}">{{ $employee->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-4">
                            <input type="number" placeholder="Points" class="form-control" name="employees[6][points]">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-title">8th</label>
                        <div class="col-lg-6">
                            <select name="employees[7][id]" class="select-tags form-control">
                                <option value="">--Select--</option>
                                @foreach($employees as $employee)
                                    <option value="{{ $employee->id }}">{{ $employee->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-4">
                            <input type="number" placeholder="Points" class="form-control" name="employees[7][points]">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-title">9th</label>
                        <div class="col-lg-6">
                            <select name="employees[8][id]" class="select-tags form-control">
                                <option value="">--Select--</option>
                                @foreach($employees as $employee)
                                    <option value="{{ $employee->id }}">{{ $employee->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-4">
                            <input type="number" placeholder="Points" class="form-control" name="employees[8][points]">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-title">10th</label>
                        <div class="col-lg-6">
                            <select name="employees[9][id]" class="select-tags form-control">
                                <option value="">--Select--</option>
                                @foreach($employees as $employee)
                                    <option value="{{ $employee->id }}">{{ $employee->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-4">
                            <input type="number" placeholder="Points" class="form-control" name="employees[9][points]">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-2 col-sm-12 col-xs-12 pull-right">
                            <button class="btn btn-block btn-success" type="submit">Create</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@push('scripts')
<script src="{{ asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.js') }}"></script>
<script src="{{ asset('assets/plugins/jcrop/jquery.Jcrop.min.js') }}"></script>
<script src="{{ asset('assets/plugins/select2/js/select2.min.js') }}"></script>
<script>
    $( ".select-tags" ).select2({
        // theme: "bootstrap"
    });

    var aspRatio = 3/2;
    // convert bytes into friendly format
    function bytesToSize(bytes) {
        var sizes = ['Bytes', 'KB', 'MB'];
        if (bytes === 0) return 'n/a';
        var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
        return (bytes / Math.pow(1024, i)).toFixed(1) + ' ' + sizes[i];
    }

    // check for selected crop region
    function checkForm() {
        if (parseInt($('#w').val())) return true;
        $('.image-error').html('Please select a crop region and then press Upload').show();
        return false;
    }

    // update info by cropping (onChange and onSelect courses handler)
    function updateInfo(e) {
        $('#x1').val(e.x);
        $('#y1').val(e.y);
        $('#x2').val(e.x2);
        $('#y2').val(e.y2);
        $('#w').val(e.w);
        $('#h').val(e.h);
    }

    // clear info by cropping (onRelease course handler)
    function clearInfo(e) {
        $('#x1').val(e.x);
        $('#y1').val(e.y);
        $('#x2').val(e.x2);
        $('#y2').val(e.y2);
        $('#w').val(300);
        $('#h').val(200);
    }
    // Create variables (in this scope) to hold the Jcrop API and image size
    var jCropApi, boundX, boundY;

    function fileSelectHandler() {
        // get selected file
        var oFile = $('#input-image-file')[0].files[0];

        if (!$('#input-image-file')[0].files[0]) {
            $('.jcrop-holder').remove();
            return;
        }
        // hide all errors
        $('.image-error').hide();
        // check for image type (jpg and png are allowed)
        var rFilter = /^(image\/jpeg|image\/png|image\/jpg|image\/gif|image\/xcf|image\/svg)$/i;
        if (!rFilter.test(oFile.type)) {
            $('#submit').prop("disabled", "disabled");
            $('.image-error').html('Please select a valid image file (jpg and png are allowed)').show();
            return;
        } else {
            $('#submit').prop("disabled", false);
        }
        // preview element
        var oImage = document.getElementById('preview');
        // prepare HTML5 FileReader
        var oReader = new FileReader();
        oReader.onload = function (e) {
            // e.target.result contains the DataURL which we can use as a source of the image
            oImage.src = e.target.result;
            oImage.onload = function () { // onload course handler
                var height = oImage.naturalHeight;

                var width = oImage.naturalWidth;

                // console.log(height);
                // console.log(width);
                window.URL.revokeObjectURL(oImage.src);

                if (height < 200 || width < 300) {

                    oImage.src = "";
                    $('#input-image-file').val('');
                    // $('#submit').prop("disabled","disabled");

                    $('.image-error').html('You have selected too small file, please select a one image with minimum size 300 X 200 px').show();

                } else {

                    $('#submit').prop("disabled", false);

                }
                var sResultFileSize = bytesToSize(oFile.size);

                // destroy Jcrop if it is existed
                if (typeof jCropApi !== 'undefined') {
                    jCropApi.destroy();
                    jCropApi = null;
                    $('#preview').width(oImage.naturalWidth);
                    $('#preview').height(oImage.naturalHeight);
                }
                setTimeout(function () {
                    // initialize Jcrop
                    $('#preview').Jcrop({
                        setSelect: [0, 0, 300, 200],
                        boxWidth: 300,
                        // boxHeight: 300,
                        minSize: [300, 200], // min crop size
                        aspectRatio: aspRatio,
                        bgFade: true, // use fade effect
                        bgOpacity: .3, // fade opacity
                        onChange: updateInfo,
                        onSelect: updateInfo,
                        onRelease: clearInfo,
                        trueSize: [oImage.naturalWidth, oImage.naturalHeight]
                    }, function () {
                        // use the Jcrop API to get the real image size
                        var bounds = this.getBounds();
                        boundx = bounds[0];
                        boundy = bounds[1];
                        // Store the Jcrop API in the jCropApi variable
                        jCropApi = this;
                    });
                }, 500);
            };
        };
        // read selected file as DataURL
        oReader.readAsDataURL(oFile);
    }

    $(function() {
        $('.date').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
        });

        $('#start-time').timepicker({
            format: 'hh:ii',
            defaultTime: false,
            autoclose: true,
        });

        $('#course-end-time').timepicker({
            format: 'hh:ii',
            defaultTime: false,
            autoclose: true,
        });

        $("#add-course").on('submit', function(course) {
            $(this).find("button[type='submit']").prop('disabled', true).html('<i class="fas fa-spinner fa-spin"></i> Creating...');
        });
    });

</script>
@endpush
