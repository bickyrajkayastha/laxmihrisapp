@extends('admin.layouts.master')
@section('content')
<div class="row">
    <div class="col">
                    @include('adminuser::header_tab')
                    <div class="panel">


                        <div class="panel-body">
                            <div class="row textright searchWrapperPosition">
                                <div class="col">
                                    <div class="pageHeading">
                                        <span class="pageTitle">
                                           Groups Management
                                        </span>
                                    </div>
                                </div>
                                <hr>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <table class="table table-striped table-bordered adminMgmtTable">
                                        <thead>
                                        <tr>
                                            <th>S.N</th>
                                            <th>Group Name</th>
                                            <th class="text-center">Status</th>
                                            <th>Operation</th>
                                        </tr>
                                        </thead>

                                        <tbody>

                                        <tr>
                                            <form action="{{ route('admin.user.groups.store') }}"
                                                  class="panel-body form-horizontal form-padding" method="post">
                                                {{ csrf_field() }}
                                                <td align="center"></td>

                                                <td>
                                                    <input type="text" name="name" class="form-control" value="{{ old('name') }}"
                                                           placeholder="Group Name">
                                                    <small class="help-block text-danger">{{$errors->first('name')}}</small>
                                                </td>

                                                <td class="text-center">
                                                    <div class="radio">
                                                        <input id="status" class="magic-radio" type="radio" name="status" value="1"
                                                               checked>
                                                        <label for="status" style="color:#000000;">Active</label>
                                                        <input id="status-2" class="magic-radio" type="radio" name="status" value="0">
                                                        <label for="status-2" style="color:#000000;">Inactive</label>
                                                    </div>
                                                </td>

                                                <td>
                                                    <input type="submit" class="btn btn-sm btn-block btn-primary" name="submit" value="CREATE">
                                                </td>

                                            </form>
                                        </tr>

                                        @if( count($groups) > 0 )
                                            <?php $i = 1; ?>
                                            @foreach($groups as $g)
                                                <tr>
                                                    <td align="center">{{ $i++ }} </td>
                                                    <td>
                                                        @if($g->id == config('constants.role_super_admin'))
                                                            <strong>{{ strtoupper($g->name) }}</strong>
                                                        @else
                                                            {{ $g->name }}
                                                        @endif
                                                    </td>
                                                    <td align="center">
                                                        {!!  $g->status == '1' ? '<strong style="color:green;">Active</strong>' : '<strong style="color:#bf302f;">Inactive</strong>' !!}
                                                    </td>
                                                    <td width="230">
                                                        @if($g->id == config('constants.role_super_admin'))
                                                            <span class="text-pink text-semibold">You cannot edit this user group.</span>
                                                        @else
                                                            <a href="{{ route('admin.user.groups.permission',[$g->id]) }}"
                                                               class="btn btn-sm btn-primary adminMgmtTableBtn"
                                                               title="Edit Group Permission"><i class="fa fa-cogs"></i></a>

                                                            @if(!in_array($g->id, [config('constants.php'),config('constants.php')]))
                                                                <a href="{{ route('admin.user.groups.edit',[$g->id]) }}"
                                                                   class="btn btn-sm btn-info adminMgmtTableBtn" title="Edit User Group"><i
                                                                            class="fa fa-edit"></i></a>
                                                                <a href="#" class="btn btn-sm btn-danger btn-delete"
                                                                   data-id="{{ $g->id }}"
                                                                   data-toggle="modal"
                                                                   data-target="#delete-modal">
                                                                    <i class="fa fa-trash"></i>
                                                                </a>
                                                            @endif
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td align="center" colspan="10">
                                                    User Groups not found. &nbsp;
                                                </td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
    </div>
</div>
    @include('admin.elements.delete-modal')
@stop

@push('scripts')
    <script>
        $(document).on('click', '.btn-delete', function () {
            var route = '{{ route('admin.user.groups.destroy', ':id') }}';

            route = route.replace(':id', $(this).attr('data-id'));

            $('#delete-modal').find('form').attr('action', route);
            $('#delete-modal').modal('show');

            $('#delete-final').click(function (e) {
                $(this).attr('disabled', true);
                $(this).html('<i class="fas fa-spinner fa-spin"></i> Please wait...');
                $(this).closest('form').submit();
            });
        });
    </script>
@endpush