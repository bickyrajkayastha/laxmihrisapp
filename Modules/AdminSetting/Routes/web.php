<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin/setting', 'as' => 'admin.', 'middleware' => 'auth:admin'], function () {
    // GENERAL SETTINGS
    Route::get('general', 'AdminSettingController@general')->name('setting.general')->middleware('can:general-setting');
    Route::get('welcome-note', 'AdminSettingController@welcomeNote')->name('setting.welcome-note')->middleware('can:general-setting');
    Route::get('ceo', 'AdminSettingController@ceoMessage')->name('setting.ceo')->middleware('can:general-setting');

    Route::post('general/store', 'AdminSettingController@generalStore')
        ->name('setting.general.store')
        ->middleware('can:general-setting');

    Route::post('welcome-note/store', 'AdminSettingController@storeWelcomeNote')
    ->name('setting.welcome-note.store')
    ->middleware('can:general-setting');

    Route::post('ceo/store', 'AdminSettingController@storeCeoMessage')
    ->name('setting.ceo.store')
    ->middleware('can:general-setting');

    // IMAGE DIMENSION SETTINGS
    Route::get('img-dimension', 'AdminSettingController@imgDimension')
        ->name('setting.img-dimension');

    Route::post('img-dimension/store', 'AdminSettingController@imgDimensionStore')
        ->name('setting.img-dimension.store')
        ->middleware('can:image-setting');

    // SWITCH MAINTAINANCE
    Route::get('switch', 'AdminSettingController@switchSettings')
        ->name('setting.switch');

    Route::post('switch', 'AdminSettingController@switchSettingsStore')
        ->name('setting.switch.store')
        ->middleware('can:switch-setting');

});
