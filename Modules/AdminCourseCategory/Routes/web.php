<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin/course-categories', 'as' => 'admin.', 'middleware' => 'auth:admin'], function () {
    Route::get('/', 'AdminCourseCategoryController@index')->name('coursecategories.index')->middleware('can:view-course-categories');
    Route::get('/create', 'AdminCourseCategoryController@create')->name('coursecategories.create')->middleware('can:create-course-categories');
    Route::post('/create', 'AdminCourseCategoryController@store')->name('coursecategories.store')->middleware('can:create-course-categories');
    Route::get('/{id}/edit', 'AdminCourseCategoryController@edit')->name('coursecategories.edit')->middleware('can:edit-course-categories');
    Route::post('/{id}/edit', 'AdminCourseCategoryController@update')->name('coursecategories.update')->middleware('can:edit-course-categories');
    Route::post('/{id}/delete', 'AdminCourseCategoryController@destroy')->name('coursecategories.destroy')->middleware('can:delete-course-categories');
});

