<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin/courses', 'as' => 'admin.', 'middleware' => 'auth:admin'], function () {
    Route::get('/{id}/assessments/questions', 'AdminCourseAssessmentController@index')->name('courses.assessment.questions')->middleware('can:view-courses');
    Route::get('/{id}/assessments/create', 'AdminCourseAssessmentController@create')->name('courses.assessment.create')->middleware('can:create-courses');
    Route::post('/assessments/create', 'AdminCourseAssessmentController@store')->name('courses.assessment.store')->middleware('can:create-courses');
    Route::get('/assessments/{id}/edit', 'AdminCourseAssessmentController@edit')->name('courses.assessment.edit')->middleware('can:edit-courses');
    Route::post('/assessments/{id}/edit', 'AdminCourseAssessmentController@update')->name('courses.assessment.update')->middleware('can:edit-courses');
    Route::post('/assessments/{id}/delete', 'AdminCourseAssessmentController@destroy')->name('courses.assessment.destroy')->middleware('can:delete-courses');
});
