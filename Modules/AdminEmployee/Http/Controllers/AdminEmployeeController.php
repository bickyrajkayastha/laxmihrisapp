<?php

namespace Modules\AdminEmployee\Http\Controllers;

use App\Helpers\Permission;
use App\Services\ImageUpload\Strategy\UploadWithAspectRatio;
use App\Models\Employee;
use App\Traits\Slugger;
use App\Events\EmployeeAdded;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;
use Modules\AdminEmployee\Services\EmployeeImageUploader;
use Illuminate\Support\Facades\Validator;
use App\Imports\EmployeesImport;
use App\Models\JobDescription;
use Excel;

class AdminEmployeeController extends Controller
{
    use Slugger;

    private $indexRoute;

    public function __construct(Request $request)
    {
        $this->indexRoute = route('admin.employees');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $employee = Employee::query();

        if ($request->get('name')) {
            $employee->where("name", 'LIKE', '%' . $request->name . '%');
        }

        if ($request->get('email')) {
            $employee->where('email', 'LIKE',  "%" . $request->email . "%");
        }

        if ($request->get('employee_code')) {
            $employee->where('employee_code', 'LIKE',  "%" . $request->employee_code . "%");
        }

        if ($request->get('mobile')) {
            $employee->where('mobile', 'LIKE',  "%" . $request->mobile . "%");
        }

        if ($request->has('status') && $request->get('status') !== 'all') {
            $employee->where('status', $request->status);
        }

        $data['employees'] = $employee->select('id', 'employee_code', 'name', 'email', 'status', 'mobile', 'designation', 'joined_date', 'image')->latest()->paginate(20);
        $data['pageTitle'] = "Employee Management";
        $data['breadcrumbs'] = '<li><a href="' . route('admin.employees') . '">Employee Management</a></li>';
        return view('adminemployee::index', $data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $data['pageTitle'] = "Add Employee";
        $data['breadcrumbs'] = '<li><a href="' . route('admin.employees') . '">Employee Management</a></li><li>Add</li>';
        $data['job_descriptions'] = JobDescription::where('status', 1)->get();

        return view('adminemployee::create', $data);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'employee_code' => 'nullable',
            'gender' => 'nullable',
            'position' => 'nullable',
            'email' => 'required|unique:employees,email',
            'mobile' => 'nullable|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
            'image' => 'nullable|image|mimes:png,jpg,jpeg',
            'joined_date' => 'nullable|date',
            'password' => 'required'
        ]);

        try {

            $employee = new Employee;
            $employee_password = $request->password;
            $password = Hash::make($request->password);
            $request->merge(['password' => $password]);
            $employee->status = 0;
            if ($request->joined_date != "") {
                $employee->joined_date = date('y-m-d', strtotime($request->joined_date));
            }

            $employee->fill($request->only(
                'name',
                'employee_code',
                'gender',
                'position',
                'email',
                'phone',
                'mobile',
                'designation',
                'password',
                'status',
                'job_description_id'
            ));


            if ($request->hasFile('image')) {
                $image = $request->file('image');

                $uploader = new EmployeeImageUploader(new UploadWithAspectRatio());

                $employee->image = $uploader->saveOriginalImage($image);

                $this->cropAndSaveImage(
                    $uploader,
                    $employee->image,
                    $request->x1,
                    $request->y1,
                    $request->w,
                    $request->h
                );

            }

            $verification_token = $this->generateVerificationToken();
            $employee->verification_token = $verification_token;

            try {

                $employee->save();
                $employee->setAttribute('employee_password', $employee_password);
                $employee->setAttribute('employee_verification_token', $verification_token);
                event(new EmployeeAdded($employee));
                session()->flash('success_message', __('alerts.create_success'));
                return redirect()->route('admin.employees');
            } catch (\Exception $e) {
                Log::info($e->getMessage());
                session()->flash('error_message', __('alerts.create_error'));
                return redirect()->route('admin.employees');
            }

        } catch (\Exception $e) {
            session()->flash('error_message', __('alerts.create_error'));
            return redirect()->route('admin.employees');
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $data['pageTitle'] = "Employee Detail";
        return view('adminemployee::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $data['pageTitle'] = "Edit Employee";
        $employee = Employee::find($id);

        if (!$employee) {
            session()->flash('error_message', 'Employee not available for editing.');
            return redirect()->back();
        }

        $data['employee'] = $employee;
        $data['breadcrumbs'] = '<li><a href="' . route('admin.employees') . '">Employee Management</a></li><li>Edit</li>';
        $data['job_descriptions'] = JobDescription::where('status', 1)->get();

        return view('adminemployee::edit', $data);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|string',
            'employee_code' => 'nullable',
            'gender' => 'nullable',
            'position' => 'nullable',
            'mobile' => 'nullable|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
            'image' => 'nullable|image|mimes:png,jpg,jpeg',
            'joined_date' => 'nullable|date',
            // 'password' => 'required',
            'email' => 'required|unique:employees,email,' . $id
        ]);

        try {

            $employee = Employee::find($id);
            if (isset($request->password) && !empty($request->password)) {
                $employee_password = $request->password;
                $password = Hash::make($request->password);
                $request->merge(['password' => $password]);
                $employee->password = $request->password;
            }

            if ($request->joined_date != "") {
                $employee->joined_date = date('y-m-d', strtotime($request->joined_date));
            }

            $employee->fill($request->only(
                'name',
                'employee_code',
                'position',
                'email',
                'designation',
                'mobile',
                'status',
                'job_description_id'
            ));


            if ($request->hasFile('image') || $request->croppreviousimage == 'true') {
                $uploader = new EmployeeImageUploader(new UploadWithAspectRatio());

                $uploader->deleteThumbImage($employee->image);

                $uploader->deleteCroppedImage($employee->image);

                if ($request->croppreviousimage == 'true') {
                    $employee->image = $employee->image;
                } else {
                    $uploader->deleteFullImage($employee->image);

                    $employee->image = $uploader->saveOriginalImage($request->file('image'));
                }

                $this->cropAndSaveImage(
                    $uploader,
                    $employee->image,
                    $request->x1,
                    $request->y1,
                    $request->w,
                    $request->h
                );
            }

            $employee->save();

            session()->flash('success_message', __('alerts.update_success'));
            return redirect()->route('admin.employees');

        } catch (\Exception $e) {
            session()->flash('error_message', __('alerts.update_error'));
            return redirect()->route('admin.employees');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        if (!Permission::check('delete-employees'))
            return redirect()->route('admin.401');


        try {

            $employee = Employee::find($id);

            if($employee->status == 1){
                session()->flash('error_message', __('alerts.delete_error_active'));
                return redirect()->to($this->indexRoute);
            }

            $uploader = new EmployeeImageUploader();

            $uploader->deleteFullImage($employee->image);

            $uploader->deleteThumbImage($employee->image);

            $uploader->deleteCroppedImage($employee->image);

            $employee->delete();


            session()->flash('success_message', __('alerts.delete_success'));
            return redirect()->route('admin.employees');

        } catch (\Exception $e) {

            session()->flash('error_message', __('alerts.delete_error'));
            return redirect()->route('admin.employees');
        }
    }

    public function generateVerificationToken()
    {
        return md5(rand(1, 10) . microtime());
    }

    public function detail($id)
    {
        $data['pageTitle'] = "Employee Detail";
        $data['breadcrumbs'] = '<li><a href="' . route('admin.employees') . '">Employee Management</a></li><li>Detail</li>';
        $data['employee'] = Employee::with(
            'employee_personal_detail',
            'employee_permanent_address_detail',
            'employee_temporary_address_detail',
            'employee_family_detail',
            'employee_children',
            'employee_work_experiences',
            'employee_relative_contact',
            'employee_relative_on_banks',
            'employee_family_loans',
            'job_description'
        )->find($id)->toArray();
        return view('adminemployee::detail', $data);
    }

    private function cropAndSaveImage($uploader, $filename, $posX1, $posY1, $width, $height)
    {
        $imgPath = $uploader->getFullImagePath($filename);

        $fullImage = Image::make($imgPath);

        $cropDestPath = $uploader->getCroppedImagePath($filename);

        $uploader->cropAndSaveImage($fullImage, $cropDestPath, $posX1, $posY1, $width, $height);

        $uploader->cropAndSaveImageThumb($fullImage, $filename);
    }

    public function passwordReset(Request $request)
    {
        $rules = array(
            '_user_id' => 'required',
            'st_user_password' => 'required',
        );

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            session()->flash('error_message', 'Invalid Employee Details. Please try again.');
            return redirect()->route('admin.employees');
        }

        $employee_data = [
            'password' => Hash::make($request->get('st_user_password')),
        ];

        Employee::where('id', $request->get('_user_id'))->update($employee_data);

        session()->flash('success_message', 'Password updated successfully.');
        return redirect()->route('admin.employees');
    }

    public function saveJobDescription(Request $request)
    {
        $request->validate([
            'id' => 'required',
            'description' => 'required'
        ]);

        $employee = Employee::find($request->id);

        try {

            $job = \App\Models\EmployeeJobDescription::updateOrCreate(
                ['employee_id' => $request->id],
                ['employee_id' => $request->id, 'description' => $request->description]
            );
            session()->flash('success_message', 'Updated successfully.');
            return redirect()->back();
        } catch (\Exception $e) {
            session()->flash('success_message', __('alerts.update_error'));
            return redirect()->back();
        }
    }

    public function import()
    {
        $data['breadcrumbs'] = '<li><a href="' . route('admin.dashboard') . '">Home</a></li><li><a href="' . route('admin.employees') . '">Employee Management</a></li><li>Import Employees List</li>';
        return view('adminemployee::import', $data);
    }

    public function importExcel(Request $request)
    {
        Excel::import(new EmployeesImport, $request->file('excel_file'));
        session()->flash('success_message', 'List will be imported in few moments.');
        return redirect()->route('admin.employees');
    }
}
