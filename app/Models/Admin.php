<?php

namespace App\Models;

use App\Helpers\Permission;
use Illuminate\Foundation\Auth\User;

class Admin extends User
{
    protected $guarded = ['id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function user_group()
    {
        return $this->hasMany(UserGroup::class,'user_id','id');
    }

    public function user_is_superadmin()
    {
        return $this->hasMany(UserGroup::class,'user_id','id')->where('group_id',config('constants.role_super_admin'));
    }

    public function check_permission($module)
    {
        if (Permission::check($module)) {
            return true;
        }

        return false;
    }
}
