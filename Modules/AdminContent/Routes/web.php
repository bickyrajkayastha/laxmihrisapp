<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('admin/welcome-note', 'AdminContentController@welcomeNote')->middleware('auth:admin')->name('admin.welcome-note')->middleware('can:view-contents');
Route::group(['prefix' => 'admin/contents', 'as' => 'admin.', 'middleware' => 'auth:admin'], function() {

    Route::get('/', 'AdminContentController@index')
        ->name('contents')
        ->middleware('can:view-contents');

    Route::get('/create', 'AdminContentController@create')
        ->name('contents.create')
        ->middleware('can:create-contents');

    Route::post('/create', 'AdminContentController@store')
        ->name('contents.store')
        ->middleware('can:create-contents');

    Route::get('/{id}/edit', 'AdminContentController@edit')
        ->name('contents.edit')
        ->middleware('can:edit-contents');

    Route::post('/{id}/edit', 'AdminContentController@update')
        ->name('contents.update')
        ->middleware('can:edit-contents');

    Route::post('/welcome-note/update', 'AdminContentController@updateWelcome')
        ->name('contents.update-welcome')
        ->middleware('can:edit-contents');

    Route::post('/{id}/delete', 'AdminContentController@destroy')
        ->name('contents.destroy')
        ->middleware('can:delete-contents');
});
