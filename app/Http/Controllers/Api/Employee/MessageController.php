<?php

namespace App\Http\Controllers\Api\Employee;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Message;
use App\Models\Employee;
use App\Models\EmployeeMessage;
use Illuminate\Support\Facades\Storage;
use Image;
use App\Models\EmployeeMessageGroup;
use Illuminate\Support\Facades\Log;
use App\Events\PrivateMessageEvent;
use App\Events\GroupMessageEvent;
use App\Http\Resources\EmployeeMessageListCollection;
use App\Http\Resources\ConversationListCollection;
use App\Http\Resources\GroupConversationListCollection;
use Illuminate\Validation\Rule;

class MessageController extends Controller
{
	/**
	* @SWG\Post(
	*     path="/employee/send-message",
	*     description="send messag to other employee.",
	*     tags={"Employee Apis"},
	*     security={{"bearerAuth":{}}},
	*     @SWG\Parameter(
	*         in="body",
	*         name="employee details",
	*         description="save employee details",
	*         required=true,
	*             @SWG\Property(
	*                   property="personal_details",
	*                   type="object",
	*                   required={"message", "receiver_id"},
	*                   @SWG\Property(
	*		                 property="message",
	*		                 type="string",
	*		                 description="long text",
	*                   ),
	*                   @SWG\Property(
	*		                 property="receiver_id",
	*		                 type="number",
	*		                 description="id of the receiver",
	*                   ),
	*              ),
	*     ),
	*     @SWG\Response(
	*         response=200,
	*         description="status, message",
	*     ),
	*     @SWG\Response(
	*         response=422,
	*         description="status, message"
	*     )
	* )
	*/
	public function sendMessage(Request $request)
	{
		$request->validate([
			'message' => 'required'
		]);

		$sender_id = auth()->user()->id;
		$receiver_id = $request->receiver_id;

		$message = new Message;
		$message->message = $request->message;

		if ($message->save()) {
			try {
				$message->employees()->attach($sender_id, ['receiver_id' => $receiver_id]);
				$sender = Employee::where('id', '=', $sender_id)->first();
				$receiver = Employee::where('id', '=', $receiver_id)->first();

				$data = [];
				$data['sender_id'] = $sender_id;
				$data['sender_image'] = $sender->thumbImgPath;
				$data['sender_name'] = $sender->name;
				$data['receiver_id'] = $receiver_id;
				$data['expo_token'] = $receiver->employee_devices()->pluck('expo_token');
				$data['content'] = $message->message;
				$data['in'] = true;
				$data['content_type'] = "text";

				event(new PrivateMessageEvent($data));

				return response()->json([
					'data' => [],
					'success' => true,
					'message' => 'Message sent successfully.'
				]);
			} catch (\Exception $e) {
				Log::info($e->getMessage());
				$message->delete();
			}
		}
	}

	/**
	* @SWG\Post(
	*     path="/employee/group/send-message",
	*     description="send messag to group.",
	*     tags={"Employee Apis"},
	*     security={{"bearerAuth":{}}},
	*     @SWG\Parameter(
	*         in="body",
	*         name="employee details",
	*         description="save employee details",
	*         required=true,
	*             @SWG\Property(
	*                   property="message details",
	*                   type="object",
	*                   required={"message", "receiver_id"},
	*                   @SWG\Property(
	*		                 property="message",
	*		                 type="string",
	*		                 description="long text",
	*                   ),
	*                   @SWG\Property(
	*		                 property="group_id",
	*		                 type="number",
	*		                 description="id of the receiver",
	*                   ),
	*              ),
	*     ),
	*     @SWG\Response(
	*         response=200,
	*         description="status, message",
	*     ),
	*     @SWG\Response(
	*         response=422,
	*         description="status, message"
	*     )
	* )
	*/
	public function sendGroupMessage(Request $request)
	{
		$success = false;
		$message = "";
		$status_code = 404;

		$user = auth()->user();
		$group_id = $request->group_id;

		$request->validate([
			'message' => 'required',
			'group_id' => 'required|exists:message_groups,id'
		], [
			'group_id.exists' => "The group does not exist."
		]);

		// check if the user exist in the group.
		if (!$user->isInGroup($request->group_id)) {

			$data['message'] = "You are not in this group.";
			$data['status_code'] = 422;
			throw new \App\Exceptions\CustomApiException($data);
		}

		try {

			$group_employee_ids = EmployeeMessageGroup::where([
				['message_group_id', '=', $group_id],
				['employee_id', '!=', $user->id]
			])->pluck('employee_id');

			$this->sendMessageToGroupEmployees($group_employee_ids, $group_id, $request->message);
			$success = true;
			$message = "Message sent successfully.";
		} catch (\Exception $e) {
			Log::debug($e->getMessage());
			$message = __('alerts.create_error');
		}

		return response()->json([
			'data' => [],
			'success' => $success,
			'message' => $message
		]);
	}

	public function sendMessageToGroupEmployees($employee_ids, $group_id, $group_message)
	{
		$message = new Message;
		$message->message = $group_message;
		$receiver_devices = [];

		if ($message->save()) {
			$sender = auth()->user();
			$sender_id = $sender->id;

			$employee_message = new \App\Models\EmployeeMessage;
			$employee_message->message_id = $message->id;
			$employee_message->sender_id = $sender_id;
			$employee_message->message_group_id = $group_id;
			$employee_message->type = 2;
			// send message to each employee.

			if ($employee_message->save()) {
				foreach ($employee_ids as $key => $employee_id) {
					$employee_message->receivers()->attach($employee_id);

					$receiver = Employee::where('id', '=', $employee_id)->first();
					// $data = [];
					// $data['group_id'] = $group_id;
					// $data['sender_id'] = $sender_id;
					// $data['sender_image'] = $sender->thumbImgPath;
					// $data['sender_name'] = $sender->name;
					// $data['receiver_id'] = $employee_id;
					// $data['expo_token'] = $receiver->employee_devices()->pluck('expo_token');
					// $data['content'] = $message->message;
					// $data['in'] = true;
					// $data['content_type'] = "text";

					$receiver_devices[$key]['receiver_id'] = $employee_id;
					$receiver_devices[$key]['expo_token'] = $receiver->employee_devices()->pluck('expo_token')->toArray();

					// event(new GroupMessageEvent($data));
				}

				$message_data = [];
				$message_data['group_id'] = $group_id;
				$message_data['group_name'] = \App\Models\MessageGroup::find($group_id)->name;
				$message_data['sender_id'] = $sender_id;
				$message_data['sender_image'] = $sender->thumbImgPath;
				$message_data['sender_name'] = $sender->name;
				$message_data['content'] = $message->message;
				$message_data['in'] = true;
				$message_data['content_type'] = "text";
				// \Log::info($receiver_devices);
				event(new GroupMessageEvent($message_data, $receiver_devices));
			}


			return 1;
		}
		return 0;
	}

	public function sendFileToGroupEmployees($employee_ids, $group_id, $message)
	{
		$sender = auth()->user();
		$sender_id = $sender->id;

		$employee_message = new \App\Models\EmployeeMessage;
		$employee_message->message_id = $message->id;
		$employee_message->sender_id = $sender_id;
		$employee_message->message_group_id = $group_id;
		$employee_message->type = 2;
		// send message to each employee.

		if ($employee_message->save()) {
			foreach ($employee_ids as $key => $employee_id) {
				$employee_message->receivers()->attach($employee_id);

				$receiver = Employee::where('id', '=', $employee_id)->first();
				// $data = [];
				// $data['group_id'] = $group_id;
				// $data['sender_id'] = $sender_id;
				// $data['sender_image'] = $sender->thumbImgPath;
				// $data['sender_name'] = $sender->name;
				// $data['receiver_id'] = $employee_id;
				// $data['expo_token'] = $receiver->employee_devices()->pluck('expo_token');
				// $data['content'] = $message->message;
				// $data['in'] = true;
				// $data['content_type'] = "text";

				$receiver_devices[$key]['receiver_id'] = $employee_id;
				$receiver_devices[$key]['expo_token'] = $receiver->employee_devices()->pluck('expo_token')->toArray();
				$receiver_devices[$key]['file_size'] = formatSizeUnits($message->file_size);
				$receiver_devices[$key]['file_url'] = $message->filePath;
				$receiver_devices[$key]['file_name'] = $message->file_original_name;
			}

			$message_data = [];
			$message_data['group_id'] = $group_id;
			$message_data['group_name'] = \App\Models\MessageGroup::find($group_id)->name;
			$message_data['sender_id'] = $sender_id;
			$message_data['sender_image'] = $sender->thumbImgPath;
			$message_data['sender_name'] = $sender->name;
			$message_data['content'] = $message->message;
			$message_data['in'] = true;
			$message_data['type'] = 2;
			$message_data['file_size'] = formatSizeUnits($message->file_size);
			$message_data['file_url'] = $message->filePath;
			$message_data['file_name'] = $message->file_original_name;

			// \Log::info($receiver_devices);
			event(new GroupMessageEvent($message_data, $receiver_devices));
		}
		return 1;
	}

	/**
	 * @SWG\Get(
	 *     path="/employee/conversations/{id}",
	 *     description="get conversations",
	 *     tags={"Employee Apis"},
	 *     security={{"bearerAuth":{}}},
	 *     @SWG\Parameter(
	 *         in="path",
	 *         name="id",
	 *         type="number",
	 *         description="id of the employee.",
	 *         required=true,
	 *     ),
	 *     @SWG\Response(
	 *         response=200,
	 *         description="status, message",
	 *     ),
	 *     @SWG\Response(
	 *         response=422,
	 *         description="status, message"
	 *     )
	 * )
	 */
	public function conversationList($employeeId)
	{
		$user = auth()->user();

		$query = EmployeeMessage::where([
			['sender_id', '=', $user->id],
			['receiver_id', '=', $employeeId],
			// ['type', '=', 1]
		])
		->orWhere([
			['receiver_id', '=', $user->id],
			['sender_id', '=', $employeeId],
			// ['type', '=', 1]
		])->latest()->with('message', 'sender')->personal();


		return new ConversationListCollection($query->paginate(30));
	}

	/**
	 * @SWG\Get(
	 *     path="/employee/group/{id}/conversations",
	 *     description="get conversations",
	 *     tags={"Employee Apis"},
	 *     security={{"bearerAuth":{}}},
	 *     @SWG\Parameter(
	 *         in="path",
	 *         name="id",
	 *         type="number",
	 *         description="id of the group.",
	 *         required=true,
	 *     ),
	 *     @SWG\Response(
	 *         response=200,
	 *         description="status, message",
	 *     ),
	 *     @SWG\Response(
	 *         response=422,
	 *         description="status, message"
	 *     )
	 * )
	 */
	public function groupConversationList($groupId)
	{
		$user = auth()->user();

		$query = EmployeeMessage::where([
			['sender_id', '=', $user->id],
			['message_group_id', '=', $groupId],
		])
		->orWhere([
			['message_group_id', '=', $groupId],
		])->latest()->with('message', 'sender')->group();

		return new GroupConversationListCollection($query->paginate(30));
	}

	/**
	 * @SWG\Get(
	 *     path="/employee/message-employee-list",
	 *     description="get employee list in message",
	 *     tags={"Employee Apis"},
	 *     security={{"bearerAuth":{}}},
	 *     @SWG\Response(
	 *         response=200,
	 *         description="status, message",
	 *     ),
	 *     @SWG\Response(
	 *         response=422,
	 *         description="status, message"
	 *     )
	 * )
	 */
	public function getMessageEmployeeList()
	{
		$user = auth()->user();
		$user_id = $user->id;

		$receiver_query = EmployeeMessage::where(function($q) use ($user_id) {
			$q->where('receiver_id', '=', $user_id)
			->orWhere('sender_id', '=', $user_id);
		})
		->where('type', '=', 1)
		->where('receiver_id', '!=', null)
		->groupBy('receiver_id')->pluck('receiver_id')
		->toArray();

		$sender_query = EmployeeMessage::where(function($q) use ($user_id) {
			$q->where('receiver_id', '=', $user_id)
			->orWhere('sender_id', '=', $user_id);
		})
		->where('type', '=', 1)
		->where('receiver_id', '!=', null)
		->groupBy('sender_id')->pluck('sender_id')
		->toArray();

		$employee_ids = array_unique(array_merge($receiver_query, $sender_query), SORT_REGULAR);

		if (($key = array_search($user_id, $employee_ids)) !== false) {
		    unset($employee_ids[$key]);
		}

		$query = Employee::whereIn('id', $employee_ids)->with([
			'messages' => function($q) use ($user_id) {
				$q->latest()->limit(1)->where([
					['receiver_id', '=', $user_id],
					['messages.type', '=', 1]
				]);
			},
			'received_messages' => function($q) use ($user_id) {
				$q->where([
					['sender_id', '=', $user_id],
					['messages.type', '=', 1]
				])->latest();
			}
		]);

		return new EmployeeMessageListCollection($query->paginate(20));
	}

	/**
	 * @SWG\Post(
	 *     path="/employee/messages/file",
	 *     description="send file in message",
	 *     tags={"Employee Apis"},
	 *     security={{"bearerAuth":{}}},
	 *     @SWG\Parameter(
	 *         in="formData",
	 *         name="receiver_id",
	 *         type="number",
	 *         description="receiver id.",
	 *         required=true,
	 *     ),
	 *     @SWG\Parameter(
	 *         in="formData",
	 *         name="file",
	 *         type="file",
	 *         description="file to be send.",
	 *         required=true,
	 *     ),
	 *     @SWG\Response(
	 *         response=200,
	 *         description="status, message",
	 *     ),
	 *     @SWG\Response(
	 *         response=422,
	 *         description="status, message"
	 *     )
	 * )
	 */
	public function sendFile(Request $request)
	{
		$request->validate([
			'receiver_id' => 'required|exists:employees,id',
			'file' => 'required|max:5000'
		], [
			'receiver_id.exists' => 'Invalid receiver id.',
			'file.max' => 'The file size cannot be more than 5MB.'
		]);

	    $file_original_name = $request->file->getClientOriginalName();
	    $file_type = $request->file->getClientOriginalExtension();
	    $new_file_name = md5(microtime()) . '.' . $file_type;
        $file_size = $request->file->getClientSize();

	    // store file.
	    $path = '/uploads/messages/files/';
	    $request->file->move(public_path($path), $new_file_name);

		$sender_id = auth()->user()->id;
		$receiver_id = $request->receiver_id;

		$message = new Message;
		$message->file_original_name = $file_original_name;
		$message->file = $new_file_name;
		$message->message = "file";
		$message->file_size = $file_size;
		$message->type = 2;

		if ($message->save()) {
			try {
				$message->employees()->attach($sender_id, ['receiver_id' => $receiver_id]);
				$sender = Employee::where('id', '=', $sender_id)->first();
				$receiver = Employee::where('id', '=', $receiver_id)->first();

				$data = [];
				$data['sender_id'] = $sender_id;
				$data['sender_image'] = $sender->thumbImgPath;
				$data['sender_name'] = $sender->name;
				$data['receiver_id'] = $receiver_id;
				$data['expo_token'] = $receiver->employee_devices()->pluck('expo_token');
				$data['content'] = "Sent a File.";
				$data['in'] = true;
				$data['type'] = 2;
				$data['file_size'] = formatSizeUnits($message->file_size);
				$data['file_url'] = $message->filePath;
				$data['file_name'] = $message->file_original_name;

				event(new PrivateMessageEvent($data));

				return response()->json([
					'data' => [
						'file_url' => $message->filePath,
						'name' => $message->file_original_name,
						'file_size' => formatSizeUnits($message->file_size)
					],
					'success' => true,
					'message' => 'Message sent successfully.'
				]);
			} catch (\Exception $e) {
				Log::info($e->getMessage());
				$message->delete();
			}
		}
	}

	/**
	 * @SWG\Post(
	 *     path="/employee/message-group/file",
	 *     description="send file in message",
	 *     tags={"Employee Apis"},
	 *     security={{"bearerAuth":{}}},
	 *     @SWG\Parameter(
	 *         in="formData",
	 *         name="group_id",
	 *         type="number",
	 *         description="group id.",
	 *         required=true,
	 *     ),
	 *     @SWG\Parameter(
	 *         in="formData",
	 *         name="file",
	 *         type="file",
	 *         description="file to be send.",
	 *         required=true,
	 *     ),
	 *     @SWG\Response(
	 *         response=200,
	 *         description="status, message",
	 *     ),
	 *     @SWG\Response(
	 *         response=422,
	 *         description="status, message"
	 *     )
	 * )
	 */
	public function sendFileToGroup(Request $request)
	{
		$user = auth()->user();
		$group_id = $request->group_id;

		$request->validate([
			'group_id' => 'required|exists:message_groups,id',
			'file' => 'required|max:5000'
		], [
			'group_id.exists' => "The group does not exist.",
			'file.max' => 'The file size cannot be more than 5MB.'
		]);

		// check if the user exist in the group.
		if (!$user->isInGroup($request->group_id)) {

			$data['message'] = "You are not in this group.";
			$data['status_code'] = 422;
			throw new \App\Exceptions\CustomApiException($data);
		}

	    $file_original_name = $request->file->getClientOriginalName();
	    $file_type = $request->file->getClientOriginalExtension();
	    $new_file_name = md5(microtime()) . '.' . $file_type;
        $file_size = $request->file->getClientSize();

	    // store file.
	    $path = '/uploads/messages/files/';
	    $request->file->move(public_path($path), $new_file_name);

		$sender_id = auth()->user()->id;
		$receiver_id = $request->receiver_id;

		$message = new Message;
		$message->file_original_name = $file_original_name;
		$message->file = $new_file_name;
		$message->message = "Sent a File.";
		$message->file_size = $file_size;
		$message->type = 2;

		if ($message->save()) {
			try {

				$group_employee_ids = EmployeeMessageGroup::where([
					['message_group_id', '=', $group_id],
					['employee_id', '!=', $user->id]
				])->pluck('employee_id');

				$this->sendFileToGroupEmployees($group_employee_ids, $group_id, $message);

				return response()->json([
					'data' => [
						'file_url' => $message->filePath,
						'name' => $message->file_original_name,
						'file_size' => formatSizeUnits($message->file_size)
					],
					'success' => true,
					'message' => 'Message sent successfully.'
				]);
			} catch (\Exception $e) {
				Log::info($e->getMessage());
				$message->delete();
			}
		}
	}
}
