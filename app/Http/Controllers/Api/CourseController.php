<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\ApiCollection;
use App\Http\Resources\ApiResource;
use App\Http\Resources\CommonCollection;
use App\Http\Resources\TrainingResourse;
use App\Models\Course;
use App\Models\CourseCategory;
use PetstoreIO\ApiResponse;

class CourseController extends Controller
{
    /**
     * @SWG\Get(
     *     path="/employee/{id}/get-all-courses",
     *     description="get all courses",
     *     tags={"Employee Apis"},
     *     security={{"bearerAuth":{}}},
     *     @SWG\Response(
     *         response=200,
     *         description="status, message",
     *     ),
     *     @SWG\Response(
     *         response=422,
     *         description="status, message"
     *     )
     * )
     */
    public function allCourses($category_id)
    {
        $courses = Course::query();
        $employee = auth()->user();
        $courses = $employee->courses()->where('category_id', $category_id);
        return new CommonCollection($courses->paginate(20));
    }

    /**
    * @SWG\Get(
    *     path="/employee/course/{id}/training",
    *     description="get all course trainings",
    *     tags={"Employee Apis"},
    *     security={{"bearerAuth":{}}},
    *     @SWG\Response(
    *         response=200,
    *         description="status, message",
    *     ),
    *     @SWG\Response(
    *         response=422,
    *         description="status, message"
    *     )
    * )
    */
    public function coursePrograms($courseId)
    {
        $course = Course::find($courseId);
        $programs = $course->trainings;

        $employee = auth()->user();

        $assessment_completed = $employee->assessment_options()->wherePivot('course_id', $course->id)->first() ? true: false;

        $result = "";
        if ($assessment_completed) {
            $employee_points = $employee->courses()->wherePivot('course_id', $course->id)->first()->pivot->assessment_points;
            $result = $course->assessments()->count();
            $result = "You've obtained " . $employee_points . " out of " . $result . ".";
        }

        $data = [
            'assessment' => [
                'completed' => $assessment_completed,
                'points' => $result
            ],
            'chapters' => $programs
        ];
        return new TrainingResourse($data);
    }

    /**
     * @SWG\Get(
     *     path="/employee/course-categories",
     *     description="get all course categories",
     *     tags={"Employee Apis"},
     *     security={{"bearerAuth":{}}},
     *     @SWG\Response(
     *         response=200,
     *         description="status, message",
     *     ),
     *     @SWG\Response(
     *         response=422,
     *         description="status, message"
     *     )
     * )
     */
    public function allCourseCategories()
    {
        $categories = CourseCategory::where('status', 1)->select('id', 'name')->get();
        $categories = $categories->map(function($category) {
            return [
                'id' => $category->id,
                'name' => $category->name,
                'courses' => $category->courses->count()
            ];
        });
        return new ApiResource($categories);
    }

    /**
     * @SWG\Get(
     *     path="/employee/course/{id}/assessment",
     *     description="get all course assessment",
     *     tags={"Employee Apis"},
     *     security={{"bearerAuth":{}}},
     *     @SWG\Response(
     *         response=200,
     *         description="status, message",
     *     ),
     *     @SWG\Response(
     *         response=422,
     *         description="status, message"
     *     )
     * )
     */
    public function assessment($course_id)
    {
        $course = Course::find($course_id);

        $assessment = $course->assessments()->select('id', 'question')->with([
            'options' => function($q) {
                $q->select('id', 'option', 'assessment_id');
            }
        ])->get();
        $data = [];
        $data['time_limit'] = $course->time_limit;
        $data['assessment'] = $assessment;
        return new ApiResource($data);
    }

    /**
     * @SWG\Post(
     *     path="/employee/course/{id}/feedback",
     *     description="post feedback.",
     *     tags={"Employee Apis"},
     *     security={{"bearerAuth":{}}},
     *     @SWG\Parameter(
     *         in="body",
     *         name="post feedback",
     *         description="post feedback",
     *         required=true,
     *         @SWG\Schema(
     *             collectionFormat="multi",
     *             type="object",
     *             @SWG\Property(
     *                   property="feedback",
     *                   type="string",
     *              )
     *         )
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="status, message",
     *     ),
     *     @SWG\Response(
     *         response=422,
     *         description="status, message"
     *     )
     * )
     */
    public function giveFeedback(Request $request, $course_id)
    {
        $success = true;
        $message = "";
        $request->validate([
            'feedback' => 'required'
        ]);

        try {
            $employee = auth()->user();
            $employee->courses()->updateExistingPivot($course_id, [
                'feedback' => $request->feedback
            ]);
            $message = "Thank you for the feedback.";
        } catch (\Throwable $th) {
            \Log::info($th->getMessage());
            $success = false;
        }

        return new ApiResource([], $success, $message);
    }

    /**
    * @SWG\Get(
    *     path="/employee/leaderboards",
    *     description="get all leaderboard courses",
    *     tags={"Employee Apis"},
    *     security={{"bearerAuth":{}}},
    *     @SWG\Response(
    *         response=200,
    *         description="status, message",
    *     ),
    *     @SWG\Response(
    *         response=422,
    *         description="status, message"
    *     )
    * )
    */
    public function leaderboards()
    {
        $data = [];
        try {
            $courses = Course::has('leaderboards')->orderBy('start_date', 'desc')->get();
            $courses = $courses->map(function($course) {
                return [
                    'id' => $course->id,
                    'title' => $course->title,
                    'date_range' => $course->date_range,
                    'leaderboards' => $course->leaderboards()->withPivot('position', 'points')->orderBy('pivot_position', 'asc')->get()->map(function($leaderboard) {
                        return [
                            'id' => $leaderboard->id,
                            'name' => $leaderboard->name,
                            'image' => $leaderboard->thumbImgPath,
                            'position' => $leaderboard->pivot->position,
                            'points' => $leaderboard->pivot->points
                        ];
                    })
                ];
            });

            $data = $courses;
        } catch (\Throwable $th) {
            \Log::info($th->getMessage());
        }

        return new ApiResource($data);
    }
}
