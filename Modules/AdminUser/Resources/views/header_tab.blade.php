        <ul class="nav nav-tabs settings-header-nav">
            <li class="{{ isset($header_nav) && $header_nav == 'users' ? 'active' : '' }}">
                <a href="{{ route('admin.user.list') }}" style="color: black;font-weight: bold;">USERS</a>
            </li>

            <li class="{{ isset($header_nav) && $header_nav == 'groups' ? 'active' : '' }}">
                <a href="{{ route('admin.user.groups') }}" style="color: black;font-weight: bold;">GROUPS</a>
            </li>

        </ul>