@extends('admin.layouts.master')
@push('css')
<link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/jcrop/jquery.Jcrop.min.css') }}">
<style>
    #multiple-file {
        padding: 0px;

    }

    #multiple-file li {
        display: inline-flex;
        margin-top: 6px;
        background-color: #da6b1c;
        padding-top: 9px;
        list-style-type: none;
        padding-bottom: 12px;
        margin-bottom: 3px;
        margin-right: 3px;
        color: white;
        padding-right: 12px;
        border-radius: 5px;
        padding-left: 12px;
        margin-right: 5px;
    }

    .remove-file {
        cursor: pointer;
        padding: 4px 0px 0px 10px;
    }

    .bar {
        height: 18px;
        background: green;
    }

    .file {
        position: relative;
        background: linear-gradient(to right, lightblue 50%, transparent 50%);
        background-size: 200% 100%;
        background-position: right bottom;
        transition: all 1s ease;
    }

    .file.done {
        background: lightgreen;
    }

    .file a {
        display: block;
        position: relative;
        padding: 5px;
        color: black;
    }

</style>
@endpush
@section('content')

<div class="row">
    <div class="col">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Add Training
                    <button class="btn btn-sm btn-primary pull-right panel-btn" onclick="window.history.go(-1);">
                        <i class="fas fa-long-arrow-alt-left"></i> Back
                    </button>
                </h3>
            </div>
            <!--Hover Rows-->
            <!--===================================================-->
            <form class="form-horizontal" id="add-training" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="course_id" value="{{ $course->id }}">
                <div class="panel-body">
                    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                        <label class="col-sm-2 control-label" for="">Title</label>
                        <div class="col-sm-9">
                            <input type="text" placeholder="Title" name="title" value="{{ old('title') }}"
                                id="input-title" class="form-control">
                            {{-- @if($errors->has('title')) --}}
                            <small class="help-block text-danger"></small>
                            {{-- @endif --}}
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('start_date') ? ' has-error' : '' }}">
                        <label class="col-sm-2 control-label" for="input-date">Start Date</label>
                        <div class="col-sm-9">
                            <div class="input-group date">
                                <input type="text" class="form-control" autocomplete="off" name="start_date"
                                    id="training-date" value="{{ old('start_date') }}">
                                <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-th"></span>
                                </div>
                            </div>
                            <small class="help-block text-danger"></small>
                            {{-- @if($errors->has('start_date'))
                            <small class="help-block text-danger">{{ $errors->first('start_date')}}</small>
                            @endif --}}
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('end_date') ? ' has-error' : '' }}">
                        <label class="col-sm-2 control-label" for="input-end-date">End Date</label>
                        <div class="col-sm-9">
                            <div class="input-group date">
                                <input type="text" class="form-control" autocomplete="off" name="end_date"
                                    id="training-end-date" value="{{ old('end_date') }}">
                                <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-th"></span>
                                </div>
                            </div>
                            <small class="help-block text-danger"></small>

                            {{-- @if($errors->has('end_date'))
                            <small class="help-block text-danger">{{ $errors->first('end_date')}}</small>
                            @endif --}}
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('start_time') ? ' has-error' : '' }}">
                        <label class="col-sm-2 control-label" for="input-time">Start Time</label>
                        <div class="col-sm-9">
                            <div class="input-group datetimepicker">
                                <input type="text" class="form-control" autocomplete="off" name="start_time"
                                    id="start-time" value="{{ formatTime(old('start_time')) }}">
                                <div class="input-group-addon">
                                    <span class="far fa-clock"></span>
                                </div>
                            </div>
                            <small class="help-block text-danger"></small>

                            {{-- @if($errors->has('start_time'))
                            <small class="help-block text-danger">{{ $errors->first('start_time')}}</small>
                            @endif --}}
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('end_time') ? ' has-error' : '' }}">
                        <label class="col-sm-2 control-label" for="input-end-time">End Time</label>
                        <div class="col-sm-9">
                            <div class="input-group datetimepicker">
                                <input type="text" class="form-control" autocomplete="off" name="end_time"
                                    id="training-end-time" value="{{ formatTime(old('end_time')) }}">
                                <div class="input-group-addon">
                                    <span class="far fa-clock"></span>
                                </div>
                            </div>
                            <small class="help-block text-danger"></small>

                            {{-- @if($errors->has('end_time'))
                            <small class="help-block text-danger">{{ $errors->first('end_time')}}</small>
                            @endif --}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="demo-hor-inputemail">Image</label>
                        <div class="col-md-10">
                            <small class="help-block text-danger">{{ $errors->first('file')}}</small>
                            <img id="preview" class="mar-btm"><br>

                            <span class="btn btn-sm btn-rose btn-primary btn-file">
                                <span>Browse</span>
                                <input type="file" name="file" id="input-image-file" onchange="fileSelectHandler()" />
                            </span>
                            <div class="image-error" style="color:red"></div>
                            <input type="hidden" id="x1" name="x1" />
                            <input type="hidden" id="y1" name="y1" />
                            <input type="hidden" id="x2" name="x2" />
                            <input type="hidden" id="y2" name="y2" />
                            <input type="hidden" id="w" name="w" />
                            <input type="hidden" id="h" name="h" />
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                        <label class="col-md-2 control-label">Publish</label>
                        <div class="col-md-9">
                            <div class="radio">

                                <!-- Inline radio buttons -->
                                <input id="input-status" class="magic-radio" type="radio" name="status" value="1"
                                    <?= ((old('status') == 1)? 'checked':''); ?>>
                                <label for="input-status">Yes</label>

                                <input id="input-status-2" class="magic-radio" value="0" type="radio" name="status"
                                    <?= ((old('status') == 0)? 'checked':''); ?>>
                                <label for="input-status-2">No</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="demo-contact-input">Description</label>
                        <div class="col-md-10">
                            <textarea name="description" id="input-description" cols="30" rows="10" class="form-control ckeditor">{{ old('description') }}</textarea>
                            <small class="help-block text-danger"></small>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="demo-contact-input">Files</label>
                        <div class="col-md-10">
                            <button id="add-files-btn" class="btn btn-sm btn-success"><i class="fas fa-plus"></i> Add
                                Files</button>
                            <input style="display: none;" id="fileupload" type="file" name="files" multiple>
                            <ul id="multiple-file">

                            </ul>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-2 col-sm-12 col-xs-12 pull-right">
                            <button class="btn btn-block btn-success" type="submit">Create</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@push('scripts')
<script src="{{ asset('js/file-upload/js/jquery.ui.widget.js') }}"></script>
<script src="{{ asset('js/file-upload/js/jquery.iframe-transport.js') }}"></script>
<script src="{{ asset('js/file-upload/js/jquery.fileupload.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.js') }}"></script>
<script src="{{ asset('assets/plugins/jcrop/jquery.Jcrop.min.js') }}"></script>
<script>
    var aspRatio = 3 / 2;
    // convert bytes into friendly format
    function bytesToSize(bytes) {
        var sizes = ['Bytes', 'KB', 'MB'];
        if (bytes === 0) return 'n/a';
        var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
        return (bytes / Math.pow(1024, i)).toFixed(1) + ' ' + sizes[i];
    }

    // check for selected crop region
    function checkForm() {
        if (parseInt($('#w').val())) return true;
        $('.image-error').html('Please select a crop region and then press Upload').show();
        return false;
    }

    // update info by cropping (onChange and onSelect trainings handler)
    function updateInfo(e) {
        $('#x1').val(e.x);
        $('#y1').val(e.y);
        $('#x2').val(e.x2);
        $('#y2').val(e.y2);
        $('#w').val(e.w);
        $('#h').val(e.h);
    }

    // clear info by cropping (onRelease training handler)
    function clearInfo(e) {
        $('#x1').val(e.x);
        $('#y1').val(e.y);
        $('#x2').val(e.x2);
        $('#y2').val(e.y2);
        $('#w').val(300);
        $('#h').val(200);
    }
    // Create variables (in this scope) to hold the Jcrop API and image size
    var jCropApi, boundX, boundY;

    function fileSelectHandler() {
        // get selected file
        var oFile = $('#input-image-file')[0].files[0];

        if (!$('#input-image-file')[0].files[0]) {
            $('.jcrop-holder').remove();
            return;
        }
        // hide all errors
        $('.image-error').hide();
        // check for image type (jpg and png are allowed)
        var rFilter = /^(image\/jpeg|image\/png|image\/jpg|image\/gif|image\/xcf|image\/svg)$/i;
        if (!rFilter.test(oFile.type)) {
            $('#submit').prop("disabled", "disabled");
            $('.image-error').html('Please select a valid image file (jpg and png are allowed)').show();
            return;
        } else {
            $('#submit').prop("disabled", false);
        }
        // preview element
        var oImage = document.getElementById('preview');
        // prepare HTML5 FileReader
        var oReader = new FileReader();
        oReader.onload = function (e) {
            // e.target.result contains the DataURL which we can use as a source of the image
            oImage.src = e.target.result;
            oImage.onload = function () { // onload training handler
                var height = oImage.naturalHeight;

                var width = oImage.naturalWidth;

                // console.log(height);
                // console.log(width);
                window.URL.revokeObjectURL(oImage.src);

                if (height < 200 || width < 300) {

                    oImage.src = "";
                    $('#input-image-file').val('');
                    // $('#submit').prop("disabled","disabled");

                    $('.image-error').html(
                        'You have selected too small file, please select a one image with minimum size 300 X 200 px'
                        ).show();

                } else {

                    $('#submit').prop("disabled", false);

                }
                var sResultFileSize = bytesToSize(oFile.size);

                // destroy Jcrop if it is existed
                if (typeof jCropApi !== 'undefined') {
                    jCropApi.destroy();
                    jCropApi = null;
                    $('#preview').width(oImage.naturalWidth);
                    $('#preview').height(oImage.naturalHeight);
                }
                setTimeout(function () {
                    // initialize Jcrop
                    $('#preview').Jcrop({
                        setSelect: [0, 0, 300, 200],
                        boxWidth: 300,
                        // boxHeight: 300,
                        minSize: [300, 200], // min crop size
                        aspectRatio: aspRatio,
                        bgFade: true, // use fade effect
                        bgOpacity: .3, // fade opacity
                        onChange: updateInfo,
                        onSelect: updateInfo,
                        onRelease: clearInfo,
                        trueSize: [oImage.naturalWidth, oImage.naturalHeight]
                    }, function () {
                        // use the Jcrop API to get the real image size
                        var bounds = this.getBounds();
                        boundx = bounds[0];
                        boundy = bounds[1];
                        // Store the Jcrop API in the jCropApi variable
                        jCropApi = this;
                    });
                }, 500);
            };
        };
        // read selected file as DataURL
        oReader.readAsDataURL(oFile);
    }

    let file_count = 0;
    let files_arr = [];

    $(function () {

        $("#add-files-btn").on('click', function (event) {
            event.preventDefault();
            $("input[name='files']").click();
        });

        $(document).on('click', '.remove-file', function (event) {
            let e = $(this);
            let li = e.closest('li');
            let key = li.data('key');
            files_arr.splice(key, 1);
            li.remove();
        });

        $("#fileupload").fileupload({
            add: function (e, data) {
                let file_div = "<li data-key=" + files_arr.length + ">" + data.files[0].name +
                    " <i class='fas fa-times remove-file'></i></li>";
                data.context = $("#multiple-file").append(file_div);
                files_arr.push(data.files[0]);
                // data.submit();
            },
            progress: function (e, data) {
                var progress = parseInt((data.loaded / data.total) * 100, 10);
                data.context.css("background-position-x", 100 - progress + "%");
            },
            done: function (e, data) {
                data.context
                    .addClass("done")
                    .find("a")
                    .prop("href", data.result.files[0].url);
            }
        });

        $('.date').datepicker({
            format: 'yyyy-mm-dd',
        });

        $('#start-time').timepicker({
            format: 'hh:ii',
            defaultTime: false
        });

        $('#training-end-time').timepicker({
            format: 'hh:ii',
            defaultTime: false
        });

        $("#add-training").on('submit', function (event) {
            event.preventDefault();
            var formData = new FormData($(this)[0]);

            files_arr.forEach(function(i,v) {
                formData.append("files[]", i);
            });
            let submit_btn = $(this).find("button[type='submit']");
            submit_btn.prop('disabled', true).html('<i class="fas fa-spinner fa-spin"></i> Creating...');
            setTimeout(() => {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "{{ route('admin.trainings.store') }}",
                    type: 'POST',
                    data: formData,
                    // dataType: 'json',
                    processData: false,
                    contentType: false,
                    async: false,
                    success: function (res) {

                        if (res.success) {
                            location.href = res.data.redirect_url;
                        }
                    },
                    error: function (xhr, status, error) {
                        // location.href = "";
                        if (xhr.responseJSON.errors) {
                            var elements = document.getElementById("add-training").elements;

                            var errors = xhr.responseJSON.errors;
                            let focus_element;
                            for (var i = 0, element; element = elements[i++];) {
                                let input_name = $(element);
                                input_name.closest('.form-group').find('small').html('');
                                $.each(errors, function(i, v) {
                                    if (input_name.attr('name') == i) {
                                        if (focus_element == null) {
                                            focus_element = input_name;
                                        }
                                        input_name.closest('.form-group').find('small').html(v[0]);
                                    }
                                });
                            }

                            focus_element.focus();
                        }
                    },
                    complete: function() {
                        submit_btn.prop('disabled', false).html('Create');
                    }
                });
            }, 500);
        });
    });

</script>
@endpush
