<!-- end of add new sport modal -->
<div class="modal fade" id="delete-modal" role="dialog" tabindex="-1" aria-labelledby="delete-modal"
     aria-hidden="true">
    <div class="modal-dialog modal-danger">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="pci-cross pci-circle"></i>
                </button>
                <h4>DELETE RECORD</h4>
                <!-- <h4 class="modal-title">Modal Heading</h4> -->
            </div>
            <form action="#" method="POST" id="delete-form">
            @csrf
            <!--Modal header-->
                <!--Modal body-->
                <div class="modal-body">
                    <h4 class=""><i class="fa fa-trash"></i> This action is not reversible, are you sure you want to continue?</h4>
                </div>

                <!--Modal footer-->
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                    <button class="btn btn-danger" id="delete-final">Yes</button>
                </div>
            </form>
        </div>
    </div>
</div>
