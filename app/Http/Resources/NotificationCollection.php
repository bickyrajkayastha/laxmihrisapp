<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Carbon;

class NotificationCollection extends ResourceCollection
{
    public $success = true;
    public $message = "";

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);

        if (iterator_count($this->resource)) {
            $this->message = "Notification list.";
            return $this->map(function($notification) {
                return [
                    'id' => $notification->id,
                    'title' => $notification->title,
                    'body' => $notification->body,
                    'seen_status' => ($notification->pivot->seen_status == 1)? true: false,
                    'created_at' => Carbon\Carbon::parse($notification->pivot->created_at)->diffForHumans()
                ];
            });
        } else {
            $this->message = "No notification has been created.";
        }

    }

    public function with($request)
    {
        return [
            'success' => $this->success,
            'message' => $this->message
        ];
    }
}
