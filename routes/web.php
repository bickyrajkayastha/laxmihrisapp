<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
     return view('adminlogin::index');
})->name('landing');

Route::get('/employee-verification', 'Auth\LoginController@verifyEmployee');
Route::post('send-message', 'Employee\MessageController@sendMessage')->name('send-message');
