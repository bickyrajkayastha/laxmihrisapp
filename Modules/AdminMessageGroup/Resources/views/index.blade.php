@extends('admin.layouts.master')

@section('content')
<div class="row">
    <div class="col">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Message Group Management
                    <a href="{{ route('admin.message-groups.create') }}" class="btn btn-primary btn-sm panel-btn pull-right"><i class="fa fa-plus"></i> Add Group</a>
                </h3>
            </div>

            <!-- Striped Table -->
            <!--===================================================-->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
	                            <th>S.N</th>
	                            <th>Name</th>
	                            <th>Status</th>
	                            <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        	@if(iterator_count($message_groups))
	                        	@foreach($message_groups as $group)
	                    	    <tr>
	                    	        <td>{{ $loop->iteration }}</td>
	                    	        <td>{{ $group->name }}</td>
	                    	        <td>{!! $group->status ? "<span class='label label-success'>Active</span>" : "<span class='label label-danger'>Inactive</span>"!!}</td>
	                    	        <td>
	                    	            <a href="{{ route('admin.message-groups.edit',$group->id) }}" class="btn btn-sm btn-primary" title="Edit">
	                    	                <i class="fas fa-edit"></i>
	                    	            </a>
                                        <a href="{{ route('admin.message-groups.employees',$group->id) }}" class="btn btn-sm btn-primary" title="Participants">
                                            <i class="fas fa-users"></i>
                                        </a>
	                    	            <a href="#" title="Remove" class="btn btn-sm btn-danger btn-delete"
	                    	               data-toggle="modal"
	                    	               data-id="{{ $group->id }}"
	                    	               data-target="#delete-modal">
	                    	                <i class="fas fa-trash"></i>
	                    	            </a>
	                    	        </td>
	                    	    </tr>
	                        	@endforeach
	                        @else
                    	    <tr>
                    	        <td colspan="5">
                    	            <em>No data available in table ...</em>
                    	        </td>
                    	    </tr>
                        	@endif
                        </tbody>
                    </table>
                </div>
            </div>
            <!--===================================================-->
            <!-- End Striped Table -->

        </div>
    </div>
    @include('admin.elements.delete-modal')
</div>
@endsection
@push('scripts')
    <script type="text/javascript">
        $(document).on('click','.btn-delete', function () {
            var route = '{{ route("admin.message-groups.destroy", ":id") }}';

            route = route.replace(':id', $(this).attr('data-id'));

            $('#delete-modal').find('form').attr('action', route);
            $('#delete-modal').modal('show');

            $('#delete-final').click(function (e) {
                $(this).attr('disabled',true);
                $(this).html('<i class="fas fa-spinner fa-spin"></i> Please wait...');
                $(this).closest('form').submit();
            });
        });
    </script>
@endpush
