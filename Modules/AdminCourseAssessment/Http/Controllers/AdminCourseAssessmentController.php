<?php

namespace Modules\AdminCourseAssessment\Http\Controllers;

use App\Helpers\Permission;
use App\Models\Course;
use App\Models\CourseAssessment;
use App\Models\CourseCategory;
use App\Models\Employee;
use App\Traits\Slugger;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Log;

class AdminCourseAssessmentController extends Controller
{
    use Slugger;

    private $indexRoute;

    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->indexRoute = route('admin.courses.assessment', 2);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index($id)
    {
        $data['pageTitle'] = "Course Assessment Management";
        $data['course'] = Course::find($id);
        $data['breadcrumbs'] = '<li><a href="' . route('admin.courses.index') . '">Course Management</a></li><li><a href="' . route('admin.courses.show', $data['course']->id) . '">'. $data['course']->title .'</a></li><li>Assessment</li>';
        $data['questions'] = CourseAssessment::where('course_id', $id)->paginate(20);

        return view('admincourseassessment::index', $data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create($id)
    {
        $data['pageTitle'] = "Add Question";
        $data['course'] = Course::find($id);
        $data['categories'] = CourseCategory::where('status', 1)->get();
        // $data['breadcrumbs'] = '<li><a href="' . route('admin.courses.index') . '">Course Management</a></li><li><a href="' . route('admin.courses.assessment', $data['course']->id) . '">' . $data['course']->title . '</a></li><li>Add</li>';
        $data['breadcrumbs'] = '<li><a href="' . route('admin.courses.index') . '">Course Management</a></li><li><a href="' . route('admin.courses.show', $data['course']->id) . '">' . $data['course']->title . '</a></li><li><a href="' . route('admin.courses.assessment.questions', $data['course']->id) . '">Assessment</a></li><li>Add</li>';

        return view('admincourseassessment::create', $data);
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        $correct_ans_validate = false;
        if ($this->request->options) {
            $options = $this->request->options;
            foreach ($options as $option) {
                if (isset($option['correct'])) {
                    $correct_ans_validate = true;
                    break;
                }
            }
        }

        if (!$correct_ans_validate) {
            session()->flash('error_message', 'Please tick the correct option.');
            return redirect()->back();
        }

        $this->request->validate([
            'course_id' => 'required',
            'question' => 'required',
            'options' => 'required|array|min:2',
            'options.*' => 'required',
            // 'options.*.correct' => 'sometimes|required|min:1',
        ], [
            'options.*.required' => 'Every option is required.',
            'options.min' => 'At least 2 options is required.',
        ]);

        try {
            $assessment = new CourseAssessment;
            $assessment->question = $this->request->question;
            $assessment->course_id = $this->request->course_id;
            $assessment->status = $this->request->status;

            if ($assessment->save()) {
                foreach ($this->request->options as $key => $option) {
                    $option_data[$key] = $option;
                }
                $assessment->options()->createMany($option_data);
            }

            session()->flash('success_message', __('alerts.create_success'));
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            session()->flash('error_message', __('alerts.create_error'));
        }

        return redirect()->route('admin.courses.assessment.questions', $this->request->course_id);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('admincourseassessment::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $data['pageTitle'] = "Edit Assessment";
        $data['assessment'] = $this->getAssessmentById($id);
        $data['course'] = Course::find($data['assessment']->course_id);
        $data['options'] = $data['assessment']->options;
        // $data['breadcrumbs'] = '<li><a href="' . route('admin.courses.index') . '">Course Management</a></li><li><a href="' . route('admin.courses.assessment', $data['course']->id) . '">'.$data['course']->title.'</a></li><li>Edit</li>';
        $data['breadcrumbs'] = '<li><a href="' . route('admin.courses.index') . '">Course Management</a></li><li><a href="' . route('admin.courses.show', $data['course']->id) . '">' . $data['course']->title . '</a></li><li><a href="' . route('admin.courses.assessment.questions', $data['course']->id) . '">Assessment</a></li><li>Edit</li>';

        return view('admincourseassessment::edit', $data);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update($id)
    {
        $this->request->validate([
            'course_id' => 'required',
            'question' => 'required',
            'options' => 'required|array|min:2',
            'options.*' => 'required'
        ], [
            'options.*.required' => 'Every option is required.',
            'options.min' => 'At least 2 options is required.',
        ]);

        try {
            $assessment = CourseAssessment::find($id);
            $assessment->question = $this->request->question;
            $assessment->status = $this->request->status;

            if ($assessment->save()) {
                foreach ($this->request->options as $key => $option) {
                    $option_data[$key] = $option;
                }
                $assessment->options()->delete();
                $assessment->options()->createMany($option_data);
            }

            session()->flash('success_message', __('alerts.update_success'));
        } catch (\Exception $e) {
            session()->flash('error_message', __('alerts.update_error'));
        }

        return redirect()->route('admin.courses.assessment.questions', $this->request->course_id);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        try {

            $assessment = $this->getAssessmentById($id);

            // if ($assessment->status == 1) {
            //     session()->flash('error_message', __('alerts.delete_error_active'));
            //     return redirect()->to($this->indexRoute);
            // }
            $assessment->delete();

            session()->flash('success_message', __('alerts.delete_success'));
        } catch (\Exception $e) {
            session()->flash('error_message', __('alerts.delete_error'));
        }

        return redirect()->back();
    }

    private function getAssessmentById($id)
    {
        return CourseAssessment::find($id);
    }
}
