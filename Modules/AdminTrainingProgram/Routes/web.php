<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin/trainings', 'as' => 'admin.', 'middleware' => 'auth:admin'], function () {
    Route::get('/', 'AdminTrainingProgramController@index')->name('trainings.index')->middleware('can:view-trainings');
    Route::get('/courses/{id}/trainings', 'AdminTrainingProgramController@coursesTainings')->name('courses.trainings.index')->middleware('can:view-trainings');
    Route::get('/courses/{id}/trainings/create', 'AdminTrainingProgramController@create')->name('trainings.create')->middleware('can:create-trainings');
    Route::post('/create', 'AdminTrainingProgramController@store')->name('trainings.store')->middleware('can:create-trainings');
    Route::get('/{id}/edit', 'AdminTrainingProgramController@edit')->name('trainings.edit')->middleware('can:edit-trainings');
    Route::post('/{id}/edit', 'AdminTrainingProgramController@update')->name('trainings.update')->middleware('can:edit-trainings');
    Route::post('/{id}/delete', 'AdminTrainingProgramController@destroy')->name('trainings.destroy')->middleware('can:delete-trainings');
});

